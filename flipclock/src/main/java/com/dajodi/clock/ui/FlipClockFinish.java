package com.dajodi.clock.ui;

/**
 * Represents a clock that has timezone support.
 * 
 * @author jonson
 *
 */
public interface FlipClockFinish {
	
	//public void setTimeZone(TimeZone timeZone);
	
	public void onFlipClockFinish();
	
}
