package me.iwf.photopicker.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.iwf.photopicker.R;
import me.iwf.photopicker.entity.Photo;
import me.iwf.photopicker.entity.PhotoDirectory;
import me.iwf.photopicker.event.OnPhotoClickListener;

/**
 * Created by donglua on 15/5/31.
 */
public class PhotoGridAdapter extends SelectableAdapter<PhotoGridAdapter.PhotoViewHolder> {

    private LayoutInflater inflater;
    private RequestManager glide;

//    private OnItemCheckListener onItemCheckListener = null;
    private OnPhotoClickListener onPhotoClickListener = null;

    public final static int ITEM_TYPE_CAMERA = 100;
    public final static int ITEM_TYPE_PHOTO = 101;
    public final static int ITEM_TYPE_BIG_PHOTO = 102;
    private final static int COL_NUMBER_DEFAULT = 3;

    private int imageSize;
    private int columnNumber = COL_NUMBER_DEFAULT;
    private int screenSize = 500;

    public PhotoGridAdapter(Context context, List<PhotoDirectory> photoDirectories) {
        this.photoDirectories = photoDirectories;
        this.glide = Glide.with(context);
        inflater = LayoutInflater.from(context);
        setColumnNumber(context, columnNumber);
    }

    public PhotoGridAdapter(Context context, List<PhotoDirectory> photoDirectories, int colNum) {
        this(context, photoDirectories);
        setColumnNumber(context, colNum);
    }

    private void setColumnNumber(Context context, int columnNumber) {
        this.columnNumber = columnNumber;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        screenSize = metrics.widthPixels;
        imageSize = screenSize / columnNumber;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? ITEM_TYPE_BIG_PHOTO : ITEM_TYPE_PHOTO;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_photo, parent, false);
        PhotoViewHolder holder = new PhotoViewHolder(itemView);
        if (viewType == ITEM_TYPE_BIG_PHOTO) {
//            holder.vSelected.setVisibility(View.GONE);
            holder.ivPhoto.setScaleType(ImageView.ScaleType.CENTER);
        }
        return holder;
    }


    @Override
    public void onBindViewHolder(final PhotoViewHolder holder, final int position) {
        List<Photo> photos = getCurrentPhotos();
        final Photo photo;
        photo = photos.get(position);

        if (getItemViewType(position) == ITEM_TYPE_PHOTO) {
            glide.load(new File(photo.getPath()))
                    .centerCrop()
                    .dontAnimate()
                    .thumbnail(0.5f)
                    .override(imageSize, imageSize)
                    .placeholder(R.drawable.ic_photo_black_48dp)
                    .error(R.drawable.ic_broken_image_black_48dp)
                    .into(holder.ivPhoto);

//            final boolean isChecked = isSelected(photo);

//            holder.vSelected.setSelected(isChecked);
//            holder.ivPhoto.setSelected(isChecked);

            holder.ivPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onPhotoClickListener != null) {
                        onPhotoClickListener.onClick(view, position,photo);
                    }
                }
            });
//            holder.vSelected.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    boolean isEnable = true;
//                    if (onItemCheckListener != null) {
//                        isEnable = onItemCheckListener.OnItemCheck(position, photo, isChecked,
//                                getSelectedPhotos().size());
//                    }
//                    if (isEnable) {
//                        toggleSelection(photo);
//                        notifyItemChanged(position);
//                    }
//                }
//            });
        } else {
            if (position == 0) {
                StaggeredGridLayoutManager.LayoutParams layoutParams = new StaggeredGridLayoutManager.LayoutParams(screenSize, screenSize);
                layoutParams.setFullSpan(true);
                holder.itemView.setLayoutParams(layoutParams);
                glide
                        .load(new File(photo.getPath()))
                        .centerCrop()
                        .dontAnimate()
                        .thumbnail(0.5f)
                        .override(screenSize, screenSize)
                        .placeholder(R.drawable.ic_photo_black_48dp)
                        .error(R.drawable.ic_broken_image_black_48dp)
                        .into(holder.ivPhoto);

                holder.ivPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onPhotoClickListener != null) {
                            onPhotoClickListener.onClick(view, position,photo);
                        }
                    }
                });
            }
        }
    }


    @Override
    public int getItemCount() {
        int photosCount = photoDirectories.size() == 0 ? 0 : getCurrentPhotos().size();

        return photosCount;
    }


    public static class PhotoViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivPhoto;
//        private View vSelected;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ivPhoto = (ImageView) itemView.findViewById(R.id.iv_photo);
//            vSelected = itemView.findViewById(R.id.v_selected);
        }
    }


//    public void setOnItemCheckListener(OnItemCheckListener onItemCheckListener) {
//        this.onItemCheckListener = onItemCheckListener;
//    }


    public void setOnPhotoClickListener(OnPhotoClickListener onPhotoClickListener) {
        this.onPhotoClickListener = onPhotoClickListener;
    }


    public ArrayList<String> getSelectedPhotoPaths() {
        ArrayList<String> selectedPhotoPaths = new ArrayList<>(getSelectedItemCount());

        for (Photo photo : selectedPhotos) {
            selectedPhotoPaths.add(photo.getPath());
        }

        return selectedPhotoPaths;
    }


}
