package me.iwf.photopicker;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import me.iwf.photopicker.fragment.PhotoPickerFragment;

public class PhotoPickerActivity extends AppCompatActivity {
    private PhotoPickerFragment pickerFragment;

    public final static String EXTRA_MAX_COUNT = "MAX_COUNT";
    public final static String EXTRA_SHOW_CAMERA = "SHOW_CAMERA";
    public final static String EXTRA_SHOW_GIF = "SHOW_GIF";
    public final static String KEY_SELECTED_PHOTOS = "SELECTED_PHOTOS";
    public final static String EXTRA_GRID_COLUMN = "column";

    public final static int DEFAULT_MAX_COUNT = 9;
    public final static int DEFAULT_COLUMN_NUMBER = 3;

    private int maxCount = DEFAULT_MAX_COUNT;

    /**
     * to prevent multiple calls to inflate menu
     */
    private int columnNumber = DEFAULT_COLUMN_NUMBER;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_photo_picker);

//        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(mToolbar);
//        setTitle(R.string.__picker_title);

//        sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));


//    ActionBar actionBar = getSupportActionBar();
//
//    assert actionBar != null;
//    actionBar.setDisplayHomeAsUpEnabled(true);
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//      actionBar.setElevation(25);
//    }

        ImageButton backOnQuoteFragment = (ImageButton) findViewById(R.id.backOnQuoteFragment);
        backOnQuoteFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        maxCount = getIntent().getIntExtra(EXTRA_MAX_COUNT, DEFAULT_MAX_COUNT);
        columnNumber = getIntent().getIntExtra(EXTRA_GRID_COLUMN, DEFAULT_COLUMN_NUMBER);

        pickerFragment = PhotoPickerFragment.newInstance(columnNumber, maxCount);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, pickerFragment)
                .commit();
        getSupportFragmentManager().executePendingTransactions();

    }


    /**
     * Overriding this method allows us to run our exit animation first, then exiting
     * the activity when it complete.
     */
    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }

    public PhotoPickerActivity getActivity() {
        return this;
    }


}
