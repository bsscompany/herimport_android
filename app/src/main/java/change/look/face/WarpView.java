package change.look.face;

import java.util.ArrayList;

import com.islayapp.bitmapmesh.Point;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

public class WarpView extends ImageView {
	private Bitmap mBitmap;
	private boolean computing = false;
	//private int currentPointIndex = -1;
	
	private ArrayList<Point> oriPoints;
	private ArrayList<Point> dstPoints;
///////////////////////////////////////////////////////////////////////////
	private Matrix matrix = new Matrix();
	private Matrix savedMatrix = new Matrix();
	private Matrix mInverse = new Matrix();
	
	private PointF start = new PointF();
	private PointF mid = new PointF();
	private PointF moveMid = new PointF();
	
	private static final int NONE = 0;
	private static final int DRAG = 1;
	private static final int ZOOM = 2;
	private int mode = NONE;
	
	private float[] lastEvent = null;
	private float oldDist = 1f;
	private float d = 0f;
	private float newRot = 0f;

    private int viewWidth=0;
    private int viewHeight=0;

	//New algorithm
	Point fromPoint;
	Point toPoint;
	Point startPoint;
	float rmax = 100;
	int rowsOfFaces = 96;
	int columnsOfFaces = 64;
	int size = (rowsOfFaces+1) * (columnsOfFaces+1);
	private float[] orgmat = new float[size * 2];
	private float[] mat = new float[size * 2];
	//initialize the matrix. Use real coordinate for drawBitmapMesh().
	void initMat(){
		int index = 0;
		for (int iy = 0; iy <= rowsOfFaces; iy++) {
			float y = (float) iy / (rowsOfFaces) * viewHeight;
			for (int ix = 0; ix <= columnsOfFaces; ix++) {
				float x = (float) ix / (float) (columnsOfFaces) *viewWidth;
				//(iy, ix)
				index = iy*(columnsOfFaces+1)+ix;
				setXY(mat, index, x, y);
				setXY(orgmat, index, x, y);
			}
		}
	}
	void calcMesh(Point fromPoint, Point toPoint) {
		int index = 0;
		for (int iy = 0; iy <= rowsOfFaces; iy++) {
			for (int ix = 0; ix <= columnsOfFaces; ix++) {
				index = iy*(columnsOfFaces+1)+ix;
				float x = mat[index*2+0];
				float y = mat[index*2+1];
				float dx = toPoint.x - fromPoint.x;
				float dy = toPoint.y - fromPoint.y;
				float r = (float)Math.sqrt(Math.pow(x - fromPoint.x, 2) + Math.pow(y - fromPoint.y, 2));
				if (r > rmax)
					continue;
				float t = r / rmax;
				float scale = 0.5f * (float)(Math.cos(t * Math.PI) + 1.0f);
				mat[index*2+0] = x + dx * scale;
				mat[index*2+1] = y + dy * scale;
			}
		}
	} //end of new algorithm


	public WarpView(Context context) {
		super(context);

	}

	@Override
	public void setImageBitmap(Bitmap bm)
	{
		super.setImageBitmap(bm);
		this.setDrawingCacheEnabled(true);
		//setFocusable(true);

		mBitmap = bm;
        matrix.reset();
        matrix.invert(mInverse);
		initMat();
	}
	public WarpView(Context context, AttributeSet attrs){
		super(context, attrs);
		oriPoints = new ArrayList<Point>();
        dstPoints = new ArrayList<Point>();
	}
	private Point adjustPointMove(Point p)
	{
		float reduceRate = 0.3f;
		Point t = this.oriPoints.get(this.oriPoints.size()-1);
		float dx = (p.x - t.x) * reduceRate;
		float dy = (p.y - t.y) * reduceRate;
		return new Point(t.x+dx, t.y+dy);
	}
	@Override
	 protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
	     super.onSizeChanged(xNew, yNew, xOld, yOld);

	     this.viewWidth = xNew;
	     this.viewHeight = yNew;

		rmax = (int)(this.viewWidth * 0.25);
		this.initMat();
	}

	private static void setXY(float[] array, int index, float x, float y) {
        array[index * 2 + 0] = x;
        array[index * 2 + 1] = y;
    }

	@Override
    protected void onDraw(Canvas canvas) {
        canvas.concat(matrix);
        if (mBitmap == null)
        	return;

		canvas.drawBitmapMesh(mBitmap, columnsOfFaces, rowsOfFaces, mat, 0, null, 0, null);
    }
	@Override
    public boolean onTouchEvent(MotionEvent event) {
		this.bringToFront();
		WarpView localImageView = (WarpView) this;
	  
		MyAbsoluteLayout.LayoutParams localLayoutParams1 =  (MyAbsoluteLayout.LayoutParams)getLayoutParams();
		//localImageView.setBackgroundColor(Color.TRANSPARENT);
		localImageView.setLayoutParams(localLayoutParams1);
		localImageView.setImageMatrix(matrix);
		localImageView.setScaleType(ScaleType.MATRIX);
		
		Point curPoint = new Point(event.getX(), event.getY());
		
		switch(event.getAction() & MotionEvent.ACTION_MASK){
			case MotionEvent.ACTION_DOWN:
			savedMatrix.set(matrix);
    		savedMatrix.invert(mInverse);
  			this.oriPoints.add(invertP(curPoint));
       		this.dstPoints.add(invertP(curPoint));
    		mode = DRAG;

			fromPoint = invertP(curPoint);
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			oldDist = spacing(event);
			if (oldDist > 10f) {
				savedMatrix.set(matrix);
				savedMatrix.invert(mInverse);
				midPoint(mid, event);
				mode = ZOOM;
			}
			
			start.set(mid);
			
			lastEvent = new float[4];
			lastEvent[0] = event.getX(0);
			lastEvent[1] = event.getX(1);
			lastEvent[2] = event.getY(0);
			lastEvent[3] = event.getY(1);
			
			d = rotation(event);
	//		savedMatrix.set(matrix);
			break;
    	case MotionEvent.ACTION_UP:
    		if(mode == ZOOM)
    			return false;
    		Point p1 = this.oriPoints.get(this.oriPoints.size()-1);
    		Point p2 = this.dstPoints.get(this.dstPoints.size()-1);
    		if(p1.infinityNormDistanceTo(p2)<10)
    		{
    			this.oriPoints.remove(this.oriPoints.size() - 1);
    			this.dstPoints.remove(this.dstPoints.size() - 1);
    		}
    		mode = NONE;
    		
    		//BitmapDrawable bitmapDrawable = ((BitmapDrawable) this.getDrawable());
    		//this.buildDrawingCache(true);
    		//this.mBitmap = Bitmap.createBitmap(this.getDrawingCache());
    		//matrix.reset();
    		//matrix.invert(mInverse);
    		//this.mBitmap = Bitmap.createBitmap(this.getDrawingCache(), 0, 0, this.imgWidth, this.imgHeight, this.mInverse, true);
    		//this.mBitmap = bitmapDrawable.getBitmap();
    		
    		break;
		case MotionEvent.ACTION_POINTER_UP:
			mode = NONE;
			lastEvent = null;
			break;
    	case MotionEvent.ACTION_MOVE:
    		if (mode == DRAG){
				matrix.set(savedMatrix);
    			matrix.invert(mInverse);
    			if(this.computing)
        			return false;
    			curPoint = invertP(curPoint);

				this.computing = true;
				Point t = this.adjustPointMove(curPoint);
				this.dstPoints.set(this.dstPoints.size() - 1, t);
				this.calcMesh(fromPoint, t);
				fromPoint = t;
				this.invalidate();
				this.computing = false;
    		}
    		else if (mode == ZOOM){
    			matrix.set(savedMatrix);
    			matrix.invert(mInverse);
    			midPoint(moveMid, event);
			
    			float dx = moveMid.x - start.x;
    			float dy = moveMid.y - start.y;
    			matrix.postTranslate(dx, dy);
				float newDist = spacing(event);
				if (newDist > 10f) {
					float scale = (newDist / oldDist);
					matrix.postScale(scale, scale, mid.x, mid.y);
				}
			
				if (lastEvent != null && event.getPointerCount() == 2) {
					newRot = rotation(event);
					// matrix.postRotate(70);
					
					float r = newRot - d;
					float[] values = new float[9];
					matrix.getValues(values);
					matrix.postRotate(r, this.getMeasuredWidth() / 2,
						this.getMeasuredHeight() / 2);
		   		}
    		}
    		break;
    	}
        return true;
    }

	private float spacing(MotionEvent event) {
	    float x = event.getX(0) - event.getX(1);
	    float y = event.getY(0) - event.getY(1);
	    return (float) Math.sqrt(x * x + y * y);
	}

	private void midPoint(PointF point, MotionEvent event) {
	    float x = event.getX(0) + event.getX(1);
	    float y = event.getY(0) + event.getY(1);
	    point.set(x / 2, y / 2);
	}

	private float rotation(MotionEvent event) 
	{
		//Toast.makeText(getApplicationContext(), "get x :-"+event.getX(0)+""+event.getX(1), Toast.LENGTH_LONG).show();
	    double delta_x = (event.getX(0) - event.getX(1));
	    double delta_y = (event.getY(0) - event.getY(1));
	    double radians = Math.atan2(delta_y, delta_x);
	    //Toast.makeText(getApplicationContext(),Math.toDegrees(radians)+"", Toast.LENGTH_LONG).show();
	    return (float) Math.toDegrees(radians);
	}

	private Point invertP(Point p){
		float[] temp = {p.x, p.y};
		mInverse.mapPoints(temp);
		return new Point(temp[0], temp[1]);
	}
}
