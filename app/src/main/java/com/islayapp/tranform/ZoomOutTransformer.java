/*
 * Copyright 2014 Toxic Bakery
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.islayapp.tranform;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.islayapp.R;

public class ZoomOutTransformer extends ABaseTransformer {


    @Override
    protected void onTransform(View view, float position) {
        int scroll = 0;
        try {
            float p = Float.parseFloat(view.getTag().toString());
            if (p > position)
                scroll = 1;
            else scroll = -1;
        } catch (Exception e) {
            view.setTag(position);
        }


        view.setTag(position);
        view.setAlpha(1f);


        ImageView imageView = (ImageView) view.findViewById(R.id.img_shop_slider);
        View view1 = view.findViewById(R.id.slider_linearlayout_caption_narrow);
        View view2 = view.findViewById(R.id.slider_linearlayout_caption);
        TextView text = (TextView) view.findViewById(R.id.tv_hottestStyles);
        if (scroll == 1) {
            if (position < 1.5f && position >= 0.5f) {
                centerView(view, imageView, text, view1, view2);
            } else {
                otherView(view, imageView, text, view1, view2);
            }
        } else if (scroll == -1) {
            if (position <= 0.5f && position > -0.5f) {
                centerView(view, imageView, text, view1, view2);
            } else {
                otherView(view, imageView, text, view1, view2);
            }
        } else {
            if (position == 0.5f) {
                centerView(view, imageView, text, view1, view2);
            } else {
                otherView(view, imageView, text, view1, view2);
            }
        }
    }

    private void centerView(View parent, ImageView img, TextView textView, View view1, View view2) {
        if (view1.getVisibility() == View.INVISIBLE)
            view1.setVisibility(View.VISIBLE);
        view2.setBackgroundColor(ContextCompat.getColor(parent.getContext(), R.color.bg_label_slider));
        textView.setTextColor(ContextCompat.getColor(parent.getContext(), R.color.white_color));
        textView.setAlpha(1f);
        img.setScaleX(1f);
        img.setScaleY(1f);
        img.setAlpha(1f);
    }

    private void otherView(View parent, ImageView img, TextView textView, View view1, View view2) {
        if (view1.getVisibility() == View.VISIBLE)
            view1.setVisibility(View.INVISIBLE);
        view2.setBackgroundColor(ContextCompat.getColor(parent.getContext(), R.color.white_color));
        textView.setTextColor(ContextCompat.getColor(parent.getContext(), R.color.bg_label_slider));
        textView.setAlpha(0.5f);
        img.setScaleX(0.8f);
        img.setScaleY(0.8f);
        img.setAlpha(0.5f);
    }

    @Override
    public boolean isPagingEnabled() {
        return true;
    }

}
