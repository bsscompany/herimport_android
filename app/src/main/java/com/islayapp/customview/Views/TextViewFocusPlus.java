package com.islayapp.customview.Views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;

import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;

/**
 * Created by DINO on 08/04/2016.
 */
public class TextViewFocusPlus extends TextViewPlus {
    String textTrue, textFasle;
    int colorTrue, colorFasle;

    public TextViewFocusPlus(Context context) {
        super(context);
    }

    public TextViewFocusPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        getTextSelect(context, attrs);
        setStable(isSelected());
    }

    public TextViewFocusPlus(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getTextSelect(context, attrs);
        setStable(isSelected());
    }

    private void getTextSelect(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewFocusPlus);
        textFasle = a.getString(R.styleable.TextViewFocusPlus_textSelectFasle);
        textTrue = a.getString(R.styleable.TextViewFocusPlus_textSelectTrue);
        colorTrue = a.getColor(R.styleable.TextViewFocusPlus_colorSelectTrue, Color.BLACK);
        colorFasle = a.getColor(R.styleable.TextViewFocusPlus_colorSelectFalse, Color.BLACK);
        a.recycle();
    }

    private void setStable(boolean selected) {
        if (selected) {
            setText(textTrue);
            setTextColor(colorTrue);
        } else {
            setText(textFasle);
            setTextColor(colorFasle);
        }
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        setStable(selected);
    }
}
