package com.islayapp.customview;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import com.islayapp.R;

/**
 * Created by maxo on 3/11/16.
 */
public class CircleView extends View {

    private static String COLOR_HEX = "#FFFFFF";
    private static String COLOR_HEX_BORDER = "#000000";
    private Paint drawPaintbg;
    private Paint drawPaintBorder;
    private float size;

    private boolean isSelect = false;

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        drawPaintbg = new Paint();
        drawPaintbg.setColor(Color.parseColor(COLOR_HEX));
        drawPaintbg.setAntiAlias(true);
        drawPaintbg.setStyle(Paint.Style.FILL);

        drawPaintBorder = new Paint();
        drawPaintBorder.setColor(Color.parseColor(COLOR_HEX_BORDER));
        drawPaintBorder.setAntiAlias(true);
        drawPaintBorder.setStyle(Paint.Style.STROKE);

        setBackgroundResource(R.color.transparent);
        setOnMeasureCallback();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isSelect())
            drawPaintbg.setColor(Color.parseColor(COLOR_HEX_BORDER));
        else
            drawPaintbg.setColor(Color.parseColor(COLOR_HEX));

        canvas.drawCircle(size, size, size, drawPaintbg);
        canvas.drawCircle(size, size, size, drawPaintBorder);
    }

    private void setOnMeasureCallback() {
        ViewTreeObserver vto = getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                removeOnGlobalLayoutListener(this);
                size = getMeasuredWidth() / 2;
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void removeOnGlobalLayoutListener(ViewTreeObserver.OnGlobalLayoutListener listener) {
        if (Build.VERSION.SDK_INT < 16) {
            getViewTreeObserver().removeGlobalOnLayoutListener(listener);
        } else {
            getViewTreeObserver().removeOnGlobalLayoutListener(listener);
        }
    }


    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
        this.invalidate();
    }

}