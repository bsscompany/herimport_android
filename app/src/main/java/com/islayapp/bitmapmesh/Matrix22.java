package com.islayapp.bitmapmesh;



public class Matrix22 {
	double m11;
	double m12;
	double m21;
	double m22;
	public Matrix22(double n11, double n12, double n21, double n22){
		this.m11 = n11;
		this.m12 = n12;
		this.m21 = n21;
		this.m22 = n22;
	}
	public Matrix22 adjugate(){
		return new Matrix22(this.m22, -this.m12, -this.m21, this.m11);
	}
	public double determinant(){
		return this.m11 * this.m22 - this.m12*this.m21;
	}
	public Matrix22 muliply(double m){
		this.m11 *= m;
		this.m12 *= m;
		this.m21 *= m;
		this.m22 *= m;
		return this;
	}
	public Matrix22 addM(Matrix22 o){
		this.m11 += o.m11;
		this.m12 += o.m12;
		this.m21 += o.m21;
		this.m22 += o.m22;
		return this;
	}
	public Matrix22 inverse(){
		return this.adjugate().muliply(1.0/this.determinant());
	}
}
