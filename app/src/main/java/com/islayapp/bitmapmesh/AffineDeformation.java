package com.islayapp.bitmapmesh;



import java.util.ArrayList;

public class AffineDeformation {
	private float[] w;
	private Point[] pRelative;
	private Point[] qRelative;
	private float[] A;
	private int n;
	private float alpha=1;
	private ArrayList<Point> fromPoints;
	private ArrayList<Point> toPoints;
	public AffineDeformation(ArrayList<Point> from, ArrayList<Point> to){
		this.fromPoints = from;
		this.toPoints = to;
		this.n = from.size();
	}
	public Point pointMover(Point point) {
		if(this.pRelative==null || this.pRelative.length<this.n){
			this.pRelative = new Point[this.n];
		}
		if(this.qRelative==null || this.qRelative.length<this.n){
			this.qRelative = new Point[this.n];
		}
		if(this.w==null || this.w.length<this.n) {
			this.w = new float[this.n];
		}
		if(this.A==null || this.A.length<this.n) {
			this.A = new float[this.n];
		}
		for(int i=0; i<this.n;i++) {
			Point t= this.fromPoints.get(i).subtract(point);
			if(t.x==0 && t.y==0)
				this.w[i]=1;
			else
				this.w[i] = (float) Math.pow(t.x * t.x + t.y * t.y, -this.alpha);
		}
		Point pAverage = point.weightedAverage(this.fromPoints, this.w);
		Point qAverage = point.weightedAverage(this.toPoints, this.w);
		
		for(int i=0; i<this.n; ++i) {
			this.pRelative[i] = this.fromPoints.get(i).subtract(pAverage);
			this.qRelative[i] = this.toPoints.get(i).subtract(qAverage);
		}
		Matrix22 B = new Matrix22(0,0,0,0);
		for(int i=0;i<this.n;++i){
			B.addM(this.pRelative[i].wXtX(this.w[i]));
		}
		B = B.inverse();
		for(int j=0;j<this.n;j++) {
			this.A[j] = point.subtract(pAverage).multiply(B).dotP(this.pRelative[j])*this.w[j];
		}
		Point r = qAverage;
		for(int i=0; i<this.n; i++) {
			r = r.add(this.qRelative[i].multiply_d(this.A[i]));
		}
		return r;
	}
	
}
