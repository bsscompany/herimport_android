package com.islayapp.bitmapmesh;



import java.util.ArrayList;
public class Point {
	public float x;
	public float y;
	public Point(float x, float y){
		this.x=x;
		this.y=y;
	}
	public Point add(Point o){
		return new Point(this.x + o.x, this.y + o.y);
	}
	public Point subtract(Point o){
		return new Point(this.x - o.x, this.y - o.y);
	}
	public Matrix22 wXtX(double w){
		return (new Matrix22(
		        this.x * this.x * w, this.x * this.y * w,
		        this.y * this.x * w, this.y * this.y * w
		        ));
	}
	public float dotP(Point o){
		return this.x * o.x + this.y * o.y;
	}
	public Point multiply(Matrix22 o){
		return new Point(
			      (int)(this.x * o.m11 + this.y * o.m21), (int)(this.x * o.m12 + this.y * o.m22));
	}
	public Point multiply_d(float o){
		return new Point((int)(this.x*o), (int)(this.y*o));
	}
	public Point weightedAverage(ArrayList<Point> p, float[] w){
		int i;
		double sx = 0;
		double sy = 0;
		double sw = 0;

		for (i = 0; i < p.size(); i++) {
			sx += p.get(i).x * w[i];
		    sy += p.get(i).y * w[i];
		    sw += w[i];
		}
		  return new Point((int)(sx / sw), (int)(sy / sw));
	}
	public float infinityNormDistanceTo(Point o){
		return Math.max(Math.abs(this.x - o.x), Math.abs(this.y - o.y));
	}
	
}
