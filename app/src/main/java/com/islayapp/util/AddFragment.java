package com.islayapp.util;

import android.support.annotation.Nullable;

/**
 * Created by maxo on 4/21/16.
 */
public interface AddFragment {
    public void addNewFragment(@Nullable Constants.FragmentEnum code, @Nullable String value);
}
