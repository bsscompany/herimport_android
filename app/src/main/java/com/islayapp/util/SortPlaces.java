package com.islayapp.util;

import android.util.Log;

import com.islayapp.models.awsmodels.Stores;

import java.util.Comparator;

/**
 * Created by maxo on 8/5/16.
 */
public class SortPlaces implements Comparator<Stores> {
    double currentLat;
    double currentLog;

    public SortPlaces(double lat, double log){
        currentLat = lat;
        currentLog = log;

        Log.d("TAG","My location: lat " + currentLat + ", Long: " + currentLog);
    }

    @Override
    public int compare(final Stores place1, final Stores place2) {
        double lat1 = place1.getLoc().get(0);
        double lon1 = place1.getLoc().get(1);

        double lat2 = place2.getLoc().get(0);
        double lon2 = place2.getLoc().get(1);

        double distanceToPlace1 = distance(currentLat, currentLog, lat1, lon1);
        double distanceToPlace2 = distance(currentLat, currentLog, lat2, lon2);

        double distanceToPlace3 = Math.abs(distanceToPlace1 - distanceToPlace2);

        Log.d("TAG","currentLat,currentLog: " + currentLat + ","+ currentLog);
        Log.d("TAG","lat1,long1: " + lat1 + ","+ lon1);
        Log.d("TAG","lat2,long2: " + lat2 + ","+ lon2);

        return (int) distanceToPlace3;
    }

    public double distance(double fromLat, double fromLon, double toLat, double toLon) {
        double radius = 6378137;   // approximate Earth radius, *in meters*
        double deltaLat = toLat - fromLat;
        double deltaLon = toLon - fromLon;
        double angle = 2 * Math.asin( Math.sqrt(
                Math.pow(Math.sin(deltaLat/2), 2) +
                        Math.cos(fromLat) * Math.cos(toLat) *
                                Math.pow(Math.sin(deltaLon/2), 2) ) );
        return radius * angle;
    }
}