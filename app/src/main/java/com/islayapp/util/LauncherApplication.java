package com.islayapp.util;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by apple on 12/23/15.
 */
public class LauncherApplication extends Application {

    public static final String TAG = LauncherApplication.class.getSimpleName();
    private static LauncherApplication mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Fabric.with(this, new Crashlytics());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static synchronized LauncherApplication getInstance() {
        return mInstance;
    }

}
