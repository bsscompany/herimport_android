package com.islayapp.util;

/**
 * Created by maxo on 6/10/16.
 */
public enum LoginResultType {
    PENDING,
    OK,
    ERROR_Provider_Error,
    ERROR_Provider_Declined,
    ERROR_Provider_Banned,
    ERROR_FB_Graph_Data_Missing,
    ERROR_FB_Declined,
    ERROR_FB_Error,
    ERROR_FB_Email_missing
}

