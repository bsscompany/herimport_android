package com.islayapp.util;

import com.islayapp.models.awsmodels.Stores;

import java.util.Comparator;

/**
 * Created by maxo on 8/5/16.
 */
public class SortAlphabe implements Comparator<Stores> {
    @Override
    public int compare(Stores p1, Stores p2)
    {
        if(p1.getName().equalsIgnoreCase(p2.getName()))
        {
            return p1.getName().compareTo(p2.getName());
        }
        return p1.getName().compareTo(p2.getName());
    }
}