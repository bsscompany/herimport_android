package com.islayapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceHelpers {

    public static void setPreference(Context context, String prefName, String data) {
        //Logger.logInfo("Pref: " + prefName + " is set to: " + (data == null ? "NULL":data) );

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(prefName, data);
        editor.commit();
    }

    public static String getPreference(Context context, String prefName) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String prefData = settings.getString(prefName, "");
        return prefData;
    }

    //int
    public static void setPreference(Context context, String prefName, int data) {
        //Logger.logInfo("Pref: " + prefName + " is set to: " + (data == null ? "NULL":data) );
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(prefName, data);
        editor.commit();
    }

    public static int getInt(Context context, String prefName) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        int prefData = settings.getInt(prefName, 0);
        return prefData;
    }

    //boolean
    public static void setPreference(Context context, String prefName, Boolean data) {
        //Logger.logInfo("Pref: " + prefName + " is set to: " + (data == null ? "NULL":data) );
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(prefName, data);
        editor.commit();
    }

    public static boolean getBoolean(Context context, String prefName) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        boolean prefData = settings.getBoolean(prefName, false);
        return prefData;
    }

    public static void removePreference(Context context, String prefName) {
//		Logger.logInfo("Pref: " + prefName + " is removed !");
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.remove(prefName);
        editor.commit();
    }


    private static final String PREF_FIRST_TIME = "PREF_FIRST_TIME";
    private static final String PREF_FIRST_TIME_SETTING = "PREF_FIRST_TIME_SETTING";
    private static final String PREF_FIRST_TIME_TEST = "PREF_FIRST_TIME_TEST";
    private static final String PREF_LAST_CLEAN_UP_SD = "PREF_LAST_CLEAN_UP_SD_CARD";

    public static boolean firstTimeRunning(Context context) {
        String firstTime = getPreference(context, PREF_FIRST_TIME);
        if (firstTime == null || firstTime.equals("")) {
            return true;
        } else return false;

    }

    public static void disableFirstTimeRunning(Context context) {
        setPreference(context, PREF_FIRST_TIME, "false");
    }

    public static boolean firstTimeRunningSetting(Context context) {
        String firstTime = getPreference(context, PREF_FIRST_TIME_SETTING);
        if (firstTime == null || firstTime.equals("")) {
            return true;
        } else return false;

    }

    public static void disableFirstTimeRunningSetting(Context context) {
        setPreference(context, PREF_FIRST_TIME_SETTING, "false");
    }


    public static boolean firstTimeRunningTest(Context context) {
        String firstTime = getPreference(context, PREF_FIRST_TIME_TEST);
        if (firstTime == null || firstTime.equals("")) {
            return true;
        } else return false;
    }

    public static void disableFirstTimeRunningTest(Context context) {
        setPreference(context, PREF_FIRST_TIME_TEST, "false");
    }


    public static long getLastCleanupTime(Context context) {
        String lastTime = getPreference(context, PREF_LAST_CLEAN_UP_SD);
        if (lastTime == null) {
            long current = System.currentTimeMillis();
            setLastCleanupTime(context, current);
            return current;
        } else {
            return Long.parseLong(lastTime);
        }

    }

    public static void setLastCleanupTime(Context context, long time) {
        setPreference(context, PREF_LAST_CLEAN_UP_SD, String.valueOf(time));
    }

    public static void setPosXGridLayout(Context context, int x) {
        setPreference(context, "PosXGirdLayout", x);
    }

    public static void setPosYGridLayout(Context context, int y) {
        setPreference(context, "PosYGirdLayout", y);
    }
}
