package com.islayapp.util;

/**
 * Created by maxo on 3/15/16.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

import java.io.InputStream;

public class BitmapUtils {
    public static Bitmap createReflectedBitmap(Bitmap srcBitmap,
                                               float reflectHeight) {
        if (null == srcBitmap) {
            return null;
        }

        int srcWidth = srcBitmap.getWidth();
        int srcHeight = srcBitmap.getHeight();
        int reflectionWidth = srcBitmap.getWidth();
        int reflectionHeight = reflectHeight == 0 ? srcHeight / 3
                : (int) (reflectHeight * srcHeight);

        if (0 == srcWidth || srcHeight == 0) {
            return null;
        }

        // The matrix
        Matrix matrix = new Matrix();
        matrix.preScale(1, -1);

        try {
            // The reflection bitmap, width is same with original's
            Bitmap reflectionBitmap = Bitmap.createBitmap(srcBitmap, 0,
                    srcHeight - reflectionHeight, reflectionWidth,
                    reflectionHeight, matrix, false);

            if (null == reflectionBitmap) {
                return null;
            }

            Canvas canvas = new Canvas(reflectionBitmap);

            Paint paint = new Paint();
            paint.setAntiAlias(true);
            LinearGradient shader = new LinearGradient(0, 0, 0,
                    reflectionBitmap.getHeight(), Color.RED, Color.BLUE,
                    Shader.TileMode.MIRROR);

            LinearGradient mShader = new LinearGradient(0, 0, 0,
                    reflectionBitmap.getHeight(), new int[]{
                    0x70FFFFFF, 0xFFFFFFFF},
                    null, Shader.TileMode.MIRROR);  // CLAMP MIRROR REPEAT

            paint.setShader(mShader);

//            paint.setXfermode(new PorterDuffXfermode(
//                    android.graphics.PorterDuff.Mode.DST_IN));

            // Draw the linear shader.
            canvas.drawRect(0, 0, reflectionBitmap.getWidth(),
                    reflectionBitmap.getHeight(), paint);

            return reflectionBitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Bitmap loadBitmapFromAssets(Context mcContext, String path) {
        InputStream stream = null;
        try {
            stream = mcContext.getAssets().open(path);
            return BitmapFactory.decodeStream(stream);
        } catch (Exception ignored) {
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    public static Drawable loadDrawableFromAssets(Context mContext, String path) {
        InputStream stream = null;
        try {
            stream = mContext.getAssets().open(path);
            return Drawable.createFromStream(stream, null);
        } catch (Exception ignored) {
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    public static Bitmap resizeImageToMaxSize(Bitmap bitmap, int maxSize) {
        Bitmap resizedBitmap = null;
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int newWidth = -1;
        int newHeight = -1;
        float multFactor = -1.0F;

        if (originalWidth < maxSize && originalHeight < maxSize)
            return bitmap;

        if (originalHeight > originalWidth) {
            newHeight = maxSize;
            multFactor = (float) originalWidth / (float) originalHeight;
            newWidth = (int) (newHeight * multFactor);
        } else if (originalWidth > originalHeight) {
            newWidth = maxSize;
            multFactor = (float) originalHeight / (float) originalWidth;
            newHeight = (int) (newWidth * multFactor);
        } else if (originalHeight == originalWidth) {
            newHeight = maxSize;
            newWidth = maxSize;
        }
        resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
        return resizedBitmap;
    }
}