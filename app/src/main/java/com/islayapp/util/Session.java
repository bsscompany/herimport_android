package com.islayapp.util;

import com.islayapp.models.awsmodels.user.JsonResource;
import com.islayapp.models.awsmodels.user.ResponseUsers;

/**
 * Created by maxo on 4/29/16.
 */
public class Session {

    private static ResponseUsers users;
    private static int currentPoint = 0;
    private static JsonResource jsonResource;

    public static ResponseUsers getUsers() {
        return users;
    }

    public static void setUsers(ResponseUsers users) {
        Session.users = users;
    }

    public static int getCurrentPoint() {
        return currentPoint;
    }

    public static void setCurrentPoint(int currentPoint) {
        Session.currentPoint = currentPoint;
    }

    public static JsonResource getJsonResource() {
        return jsonResource;
    }

    public static void setJsonResource(JsonResource jsonResource) {
        Session.jsonResource = jsonResource;
    }
}