package com.islayapp.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;

/**
 * Created by maxo on 4/8/16.
 */
public class DialogUtil {

    public static void showAlertMessage(Context mContext, String msg) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int wwidth = metrics.widthPixels;


        final Dialog dialog = new Dialog(mContext);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.alert_dialog_message_btn_ok);

        dialog.getWindow().setLayout(600, 400);

        TextViewPlus alert_btn_ok = (TextViewPlus) dialog.findViewById(R.id.alert_btn_ok);
        alert_btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        TextViewPlus message_alert = (TextViewPlus) dialog.findViewById(R.id.message_alert);
        message_alert.setText(msg);

        dialog.show();

    }



    // Blue toast
    public static void toastMessNoIcon(Context context, String mess) {
        LayoutInflater inflaterRoot = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View customToastroot = inflaterRoot.inflate(R.layout.custom_toast_no_icon, null);
        Toast customtoast = new Toast(context);
        TextView textView_custom_toast = (TextView) customToastroot.findViewById(R.id.textView_custom_toast);
        textView_custom_toast.setText(mess);
        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();
    }

    // Blue toast
    public static void toastMess(Context context, String mess) {
        LayoutInflater inflaterRoot = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View customToastroot = inflaterRoot.inflate(R.layout.custom_toast, null);
        Toast customtoast = new Toast(context);
        TextView textView_custom_toast = (TextView) customToastroot.findViewById(R.id.textView_custom_toast);
        textView_custom_toast.setText(mess);
        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();
    }

    public static Dialog showProgressDialog(Context mcontext) {
        Dialog progress;
        progress = new Dialog(mcontext);
        progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        progress.getWindow().requestFeature(Window.FEATURE_PROGRESS);
        progress.getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        progress.setCancelable(false);
//        progress.setIndeterminate(true);
        progress.show();
        progress.setContentView(R.layout.progressdialog);
//        progress = ProgressDialog.show(mcontext, "", "", true);
        return progress;
    }

    public static Dialog showDialog(Context mcontext) {
        Dialog progress;
        progress = new Dialog(mcontext);
        progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        progress.getWindow().requestFeature(Window.FEATURE_PROGRESS);
        progress.getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        progress.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progress.setCancelable(false);
        progress.show();
        progress.setContentView(R.layout.progressdialog);
//        progress = ProgressDialog.show(mcontext, "", "", true);
        return progress;
    }
    public static Dialog showProgressDialog(Context mcontext, boolean ishow) {
        if(ishow) {
            Dialog progress;
            progress = new Dialog(mcontext);
            progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progress.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
            progress.getWindow().requestFeature(Window.FEATURE_PROGRESS);
            progress.getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
            progress.setCancelable(false);
//            progress.setIndeterminate(true);
            progress.show();
            progress.setContentView(R.layout.progressdialog);
            return progress;
        }else
            return null;
    }
}
