package com.islayapp.util;

/**
 * Created by apple on 12/30/15.
 */
public class Constants {

    //constant request message
    public static final String RESULT_SUCCESS = "Success";

    public static final String BASE_URL = "https://m.herimports.com/";
    public static final String URL_WHAT_HOT = "https://www.herimports.com/";

    public static final String BASE_AWS_URL = "http://sewin-api.herimports.com/";

    public static final String POST_IMAGES = "https://s3.amazonaws.com/sewin-media/posts/images/";

    public static final String BASE_GOOGLE_MAP_URL = "https://maps.googleapis.com/";

    //    public static final String BASE_URL_GOOGLE_MAP_API = "https://maps.googleapis.com/maps/api/";
    public static final String GOOGLE_MAPS_KEY = "AIzaSyBHk5RcwrN6Ua8bUANsv7uiSd5WdrFLjdE";
    public static final String DISTANCE_TYPE = "Miles";
    public static final int PAGE_SIZE = 20;


    public static int READ_EXTERNAL_STORAGE  = 1;
    public static int ACCESS_FINE_LOCATION  = 2;
    public static int ACCESS_COARSE_LOCATION  = 3;

    public enum FragmentEnum {
        // shop
        FRAGMENT_SHOP, FRAGMENT_SHOP_PRODUCT_DETAILS, FRAGMENT_SHOP_MY_CART,
        FRAGMENT_SHOP_SHIPPING_METHOD_AND_PAYMENT,
        FRAGMENT_SHOP_BILLING_AND_SHIPPING, FRAGMENT_SHOP_THANK_YOU, FRAGMENT_START_PAYPAL,
        // get lay
        FRAGMENT_GETLAYD_STYLIST_DIRECTORY, FRAGMENT_GETLAYD_STYLIST_FEED,
        FRAGMENT_GETLAYD_VIRTUAL_HAIR;
    }

    public enum PRELOADVIEW {
        // shop
        LOADING, FAILURE, NO_RESULT, GONE_ALL;
    }


    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_ENDPOINT_COMPLETED = "registration_endpoint_completed";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String GCM_TOKEN = "gcm_token";

    //    table name
    public static final String USER_TABLE = "User";

    // what hot fragment key
    public static final int FRAGMENT_WHAT_HOT_USER_PHOTO = 200;
    public static final int FRAGMENT_WHAT_HOT_PROFILE_IMAGE = 201;
    public static final int FRAGMENT_WHAT_HOT_HAIR_STYLE = 202;
    public static final int FRAGMENT_WHAT_HOT_HAIR_FILTER = 203;
    public static final int FRAGMENT_WHAT_HOT_HAIR_COVER = 204;
    public static final int FRAGMENT_WHAT_HOT_HAIR_FINAL = 205;
    public static final int FRAGMENT_WHAT_HOT_HAIR_SHARE = 206;
    public static final int FRAGMENT_WHAT_HOT_HAIR_SAVE = 207;
    public static final int FRAGMENT_WHAT_HOT_HAIR_SUBMIT = 208;
    public static final int FRAGMENT_WHAT_HOT_HOME = 209;

    // general fragment key
    public static final int FRAGMENT_GENERAL_STORE_LOCATOR = 300;
    public static final int FRAGMENT_GENERAL_MY_PROFILE = 301;
    public static final int FRAGMENT_GENERAL_NEW_POST = 302;
    public static final int FRAGMENT_GENERAL_APPLY_STYLIST = 303;

    //get layd fragment
    public static final int FRAGMENT_GETLAYD_QUOTE_ROOT = 400;
    public static final int FRAGMENT_GETLAYD_QUOTE_CHOOSE_PHOTO = 401;
    public static final int FRAGMENT_GETLAYD_BUNDLES_AND_CLOSURES = 402;
    public static final int FRAGMENT_GETLAYD_ADDITIONAL_DETAIL = 403;
    public static final int FRAGMENT_GETLAYD_ATTACH_VOICE = 404;
    public static final int FRAGMENT_GETLAYD_CALENDER = 405;
    public static final int FRAGMENT_GETLAYD_SCHEDULE = 406;
    public static final int FRAGMENT_GETLAYD_VERIFY_ACCOUNT = 407;
    public static final int FRAGMENT_GETLAYD_PAYMENT = 408;
    public static final int FRAGMENT_GETLAYD_WAIT_AND_SUCCESS = 409;
    public static final int FRAGMENT_GETLAYD_STYLIST_DIRECTORY = 410;
    public static final int FRAGMENT_GETLAYD_STYLIST_FEED = 411;
    public static final int FRAGMENT_GETLAYD_VIRTUAL_HAIR = 412;

    public static final int REQUEST_CODE_GET_IMAGE = 2000;


    public static final String STYLIST = "S";
    public static final String CUSTOMER = "C";
    public static final String MOD = "M";
    public final static String USER_TYPE = "usertype";

    // constants profile info
    public final static String FIRST_NAME = "firstname";
    public final static String LAST_NAME = "lastname";
    public final static String PHONE = "phone";
    public final static String PHONE_VERIFIRED = "phoneverifired";
    public final static String REFERREDBY = "referredby";
    public final static String NICK_NAME = "nickname";
    public final static String EMAIL = "email";
    public final static String EMAIL_VERIFIRED = "emailverifired";

    // constans for Payment and billing address
    public final static String NAME_ON_CARD = "name_on_card";
    public final static String CARD_NUMBER = "card_number";
    public final static String EXPIRATION_MONTH = "expiration_month";
    public final static String EXPIRATION_YEAR = "expiration_year";
    public final static String CVV = "cvv";
    public final static String BILLING_COUNTRY = "billing_country";
    public final static String BILLING_ZIP_CODE = "billing_zip_code";
    public final static String BILLING_ADDRESS1 = "billing_address1";
    public final static String BILLING_ADDRESS2 = "billing_address2";
    public final static String BILLING_CITY = "billing_city";
    public final static String BILLING_STATE = "billing_state";

}
