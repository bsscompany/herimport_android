package com.islayapp.models.shop;

/**
 * Created by maxo on 3/24/16.
 */
public class StoreLocator {
    private int id;
    private String name;

    public StoreLocator(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
