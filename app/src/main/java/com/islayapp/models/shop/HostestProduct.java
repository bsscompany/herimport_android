package com.islayapp.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HostestProduct {

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("data")
    @Expose
    private List<ProductDetails> productDetailsList = new ArrayList<ProductDetails>();

    /**
     * @return The status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return The productDetailsList
     */
    public List<ProductDetails> getProductDetailsList() {
        return productDetailsList;
    }

    /**
     * @param productDetailsList The data
     */
    public void setProductDetailsList(List<ProductDetails> productDetailsList) {
        this.productDetailsList = productDetailsList;
    }
}

