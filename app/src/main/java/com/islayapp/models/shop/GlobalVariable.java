package com.islayapp.models.shop;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Environment;

import com.islayapp.util.Session;

import java.io.File;
import java.io.FileOutputStream;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageColorInvertFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageGrayscaleFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageHueFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageMonochromeFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSepiaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageWhiteBalanceFilter;

public class GlobalVariable {

//    public static String API_URL = "http://herimport.bss-co.com/";
//    public static final String UPLOAD_PHOTO = "UploadToServer.php";

//    public static final String HairColorsDir = "haircolors";
    public static final String HairStylesDir = "hairstyles";
//    public static final String CoversDir = "covers";
    public static final String FiltersDir = "filters";

    public static int[] HAIRSTYLES;
    public static int[] HAIRCOLORS;
    public static int[] HAIRFILTERS;
    public static int[] HAIRCOVERS;
    public static String[] HAIRFILTERNAMES;

    public static Bitmap styleBitmap;
    public static Bitmap originBitmap;

//    public static String[] COUNTRY_LIST = {""};
    public static int selectHairCategoryIndex;
    public static int selectHairIndex;
    public static int selectHairColorIndex;
    public static int selectFilterIndex;

    public static GPUImageBrightnessFilter noneFilter, noirFilter, monoFilter;
    public static GPUImageVignetteFilter vignetteFilter;
    public static GPUImageHueFilter instantFilter;
    public static GPUImageSaturationFilter satFilter, linearFilter;
    public static GPUImageWhiteBalanceFilter transferFilter;
    public static GPUImageSepiaFilter sepiaFilter;
    public static GPUImageMonochromeFilter chromeFilter;
    public static GPUImageToneCurveFilter toneCurveFilter;
    public static GPUImageGrayscaleFilter grayFilter;
    public static GPUImageColorInvertFilter invertFilter;

    public static Bitmap userPhoto;
    public static Bitmap tempUserPhoto;

    public static GPUImage gpuImage;

    public static void writeUserPhoto() {
        File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/herimports");
        path.mkdir();

        String filename = path.getAbsolutePath() + "/" + Session.getUsers().getUser().getFacebook().getId();
        FileOutputStream outputStream;

        try {
            outputStream = new FileOutputStream(filename);
            userPhoto.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




//    public static void readUserPhoto() {
//        try {
//            File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/herimports/" + Session.getUsers().getUser().getFacebook().getId());
//            if (!f.exists()) {
//                userPhoto = null;
//                return;
//            }
//            Bitmap tmp = BitmapFactory.decodeFile(f.getAbsolutePath());
//
//            //tmp = Bitmap.createScaledBitmap(tmp, tmp.getWidth() / 2, tmp.getHeight() / 2, true);
//            userPhoto = tmp;
//        } catch (Exception e) {
//            userPhoto = null;
//        }
//    }

    public static Bitmap renderFilter(Activity activity) {
        if (gpuImage == null) {
            gpuImage = new GPUImage(activity);
        }
        Bitmap effectsBitamap;

        switch (GlobalVariable.selectFilterIndex) {
            case 0:
                gpuImage.setFilter(GlobalVariable.noneFilter);
                //imgPerson.requestRender();
                break;
            case 1:
                gpuImage.setFilter(GlobalVariable.linearFilter);
                break;
            case 2:
                gpuImage.setFilter(GlobalVariable.vignetteFilter);
                break;
            case 3:
                gpuImage.setFilter(GlobalVariable.instantFilter);
                break;
            case 4:
                gpuImage.setFilter(GlobalVariable.satFilter);
                break;
            case 5:
                gpuImage.setFilter(GlobalVariable.transferFilter);
                break;
            case 6:
                gpuImage.setFilter(GlobalVariable.sepiaFilter);
                break;
            case 7:
                gpuImage.setFilter(GlobalVariable.chromeFilter);
                break;
            case 8:
                gpuImage.setFilter(GlobalVariable.toneCurveFilter);
                break;
            case 9:
                gpuImage.setFilter(GlobalVariable.grayFilter);
                break;
            case 10:
                gpuImage.setFilter(GlobalVariable.grayFilter);
                effectsBitamap = gpuImage.getBitmapWithFilterApplied(GlobalVariable.styleBitmap);
                gpuImage.setFilter(noirFilter);
                effectsBitamap = gpuImage.getBitmapWithFilterApplied(effectsBitamap);
                return effectsBitamap;
            case 11:
                gpuImage.setFilter(GlobalVariable.grayFilter);
                effectsBitamap = gpuImage.getBitmapWithFilterApplied(GlobalVariable.styleBitmap);
                gpuImage.setFilter(monoFilter);
                effectsBitamap = gpuImage.getBitmapWithFilterApplied(effectsBitamap);
                return effectsBitamap;
            case 12:
                gpuImage.setFilter(GlobalVariable.invertFilter);
                break;
            default:
                gpuImage.setFilter(GlobalVariable.linearFilter);
                break;
        }

        if (GlobalVariable.styleBitmap != null) {
            effectsBitamap = gpuImage.getBitmapWithFilterApplied(GlobalVariable.styleBitmap);

            return effectsBitamap;
        } else
            return null;
    }
}
