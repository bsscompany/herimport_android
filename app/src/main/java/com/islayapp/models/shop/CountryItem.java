package com.islayapp.models.shop;

/**
 * Created by maxo on 4/14/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountryItem {

    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("countryLabel")
    @Expose
    private String countryLabel;

    /**
     *
     * @return
     * The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     *
     * @param countryCode
     * The countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     *
     * @return
     * The countryLabel
     */
    public String getCountryLabel() {
        return countryLabel;
    }

    /**
     *
     * @param countryLabel
     * The countryLabel
     */
    public void setCountryLabel(String countryLabel) {
        this.countryLabel = countryLabel;
    }

}