package com.islayapp.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 4/14/16.
 */
public class PaymentMethodItem {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("cc_types")
    @Expose
    private String ccTypes;

    public PaymentMethodItem(String code, String title, String ccTypes) {
        this.code = code;
        this.title = title;
        this.ccTypes = ccTypes;
    }

    /**
     * @return The code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The ccTypes
     */
    public String getCcTypes() {
        return ccTypes;
    }

    /**
     * @param ccTypes The cc_types
     */
    public void setCcTypes(String ccTypes) {
        this.ccTypes = ccTypes;
    }
}
