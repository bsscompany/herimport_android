package com.islayapp.models.shop;

/**
 * Created by maxo on 3/7/16.
 */
public class HottestStyles {
    private String linkImageSlider;
    private boolean isSelect= false;

    public HottestStyles(String linkImageSlider, boolean isSelect) {
        this.linkImageSlider = linkImageSlider;
        this.isSelect = isSelect;
    }

    public String getLinkImageSlider() {
        return linkImageSlider;
    }

    public void setLinkImageSlider(String linkImageSlider) {
        this.linkImageSlider = linkImageSlider;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }




}
