package com.islayapp.models.shop;

import android.graphics.Bitmap;

/**
 * Created by maxo on 3/14/16.
 */
public class ProductSliderBottom {

    private String name;
    private String urlImage;

    private boolean isLoad = false;
    private Bitmap bitmapImg;
    private int product_id;

    public ProductSliderBottom(String name, String urlImage, int product_id) {
        this.name = name;
        this.urlImage = urlImage;
        this.product_id = product_id;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public Bitmap getBitmapImg() {
        return bitmapImg;
    }

    public void setBitmapImg(Bitmap bitmapImg) {
        this.bitmapImg = bitmapImg;
    }


    public boolean isLoad() {
        return isLoad;
    }

    public void setLoad(boolean load) {
        isLoad = load;
    }

}
