package com.islayapp.models.shop;

/**
 * Created by maxo on 4/19/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubmitOrder {

    @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    /**
     * @return The data
     */
    public Data getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("orderId")
        @Expose
        private String orderId;

        /**
         * @return The orderId
         */
        public String getOrderId() {
            return orderId;
        }

        /**
         * @param orderId The orderId
         */
        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

    }

}