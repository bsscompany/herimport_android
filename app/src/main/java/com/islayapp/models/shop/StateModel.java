package com.islayapp.models.shop;

/**
 * Created by maxo on 4/14/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StateModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("fieldType")
    @Expose
    private Integer fieldType;
    @SerializedName("data")
    @Expose
    private List<StateItem> stateItemList = new ArrayList<StateItem>();

    /**
     *
     * @return
     * The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The fieldType
     */
    public Integer getFieldType() {
        return fieldType;
    }

    /**
     *
     * @param fieldType
     * The fieldType
     */
    public void setFieldType(Integer fieldType) {
        this.fieldType = fieldType;
    }

    /**
     *
     * @return
     * The data
     */
    public List<StateItem> getStateItemList() {
        return stateItemList;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setStateItemList(List<StateItem> data) {
        this.stateItemList = data;
    }

}