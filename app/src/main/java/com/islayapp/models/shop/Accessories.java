package com.islayapp.models.shop;

/**
 * Created by maxo on 3/8/16.
 */
public class Accessories {

    private String linkImageSlider;
    private boolean isSelect= false;

    public Accessories(String linkImageSlider, boolean isSelect) {
        this.linkImageSlider = linkImageSlider;
        this.isSelect = isSelect;
    }


    public String getLinkImageSlider() {
        return linkImageSlider;
    }

    public void setLinkImageSlider(String linkImageSlider) {
        this.linkImageSlider = linkImageSlider;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
