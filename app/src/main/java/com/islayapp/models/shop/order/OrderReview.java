package com.islayapp.models.shop.order;

/**
 * Created by maxo on 4/18/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OrderReview {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("shippingMethod")
    @Expose
    private ShippingMethod shippingMethod;
    @SerializedName("tax")
    @Expose
    private Double tax;
    @SerializedName("subTotal")
    @Expose
    private Double subTotal;
    @SerializedName("grandTotal")
    @Expose
    private Double grandTotal;
    @SerializedName("discountInfo")
    @Expose
    private DiscountInfo discountInfo;
    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<Item>();

    /**
     *
     * @return
     * The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The shippingMethod
     */
    public ShippingMethod getShippingMethod() {
        return shippingMethod;
    }

    /**
     *
     * @param shippingMethod
     * The shippingMethod
     */
    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     *
     * @return
     * The tax
     */
    public Double getTax() {
        return tax;
    }

    /**
     *
     * @param tax
     * The tax
     */
    public void setTax(Double tax) {
        this.tax = tax;
    }

    /**
     *
     * @return
     * The subTotal
     */
    public Double getSubTotal() {
        return subTotal;
    }

    /**
     *
     * @param subTotal
     * The subTotal
     */
    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    /**
     *
     * @return
     * The grandTotal
     */
    public Double getGrandTotal() {
        return grandTotal;
    }

    /**
     *
     * @param grandTotal
     * The grandTotal
     */
    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    /**
     *
     * @return
     * The discountInfo
     */
    public DiscountInfo getDiscountInfo() {
        return discountInfo;
    }

    /**
     *
     * @param discountInfo
     * The discountInfo
     */
    public void setDiscountInfo(DiscountInfo discountInfo) {
        this.discountInfo = discountInfo;
    }

    /**
     *
     * @return
     * The items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

}