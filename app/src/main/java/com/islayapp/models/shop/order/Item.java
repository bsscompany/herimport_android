package com.islayapp.models.shop.order;

/**
 * Created by maxo on 4/18/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Item {

    @SerializedName("optionData")
    @Expose
    private List<OptionDatum> optionData = new ArrayList<OptionDatum>();
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("itemQty")
    @Expose
    private Integer itemQty;
    @SerializedName("itemPrice")
    @Expose
    private Double itemPrice;
    @SerializedName("rowPrice")
    @Expose
    private Double rowPrice;

    /**
     *
     * @return
     * The optionData
     */
    public List<OptionDatum> getOptionData() {
        return optionData;
    }

    /**
     *
     * @param optionData
     * The optionData
     */
    public void setOptionData(List<OptionDatum> optionData) {
        this.optionData = optionData;
    }

    /**
     *
     * @return
     * The itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     *
     * @param itemName
     * The itemName
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     *
     * @return
     * The itemQty
     */
    public Integer getItemQty() {
        return itemQty;
    }

    /**
     *
     * @param itemQty
     * The itemQty
     */
    public void setItemQty(Integer itemQty) {
        this.itemQty = itemQty;
    }

    /**
     *
     * @return
     * The itemPrice
     */
    public Double getItemPrice() {
        return itemPrice;
    }

    /**
     *
     * @param itemPrice
     * The itemPrice
     */
    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }

    /**
     *
     * @return
     * The rowPrice
     */
    public Double getRowPrice() {
        return rowPrice;
    }

    /**
     *
     * @param rowPrice
     * The rowPrice
     */
    public void setRowPrice(Double rowPrice) {
        this.rowPrice = rowPrice;
    }

}