package com.islayapp.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 3/10/16.
 */
public class ProductDetails {
    @SerializedName("name")
    @Expose
    private String name = "";
    @SerializedName("id")
    @Expose
    private String id = "";
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("mobileimage")
    @Expose
    private String mobileimage;
    @SerializedName("image")
    @Expose
    private List<String> image = new ArrayList<String>();
    @SerializedName("price")
    @Expose
    private String price = "";
    @SerializedName("options")
    @Expose
    private List<Option> options = new ArrayList<Option>();

    private boolean isSelect = false;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public ProductDetails() {
    }

    public ProductDetails(ProductDetails product) {
        this.name = product.name;
        this.id = product.id;
        this.description = product.description;
        if (product.image != null)
            this.image = new ArrayList<>(product.image);
        else
            this.image = new ArrayList<>();
        this.price = product.price;
        if (product.options != null)
            this.options = new ArrayList<>(product.options);
        else
            this.options = new ArrayList<>();
        this.isSelect = product.isSelect;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The image
     */
    public List<String> getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(List<String> image) {
        this.image = image;
    }

    /**
     * @return The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return The options
     */
    public List<Option> getOptions() {
        return options;
    }

    /**
     * @param options The options
     */
    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public String getMobileimage() {
        return mobileimage;
    }

    public void setMobileimage(String mobileimage) {
        this.mobileimage = mobileimage;
    }

    //======================================== child class ====================================
    public class Option {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("values")
        @Expose
        private List<Value> values = new ArrayList<Value>();
        @SerializedName("attribute_id")
        @Expose
        private String attributeId;

        /**
         * @return The id
         */
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The label
         */
        public String getLabel() {
            return label;
        }

        /**
         * @param label The label
         */
        public void setLabel(String label) {
            this.label = label;
        }

        /**
         * @return The values
         */
        public List<Value> getValues() {
            return values;
        }

        /**
         * @param values The values
         */
        public void setValues(List<Value> values) {
            this.values = values;
        }

        /**
         * @return The attributeId
         */
        public String getAttributeId() {
            return attributeId;
        }

        /**
         * @param attributeId The attribute_id
         */
        public void setAttributeId(String attributeId) {
            this.attributeId = attributeId;
        }

    }


}
