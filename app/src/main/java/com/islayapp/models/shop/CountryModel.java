package com.islayapp.models.shop;

/**
 * Created by maxo on 4/14/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CountryModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<CountryItem> countryItems = new ArrayList<CountryItem>();

    /**
     *
     * @return
     * The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The data
     */
    public List<CountryItem> getCountryItems() {
        return countryItems;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setCountryItems(List<CountryItem> data) {
        this.countryItems = data;
    }

}