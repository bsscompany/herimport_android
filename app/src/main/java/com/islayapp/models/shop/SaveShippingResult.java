
package com.islayapp.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveShippingResult {

    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

}