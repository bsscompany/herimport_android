package com.islayapp.models.shop;

/**
 * Created by maxo on 4/14/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateItem {

    @SerializedName("stateCode")
    @Expose
    private String stateCode;
    @SerializedName("stateLabel")
    @Expose
    private String stateLabel;

    /**
     *
     * @return
     * The stateCode
     */
    public String getStateCode() {
        return stateCode;
    }

    /**
     *
     * @param stateCode
     * The stateCode
     */
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    /**
     *
     * @return
     * The stateLabel
     */
    public String getStateLabel() {
        return stateLabel;
    }

    /**
     *
     * @param stateLabel
     * The stateLabel
     */
    public void setStateLabel(String stateLabel) {
        this.stateLabel = stateLabel;
    }

}