package com.islayapp.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 4/14/16.
 */
public class ShippingMethod {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<ShippingMethodItem> shippingMethodItemList = new ArrayList<ShippingMethodItem>();

    /**
     *
     * @return
     * The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The data
     */
    public List<ShippingMethodItem> getShippingMethodItemList() {
        return shippingMethodItemList;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setShippingMethodItemList(List<ShippingMethodItem> data) {
        this.shippingMethodItemList = data;
    }

}