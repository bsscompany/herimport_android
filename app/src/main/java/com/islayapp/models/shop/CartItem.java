package com.islayapp.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 4/8/16.
 */
public class CartItem {

    @SerializedName("itemId")
    @Expose
    private String itemId;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("itemSku")
    @Expose
    private String itemSku;
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("itemQty")
    @Expose
    private Integer itemQty;
    @SerializedName("itemImage")
    @Expose
    private List<String> itemImage = new ArrayList<String>();
    @SerializedName("itemPrice")
    @Expose
    private Double itemPrice;
    /**
     * @return The itemId
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * @param itemId The itemId
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * @return The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId The productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return The itemSku
     */
    public String getItemSku() {
        return itemSku;
    }

    /**
     * @param itemSku The itemSku
     */
    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    /**
     * @return The itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName The itemName
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * @return The itemQty
     */
    public Integer getItemQty() {
        return itemQty;
    }

    /**
     * @param itemQty The itemQty
     */
    public void setItemQty(Integer itemQty) {
        this.itemQty = itemQty;
    }

    /**
     * @return The itemImage
     */
    public List<String> getItemImage() {
        return itemImage;
    }

    /**
     * @param itemImage The itemImage
     */
    public void setItemImage(List<String> itemImage) {
        this.itemImage = itemImage;
    }

    /**
     * @return The itemPrice
     */
    public Double getItemPrice() {
        return itemPrice;
    }

    /**
     * @param itemPrice The itemPrice
     */
    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }
}
