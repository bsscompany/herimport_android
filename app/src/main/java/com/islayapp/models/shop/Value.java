package com.islayapp.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 4/6/16.
 */
public class Value {

    @SerializedName("product_super_attribute_id")
    @Expose
    private String productSuperAttributeId;
    @SerializedName("value_index")
    @Expose
    private String valueIndex;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value_id")
    @Expose
    private String valueId;
    @SerializedName("priceMatrix")
    @Expose
    private List<PriceMatrix> priceMatrix = new ArrayList<PriceMatrix>();

    private PriceMatrix currentPriceMatrix = new PriceMatrix();

    public PriceMatrix getCurrentPriceMatrix() {
        return currentPriceMatrix;
    }

    public void setCurrentPriceMatrix(PriceMatrix currentPriceMatrix) {
        this.currentPriceMatrix = currentPriceMatrix;
    }

    private boolean isSelect = false;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }


    public Value(String label, String valueId) {
        this.label = label;
        this.valueId = valueId;
    }

    /**
     * @return The productSuperAttributeId
     */
    public String getProductSuperAttributeId() {
        return productSuperAttributeId;
    }

    /**
     * @param productSuperAttributeId The product_super_attribute_id
     */
    public void setProductSuperAttributeId(String productSuperAttributeId) {
        this.productSuperAttributeId = productSuperAttributeId;
    }

    /**
     * @return The valueIndex
     */
    public String getValueIndex() {
        return valueIndex;
    }

    /**
     * @param valueIndex The value_index
     */
    public void setValueIndex(String valueIndex) {
        this.valueIndex = valueIndex;
    }

    /**
     * @return The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return The valueId
     */
    public String getValueId() {
        return valueId;
    }

    /**
     * @param valueId The value_id
     */
    public void setValueId(String valueId) {
        this.valueId = valueId;
    }

    /**
     * @return The priceMatrix
     */
    public List<PriceMatrix> getPriceMatrix() {
        return priceMatrix;
    }

    /**
     * @param priceMatrix The priceMatrix
     */
    public void setPriceMatrix(List<PriceMatrix> priceMatrix) {
        this.priceMatrix = priceMatrix;
    }

}
