package com.islayapp.models.shop;

/**
 * Created by maxo on 4/8/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class GetCartModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("items")
    @Expose
    private List<CartItem> items = new ArrayList<CartItem>();

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * @return The items
     */
    public List<CartItem> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<CartItem> items) {
        this.items = items;
    }

}