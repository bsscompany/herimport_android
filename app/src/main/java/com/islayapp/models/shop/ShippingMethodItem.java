package com.islayapp.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 4/14/16.
 */
public class ShippingMethodItem {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("carrier")
    @Expose
    private String carrier;
    @SerializedName("carrier_title")
    @Expose
    private String carrierTitle;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("method_title")
    @Expose
    private String methodTitle;
    @SerializedName("method_description")
    @Expose
    private Object methodDescription;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("carrier_sort_order")
    @Expose
    private String carrierSortOrder;

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The carrier
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     *
     * @param carrier
     * The carrier
     */
    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    /**
     *
     * @return
     * The carrierTitle
     */
    public String getCarrierTitle() {
        return carrierTitle;
    }

    /**
     *
     * @param carrierTitle
     * The carrier_title
     */
    public void setCarrierTitle(String carrierTitle) {
        this.carrierTitle = carrierTitle;
    }

    /**
     *
     * @return
     * The method
     */
    public String getMethod() {
        return method;
    }

    /**
     *
     * @param method
     * The method
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     *
     * @return
     * The methodTitle
     */
    public String getMethodTitle() {
        return methodTitle;
    }

    /**
     *
     * @param methodTitle
     * The method_title
     */
    public void setMethodTitle(String methodTitle) {
        this.methodTitle = methodTitle;
    }

    /**
     *
     * @return
     * The methodDescription
     */
    public Object getMethodDescription() {
        return methodDescription;
    }

    /**
     *
     * @param methodDescription
     * The method_description
     */
    public void setMethodDescription(Object methodDescription) {
        this.methodDescription = methodDescription;
    }

    /**
     *
     * @return
     * The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The carrierSortOrder
     */
    public String getCarrierSortOrder() {
        return carrierSortOrder;
    }

    /**
     *
     * @param carrierSortOrder
     * The carrier_sort_order
     */
    public void setCarrierSortOrder(String carrierSortOrder) {
        this.carrierSortOrder = carrierSortOrder;
    }

}