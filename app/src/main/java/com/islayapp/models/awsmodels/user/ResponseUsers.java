package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseUsers {
    @SerializedName("valid")
    @Expose
    private String valid;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cognitoidentityid")
    @Expose
    private String cognitoidentityid;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("newUser")
    @Expose
    private String newUser;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("apitoken")
    @Expose
    private String apitoken;
    @SerializedName("resourcelocation")
    @Expose
    private Resourcelocation resourcelocation;
    @SerializedName("availablepoints")
    @Expose
    private int availablepoints;

    public int getAvailablepoints() {
        return availablepoints;
    }

    public void setAvailablepoints(int availablepoints) {
        this.availablepoints = availablepoints;
    }

    public Resourcelocation getResourcelocation() {
        return resourcelocation;
    }

    public void setResourcelocation(Resourcelocation resourcelocation) {
        this.resourcelocation = resourcelocation;
    }

    public String getApitoken() {
        return apitoken;
    }

    public void setApitoken(String apitoken) {
        this.apitoken = apitoken;
    }

    /**
     *
     * @return
     * The valid
     */
    public String getValid() {
        return valid;
    }

    /**
     *
     * @param valid
     * The valid
     */
    public void setValid(String valid) {
        this.valid = valid;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The cognitoidentityid
     */
    public String getCognitoidentityid() {
        return cognitoidentityid;
    }

    /**
     *
     * @param cognitoidentityid
     * The cognitoidentityid
     */
    public void setCognitoidentityid(String cognitoidentityid) {
        this.cognitoidentityid = cognitoidentityid;
    }

    /**
     *
     * @return
     * The token
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     * The newUser
     */
    public String getNewUser() {
        return newUser;
    }

    /**
     *
     * @param newUser
     * The newUser
     */
    public void setNewUser(String newUser) {
        this.newUser = newUser;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

}