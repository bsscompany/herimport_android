package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Countof {

    @SerializedName("logins")
    @Expose
    private Integer logins;
    @SerializedName("following")
    @Expose
    private Integer following  = 0;
    @SerializedName("followers")
    @Expose
    private Integer followers;
    @SerializedName("posts")
    @Expose
    private Integer posts;
    @SerializedName("comments")
    @Expose
    private Integer comments;
    @SerializedName("gigs")
    @Expose
    private Integer gigs;
    @SerializedName("gigsWon")
    @Expose
    private Integer gigsWon;

    /**
     *
     * @return
     * The logins
     */
    public Integer getLogins() {
        return logins;
    }

    /**
     *
     * @param logins
     * The logins
     */
    public void setLogins(Integer logins) {
        this.logins = logins;
    }

    /**
     *
     * @return
     * The following
     */
    public Integer getFollowing() {
        return following;
    }

    /**
     *
     * @param following
     * The following
     */
    public void setFollowing(Integer following) {
        this.following = following;
    }

    /**
     *
     * @return
     * The followers
     */
    public Integer getFollowers() {
        return followers;
    }

    /**
     *
     * @param followers
     * The followers
     */
    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    /**
     *
     * @return
     * The posts
     */
    public Integer getPosts() {
        return posts;
    }

    /**
     *
     * @param posts
     * The posts
     */
    public void setPosts(Integer posts) {
        this.posts = posts;
    }

    /**
     *
     * @return
     * The comments
     */
    public Integer getComments() {
        return comments;
    }

    /**
     *
     * @param comments
     * The comments
     */
    public void setComments(Integer comments) {
        this.comments = comments;
    }

    /**
     *
     * @return
     * The gigs
     */
    public Integer getGigs() {
        return gigs;
    }

    /**
     *
     * @param gigs
     * The gigs
     */
    public void setGigs(Integer gigs) {
        this.gigs = gigs;
    }

    /**
     *
     * @return
     * The gigsWon
     */
    public Integer getGigsWon() {
        return gigsWon;
    }

    /**
     *
     * @param gigsWon
     * The gigsWon
     */
    public void setGigsWon(Integer gigsWon) {
        this.gigsWon = gigsWon;
    }

}