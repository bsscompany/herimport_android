package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/20/16.
 */
public class Games {

    @SerializedName("lastupdate")
    @Expose
    private long lastupdate;
    @SerializedName("list")
    @Expose
    private List<GamesItem> gamesItemList = new ArrayList<GamesItem>();

    /**
     * @return The lastupdate
     */
    public long getLastupdate() {
        return lastupdate;
    }

    /**
     * @param lastupdate The lastupdate
     */
    public void setLastupdate(long lastupdate) {
        this.lastupdate = lastupdate;
    }

    /**
     * @return The list
     */
    public List<GamesItem> getGamesItemList() {
        return gamesItemList;
    }

    /**
     * @param list The list
     */
    public void setGamesItemList(List<GamesItem> list) {
        this.gamesItemList = list;
    }

}