package com.islayapp.models.awsmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/8/16.
 */
public class PointsGetResponse {

    @SerializedName("countof")
    @Expose
    private Countof countof;
    @SerializedName("timestamp")
    @Expose
    private Timestamp timestamp;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();
    @SerializedName("pagenumber")
    @Expose
    private Integer pagenumber;
    @SerializedName("results")
    @Expose
    private Integer results;
    @SerializedName("moreavailable")
    @Expose
    private String moreavailable;
    @SerializedName("valid")
    @Expose
    private String valid;
    @SerializedName("message")
    @Expose
    private String message;

    /**
     * @return The countof
     */
    public Countof getCountof() {
        return countof;
    }

    /**
     * @param countof The countof
     */
    public void setCountof(Countof countof) {
        this.countof = countof;
    }

    /**
     * @return The timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The data
     */
    public List<Datum> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<Datum> data) {
        this.data = data;
    }

    /**
     * @return The pagenumber
     */
    public Integer getPagenumber() {
        return pagenumber;
    }

    /**
     * @param pagenumber The pagenumber
     */
    public void setPagenumber(Integer pagenumber) {
        this.pagenumber = pagenumber;
    }

    /**
     * @return The results
     */
    public Integer getResults() {
        return results;
    }

    /**
     * @param results The results
     */
    public void setResults(Integer results) {
        this.results = results;
    }

    /**
     * @return The moreavailable
     */
    public String getMoreavailable() {
        return moreavailable;
    }

    /**
     * @param moreavailable The moreavailable
     */
    public void setMoreavailable(String moreavailable) {
        this.moreavailable = moreavailable;
    }

    /**
     * @return The valid
     */
    public String getValid() {
        return valid;
    }

    /**
     * @param valid The valid
     */
    public void setValid(String valid) {
        this.valid = valid;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }


    public class Timestamp {

        @SerializedName("lastearned")
        @Expose
        private long lastearned;
        @SerializedName("lastused")
        @Expose
        private long lastused;

        /**
         * @return The lastearned
         */
        public long getLastearned() {
            return lastearned;
        }

        /**
         * @param lastearned The lastearned
         */
        public void setLastearned(long lastearned) {
            this.lastearned = lastearned;
        }

        /**
         * @return The lastused
         */
        public long getLastused() {
            return lastused;
        }

        /**
         * @param lastused The lastused
         */
        public void setLastused(long lastused) {
            this.lastused = lastused;
        }

    }

    public class Timestamp_ {

        @SerializedName("added")
        @Expose
        private long added;

        /**
         * @return The added
         */
        public long getAdded() {
            return added;
        }

        /**
         * @param added The added
         */
        public void setAdded(long added) {
            this.added = added;
        }

    }

    public class Datum {

        @SerializedName("eventid")
        @Expose
        private String eventid;
        @SerializedName("userid")
        @Expose
        private String userid;
        @SerializedName("awardlevel")
        @Expose
        private Integer awardlevel;
        @SerializedName("pointawardruleid")
        @Expose
        private String pointawardruleid;
        @SerializedName("awardtype")
        @Expose
        private String awardtype;
        @SerializedName("points")
        @Expose
        private int points;
        @SerializedName("timestamp")
        @Expose
        private Timestamp_ timestamp;
        @SerializedName("type")
        @Expose
        private String type;

        /**
         * @return The eventid
         */
        public String getEventid() {
            return eventid;
        }

        /**
         * @param eventid The eventid
         */
        public void setEventid(String eventid) {
            this.eventid = eventid;
        }

        /**
         * @return The userid
         */
        public String getUserid() {
            return userid;
        }

        /**
         * @param userid The userid
         */
        public void setUserid(String userid) {
            this.userid = userid;
        }

        /**
         * @return The awardlevel
         */
        public Integer getAwardlevel() {
            return awardlevel;
        }

        /**
         * @param awardlevel The awardlevel
         */
        public void setAwardlevel(Integer awardlevel) {
            this.awardlevel = awardlevel;
        }

        /**
         * @return The pointawardruleid
         */
        public String getPointawardruleid() {
            return pointawardruleid;
        }

        /**
         * @param pointawardruleid The pointawardruleid
         */
        public void setPointawardruleid(String pointawardruleid) {
            this.pointawardruleid = pointawardruleid;
        }

        /**
         * @return The awardtype
         */
        public String getAwardtype() {
            return awardtype;
        }

        /**
         * @param awardtype The awardtype
         */
        public void setAwardtype(String awardtype) {
            this.awardtype = awardtype;
        }

        /**
         * @return The points
         */
        public int getPoints() {
            return points;
        }

        /**
         * @param points The points
         */
        public void setPoints(int points) {
            this.points = points;
        }

        /**
         * @return The timestamp
         */
        public Timestamp_ getTimestamp() {
            return timestamp;
        }

        /**
         * @param timestamp The timestamp
         */
        public void setTimestamp(Timestamp_ timestamp) {
            this.timestamp = timestamp;
        }

        /**
         * @return The type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type The type
         */
        public void setType(String type) {
            this.type = type;
        }

    }


    public class Countof {

        @SerializedName("availablepoints")
        @Expose
        private int availablepoints;
        @SerializedName("usedpoints")
        @Expose
        private int usedpoints;

        /**
         * @return The availablepoints
         */
        public int getAvailablepoints() {
            return availablepoints;
        }

        /**
         * @param availablepoints The availablepoints
         */
        public void setAvailablepoints(int availablepoints) {
            this.availablepoints = availablepoints;
        }

        /**
         * @return The usedpoints
         */
        public int getUsedpoints() {
            return usedpoints;
        }

        /**
         * @param usedpoints The usedpoints
         */
        public void setUsedpoints(int usedpoints) {
            this.usedpoints = usedpoints;
        }
    }
}
