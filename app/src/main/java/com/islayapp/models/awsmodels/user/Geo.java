
package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Geo {

    @SerializedName("salon")
    @Expose
    private List<Double> salon = new ArrayList<Double>();

    /**
     *
     * @return
     * The salon
     */
    public List<Double> getSalon() {
        return salon;
    }

    /**
     *
     * @param salon
     * The salon
     */
    public void setSalon(List<Double> salon) {
        this.salon = salon;
    }

}