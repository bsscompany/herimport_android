package com.islayapp.models.awsmodels.user;

/**
 * Created by maxo on 7/6/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Storelocations {

    @SerializedName("lastupdate")
    @Expose
    private long lastupdate;

    /**
     *
     * @return
     * The lastupdate
     */
    public long getLastupdate() {
        return lastupdate;
    }

    /**
     *
     * @param lastupdate
     * The lastupdate
     */
    public void setLastupdate(long lastupdate) {
        this.lastupdate = lastupdate;
    }

}