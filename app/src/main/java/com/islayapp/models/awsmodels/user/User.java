package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.islayapp.models.awsmodels.Salon;

public class User {

//    public enum UserType {
//        CUSTOMER("C"),
//        STYLIST("S"),
//        MODERATOR("M");
//
//        private String value;
//
//        UserType(String value) {
//            this.value = value;
//        }
//
//        public String getValue() {
//            return value;
//        }
//
//        @Override
//        public String toString() {
//            return this.getValue();
//        }
//    }
//
//    public enum Gender {
//        MALE("M"),
//        FEMALE("F"),
//        UNKNOWN("U");
//
//        private String value;
//
//        Gender(String value) {
//            this.value = value;
//        }
//
//        public String getValue() {
//            return value;
//        }
//
//        @Override
//        public String toString() {
//            return this.getValue();
//        }
//
//    }


    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("id")
    @Expose
    private String id="";
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("nickname")
    @Expose
    private String nickname;
    @SerializedName("email")
    @Expose
    private String email="";
    @SerializedName("usertype")
    @Expose
    private String usertype;
    @SerializedName("isbanned")
    @Expose
    private String isbanned;
    @SerializedName("facebook")
    @Expose
    private Facebook facebook;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("geo")
    @Expose
    private Geo geo;
    @SerializedName("timestamp")
    @Expose
    private Timestamp timestamp;
    @SerializedName("countof")
    @Expose
    private Countof countof = new Countof();
    @SerializedName("salon")
    @Expose
    private Salon salon;

    @SerializedName("reg_id")
    @Expose
    private String reg_id;

    @SerializedName("type_device")
    @Expose
    private String type_device;

    @SerializedName("newUser")
    @Expose
    private String newUser;

    public String getReg_id() {
        return reg_id;
    }

    public void setReg_id(String reg_id) {
        this.reg_id = reg_id;
    }

    public String getType_device() {
        return type_device;
    }

    public void setType_device(String type_device) {
        this.type_device = type_device;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNewUser() {
        return newUser;
    }

    public void setNewUser(String newUser) {
        this.newUser = newUser;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     *
     * @param firstname
     * The firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     *
     * @return
     * The lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     *
     * @param lastname
     * The lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The usertype
     */
    public String getUsertype() {
        return usertype;
    }

    /**
     *
     * @param usertype
     * The usertype
     */
    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    /**
     *
     * @return
     * The isbanned
     */
    public String getIsbanned() {
        return isbanned;
    }

    /**
     *
     * @param isbanned
     * The isbanned
     */
    public void setIsbanned(String isbanned) {
        this.isbanned = isbanned;
    }

    /**
     *
     * @return
     * The facebook
     */
    public Facebook getFacebook() {
        return facebook;
    }

    /**
     *
     * @param facebook
     * The facebook
     */
    public void setFacebook(Facebook facebook) {
        this.facebook = facebook;
    }

    /**
     *
     * @return
     * The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     *
     * @param meta
     * The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     *
     * @return
     * The geo
     */
    public Geo getGeo() {
        return geo;
    }

    /**
     *
     * @param geo
     * The geo
     */
    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    /**
     *
     * @return
     * The timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     *
     * @param timestamp
     * The timestamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     *
     * @return
     * The countof
     */
    public Countof getCountof() {
        return countof;
    }

    /**
     *
     * @param countof
     * The countof
     */
    public void setCountof(Countof countof) {
        this.countof = countof;
    }

    /**
     *
     * @return
     * The salon
     */
    public Salon getSalon() {
        return salon;
    }

    /**
     *
     * @param salon
     * The salon
     */
    public void setSalon(Salon salon) {
        this.salon = salon;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

}