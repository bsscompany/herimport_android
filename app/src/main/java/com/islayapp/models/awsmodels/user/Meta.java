package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("pushendpoint")
    @Expose
    private String pushendpoint;
    @SerializedName("emailverified")
    @Expose
    private String emailverified;
    @SerializedName("phoneverified")
    @Expose
    private String phoneverified;
    @SerializedName("apitoken")
    @Expose
    private String apitoken;

    public String getApitoken() {
        return apitoken;
    }

    public void setApitoken(String apitoken) {
        this.apitoken = apitoken;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The pushendpoint
     */
    public String getPushendpoint() {
        return pushendpoint;
    }

    /**
     * @param pushendpoint The pushendpoint
     */
    public void setPushendpoint(String pushendpoint) {
        this.pushendpoint = pushendpoint;
    }

    /**
     * @return The emailverified
     */
    public String getEmailverified() {
        return emailverified;
    }

    /**
     * @param emailverified The emailverified
     */
    public void setEmailverified(String emailverified) {
        this.emailverified = emailverified;
    }

    /**
     * @return The phoneverified
     */
    public String getPhoneverified() {
        return phoneverified;
    }

    /**
     * @param phoneverified The phoneverified
     */
    public void setPhoneverified(String phoneverified) {
        this.phoneverified = phoneverified;
    }

}