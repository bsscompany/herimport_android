package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/17/16.
 */
public class RewardsObj implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("description")
    @Expose
    private List<String> description = new ArrayList<String>();
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("points")
    @Expose
    private Integer points;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("video")
    @Expose
    private String video;

    public RewardsObj(String id, List<String> description, String name, Integer points, String image, String video) {
        this.id = id;
        this.description = description;
        this.name = name;
        this.points = points;
        this.image = image;
        this.video = video;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The description
     */
    public List<String> getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(List<String> description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     *
     * @param points
     * The points
     */
    public void setPoints(Integer points) {
        this.points = points;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The video
     */
    public String getVideo() {
        return video;
    }

    /**
     *
     * @param video
     * The video
     */
    public void setVideo(String video) {
        this.video = video;
    }

}
