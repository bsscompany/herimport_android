package com.islayapp.models.awsmodels;

/**
 * Created by maxo on 4/24/16.
 */
public class CommonObj {
    public String table_name;

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

}
