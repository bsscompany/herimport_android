package com.islayapp.models.awsmodels;

import android.util.Log;

import com.islayapp.util.LoginResultType;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by maxo on 4/24/16.
 */
public class Users {


    public enum UserType {
        CUSTOMER("C"),
        STYLIST("S"),
        MODERATOR("M");

        private String value;

        UserType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return this.getValue();
        }
    }

    public enum Gender {
        MALE("M"),
        FEMALE("F"),
        UNKNOWN("U");

        private String value;

        Gender(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return this.getValue();
        }

    }

    private LoginResultType loginResultType = LoginResultType.PENDING;
    private String id = "";
    private String nickname = "";
    private String firstname = "";
    private String lastname = "";
    private String email = "";
    private String phone = "";
    private String usertype = "";
    private String referredby = "";
    private String isbanned = "";
    private String timestamp = "";
    private Meta meta = new Meta();
    private FaceBook facebook = new FaceBook();
    private boolean loggedIn = false;

    public Users() {

    }

    public Users(String id, String nickname, String firstname, String lastname, String email, String phone, String usertype, String referredby, String isbanned, String timestamp, Meta meta, boolean loggedIn) {
        this.id = id;
        this.nickname = nickname;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
        this.usertype = usertype;
        this.referredby = referredby;
        this.isbanned = isbanned;
        this.timestamp = timestamp;
        this.meta = meta;
        this.loggedIn = loggedIn;
    }

    public LoginResultType getLoginResultType() {
        return loginResultType;
    }

    public void setLoginResultType(LoginResultType loginResultType) {
        this.loginResultType = loginResultType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getReferredby() {
        return referredby;
    }

    public void setReferredby(String referredby) {
        this.referredby = referredby;
    }

    public String getIsbanned() {
        return isbanned;
    }

    public void setIsbanned(String isbanned) {
        this.isbanned = isbanned;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public FaceBook getFacebook() {
        return facebook;
    }

    public void setFacebook(FaceBook facebook) {
        this.facebook = facebook;
    }

    public JSONObject toJSON() {
        JSONObject rootJsonObject = new JSONObject();

        try {
            JSONObject jsonObjectTimestamp = new JSONObject();
            jsonObjectTimestamp.put("lastfollow", 0);
            rootJsonObject.put("timestamp", jsonObjectTimestamp);

            JSONObject userJsonObj = new JSONObject();
            userJsonObj.put("isbanned", "");
            userJsonObj.put("id", getId());
            userJsonObj.put("phone", "");
            userJsonObj.put("usertype", "");
            userJsonObj.put("lastname", getLastname());
            userJsonObj.put("meta", getMeta().toJSON());
            userJsonObj.put("firstname", getFirstname());
            userJsonObj.put("email", getEmail());
            userJsonObj.put("nickname", "");
            userJsonObj.put("facebook", getFacebook().toJSON());
            JSONObject jsonObjectUserTimestamp = new JSONObject();
            jsonObjectUserTimestamp.put("lastsignin", 0);
            jsonObjectUserTimestamp.put("signup", 0);
            userJsonObj.put("timestamp", jsonObjectUserTimestamp);
            rootJsonObject.put("user", userJsonObj);
            Log.d("", "jsonObject user: " + rootJsonObject.toString());
            return rootJsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return rootJsonObject;
        }
    }
}
