package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Facebook {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("picurl")
    @Expose
    private String picurl;
    @SerializedName("authtoken")
    @Expose
    private String authtoken;
    @SerializedName("friendcount")
    @Expose
    private Integer friendcount;
    @SerializedName("verified")
    @Expose
    private String verified;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The picurl
     */
    public String getPicurl() {
        return picurl;
    }

    /**
     *
     * @param picurl
     * The picurl
     */
    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    /**
     *
     * @return
     * The authtoken
     */
    public String getAuthtoken() {
        return authtoken;
    }

    /**
     *
     * @param authtoken
     * The authtoken
     */
    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    /**
     *
     * @return
     * The friendcount
     */
    public Integer getFriendcount() {
        return friendcount;
    }

    /**
     *
     * @param friendcount
     * The friendcount
     */
    public void setFriendcount(Integer friendcount) {
        this.friendcount = friendcount;
    }

    /**
     *
     * @return
     * The verified
     */
    public String getVerified() {
        return verified;
    }

    /**
     *
     * @param verified
     * The verified
     */
    public void setVerified(String verified) {
        this.verified = verified;
    }

}