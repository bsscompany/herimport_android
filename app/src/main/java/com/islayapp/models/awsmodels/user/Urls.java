package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/20/16.
 */
public class Urls {
    @SerializedName("lastupdate")
    @Expose
    private long lastupdate;
    @SerializedName("list")
    @Expose
    private UrlsObj urlsObj;

    /**
     * @return The lastupdate
     */
    public long getLastupdate() {
        return lastupdate;
    }

    /**
     * @param lastupdate The lastupdate
     */
    public void setLastupdate(long lastupdate) {
        this.lastupdate = lastupdate;
    }

    /**
     * @return The list
     */
    public UrlsObj getUrlsObj() {
        return urlsObj;
    }

    /**
     * @param list The list
     */
    public void setUrlsObj(UrlsObj list) {
        this.urlsObj = list;
    }

}