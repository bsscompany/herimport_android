package com.islayapp.models.awsmodels;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by maxo on 4/24/16.
 */
public class FaceBook {

    private String id="";
    private String picurl="";
    private String authtoken="";
    private int friendcount=0;
    private String verified="";

    public FaceBook() {
    }

    public FaceBook(String id, String picurl, String authtoken, int friendcount, String verified) {
        this.id = id;
        this.picurl = picurl;
        this.authtoken = authtoken;
        this.friendcount = friendcount;
        this.verified = verified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public int getFriendcount() {
        return friendcount;
    }

    public void setFriendcount(int friendcount) {
        this.friendcount = friendcount;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public JSONObject toJSON(){

        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("verified", "");
            jsonObject.put("picurl", getPicurl());
            jsonObject.put("id", getId());
            jsonObject.put("friendcount", getFriendcount());
            jsonObject.put("authtoken", getAuthtoken());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return jsonObject;
        }

    }
}