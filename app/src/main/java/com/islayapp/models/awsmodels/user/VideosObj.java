package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/20/16.
 */
public class VideosObj {
    @SerializedName("how_to_points")
    @Expose
    private String howToPoints;
    @SerializedName("login_screen")
    @Expose
    private String loginScreen;

    @SerializedName("whats_hot_after_signup")
    @Expose
    private String whatsHotAfterSignup;

    /**
     * @return The howToPoints
     */
    public String getHowToPoints() {
        return howToPoints;
    }

    /**
     * @param howToPoints The how_to_points
     */
    public void setHowToPoints(String howToPoints) {
        this.howToPoints = howToPoints;
    }

    /**
     * @return The loginScreen
     */
    public String getLoginScreen() {
        return loginScreen;
    }

    /**
     * @param loginScreen The login_screen
     */
    public void setLoginScreen(String loginScreen) {
        this.loginScreen = loginScreen;
    }

    /**
     * @return The whatsHotAfterSignup
     */
    public String getWhatsHotAfterSignup() {
        return whatsHotAfterSignup;
    }

    /**
     * @param whatsHotAfterSignup The whats_hot_after_signup
     */
    public void setWhatsHotAfterSignup(String whatsHotAfterSignup) {
        this.whatsHotAfterSignup = whatsHotAfterSignup;
    }
}