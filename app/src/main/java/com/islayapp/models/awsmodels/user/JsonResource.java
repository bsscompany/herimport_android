package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/20/16.
 */
public class JsonResource {

    @SerializedName("videos")
    @Expose
    private Videos videos;
    @SerializedName("images")
    @Expose
    private Images images;
    @SerializedName("urls")
    @Expose
    private Urls urls;
    @SerializedName("games")
    @Expose
    private Games games;
    @SerializedName("shopping_items_last_update")
    @Expose
    private long shoppingItemsLastUpdate;
    @SerializedName("rewards")
    @Expose
    private Rewards rewards;

    @SerializedName("storelocations")
    @Expose
    private Storelocations storelocations;

    /**
     * @return The videos
     */
    public Videos getVideos() {
        return videos;
    }

    /**
     * @param videos The videos
     */
    public void setVideos(Videos videos) {
        this.videos = videos;
    }

    /**
     * @return The images
     */
    public Images getImages() {
        return images;
    }

    /**
     * @param images The images
     */
    public void setImages(Images images) {
        this.images = images;
    }

    /**
     * @return The urls
     */
    public Urls getUrls() {
        return urls;
    }

    /**
     * @param urls The urls
     */
    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    /**
     * @return The games
     */
    public Games getGames() {
        return games;
    }

    /**
     * @param games The games
     */
    public void setGames(Games games) {
        this.games = games;
    }

    /**
     * @return The shoppingItemsLastUpdate
     */
    public long getShoppingItemsLastUpdate() {
        return shoppingItemsLastUpdate;
    }

    /**
     * @param shoppingItemsLastUpdate The shopping_items_last_update
     */
    public void setShoppingItemsLastUpdate(long shoppingItemsLastUpdate) {
        this.shoppingItemsLastUpdate = shoppingItemsLastUpdate;
    }

    /**
     * @return The rewards
     */
    public Rewards getRewards() {
        return rewards;
    }

    /**
     * @param rewards The rewards
     */
    public void setRewards(Rewards rewards) {
        this.rewards = rewards;
    }
    /**
     *
     * @return
     * The storelocations
     */
    public Storelocations getStorelocations() {
        return storelocations;
    }

    /**
     *
     * @param storelocations
     * The storelocations
     */
    public void setStorelocations(Storelocations storelocations) {
        this.storelocations = storelocations;
    }
}