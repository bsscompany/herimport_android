package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/20/16.
 */
public class UrlsObj {

    @SerializedName("whats_hot")
    @Expose
    private String whatsHot;
    @SerializedName("whats_hot_after_signup")
    @Expose
    private String whatsHotAfterSignup;
    @SerializedName("share_link")
    @Expose
    private String shareLink;

    /**
     *
     * @return
     * The whatsHot
     */
    public String getWhatsHot() {
        return whatsHot;
    }

    /**
     *
     * @param whatsHot
     * The whats_hot
     */
    public void setWhatsHot(String whatsHot) {
        this.whatsHot = whatsHot;
    }

    /**
     *
     * @return
     * The whatsHotAfterSignup
     */
    public String getWhatsHotAfterSignup() {
        return whatsHotAfterSignup;
    }

    /**
     *
     * @param whatsHotAfterSignup
     * The whats_hot_after_signup
     */
    public void setWhatsHotAfterSignup(String whatsHotAfterSignup) {
        this.whatsHotAfterSignup = whatsHotAfterSignup;
    }

    /**
     *
     * @return
     * The shareLink
     */
    public String getShareLink() {
        return shareLink;
    }

    /**
     *
     * @param shareLink
     * The share_link
     */
    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

}