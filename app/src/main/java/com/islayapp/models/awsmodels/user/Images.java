package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/20/16.
 */
public class Images {
    @SerializedName("lastupdate")
    @Expose
    private long lastupdate;
    @SerializedName("list")
    @Expose
    private ImagesObj imageobj;

    /**
     * @return The lastupdate
     */
    public long getLastupdate() {
        return lastupdate;
    }

    /**
     * @param lastupdate The lastupdate
     */
    public void setLastupdate(long lastupdate) {
        this.lastupdate = lastupdate;
    }

    /**
     * @return The list
     */
    public ImagesObj getImageobj() {
        return imageobj;
    }

    /**
     * @param list The list
     */
    public void setImageobj(ImagesObj list) {
        this.imageobj = list;
    }

    public class ImagesObj {

        @SerializedName("login_screen")
        @Expose
        private String loginScreen;

        /**
         * @return The loginScreen
         */
        public String getLoginScreen() {
            return loginScreen;
        }

        /**
         * @param loginScreen The login_screen
         */
        public void setLoginScreen(String loginScreen) {
            this.loginScreen = loginScreen;
        }

    }
}