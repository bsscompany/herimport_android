package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Timestamp {

    @SerializedName("lastsignin")
    @Expose
    private double lastsignin;
    @SerializedName("signup")
    @Expose
    private double signup;


    public Timestamp(double lastsignin, double signup) {
        this.lastsignin = lastsignin;
        this.signup = signup;
    }


    /**
     *
     * @return
     * The lastsignin
     */
    public double getLastsignin() {
        return lastsignin;
    }

    /**
     *
     * @param lastsignin
     * The lastsignin
     */
    public void setLastsignin(double lastsignin) {
        this.lastsignin = lastsignin;
    }

    /**
     *
     * @return
     * The signup
     */
    public double getSignup() {
        return signup;
    }

    /**
     *
     * @param signup
     * The signup
     */
    public void setSignup(double signup) {
        this.signup = signup;
    }

}