package com.islayapp.models.awsmodels.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/20/16.
 */
public class Resourcelocation {

    @SerializedName("lastupdate")
    @Expose
    private long lastupdate;
    @SerializedName("location")
    @Expose
    private String location;

    /**
     *
     * @return
     * The lastupdate
     */
    public long getLastupdate() {
        return lastupdate;
    }

    /**
     *
     * @param lastupdate
     * The lastupdate
     */
    public void setLastupdate(long lastupdate) {
        this.lastupdate = lastupdate;
    }

    /**
     *
     * @return
     * The location
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

}