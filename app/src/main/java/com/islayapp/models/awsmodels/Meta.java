package com.islayapp.models.awsmodels;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by maxo on 4/24/16.
 */
public class Meta extends CommonObj {

    private String cognitoidentityid="";
    private String pushendpoint="";
    private String emailverified="";
    private String phoneverified="";
    private String gender="";

    public Meta() {
        table_name = "Meta";
    }

    public Meta(String cognitoidentityid, String pushendpoint, String emailverified, String phoneverified, String gender) {
        table_name = "Meta";
        this.cognitoidentityid = cognitoidentityid;
        this.pushendpoint = pushendpoint;
        this.emailverified = emailverified;
        this.phoneverified = phoneverified;
        this.gender = gender;
    }

    public String getCognitoidentityid() {
        return cognitoidentityid;
    }

    public void setCognitoidentityid(String cognitoidentityid) {
        this.cognitoidentityid = cognitoidentityid;
    }

    public String getPushendpoint() {
        return pushendpoint;
    }

    public void setPushendpoint(String pushendpoint) {
        this.pushendpoint = pushendpoint;
    }

    public String getEmailverified() {
        return emailverified;
    }

    public void setEmailverified(String emailverified) {
        this.emailverified = emailverified;
    }

    public String getPhoneverified() {
        return phoneverified;
    }

    public void setPhoneverified(String phoneverified) {
        this.phoneverified = phoneverified;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    public JSONObject toJSON(){

        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("pushendpoint", "");
            jsonObject.put("emailverified", getEmailverified());
            jsonObject.put("gender", getGender());
            jsonObject.put("cognitoidentityid", getCognitoidentityid());
            jsonObject.put("phoneverified", getPhoneverified());
            return jsonObject;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return jsonObject;
        }
    }
}