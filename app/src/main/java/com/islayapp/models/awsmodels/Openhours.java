package com.islayapp.models.awsmodels;

/**
 * Created by maxo on 7/6/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Openhours {

    @SerializedName("weekday1")
    @Expose
    private String weekday1;
    @SerializedName("weekday2")
    @Expose
    private String weekday2;
    @SerializedName("weekday3")
    @Expose
    private String weekday3;
    @SerializedName("weekday4")
    @Expose
    private String weekday4;
    @SerializedName("weekday5")
    @Expose
    private String weekday5;
    @SerializedName("weekday6")
    @Expose
    private String weekday6;
    @SerializedName("weekday7")
    @Expose
    private String weekday7;


    /**
     *
     * @return
     * The weekday2
     */
    public String getWeekday1() {
        return weekday1;
    }

    /**
     *
     * @param weekday1
     * The weekday2
     */
    public void setWeekday1(String weekday1) {
        this.weekday1 = weekday1;
    }


    /**
     *
     * @return
     * The weekday2
     */
    public String getWeekday2() {
        return weekday2;
    }

    /**
     *
     * @param weekday2
     * The weekday2
     */
    public void setWeekday2(String weekday2) {
        this.weekday2 = weekday2;
    }

    /**
     *
     * @return
     * The weekday3
     */
    public String getWeekday3() {
        return weekday3;
    }

    /**
     *
     * @param weekday3
     * The weekday3
     */
    public void setWeekday3(String weekday3) {
        this.weekday3 = weekday3;
    }

    /**
     *
     * @return
     * The weekday4
     */
    public String getWeekday4() {
        return weekday4;
    }

    /**
     *
     * @param weekday4
     * The weekday4
     */
    public void setWeekday4(String weekday4) {
        this.weekday4 = weekday4;
    }

    /**
     *
     * @return
     * The weekday5
     */
    public String getWeekday5() {
        return weekday5;
    }

    /**
     *
     * @param weekday5
     * The weekday5
     */
    public void setWeekday5(String weekday5) {
        this.weekday5 = weekday5;
    }

    /**
     *
     * @return
     * The weekday6
     */
    public String getWeekday6() {
        return weekday6;
    }

    /**
     *
     * @param weekday6
     * The weekday6
     */
    public void setWeekday6(String weekday6) {
        this.weekday6 = weekday6;
    }

    /**
     *
     * @return
     * The weekday7
     */
    public String getWeekday7() {
        return weekday7;
    }

    /**
     *
     * @param weekday7
     * The weekday7
     */
    public void setWeekday7(String weekday7) {
        this.weekday7 = weekday7;
    }

}