package com.islayapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.cropper.CropImageView;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.util.Utility;

public class CropImageActivity extends AppCompatActivity {


    // Static final constants
    private static final int DEFAULT_ASPECT_RATIO_VALUES = 10;
    private static final int ROTATE_NINETY_DEGREES = 90;
    private static final String ASPECT_RATIO_X = "ASPECT_RATIO_X";
    private static final String ASPECT_RATIO_Y = "ASPECT_RATIO_Y";
    private static final int ON_TOUCH = 1;

    // Instance variables
    private int mAspectRatioX = DEFAULT_ASPECT_RATIO_VALUES;
    private int mAspectRatioY = DEFAULT_ASPECT_RATIO_VALUES;

    String path_img = "";

    // Saves the state upon rotating the screen/restarting the activity
    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt(ASPECT_RATIO_X, mAspectRatioX);
        bundle.putInt(ASPECT_RATIO_Y, mAspectRatioY);
    }

    // Restores the state upon rotating the screen/restarting the activity
    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        mAspectRatioX = bundle.getInt(ASPECT_RATIO_X);
        mAspectRatioY = bundle.getInt(ASPECT_RATIO_Y);
    }

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_crop_image);

        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            path_img = intent.getStringExtra("path_img");
        }

        // Sets fonts for all
//        ViewGroup root = (ViewGroup) findViewById(R.id.mylayout);

        // Initialize components of the app
        final CropImageView cropImageView = (CropImageView) findViewById(R.id.CropImageView);

        // Set initial spinner value
        //Set AspectRatio fixed for circular selection
        cropImageView.setFixedAspectRatio(true);
        cropImageView.setGuidelines(2);

        // Sets initial aspect ratio to 10/10
        cropImageView.setAspectRatio(DEFAULT_ASPECT_RATIO_VALUES, DEFAULT_ASPECT_RATIO_VALUES);


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(path_img, options);
        cropImageView.setImageBitmap(bitmap);

        ImageButton imgbtn = (ImageButton) findViewById(R.id.backOnQuoteFragment);
        imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextViewPlus tv_dome = (TextViewPlus) findViewById(R.id.done_crop);
        tv_dome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Bitmap croppedImage = cropImageView.getCroppedImage();
                new SaveData(CropImageActivity.this).execute(croppedImage);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private class SaveData extends AsyncTask<Bitmap, Void, String> {
        private Context context;

        public SaveData(Context mContext) {
            context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Bitmap... bm) {
            return Utility.saveProfileToInternalStorage(context, bm[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("CropImage", " file save to:  " + result);
            Intent intent = new Intent();
            intent.putExtra("path_image", result);
            CropImageActivity.this.setResult(Activity.RESULT_OK, intent);
            CropImageActivity.this.finish();



        }
    }
}