package com.islayapp;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.Record;
import com.amazonaws.mobileconnectors.cognito.SyncConflict;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.amazonaws.models.CallPhoneResponse;
import com.core.APIService;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.SendPhoneActionSmsResponse;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Session;

import net.rimoto.intlphoneinput.IntlPhoneInput;

import org.apache.commons.codec.binary.Base64;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.islayapp.util.Constants.PHONE;
import static com.islayapp.util.Constants.PHONE_VERIFIRED;
import static com.islayapp.util.Constants.USER_TABLE;

public class VerifyPhoneActivity extends AppCompatActivity {
    private static final String TAG = VerifyPhoneActivity.class.getName();

    private IntlPhoneInput phoneInputView;
    private String inputPhoneVerifyNumber = "";
    private String codeVerify = "";
    private Dataset datasetUser;
    boolean mergeInProgress = false;
//    private TextViewPlus btn_skip_verify_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        datasetUser = CognitoSyncClientManager.getInstance().openOrCreateDataset(USER_TABLE);

        initLayout();
    }

    private void initLayout() {

        phoneInputView = (IntlPhoneInput) findViewById(R.id.my_phone_input);
        phoneInputView.setDefault();

        TextViewPlus btn_alert_dlg_verify_phone = (TextViewPlus) findViewById(R.id.btn_alert_dlg_verify_phone);
        btn_alert_dlg_verify_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (phoneInputView.isValid()) {
                    inputPhoneVerifyNumber = phoneInputView.getNumber();
                    Log.d(TAG, "phone number: " + inputPhoneVerifyNumber);
                    requestSendPhoneNumber(inputPhoneVerifyNumber);
                } else {
                    Toast.makeText(VerifyPhoneActivity.this, "Please input exactly phone number!", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        btn_skip_verify_phone = (TextViewPlus) findViewById(R.id.btn_skip_verify_phone);
//        btn_skip_verify_phone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                PreferenceHelpers.disableFirstTimeRunning(getBaseContext());
//
//                Intent intent = new Intent(VerifyPhoneActivity.this, NewHomeActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        if (countDownTimer != null)
            countDownTimer.cancel();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null)
            countDownTimer.cancel();

    }

    @Override
    public void onStop() {
        super.onStop();
        if (countDownTimer != null)
            countDownTimer.cancel();

    }

    /**
     * @param phone
     */
    private void requestSendPhoneNumber(String phone) {
        final Dialog progress = DialogUtil.showDialog(VerifyPhoneActivity.this);

        APIService service = NetworkConnectionUtil.createApiService(VerifyPhoneActivity.this);

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<SendPhoneActionSmsResponse> response = service.requestSendPhoneVerify(String.format(basic, string), phone);

        response.enqueue(new Callback<SendPhoneActionSmsResponse>() {
            @Override
            public void onResponse(Call<SendPhoneActionSmsResponse> call, Response<SendPhoneActionSmsResponse> response) {
                if (progress != null)
                    progress.dismiss();

                try {
                    SendPhoneActionSmsResponse sendPhoneActionSmsResponse = response.body();
                    if (sendPhoneActionSmsResponse.getSuccess()) {
                        codeVerify = sendPhoneActionSmsResponse.getCode();
                        Log.d(TAG, "Send phone success");
                        showDialogVerifyPhone2();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SendPhoneActionSmsResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                if (progress != null)
                    progress.dismiss();

                t.printStackTrace();
            }
        });
    }

    private void showDialogVerifyPhone2() {
        final Dialog dialog = new Dialog(VerifyPhoneActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.fragment_general_profile_content_input_verify_code);

        TextViewPlus btn_verify_phone_code = (TextViewPlus) dialog.findViewById(R.id.btn_verify_phone_code);
        btn_verify_phone_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edticode = (EditText) dialog.findViewById(R.id.phone_verify_code);
                if (edticode.getText().toString().length() > 0) {
                    if (!codeVerify.equals("") && codeVerify.equals(edticode.getText().toString())) {
                        dialog.dismiss();
                        requestChangeStatusPhone();

                        Intent intent = new Intent(VerifyPhoneActivity.this, NewHomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Toast.makeText(VerifyPhoneActivity.this, "Please input verification code", Toast.LENGTH_SHORT).show();
                }
            }
        });

        TextViewPlus resend_sms = (TextViewPlus) dialog.findViewById(R.id.resend_sms);
        resend_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phoneInputView.isValid()) {
                    inputPhoneVerifyNumber = phoneInputView.getNumber();
                    Log.d(TAG, "phone number: " + inputPhoneVerifyNumber);
                    requestSendPhoneNumber(inputPhoneVerifyNumber);
                } else {
                    Toast.makeText(VerifyPhoneActivity.this, "Please input exactly phone number!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageView button_close = (ImageView) dialog.findViewById(R.id.button_close);
        button_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (countDownTimer != null)
                    countDownTimer.cancel();
            }
        });


        TextViewPlus timer_alert = (TextViewPlus) dialog.findViewById(R.id.timer_alert);
        startCountDown(2, 1, timer_alert);

        dialog.show();
    }


    private void requestChangeStatusPhone() {

        APIService service = NetworkConnectionUtil.createApiService(VerifyPhoneActivity.this);

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<SendPhoneActionSmsResponse> response = service.requestSignupcomplete(String.format(basic, string));

        response.enqueue(new Callback<SendPhoneActionSmsResponse>() {
            @Override
            public void onResponse(Call<SendPhoneActionSmsResponse> call, Response<SendPhoneActionSmsResponse> response) {

                try {
                    SendPhoneActionSmsResponse sendPhoneActionSmsResponse = response.body();
                    if (sendPhoneActionSmsResponse.getSuccess()) {
                        Session.getUsers().getUser().getMeta().setPhoneverified("Y");
                        Session.getUsers().getUser().setPhone(inputPhoneVerifyNumber);

                        datasetUser.put(PHONE, inputPhoneVerifyNumber);
                        datasetUser.put(PHONE_VERIFIRED, "Y");

                        synchronize();
                        PreferenceHelpers.disableFirstTimeRunning(getBaseContext());


                        Intent intent = new Intent(VerifyPhoneActivity.this, NewHomeActivity.class);
                        startActivity(intent);
                        finish();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SendPhoneActionSmsResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                t.printStackTrace();
            }
        });
    }


    /**
     * @param minutes
     * @param second
     */
    int maxtime;
    private CountDownTimer countDownTimer;

    public void startCountDown(int minutes, int second, final TextViewPlus textViewPlus) {
        textViewPlus.setEnabled(false);

        maxtime = second * 1000 + minutes * 60 * 1000;
        if (countDownTimer != null)
            countDownTimer.cancel();
        Log.e("countDownTimer", "countDownTimer");
        countDownTimer = null;
        countDownTimer = new CountDownTimer(maxtime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                maxtime -= 1000;
                if (textViewPlus != null) {
                    int min = (int) (maxtime) / 60000;
                    int sec = (int) (maxtime - min * 60000) / 1000;

                    String smin = String.format("%02d", min);
                    String ssec = String.format("%02d", sec);

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(smin).append(":").append(ssec);

                    textViewPlus.setText(getString(R.string.will_call_you_in, stringBuilder));
                }
            }

            @Override
            public void onFinish() {
                if (textViewPlus != null) {
                    textViewPlus.setEnabled(true);
                    textViewPlus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestCallPhoneNumber(inputPhoneVerifyNumber);
                        }
                    });
                    textViewPlus.setText(getString(R.string.call_now));
                }
            }
        };
        countDownTimer.start();
    }

    /**
     * request call to number and get code verify
     *
     * @param phoneCal
     */
    private void requestCallPhoneNumber(String phoneCal) {

        final Dialog progress = DialogUtil.showDialog(VerifyPhoneActivity.this);

        APIService service = NetworkConnectionUtil.createApiService(VerifyPhoneActivity.this);

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<CallPhoneResponse> response = service.requestCallToPhone(String.format(basic, string), phoneCal);

        response.enqueue(new Callback<CallPhoneResponse>() {
            @Override
            public void onResponse(Call<CallPhoneResponse> call, Response<CallPhoneResponse> response) {
                if (progress != null)
                    progress.dismiss();

                try {
                    CallPhoneResponse sendPhoneActionSmsResponse = response.body();
                    if (sendPhoneActionSmsResponse.getSuccess()) {
                        codeVerify = sendPhoneActionSmsResponse.getCode();

                        Log.d(TAG, "Call phone success");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CallPhoneResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                if (progress != null)
                    progress.dismiss();

                t.printStackTrace();
            }
        });
    }


    private void synchronize() {
        datasetUser.synchronize(new Dataset.SyncCallback() {
            @Override
            public void onSuccess(Dataset dataset, final List<Record> newRecords) {
                Log.i("Sync", "success" + dataset.toString());
                if (mergeInProgress) return;
            }

            @Override
            public void onFailure(final DataStorageException dse) {
                Log.i("Sync", "failure: ", dse);
            }

            @Override
            public boolean onConflict(final Dataset dataset,
                                      final List<SyncConflict> conflicts) {
                Log.i("Sync", "conflict: " + conflicts);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, String.format("%s records in conflict",
                                conflicts.size()));
                        List<Record> resolvedRecords = new ArrayList<Record>();
                        for (SyncConflict conflict : conflicts) {
                            Log.i(TAG, String.format("remote: %s; local: %s",
                                    conflict.getRemoteRecord(),
                                    conflict.getLocalRecord()));
                            /* resolve by taking remote records */
                            resolvedRecords.add(conflict
                                    .resolveWithRemoteRecord());

                            /* resolve by taking local records */
                            // resolvedRecords.add(conflict.resolveWithLocalRecord());

                            /*
                             * resolve with customized logic, e.g. concatenate
                             * strings
                             */
                            // String newValue =
                            // conflict.getRemoteRecord().getValue()
                            // + conflict.getLocalRecord().getValue();
                            // resolvedRecords.add(conflict.resolveWithValue(newValue));
                        }
                        dataset.resolve(resolvedRecords);

                    }
                });
                return true;
            }

            @Override
            public boolean onDatasetDeleted(Dataset dataset, String datasetName) {
                Log.i("Sync", "delete: " + datasetName);
                return true;
            }

            @Override
            public boolean onDatasetsMerged(Dataset dataset, List<String> mergedDatasetNames) {

                mergeInProgress = true;
                Log.i("Sync", "merge: " + dataset.getDatasetMetadata().getDatasetName());

                CognitoSyncManager client = CognitoSyncClientManager.getInstance();
                for (final String name : mergedDatasetNames) {
                    Log.i("Merge", "syncing merged: " + name);
                    final Dataset d = client.openOrCreateDataset(name);
                    d.synchronize(new Dataset.SyncCallback() {
                        @Override
                        public void onSuccess(Dataset dataset, List<Record> records) {

                            //This is the actual merge code, in this sample we will just join fields in both datasets into a single one
                            Log.i("Merge", "joining records");
                            VerifyPhoneActivity.this.datasetUser.putAll(dataset.getAll());

                            //To finish and resolve the merge, we have to delete the merged dataset
                            Log.e("Merge", "deleting merged: " + name);
                            dataset.delete();
                            dataset.synchronize(new Dataset.SyncCallback() {
                                @Override
                                public void onSuccess(Dataset dataset, List<Record> records) {
                                    Log.i("Merge", "merged dataset deleted");

                                    //And finally we should sync back the new merged dataset
                                    Log.i("Merge", "now syncing the resulting new dataset");
                                    VerifyPhoneActivity.this.datasetUser.synchronize(new Dataset.SyncCallback() {
                                        @Override
                                        public void onSuccess(Dataset dataset, List<Record> newRecords) {
                                            Log.i("Merge", "merge completed");
                                            mergeInProgress = false;


                                        }

                                        @Override
                                        public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                            Log.e("Merge", "Unhandled onConflict");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetDeleted(Dataset dataset, String s) {
                                            Log.e("Merge", "Unhandled onDatasetDeleted");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                            Log.e("Merge", "Unhandled onDatasetMerged");
                                            return false;
                                        }

                                        @Override
                                        public void onFailure(DataStorageException e) {
                                            e.printStackTrace();
                                            Log.e("Merge", "Exception");
                                        }
                                    });
                                }

                                @Override
                                public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                    Log.e("Merge", "Unhandled onConflict");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetDeleted(Dataset dataset, String s) {
                                    Log.e("Merge", "Unhandled onDatasetDeleted");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                    Log.e("Merge", "Unhandled onDatasetMerged");
                                    return false;
                                }

                                @Override
                                public void onFailure(DataStorageException e) {
                                    e.printStackTrace();
                                    Log.e("Merge", "Exception");
                                }
                            });
                        }

                        @Override
                        public boolean onDatasetDeleted(Dataset dataset, String s) {

                            //This will trigger in the scenario were we had a local dataset that was not present on the identity we are merging

                            Log.i("Merge", "onDatasetDeleted");
                            final Dataset previous = dataset;

                            //Sync the local dataset
                            VerifyPhoneActivity.this.datasetUser.synchronize(new Dataset.SyncCallback() {
                                @Override
                                public void onSuccess(Dataset dataset, final List<Record> newRecords) {

                                    // Delete the local dataset from the old identity, now it's merged into the new one
                                    Log.i("Merge", "local dataset synced to the new identity");
                                    previous.delete();
                                    previous.synchronize(new Dataset.SyncCallback() {
                                        @Override
                                        public void onSuccess(Dataset dataset, List<Record> deletedRecords) {
                                            mergeInProgress = false;


                                        }

                                        @Override
                                        public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                            Log.e("Merge", "Unhandled onConflict");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetDeleted(Dataset dataset, String s) {
                                            Log.e("Merge", "Unhandled onDatasetDeleted");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                            Log.e("Merge", "Unhandled onDatasetMerged");
                                            return false;
                                        }

                                        @Override
                                        public void onFailure(DataStorageException e) {
                                            e.printStackTrace();
                                            Log.e("Merge", "Exception");
                                        }
                                    });
                                }

                                @Override
                                public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                    Log.e("Merge", "Unhandled onConflict");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetDeleted(Dataset dataset, String s) {
                                    Log.e("Merge", "Unhandled onDatasetDeleted");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                    Log.e("Merge", "Unhandled onDatasetMerged");
                                    return false;
                                }

                                @Override
                                public void onFailure(DataStorageException e) {
                                    e.printStackTrace();
                                    Log.e("Merge", "Exception");
                                }
                            });
                            return false;
                        }

                        @Override
                        public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                            Log.e("Merge", "Unhandled onConflict");
                            return false;
                        }

                        @Override
                        public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                            Log.e("Merge", "Unhandled onDatasetMerged");
                            return false;
                        }

                        @Override
                        public void onFailure(DataStorageException e) {
                            e.printStackTrace();
                            Log.e("Merge", "Exception");
                        }
                    });
                }
                return true;
            }
        });
    }

}
