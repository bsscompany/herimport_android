package com.islayapp;

import change.look.face.DragController;
import change.look.face.DragLayer;
import change.look.face.MyAbsoluteLayout;

import com.islayapp.models.shop.GlobalVariable;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ImageView.ScaleType;

@SuppressLint("NewApi")
public class UserImageActivity extends Activity implements View.OnTouchListener{

	ImageView imgPerson;
	private Matrix matrix = new Matrix();
	private DragLayer mDragLayer;
	private DragController mDragController;
	
	Matrix savedMatrix = new Matrix();
	private PointF start = new PointF();
	private PointF mid = new PointF();
	// we can be in one of these 3 states
	private static final int NONE = 0;
	private static final int DRAG = 1;
	private static final int ZOOM = 2;
	private int mode = NONE;
	
	private float[] lastEvent = null;
	private float oldDist = 1f;
	private float d = 0f;
	private float newRot = 0f;
	
	private RelativeLayout dragLayout;
	
	private ImageView imgFrame;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_userimage);
		
		imgPerson = (ImageView) findViewById(R.id.imgPerson);
		imgPerson.setImageBitmap(GlobalVariable.tempUserPhoto);
		imgFrame = (ImageView) findViewById(R.id.frame);
		
		mDragController = new DragController(this);
	    DragController localDragController = mDragController;
	    
		mDragLayer = (DragLayer) findViewById(R.id.drag_layer);
		mDragLayer.setDragController(localDragController);
		localDragController.addDropTarget(mDragLayer);
		
		imgPerson.setOnTouchListener(this);
		
		dragLayout = (RelativeLayout) findViewById(R.id.draglayout);
		
		Button btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				imgFrame.setVisibility(View.GONE);
				
				dragLayout.setDrawingCacheEnabled(true);
				Bitmap adjustedBitmap = dragLayout.getDrawingCache();
				
				adjustedBitmap = Bitmap.createScaledBitmap(adjustedBitmap, adjustedBitmap.getWidth(), adjustedBitmap.getHeight(), true);
				
				//adjustedBitmap.setConfig(Bitmap.Config.ARGB_8888);
		        adjustedBitmap.setHasAlpha(true); 
		       
		        Bitmap maskBmp = BitmapFactory.decodeResource(getResources(), R.raw.cameraimagemask1);
		        maskBmp = Bitmap.createScaledBitmap(maskBmp, adjustedBitmap.getWidth(), adjustedBitmap.getHeight(), true);
		        
		        Canvas canvas = new Canvas(adjustedBitmap);
		        Paint paint = new Paint();
		        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
		        canvas.drawBitmap(maskBmp, 0, 0, paint);
		        // We do not need the mask bitmap anymore.
		        //maskBmp.recycle();
		        
		        GlobalVariable.userPhoto = adjustedBitmap;
		        
		        GlobalVariable.writeUserPhoto();
		        
				finish();
				
				imgFrame.setVisibility(View.VISIBLE);
			}
		});
		
		Button btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
			  v.bringToFront();
			  ImageView localImageView = (ImageView) v;
			  MyAbsoluteLayout.LayoutParams localLayoutParams1 =  (MyAbsoluteLayout.LayoutParams)v.getLayoutParams();
			  //localImageView.setBackgroundColor(Color.TRANSPARENT);
			  localImageView.setLayoutParams(localLayoutParams1);
			  localImageView.setScaleType(ScaleType.MATRIX);
			  localLayoutParams1.height= -1;
			  localLayoutParams1.width = -1;
			  localImageView.setImageMatrix(matrix);
			  mDragLayer.updateViewLayout(localImageView, localLayoutParams1);
			
						switch (event.getAction() & MotionEvent.ACTION_MASK) {
						case MotionEvent.ACTION_DOWN:
							savedMatrix.set(matrix);
							start.set(event.getX(), event.getY());
							mode = DRAG;
							lastEvent = null;
							break;
						case MotionEvent.ACTION_POINTER_DOWN:
							oldDist = spacing(event);
							if (oldDist > 10f) {
								savedMatrix.set(matrix);
								midPoint(mid, event);
								mode = ZOOM;
							}
							lastEvent = new float[4];
							lastEvent[0] = event.getX(0);
							lastEvent[1] = event.getX(1);
							lastEvent[2] = event.getY(0);
							lastEvent[3] = event.getY(1);
							d = rotation(event);
							break;
						case MotionEvent.ACTION_UP:
						case MotionEvent.ACTION_POINTER_UP:
							mode = NONE;
							lastEvent = null;
							break;
						case MotionEvent.ACTION_MOVE:
							if (mode == DRAG) {
								matrix.set(savedMatrix);
								float dx = event.getX() - start.x;
								float dy = event.getY() - start.y;
								matrix.postTranslate(dx, dy);
							} else if (mode == ZOOM) {
								float newDist = spacing(event);
								if (newDist > 10f) {
									matrix.set(savedMatrix);
									float scale = (newDist / oldDist);
									matrix.postScale(scale, scale, mid.x, mid.y);

								}
								if (lastEvent != null && event.getPointerCount() == 2) {
									newRot = rotation(event);
									// matrix.postRotate(70);

									float r = newRot - d;
									float[] values = new float[9];
									matrix.getValues(values);
									matrix.postRotate(r, v.getMeasuredWidth() / 2,
											v.getMeasuredHeight() / 2);
									}
							}
				}
					return true;
	}
	
	/**
	 * Determine the space between the first two fingers
	 */
	private float spacing(MotionEvent event) {
	    float x = event.getX(0) - event.getX(1);
	    float y = event.getY(0) - event.getY(1);
	    return (float) Math.sqrt(x * x + y * y);
	}

	private void midPoint(PointF point, MotionEvent event) {
	    float x = event.getX(0) + event.getX(1);
	    float y = event.getY(0) + event.getY(1);
	    point.set(x / 2, y / 2);
	}

	private float rotation(MotionEvent event) 
	{
		//Toast.makeText(getApplicationContext(), "get x :-"+event.getX(0)+""+event.getX(1), Toast.LENGTH_LONG).show();
	    double delta_x = (event.getX(0) - event.getX(1));
	    double delta_y = (event.getY(0) - event.getY(1));
	    double radians = Math.atan2(delta_y, delta_x);
	    //Toast.makeText(getApplicationContext(),Math.toDegrees(radians)+"", Toast.LENGTH_LONG).show();
	    return (float) Math.toDegrees(radians);
	}
}
