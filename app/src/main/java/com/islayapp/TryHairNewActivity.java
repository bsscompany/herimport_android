package com.islayapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.islayapp.models.shop.GlobalVariable;
import com.islayapp.newfragment.tryhair.FinalFragment;
import com.islayapp.newfragment.tryhair.HairFilterFragment;
import com.islayapp.newfragment.tryhair.HairStyleFragment;
import com.islayapp.newfragment.tryhair.ProfileImageFragment;
import com.islayapp.newfragment.tryhair.UserPhotoFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.MarshMallowPermission;

import java.io.FileNotFoundException;
import java.io.InputStream;

import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageColorInvertFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageGrayscaleFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageHueFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageMonochromeFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSepiaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageWhiteBalanceFilter;

//import com.islayapp.newfragment.tryhair.HairCoverFragment;

public class TryHairNewActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = TryHairNewActivity.class.getSimpleName();
    private LinearLayout layoutBack1, layoutGetImage, layoutTakePicture, layoutNext1, layoutBack2, layoutHairStyle, layoutFilter, /*layoutCover,*/
            layoutNext2, layoutBack3, layoutShare, layoutSave;

    private HorizontalScrollView bottomScrollView;

    private UserPhotoFragment userPhotoFragment;
    private HairStyleFragment hairStyleFragment;
    private HairFilterFragment hairFilterFragment;
    //    private HairCoverFragment hairCoverFragment;
    private FinalFragment finalFragment;
    private ProfileImageFragment profileImageFragment;
    private FragmentManager fm;

    private int screenWidth = 500;

    private static final int HairStylesCount = 7;
    private static final int HairColorsCount = 32;
    private static final int HairFiltersCount = 12;
    private static final int HairCoversCount = 6;
    private MarshMallowPermission marshMallowPermission;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_try_hair_new);
        bottomScrollView = (HorizontalScrollView) findViewById(R.id.bottomScrollView);
        bottomScrollView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        layoutBack1 = (LinearLayout) findViewById(R.id.layoutBack1);
        layoutGetImage = (LinearLayout) findViewById(R.id.layoutGetImage);
        layoutTakePicture = (LinearLayout) findViewById(R.id.layoutTakePicture);
        layoutNext1 = (LinearLayout) findViewById(R.id.layoutNext1);
        layoutBack2 = (LinearLayout) findViewById(R.id.layoutBack2);
        layoutHairStyle = (LinearLayout) findViewById(R.id.layoutHairStyle);
        layoutFilter = (LinearLayout) findViewById(R.id.layoutFilter);
//        layoutCover = (LinearLayout) findViewById(R.id.layoutCover);
        layoutNext2 = (LinearLayout) findViewById(R.id.layoutNext2);
        layoutBack3 = (LinearLayout) findViewById(R.id.layoutBack3);
        layoutShare = (LinearLayout) findViewById(R.id.layoutShare);
        layoutSave = (LinearLayout) findViewById(R.id.layoutSave);

        layoutBack1.setOnClickListener(this);
        layoutGetImage.setOnClickListener(this);
        layoutTakePicture.setOnClickListener(this);
        layoutNext1.setOnClickListener(this);
        layoutBack2.setOnClickListener(this);
        layoutHairStyle.setOnClickListener(this);
        layoutFilter.setOnClickListener(this);
//        layoutCover.setOnClickListener(this);

        layoutNext2.setOnClickListener(this);
        layoutBack3.setOnClickListener(this);
        layoutShare.setOnClickListener(this);
        layoutSave.setOnClickListener(this);

        fm = getSupportFragmentManager();
        // permission on android M
        marshMallowPermission = new MarshMallowPermission(this);

        // init bottom menu width
        initBottomMenuWidth();


        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_left, R.anim.exit_to_left);
        fragmentTransaction.add(R.id.contentfragment, new UserPhotoFragment());

        fragmentTransaction.commit();


        GlobalVariable.userPhoto = null;

        initHairStyles();
        initHairColors();
        initHairFilters();
        initHairCovers();
        initFilters();

//        GlobalVariable.selectCoverIndex = 0;
        GlobalVariable.selectFilterIndex = 0;
        GlobalVariable.selectHairCategoryIndex = 0;
        GlobalVariable.selectHairColorIndex = 0;
        GlobalVariable.selectHairIndex = 0;


        userPhotoFragment = new UserPhotoFragment();
        profileImageFragment = new ProfileImageFragment();
        hairStyleFragment = new HairStyleFragment();
        hairFilterFragment = new HairFilterFragment();
//        hairCoverFragment = new HairCoverFragment();
        finalFragment = new FinalFragment();


    }


    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;

        if (requestCode == Constants.REQUEST_CODE_GET_IMAGE) {
            try {
                //mFileTemp = new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
                Uri selectedImageUri = data.getData();
                InputStream imageStream = getContentResolver().openInputStream(selectedImageUri);
                //BitmapFactory.Options options=new BitmapFactory.Options();
                //options.inSampleSize = 8;
                GlobalVariable.tempUserPhoto = BitmapFactory.decodeStream(imageStream);

                Matrix matrix = new Matrix();
                if (GlobalVariable.tempUserPhoto != null && GlobalVariable.tempUserPhoto.getHeight() < GlobalVariable.tempUserPhoto.getWidth())
                    matrix.preRotate(270);
                else
                    matrix.preRotate(0);
                //Bitmap thumbnail = BitmapFactory.decodeFile(mFileTemp.getPath());
                if (GlobalVariable.tempUserPhoto.getHeight() > 1200) {
                    GlobalVariable.tempUserPhoto = Bitmap.createScaledBitmap(GlobalVariable.tempUserPhoto, GlobalVariable.tempUserPhoto.getWidth() * 1200 / GlobalVariable.tempUserPhoto.getHeight(), 1200, false);
                    GlobalVariable.tempUserPhoto = Bitmap.createBitmap(GlobalVariable.tempUserPhoto, 0, 0, GlobalVariable.tempUserPhoto.getWidth(), GlobalVariable.tempUserPhoto.getHeight(), matrix, true);
                } else {
                    GlobalVariable.tempUserPhoto = Bitmap.createBitmap(GlobalVariable.tempUserPhoto, 0, 0, GlobalVariable.tempUserPhoto.getWidth(), GlobalVariable.tempUserPhoto.getHeight(), matrix, true);
                }
                System.gc();

                startActivity(new Intent(this, UserImageActivity.class));
                return;
            } catch (FileNotFoundException localFileNotFoundException) {
                localFileNotFoundException.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        switch (v.getId()) {
            case R.id.layoutBack1:
                resetSelectClickItemBottomBar(null);
//                onBackPressed();
                finish();
                break;
            case R.id.layoutGetImage:
                resetSelectClickItemBottomBar(layoutGetImage);
                Intent localIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                localIntent.setType("image/*");
                localIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(localIntent, Constants.REQUEST_CODE_GET_IMAGE);

                break;
            case R.id.layoutTakePicture:
                if (!marshMallowPermission.checkPermissionForCamera()) {
                    marshMallowPermission.requestPermissionForCamera();
                } else {
                    if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        resetSelectClickItemBottomBar(layoutTakePicture);
                        if (profileImageFragment == null)
                            profileImageFragment = new ProfileImageFragment();

                        if (profileImageFragment.isAdded()) {
                            fragmentTransaction.show(profileImageFragment);
                        } else
                            fragmentTransaction.replace(R.id.contentfragment, profileImageFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_PROFILE_IMAGE));
                        fragmentTransaction.commitAllowingStateLoss();
                    }
                }

                break;
            case R.id.layoutNext1:
                if (GlobalVariable.userPhoto == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(TryHairNewActivity.this);
                    builder.setTitle(getResources().getString(R.string.app_name)).setIcon(android.R.drawable.ic_dialog_alert).setMessage(getResources().getString(R.string.select_photo));
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    });
                    builder.create().show();
                    return;
                }

                resetSelectClickItemBottomBar(layoutHairStyle);
                bottomScrollView.smoothScrollTo(screenWidth, 0);
                if (hairStyleFragment == null)
                    hairStyleFragment = new HairStyleFragment();

                if (hairStyleFragment.isAdded()) {
                    fragmentTransaction.show(hairStyleFragment);
                } else {
                    fragmentTransaction.replace(R.id.contentfragment, hairStyleFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_HAIR_STYLE));
                }

                fragmentTransaction.commitAllowingStateLoss();
                break;
            case R.id.layoutBack2:

                resetSelectClickItemBottomBar(null);
                bottomScrollView.smoothScrollTo(0, 0);
                if (userPhotoFragment == null)
                    userPhotoFragment = new UserPhotoFragment();

                if (fm.getBackStackEntryCount() != 0) {
                    fm.popBackStack();
                } else {
                    if (userPhotoFragment.isAdded()) {
                        fragmentTransaction.show(userPhotoFragment);
                    } else
                        fragmentTransaction.replace(R.id.contentfragment, userPhotoFragment).addToBackStack(null).commitAllowingStateLoss();
                }

                break;
            case R.id.layoutHairStyle:
                resetSelectClickItemBottomBar(layoutHairStyle);
                if (hairStyleFragment == null)
                    hairStyleFragment = new HairStyleFragment();

                if (hairStyleFragment.isAdded()) {
                    fragmentTransaction.show(hairStyleFragment);
                } else
                    fragmentTransaction.replace(R.id.contentfragment, hairStyleFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_HAIR_STYLE));
                fragmentTransaction.commitAllowingStateLoss();
                break;
            case R.id.layoutFilter:
                resetSelectClickItemBottomBar(layoutFilter);

                if (hairStyleFragment != null) {
                    hairStyleFragment.getOriginBmp();
                    hairStyleFragment.getOriginBeforeBmp();
                }
//                if (hairStyleFragment.isAdded()) {
//                    fragmentTransaction.show(hairStyleFragment);
//                } else
                    fragmentTransaction.replace(R.id.contentfragment, hairFilterFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_HAIR_FILTER));
                fragmentTransaction.commitAllowingStateLoss();
                break;
            case R.id.layoutNext2:
                resetSelectClickItemBottomBar(null);
                bottomScrollView.smoothScrollTo(screenWidth * 2, 0);
                if (finalFragment.isAdded()) {
                    fragmentTransaction.show(finalFragment);
                } else
                    fragmentTransaction.replace(R.id.contentfragment, finalFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_HAIR_FINAL));
                fragmentTransaction.commitAllowingStateLoss();
                break;
            case R.id.layoutBack3:
                resetSelectClickItemBottomBar(null);
                bottomScrollView.smoothScrollTo(screenWidth, 0);

                if (hairStyleFragment == null)
                    hairStyleFragment = new HairStyleFragment();

                if (fm.getBackStackEntryCount() != 0) {
                    fm.popBackStack();
                } else {
                    if (hairFilterFragment.isAdded()) {
                        fragmentTransaction.show(hairFilterFragment);
                    } else
                        fragmentTransaction.replace(R.id.contentfragment, hairFilterFragment);
                }
                fragmentTransaction.commitAllowingStateLoss();
                break;
            case R.id.layoutShare:
                resetSelectClickItemBottomBar(null);
                if (finalFragment != null)
                    finalFragment.ShareImage();
                break;
            case R.id.layoutSave:
                resetSelectClickItemBottomBar(null);
                if (finalFragment != null)
                    finalFragment.SaveImage();
                break;
        }
    }

    private void initBottomMenuWidth() {
        Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();
        // Note, screenHeightDp isn't reliable
        // (it seems to be too small by the height of the status bar),
        // but we assume screenWidthDp is reliable.
        // Note also, dm.widthPixels,dm.heightPixels aren't reliably pixels
        // (they get confused when in screen compatibility mode, it seems),
        // but we assume their ratio is correct.
        double width = (double) config.screenWidthDp * dm.density;

        screenWidth = (int) width;
        //=============================== first menu ================================

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) layoutBack1.getLayoutParams();

        lp = (LinearLayout.LayoutParams) layoutBack1.getLayoutParams();
        lp.width = (int) (50 * dm.density);//(int) (width / 4);
        layoutBack1.setLayoutParams(lp);

        lp = (LinearLayout.LayoutParams) layoutGetImage.getLayoutParams();
        lp.width = (int) ((width - 100 * dm.density) / 2);
        layoutGetImage.setLayoutParams(lp);

        lp = (LinearLayout.LayoutParams) layoutTakePicture.getLayoutParams();
        lp.width = (int) ((width - 100 * dm.density) / 2);
        layoutTakePicture.setLayoutParams(lp);

        lp = (LinearLayout.LayoutParams) layoutNext1.getLayoutParams();
        lp.width = (int) (50 * dm.density);//(int) (width / 4);
        layoutNext1.setLayoutParams(lp);

        //=============================== third menu ================================
        lp = (LinearLayout.LayoutParams) layoutBack2.getLayoutParams();
        lp.width = (int) (width / 4);
        layoutBack2.setLayoutParams(lp);

        lp = (LinearLayout.LayoutParams) layoutHairStyle.getLayoutParams();
        lp.width = (int) (width / 4);
        layoutHairStyle.setLayoutParams(lp);

        lp = (LinearLayout.LayoutParams) layoutFilter.getLayoutParams();
        lp.width = (int) (width / 4);
        layoutFilter.setLayoutParams(lp);

//        lp = (LinearLayout.LayoutParams) layoutCover.getLayoutParams();
//        lp.width = (int) (width / 5);
//        layoutCover.setLayoutParams(lp);

        lp = (LinearLayout.LayoutParams) layoutNext2.getLayoutParams();
        lp.width = (int) (width / 4);
        layoutNext2.setLayoutParams(lp);

        //=============================== four menu ================================
        lp = (LinearLayout.LayoutParams) layoutBack3.getLayoutParams();
        lp.width = (int) (width / 3);
        layoutBack3.setLayoutParams(lp);

        lp = (LinearLayout.LayoutParams) layoutSave.getLayoutParams();
        lp.width = (int) (width / 3);
        layoutSave.setLayoutParams(lp);

        lp = (LinearLayout.LayoutParams) layoutShare.getLayoutParams();
        lp.width = (int) (width / 3);
        layoutShare.setLayoutParams(lp);

    }

    private void resetSelectClickItemBottomBar(@Nullable View view) {

        layoutBack1.setSelected(false);
        layoutGetImage.setSelected(false);
        layoutTakePicture.setSelected(false);
        layoutNext1.setSelected(false);

        layoutBack2.setSelected(false);
        layoutHairStyle.setSelected(false);
        layoutFilter.setSelected(false);
//        layoutCover.setSelected(false);
        layoutNext2.setSelected(false);

        layoutBack3.setSelected(false);
        layoutShare.setSelected(false);
        layoutSave.setSelected(false);
        if (view != null)
            view.setSelected(true);
    }


    private void initHairStyles() {
        GlobalVariable.HAIRSTYLES = new int[HairStylesCount + 1];
        for (int i = 0; i <= HairStylesCount; i++) {
            GlobalVariable.HAIRSTYLES[i] = i;
        }
    }

    private void initHairColors() {
        GlobalVariable.HAIRCOLORS = new int[HairColorsCount + 1];
        for (int i = 0; i <= HairColorsCount; i++) {
            GlobalVariable.HAIRCOLORS[i] = i;
        }
    }

    private void initHairFilters() {
        GlobalVariable.HAIRFILTERS = new int[HairFiltersCount + 1];
        GlobalVariable.HAIRFILTERNAMES = new String[HairFiltersCount + 1];
        for (int i = 0; i <= HairFiltersCount; i++) {
            GlobalVariable.HAIRFILTERS[i] = i;
        }

        GlobalVariable.HAIRFILTERNAMES[0] = "";
        GlobalVariable.HAIRFILTERNAMES[1] = "Linear";
        GlobalVariable.HAIRFILTERNAMES[2] = "Vignette";
        GlobalVariable.HAIRFILTERNAMES[3] = "Instant";
        GlobalVariable.HAIRFILTERNAMES[4] = "Process";
        GlobalVariable.HAIRFILTERNAMES[5] = "Transfer";
        GlobalVariable.HAIRFILTERNAMES[6] = "Sepia";
        GlobalVariable.HAIRFILTERNAMES[7] = "Chrome";
        GlobalVariable.HAIRFILTERNAMES[8] = "Curve";
        GlobalVariable.HAIRFILTERNAMES[9] = "Tonal";
        GlobalVariable.HAIRFILTERNAMES[10] = "Noir";
        GlobalVariable.HAIRFILTERNAMES[11] = "Mono";
        GlobalVariable.HAIRFILTERNAMES[12] = "Invert";
    }

    private void initFilters() {
        GlobalVariable.noneFilter = new GPUImageBrightnessFilter(0.0f);
        GlobalVariable.noirFilter = new GPUImageBrightnessFilter(0.5f);
        GlobalVariable.monoFilter = new GPUImageBrightnessFilter(-0.1f);
        GlobalVariable.linearFilter = new GPUImageSaturationFilter(2.0f);

        PointF centerPoint = new PointF();
        centerPoint.x = 0.5f;
        centerPoint.y = 0.5f;
        GlobalVariable.vignetteFilter = new GPUImageVignetteFilter(centerPoint, new float[]{0.0f, 0.0f, 0.0f}, 0.2f, 0.75f);

        GlobalVariable.instantFilter = new GPUImageHueFilter(30.0f);

        GlobalVariable.satFilter = new GPUImageSaturationFilter(0.6f);

        GlobalVariable.transferFilter = new GPUImageWhiteBalanceFilter(9000.0f, 0.0f);

        GlobalVariable.sepiaFilter = new GPUImageSepiaFilter();

        GlobalVariable.chromeFilter = new GPUImageMonochromeFilter(1.0f, new float[]{0.6f, 0.45f, 0.3f, 1.0f});

        GlobalVariable.toneCurveFilter = new GPUImageToneCurveFilter();
        GlobalVariable.toneCurveFilter.setFromCurveFileInputStream(
                getResources().openRawResource(R.raw.tone_cuver_sample));

        GlobalVariable.grayFilter = new GPUImageGrayscaleFilter();

        GlobalVariable.invertFilter = new GPUImageColorInvertFilter();
    }

    private void initHairCovers() {
        GlobalVariable.HAIRCOVERS = new int[HairCoversCount + 1];
        for (int i = 0; i <= HairCoversCount; i++) {
            GlobalVariable.HAIRCOVERS[i] = i;
        }
    }

}
