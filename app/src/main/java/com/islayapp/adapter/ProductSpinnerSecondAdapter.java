package com.islayapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.islayapp.R;
import com.islayapp.models.shop.PriceMatrix;
import com.islayapp.models.shop.Value;

import java.util.List;

/**
 * Created by maxo on 4/5/16.
 */
public class ProductSpinnerSecondAdapter extends ArrayAdapter<Value> {
    private LayoutInflater inflater;
//    private Context mContext;
    private List<Value> mValueList;
//    private Value mValueFirst;
    private String firstAttributeID, secondAttributeID;

    public ProductSpinnerSecondAdapter(Context context, int textViewResourceId, List<Value> objects, Value optionFirst) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        mContext = context;
        mValueList = objects;
//        mValueFirst = optionFirst;

        if (optionFirst.getPriceMatrix().size() == 1)
            firstAttributeID = optionFirst.getPriceMatrix().get(0).getId();
        else if (optionFirst.getPriceMatrix().size() == 2) {
            firstAttributeID = optionFirst.getPriceMatrix().get(0).getId();
            secondAttributeID = optionFirst.getPriceMatrix().get(1).getId();
        }
        Log.d("tag", " firstAttributeID :  " + firstAttributeID);
        Log.d("tag", " secondAttributeID :  " + secondAttributeID);
    }


    @Override
    public Value getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        convertView = inflater.inflate(R.layout.layout_spinner_select_option_item, parent, false);
        TextView label = (TextView) convertView.findViewById(R.id.spinner_tv);

        //return super.getView(position, convertView, parent);
        Value item = mValueList.get(position);
        String name = item.getLabel();

            for (int i = 0; i < item.getPriceMatrix().size(); i++) {
                PriceMatrix priceMatrix = item.getPriceMatrix().get(i);
                if (priceMatrix.getId().equals(firstAttributeID) || priceMatrix.getId().equals(secondAttributeID)) {
                    name = name + ": ( $" + priceMatrix.getPrice() + ".00)";
                    convertView.setTag(priceMatrix.getPrice());
                }
            }
        label.setText(name);
        return convertView;
    }
}

