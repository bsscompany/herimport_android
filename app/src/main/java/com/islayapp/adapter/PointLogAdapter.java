package com.islayapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.islayapp.R;
import com.islayapp.customview.RelativeTimeTextView;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.PointsGetResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/5/16.
 */
public class PointLogAdapter extends ArrayAdapter<PointsGetResponse.Datum> {

    private List<PointsGetResponse.Datum> listPointLog = new ArrayList<>();
    private Context mContext;

    private int[] color = {R.color.point_log_1, R.color.point_log_2, R.color.point_log_3, R.color.point_log_4};
    private int[] color1 = {R.color.tv_point_log_1, R.color.tv_point_log_2, R.color.tv_point_log_3, R.color.tv_point_log_4};


    public PointLogAdapter(Context context, int resource, List<PointsGetResponse.Datum> mListPointLog) {
        super(context, resource, mListPointLog);
        listPointLog = mListPointLog;
        mContext = context;
    }

    @Override
    public int getCount() {
        return listPointLog.size();
    }

    @Override
    public PointsGetResponse.Datum getItem(int position) {
        return listPointLog.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PointsGetResponse.Datum item = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_get_payd_list_points_log, null);
            holder = new ViewHolder();
            holder.level_title = (TextViewPlus) convertView.findViewById(R.id.level_title);
            holder.source_title = (TextViewPlus) convertView.findViewById(R.id.source_title);
            holder.point_title = (TextViewPlus) convertView.findViewById(R.id.point_title);
            holder.time_ago = (RelativeTimeTextView) convertView.findViewById(R.id.time_ago);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.level_title.setText(item.getAwardlevel() + "");
        holder.source_title.setText(item.getType());
        holder.point_title.setText(item.getPoints() + "");
        holder.time_ago.setReferenceTime(item.getTimestamp().getAdded());

        int temp = position % color.length;
        convertView.setBackgroundResource(color[temp]);

        holder.level_title.setTextColor(ContextCompat.getColor(getContext(), color1[temp]));
        holder.source_title.setTextColor(ContextCompat.getColor(getContext(), color1[temp]));
        holder.point_title.setTextColor(ContextCompat.getColor(getContext(), color1[temp]));

        return convertView;
    }


    class ViewHolder {
        public TextViewPlus level_title;
        public TextViewPlus source_title;
        public TextViewPlus point_title;
        public RelativeTimeTextView time_ago;
    }

}