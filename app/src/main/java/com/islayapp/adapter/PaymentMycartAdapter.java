package com.islayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.shop.order.Item;

import java.util.List;

/**
 * Created by maxo on 4/18/16.
 */
public class PaymentMycartAdapter extends ArrayAdapter<Item> {
    private LayoutInflater mInflater;
    private int resId;
    private Context mContext;

    public PaymentMycartAdapter(Context context, int textViewResourceId, List<Item> objects) {
        super(context, textViewResourceId, objects);
        this.resId = textViewResourceId;
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        Item cell = getItem(position);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.fragment_shop_shippingmethod_and_payment_my_cart_item, parent, false);
            setViewHolder(convertView);

            vh = new ViewHolder();
            vh.textTitleProduct = (TextViewPlus) convertView.findViewById(R.id.title_product_in_cart);
            vh.textQuanlityNumber = (TextViewPlus) convertView.findViewById(R.id.quanlity_cart_number);
            vh.textPriceNumber = (TextViewPlus) convertView.findViewById(R.id.price_cart_number);
            vh.title_product_option1 = (TextViewPlus) convertView.findViewById(R.id.title_product_option1);
            vh.title_product_option2 = (TextViewPlus) convertView.findViewById(R.id.title_product_option2);
            vh.title_product_option3 = (TextViewPlus) convertView.findViewById(R.id.title_product_option3);
            vh.title_product_option4 = (TextViewPlus) convertView.findViewById(R.id.title_product_option4);
            vh.linearLayoutOption1 = (LinearLayout) convertView.findViewById(R.id.line_option1);
            vh.linearLayoutOption2 = (LinearLayout) convertView.findViewById(R.id.line_option2);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        vh = (ViewHolder) convertView.getTag();
        vh.textTitleProduct.setText(cell.getItemName());
        if (cell != null && cell.getItemQty() != null)
            vh.textQuanlityNumber.setText(cell.getItemQty() + "");
        vh.textPriceNumber.setText(getContext().getString(R.string.price_no_title, cell.getItemPrice()));

        vh.textTitleProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (cell.getOptionData().size() == 0) {
            vh.linearLayoutOption1.setVisibility(View.GONE);
            vh.linearLayoutOption2.setVisibility(View.GONE);
        }
        if (cell.getOptionData().size() == 1) {
            vh.linearLayoutOption1.setVisibility(View.VISIBLE);
            vh.title_product_option1.setText(cell.getOptionData().get(0).getLabel());
            vh.title_product_option2.setText(cell.getOptionData().get(0).getValue());
        }

        if (cell.getOptionData().size() == 2) {

            vh.linearLayoutOption1.setVisibility(View.VISIBLE);
            vh.title_product_option1.setText(cell.getOptionData().get(0).getLabel());
            vh.title_product_option2.setText(cell.getOptionData().get(0).getValue());

            vh.linearLayoutOption2.setVisibility(View.VISIBLE);
            vh.title_product_option3.setText(cell.getOptionData().get(1).getLabel());
            vh.title_product_option4.setText(cell.getOptionData().get(1).getValue());
        }

        return convertView;
    }

    private void setViewHolder(View view) {

    }

    private class ViewHolder {
        public LinearLayout linearLayoutOption1, linearLayoutOption2;
        public TextViewPlus textTitleProduct, textQuanlityNumber, textPriceNumber, title_product_option1, title_product_option2,
                title_product_option3, title_product_option4;
    }
}
