package com.islayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.shop.CountryItem;

import java.util.List;

/**
 * Created by maxo on 4/5/16.
 */
public class CountrySpinnerAdapter extends ArrayAdapter<CountryItem> {
    private LayoutInflater inflater;
    private Context mContext;
    private List<CountryItem> countryItemList;

    public CountrySpinnerAdapter(Context context, int textViewResourceId, List<CountryItem> objects) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        countryItemList = objects;
    }

    @Override
    public CountryItem getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //return super.getView(position, convertView, parent);
        CountryItem item = countryItemList.get(position);

        View row = inflater.inflate(R.layout.layout_spinner_country_state, parent, false);
        TextViewPlus label = (TextViewPlus) row.findViewById(R.id.spinner_tv);
        label.setTextSize(20);
        label.setText(item.getCountryLabel());

        return row;
    }
}

