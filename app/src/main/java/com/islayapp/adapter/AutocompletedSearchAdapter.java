package com.islayapp.adapter;

/**
 * Created by maxo on 5/4/16.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;

import java.util.List;

public class AutocompletedSearchAdapter extends ArrayAdapter<String> {

    private LayoutInflater inflater;
    private Context mContext;
    private List<String> predictionList;

    public AutocompletedSearchAdapter(Context context, int textViewResourceId, List<String> objects) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        predictionList = objects;
    }

    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //return super.getView(position, convertView, parent);
        String item = predictionList.get(position);

        View row = inflater.inflate(R.layout.layout_autocompleted_search_stylist, parent, false);
        TextViewPlus label = (TextViewPlus) row.findViewById(R.id.spinner_tv);
        label.setText(item);

        return row;
    }
}