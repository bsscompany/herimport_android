package com.islayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.islayapp.R;
import com.islayapp.models.shop.Value;

import java.util.List;

/**
 * Created by maxo on 4/5/16.
 */
public class ProductSpinnerAdapter extends ArrayAdapter<Value> {
    private LayoutInflater inflater;
    private Context mContext;
    private List<Value> mValueList;

    public ProductSpinnerAdapter(Context context, int textViewResourceId, List<Value> objects) {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        mValueList = objects;
    }

    @Override
    public Value getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        //return super.getView(position, convertView, parent);
        Value item = mValueList.get(position);
        String name = item.getLabel();

        if (item.getPriceMatrix().size() == 1)
            name = name + ": ( $" + item.getPriceMatrix().get(0).getPrice()+".00)";
        else if (item.getPriceMatrix().size() == 2)
            name = name + ": ( $" + item.getPriceMatrix().get(0).getPrice() + ".00 - $" + item.getPriceMatrix().get(1).getPrice()+".00)";

        View row = inflater.inflate(R.layout.layout_spinner_select_option_item, parent, false);
        TextView label = (TextView) row.findViewById(R.id.spinner_tv);
        label.setText(name);

        return row;
    }
}

