package com.islayapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.shop.ProductDetails;

import java.util.List;

/**
 * Created by maxo on 3/8/16.
 */
public class ShopHostestProductAdapter extends PagerAdapter {
    private int mWidthScreen, widthImg, heightImg;
    private LayoutInflater inflater;
    private List<ProductDetails> hostestProductDetailsList;
    private Activity mActivity;

    public ShopHostestProductAdapter(Activity activity, List<ProductDetails> hottestStylesList, int widthSCreen) {
        mActivity = activity;
        mWidthScreen = widthSCreen;
        hostestProductDetailsList = hottestStylesList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        widthImg = (int) (0.75 * mWidthScreen) / 2;
        heightImg = (int) mWidthScreen / 2;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.fragment_shop_new_slider, container, false);

        LinearLayout linearLayout_image_hostest = (LinearLayout) view.findViewById(R.id.linearLayout_image_hostest);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) linearLayout_image_hostest.getLayoutParams();
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = heightImg;
        linearLayout_image_hostest.setLayoutParams(layoutParams);

        ImageView imageView = (ImageView) view.findViewById(R.id.img_shop_slider);
        LinearLayout.LayoutParams layoutParamsimg = (LinearLayout.LayoutParams) imageView.getLayoutParams();
        layoutParamsimg.width = widthImg;
        layoutParamsimg.height = heightImg;
        imageView.setLayoutParams(layoutParamsimg);

        ProductDetails item = hostestProductDetailsList.get(position);
        if (item.getImage().size() > 0)
            Glide.with(mActivity)
                    .load(item.getMobileimage())
                    .into(imageView);
        TextViewPlus tv_hottestStyles = (TextViewPlus) view.findViewById(R.id.tv_hottestStyles);
        tv_hottestStyles.setText(item.getName());
        //updateLayout(view, item);
        String tag = "slider_hostet_tag" + position;
        view.setTag(tag);
        container.addView(view);
//        views.put(position, view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return hostestProductDetailsList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }


}
