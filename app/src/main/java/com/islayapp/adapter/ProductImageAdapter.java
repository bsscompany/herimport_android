package com.islayapp.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.islayapp.R;

import java.util.List;

/**
 * Created by maxo on 3/15/16.
 */
public class ProductImageAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mStringUrlList;
    private LinearLayout.LayoutParams layoutParams;
    public ProductImageAdapter(Context context, List<String> stringUrlList, int sizeWidthScreen) {
        mContext = context;
        mStringUrlList = stringUrlList;

         layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (sizeWidthScreen*0.25));


    }

    @Override
    public int getCount() {
        return mStringUrlList.size();
    }

    @Override
    public Object getItem(int pos) {
        return mStringUrlList.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.fragment_shop_product_image_select, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) rowView.findViewById(R.id.img_product_thumbnail);
            viewHolder.image.setLayoutParams(layoutParams);

            rowView.setTag(viewHolder);
        }


        ViewHolder holder = (ViewHolder) rowView.getTag();

        Glide.with(mContext)
                .load((String) getItem(position))
                .into(holder.image);
        return rowView;
    }

    static class ViewHolder {
        public ImageView image;
    }
}