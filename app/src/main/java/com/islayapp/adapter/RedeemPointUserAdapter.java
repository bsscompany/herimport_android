package com.islayapp.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.user.RewardsObj;
import com.islayapp.newfragment.getPayD.DefaultFragment;
import com.islayapp.newfragment.getPayD.Views.ImageAndPointView;
import com.islayapp.newfragment.getPayD.Views.PointLeftToRedeemView;
import com.islayapp.util.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/20/16.
 */
public class RedeemPointUserAdapter extends PagerAdapter {
    private Context context;
    private List<RewardsObj> mRewardList = new ArrayList<>();
    private DefaultFragment defaultFragment;

    public RedeemPointUserAdapter(Context context, DefaultFragment defaultFragment, List<RewardsObj> rewardList) {
        this.context = context;
        mRewardList = rewardList;
        this.defaultFragment = defaultFragment;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_get_payd_redeem_points_user, collection, false);
        collection.addView(layout);

        final RewardsObj item = mRewardList.get(position);
        ImageAndPointView max_point_top = (ImageAndPointView) layout.findViewById(R.id.max_point_top);
        max_point_top.setPoints(item.getPoints());

        PointLeftToRedeemView point = (PointLeftToRedeemView) layout.findViewById(R.id.pointleft);
        point.setOnClickClaimPizze(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                defaultFragment.addFragment(DefaultFragment.GetPayD.WHERESHIP, item);
            }
        });

        TextViewPlus item_name = (TextViewPlus) layout.findViewById(R.id.item_name);
        item_name.setText(item.getName());

        TextViewPlus description_reward = (TextViewPlus) layout.findViewById(R.id.description_reward);
        description_reward.setText(item.getDescription().get(1));

        point.setMax(item.getPoints());
        point.setProgress(Session.getCurrentPoint());

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mRewardList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}