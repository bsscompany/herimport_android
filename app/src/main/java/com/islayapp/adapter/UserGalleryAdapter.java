package com.islayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.amazonaws.models.Post;
import com.bumptech.glide.Glide;
import com.islayapp.R;
import com.islayapp.customview.Views.SquareImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/6/16.
 */
public class UserGalleryAdapter extends ArrayAdapter<Post> {

    private List<Post> mPostList = new ArrayList<>();

    public UserGalleryAdapter(Context context, int resource, List<Post> objects) {
        super(context, resource, objects);
        mPostList = objects;
    }

    @Override
    public int getCount() {
        return mPostList.size();
    }

    @Override
    public Post getItem(int position) {
        return mPostList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Post item = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_image_grid_profile, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.img_post_thumb = (SquareImageView) convertView.findViewById(R.id.img_post_thumb);

            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();


        Glide.with(getContext())
                .load(item.getThumbUrl())
                .into(viewHolder.img_post_thumb);

        return convertView;
    }

    class ViewHolder {
        public SquareImageView img_post_thumb;

    }


}
