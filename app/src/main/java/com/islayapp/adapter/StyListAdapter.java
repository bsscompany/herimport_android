package com.islayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.amazonaws.models.Stylist;
import com.bumptech.glide.Glide;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.customview.Views.EmojiconTextViewPlus;
import com.islayapp.customview.Views.SquareImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/5/16.
 */
public class StyListAdapter extends ArrayAdapter<Stylist> {

    private List<Stylist> mStylistList = new ArrayList<>();
    private Context mContext;

    public StyListAdapter(Context context, int resource, List<Stylist> stylistList) {
        super(context, resource, stylistList);
        mStylistList = stylistList;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mStylistList.size();
    }

    @Override
    public Stylist getItem(int position) {
        return mStylistList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_entry_stylist_director, null);

             viewHolder = new ViewHolder();
            viewHolder.stylist_img = (SquareImageView) convertView.findViewById(R.id.stylist_img);
            viewHolder.stylist_name = (TextViewPlus) convertView.findViewById(R.id.stylist_name);
            viewHolder.stylist_address = (TextViewPlus) convertView.findViewById(R.id.stylist_address);
            viewHolder.stylist_following = (TextViewPlus) convertView.findViewById(R.id.stylist_following);
            viewHolder.stylist_post = (TextViewPlus) convertView.findViewById(R.id.stylist_post);
            viewHolder.description = (EmojiconTextViewPlus) convertView.findViewById(R.id.description);

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Stylist item = getItem(position);
        Glide.with(mContext)
                .load(item.getPicurl())
                .into(viewHolder.stylist_img);

        viewHolder.stylist_name.setText(item.getName());
        viewHolder.stylist_address.setText(item.getAddress1() + ", " + item.getCity() + ", " + item.getState());
        viewHolder.stylist_following.setText(item.getFollowers());
        viewHolder.stylist_post.setText(item.getPosts());

        viewHolder.description.setText(item.getDescription());

        return convertView;
    }


    class ViewHolder {
        public SquareImageView stylist_img;
        public TextViewPlus stylist_name;
        public TextViewPlus stylist_address;
        public TextViewPlus stylist_following;
        public TextViewPlus stylist_post;
        public EmojiconTextViewPlus description;
    }

}