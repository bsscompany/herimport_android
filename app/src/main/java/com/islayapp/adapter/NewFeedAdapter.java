package com.islayapp.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.LambdaInterface;
import com.amazonaws.models.Comment;
import com.amazonaws.models.CommonResponse;
import com.amazonaws.models.ItemFeed;
import com.amazonaws.models.PostComment;
import com.amazonaws.models.UserLikePostRequest;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.bumptech.glide.Glide;
import com.islayapp.R;
import com.islayapp.customview.CircularImageView;
import com.islayapp.customview.RelativeTimeTextView;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.customview.Views.EmojiconTextViewPlus;
import com.islayapp.newfragment.getlayd.GetlaydDefaultFragment;
import com.islayapp.newfragment.getlayd.StylistFeedFragment;
import com.islayapp.newfragment.getlayd.Views.ViewComment;
import com.islayapp.util.ScreenUtils;
import com.islayapp.util.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/13/16.
 */
public class NewFeedAdapter extends ArrayAdapter<ItemFeed> {
    private List<ItemFeed> itemFeedList;
    private GetlaydDefaultFragment mDefaultFragment;
    private int width = 500;
    private StylistFeedFragment stylistFeedFragment;

    public NewFeedAdapter(StylistFeedFragment mFragment, Context context, GetlaydDefaultFragment defaultFragment, int resource, List<ItemFeed> objects) {
        super(context, resource, objects);
        itemFeedList = objects;
        mDefaultFragment = defaultFragment;
        width = ScreenUtils.getWidthPixel(getContext());
        stylistFeedFragment = mFragment;
    }

    @Override
    public int getCount() {
        return itemFeedList.size();
    }

    @Override
    public ItemFeed getItem(int position) {
        return itemFeedList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        ItemFeed item = getItem(position);
        Log.d("TAG", "positioin itme: " + position);
//        final Stylist stylistComment = item.getStylist();

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_get_layd_stylist_feed, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.thumb = (CircularImageView) convertView.findViewById(R.id.thumb);
            viewHolder.name_stylist = (TextViewPlus) convertView.findViewById(R.id.name_stylist);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)viewHolder.image.getLayoutParams();
            params.width = width;
            params.height = width;
            viewHolder.image.setLayoutParams(params);

            viewHolder.number_likes = (TextViewPlus) convertView.findViewById(R.id.number_likes);
            viewHolder.allcomment = (TextViewPlus) convertView.findViewById(R.id.allcomment);
            viewHolder.viewcomment = (ViewComment) convertView.findViewById(R.id.viewcomment);
            viewHolder.comment = (EmojiconTextViewPlus) convertView.findViewById(R.id.comment);
            viewHolder.like_unlike = (ImageView) convertView.findViewById(R.id.like_unlike);
            viewHolder.imgcomment = (ImageView) convertView.findViewById(R.id.imgcomment);
//            viewHolder.add_comment = (ImageView) convertView.findViewById(R.id.add_comment);
            viewHolder.time_ago = (RelativeTimeTextView) convertView.findViewById(R.id.time_ago);

            viewHolder.thumb.setLayoutParams(new RelativeLayout.LayoutParams((int) (width * 0.2), (int) (width * 0.2)));

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.viewcomment.removeAllViews();
        viewHolder.allcomment.setVisibility(View.VISIBLE);
        if (item.getComments().size() > 0) {
            if (item.getCountof().getComments() > 2) {
                viewHolder.allcomment.setVisibility(View.VISIBLE);
                viewHolder.allcomment.setText(getContext().getString(R.string.view_all_number_comment, item.getCountof().getComments()));
            } else if (item.getCountof().getComments() == 2) {
                viewHolder.allcomment.setVisibility(View.GONE);
            }
            for (int i = 0; i < item.getComments().size(); i++) {
                if (item.getComments().get(i).getStylist() != null)
                    viewHolder.viewcomment.AddComment(item.getComments().get(i).getStylist().getNickname(), item.getComments().get(i).getComment());
            }
        } else {
            viewHolder.allcomment.setText(getContext().getString(R.string.have_no_comment));
        }


        viewHolder.like_unlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animScale = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale);
                v.startAnimation(animScale);

                UserLikePostRequest likeRequest = new UserLikePostRequest();
                likeRequest.setUserid(Session.getUsers().getUser().getId());
                likeRequest.setPostid(getItem(position).getId());
                if (viewHolder.like_unlike.isSelected())
                    likeRequest.setLike(false);
                else
                    likeRequest.setLike(true);
                // request like
                requestUserLike(likeRequest, viewHolder.like_unlike, viewHolder.number_likes, position);
            }
        });

        viewHolder.allcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("tag", "position: " + position);
                clickToComment(getItem(position).getComments(), getItem(position).getId());
            }
        });

        viewHolder.imgcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("tag", "position: " + position);
                clickToComment(getItem(position).getComments(), getItem(position).getId());
            }
        });

//        viewHolder.add_comment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("tag", "position: " + position);
//                clickToComment(getItem(position).getComments(), getItem(position).getId());
//
//            }
//        });

        viewHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("tag", "position: " + position);
                clickToDetails(getItem(position));

            }
        });

        if (getItem(position).getStylist() != null) {
            // update stylist info
            viewHolder.name_stylist.setText(getItem(position).getStylist().getName());
            Glide.with(getContext())
                    .load(getItem(position).getStylist().getPicurl())
                    .placeholder(R.drawable.placeholder)
                    .into(viewHolder.thumb);

//            viewHolder.name_stylist.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mDefaultFragment.stylistDetailsFragment(getItem(position).getStylist());
//                }
//            });
//
//            viewHolder.thumb.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mDefaultFragment.stylistDetailsFragment(getItem(position).getStylist());
//                }
//            });
        }

        viewHolder.like_unlike.setSelected(item.getCommentliked());

        Glide.with(getContext())
                .load(item.getOriginalImageUrl())
                .placeholder(R.drawable.placeholder)
                .into(viewHolder.image);

        viewHolder.number_likes.setText(getContext().getString(R.string.number_like, item.getCountof().getLikes()));
        viewHolder.time_ago.setReferenceTime(item.getTimestamp().getAdded());
        viewHolder.comment.setText(item.getComment());

        return convertView;
    }

    private void clickToDetails(ItemFeed item) {
        mDefaultFragment.PostDetailsFragment(stylistFeedFragment, item);
    }

    private void clickToComment(List<Comment> commentlist, String postID) {

        List<PostComment> listPostComment = new ArrayList<>();
        for (int i = 0; i < commentlist.size(); i++) {

            PostComment item = new PostComment();
            item.setId(commentlist.get(i).getPostid());
            item.setUserid(commentlist.get(i).getUserid());
            item.setComment(commentlist.get(i).getComment());
            item.setNickname(commentlist.get(i).getNickname());
            if (commentlist.get(i).getStylist() != null)
                item.setPicurl(commentlist.get(i).getStylist().getPicurl());
            listPostComment.add(item);
        }

        mDefaultFragment.CommentsFragment(stylistFeedFragment, listPostComment, postID);
    }

    class ViewHolder {
        public CircularImageView thumb;
        public TextViewPlus name_stylist, number_likes, allcomment;
        public ViewComment viewcomment;
        public ImageView image, like_unlike, imgcomment;
        public RelativeTimeTextView time_ago;
        public EmojiconTextViewPlus comment;

    }

    private void requestUserLike(UserLikePostRequest request, final ImageView imgLike, final TextViewPlus numberLike, final int position) {

        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        final LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());
        final LambdaInterface myInterface = factory.build(LambdaInterface.class);
        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<UserLikePostRequest, Void, CommonResponse>() {


            @Override
            protected CommonResponse doInBackground(UserLikePostRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    return myInterface.UserLikesPost(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(CommonResponse result) {

                if (result == null) {
                    return;
                } else {

                    if (result.getValid().equals("Y")) {
                        if (imgLike.isSelected()) {
                            imgLike.setSelected(false);
                            if (getItem(position).getCountof().getLikes() > 0) {
                                int count = getItem(position).getCountof().getLikes() - 1;
                                getItem(position).getCountof().setLikes(count);
                                numberLike.setText(getContext().getString(R.string.number_like, count));
                            }
                        } else {
                            imgLike.setSelected(true);
                            int count = getItem(position).getCountof().getLikes() + 1;
                            getItem(position).getCountof().setLikes(count);
                            numberLike.setText(getContext().getString(R.string.number_like, count));
                        }
                    }
                }
            }
        }.execute(request);
    }
}
