package com.islayapp.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.islayapp.R;
import com.islayapp.models.awsmodels.PointsGetResponse;
import com.islayapp.models.awsmodels.user.RewardsObj;
import com.islayapp.newfragment.getPayD.DefaultFragment;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

public class RedeemPointFragmentAdapter extends PagerAdapter {
    private Context context;
    private List<PointsGetResponse.Datum> mListPointLog = new ArrayList<>();
    private List<RewardsObj> mRewardList = new ArrayList<>();
    private DefaultFragment defaultFragment;

    public RedeemPointFragmentAdapter(Context context, DefaultFragment defaultFragment, List<PointsGetResponse.Datum> listPointLog, List<RewardsObj> rewardList) {
        this.context = context;
        mListPointLog = listPointLog;
        mRewardList = rewardList;
        this.defaultFragment = defaultFragment;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = null;
        if (position == 0) {

            layout = (ViewGroup) inflater.inflate(R.layout.fragment_get_payd_redeem_points_default, collection, false);

            collection.addView(layout);

            RedeemPointUserAdapter adapter = new RedeemPointUserAdapter(context, defaultFragment, mRewardList);

            ViewPager paper = (ViewPager) layout.findViewById(R.id.pager);
            paper.setAdapter(adapter);

            CirclePageIndicator titleIndicator = (CirclePageIndicator) layout.findViewById(R.id.paper_indicator);
            titleIndicator.setViewPager(paper);

        } else {
            layout = (ViewGroup) inflater.inflate(R.layout.fragment_get_payd_redeem_points_log, collection, false);
            updatePointLog(layout, mListPointLog);
            collection.addView(layout);
        }
        return layout;
    }

    /**
     * update point log
     */
    private void updatePointLog(ViewGroup viewgroup, List<PointsGetResponse.Datum> mListPointLog) {
        ListView listView5 = (ListView) viewgroup.findViewById(R.id.listView5);
        PointLogAdapter adapter = new PointLogAdapter(context, R.layout.item_get_payd_list_points_log, mListPointLog);
        listView5.setAdapter(adapter);

    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "REDEEM POINTS";
            case 1:
                return "POINTS LOG";
        }
        return "";
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}