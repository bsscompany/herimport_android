package com.islayapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.islayapp.R;
import com.islayapp.models.shop.ProductDetails;

import java.util.List;

/**
 * Created by maxo on 4/9/16.
 */
public class ShopAccessoriesProductAdapter extends PagerAdapter {
    private int mWidthScreen, widthImg, heightImg;
    private LayoutInflater inflater;
    private List<ProductDetails> hostestProductDetailsList;
    private Activity mActivity;

    public ShopAccessoriesProductAdapter(Activity activity, List<ProductDetails> hottestStylesList, int widthSCreen) {
        mActivity = activity;
        mWidthScreen = widthSCreen;
        widthImg = (int) (0.75 * mWidthScreen) / 2;
        heightImg = (int) mWidthScreen / 2;
        hostestProductDetailsList = hottestStylesList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.fragment_shop_accessories_slider, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.img_shop_slider);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(widthImg, heightImg);
        imageView.setLayoutParams(layoutParams);

        ProductDetails item = hostestProductDetailsList.get(position);

        if (item.getImage().size() > 0)
            Glide.with(mActivity)
                    .load(item.getImage().get(0))
                    .into(imageView);

        updateLayout(view, item);
        String tag = "slider_accessories_tag" + position;
        view.setTag(tag);
        container.addView(view);
//        views.put(position, view);
        return view;
    }


    private void updateLayout(View view, ProductDetails item) {
        View ocpatibg = view.findViewById(R.id.ocpatibg);
        if (item.isSelect()) {
            ocpatibg.setVisibility(View.INVISIBLE);
        } else {
            ocpatibg.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return hostestProductDetailsList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }


}
