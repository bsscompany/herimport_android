package com.islayapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.amazonaws.AWSCreateEndpointTask;
import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.Record;
import com.amazonaws.mobileconnectors.cognito.SyncConflict;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.core.APIService;
import com.facebook.login.LoginManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.islayapp.customview.NonSwipeableViewPager;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.user.JsonResource;
import com.islayapp.models.shop.GlobalVariable;
import com.islayapp.newfragment.general.ApplyToStylistFragment;
import com.islayapp.newfragment.general.MyProfileFragment;
import com.islayapp.newfragment.general.NewPostFragment;
import com.islayapp.newfragment.general.StoreLocatorFragment;
import com.islayapp.newfragment.getPayD.DefaultFragment;
import com.islayapp.newfragment.getPayD.Views.onChangeFragment;
import com.islayapp.newfragment.getlayd.GetlaydDefaultFragment;
import com.islayapp.newfragment.whathot.WebviewFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Session;
import com.islayapp.util.Utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.islayapp.util.Constants.USER_TABLE;

//import io.card.payment.CardIOActivity;
//import io.card.payment.CreditCard;

public class NewHomeActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = NewHomeActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final int MY_SCAN_REQUEST_CODE = 1000; // arbitrary int

    private NonSwipeableViewPager viewPager;
    //    private RootTryHairFragment rootWhatsHotFragment;
//    private RootGeneralFragment rootGeneralFragment;
    private int screenWidth;
    private int lastCurrentpage;
    private DrawerLayout mDrawerLayout;
    // menu bottom bar
    private LinearLayout layoutLayD, layoutPayD, layoutWhatHot, layoutShop;
    // textviewplus menu nav
    private TextViewPlus nav_store_locator, nav_my_profile, nav_apply_to_become_a_stylist,
            nav_edit_stylist_profile, nav_post_to_stylist_page, stylist_option, nav_logout;

    private onChangeFragment onChangeFragment;
    private FrameLayout fragment_general;
    //    public static Boolean inBackground = true;
    private Dataset datasetUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_new);

        initLayout();

        FirebaseInstanceId.getInstance().getId();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        boolean isCompleted = PreferenceHelpers.getBoolean(this, Constants.REGISTRATION_ENDPOINT_COMPLETED);
        if (!isCompleted && refreshedToken != null && !refreshedToken.equals("")) {
            PreferenceHelpers.setPreference(this, Constants.REGISTRATION_ENDPOINT_COMPLETED, false);
            // Start IntentService to register this application with GCM.
            new AWSCreateEndpointTask(this).execute(
                    CognitoSyncClientManager.ARN_PUSH,
                    refreshedToken,
                    Session.getUsers().getUser().getId());
        }

        String string = PreferenceHelpers.getPreference(this, "json_resource");

        String currentVersionResource = PreferenceHelpers.getPreference(this, "resource_version");
        if (string.equals("") || !Utility.getLastUpdate().equals(currentVersionResource)) {
            if (Session.getUsers() != null && Session.getUsers().getResourcelocation() != null)
                requestGetNewResource(Session.getUsers().getResourcelocation().getLocation());
        } else {
            Gson gson = new Gson();
            JsonResource resource = gson.fromJson(string, JsonResource.class);
            Session.setJsonResource(resource);
        }

        setupViewPager(viewPager);

    }


    @Override
    protected void onStart() {
        super.onStart();

        datasetUser = CognitoSyncClientManager.getInstance().openOrCreateDataset(USER_TABLE);
        synchronize();
    }


    @Override
    protected void onStop() {
        super.onStop();
//        inBackground = true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                } else {
                    v.setEnabled(true);
                    v.setFocusable(true);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private void initLayout() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        viewPager = (NonSwipeableViewPager) findViewById(R.id.viewpager);

        initLayoutNavMenu();

        //============================ bottom bar ========================================//

        layoutPayD = (LinearLayout) findViewById(R.id.layoutPayD);
        layoutLayD = (LinearLayout) findViewById(R.id.layoutLayD);
        layoutWhatHot = (LinearLayout) findViewById(R.id.layoutWhatHot);
        layoutShop = (LinearLayout) findViewById(R.id.layoutShop);

        layoutLayD.setSelected(true);

        layoutPayD.setOnClickListener(this);
        layoutLayD.setOnClickListener(this);
        layoutWhatHot.setOnClickListener(this);
        layoutShop.setOnClickListener(this);

        fragment_general = (FrameLayout) findViewById(R.id.fragment_general);

    }

    private void initLayoutNavMenu() {
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

//        nav_home = (TextViewPlus) findViewById(R.id.nav_home);
        nav_store_locator = (TextViewPlus) findViewById(R.id.nav_store_locator);
//        nav_celebrity_stylists = (TextViewPlus) findViewById(R.id.nav_celebrity_stylists);
//        nav_game_winners = (TextViewPlus) findViewById(R.id.nav_game_winners);
        nav_my_profile = (TextViewPlus) findViewById(R.id.nav_my_profile);
        nav_apply_to_become_a_stylist = (TextViewPlus) findViewById(R.id.nav_apply_to_become_a_stylist);
        nav_edit_stylist_profile = (TextViewPlus) findViewById(R.id.nav_edit_stylist_profile);
        nav_post_to_stylist_page = (TextViewPlus) findViewById(R.id.nav_post_to_stylist_page);
        stylist_option = (TextViewPlus) findViewById(R.id.stylist_option);

        nav_logout = (TextViewPlus) findViewById(R.id.nav_logout);

//        if (Session.getUsers()!=null && Session.getUsers().getUser()!=null&& Session.getUsers().getUser().getUsertype()!=null && Session.getUsers().getUser().getUsertype().equals(Constants.STYLIST)) {
//            nav_apply_to_become_a_stylist.setVisibility(View.GONE);
//            nav_my_profile.setVisibility(View.GONE);
//            nav_post_to_stylist_page.setVisibility(View.VISIBLE);
//        }
//
//        if (Session.getUsers()!=null && Session.getUsers().getUser()!=null&& Session.getUsers().getUser().getUsertype()!=null && Session.getUsers().getUser().getUsertype().equals(Constants.CUSTOMER)) {
//            nav_apply_to_become_a_stylist.setVisibility(View.VISIBLE);
//            nav_my_profile.setVisibility(View.VISIBLE);
//            nav_edit_stylist_profile.setVisibility(View.GONE);
//            nav_post_to_stylist_page.setVisibility(View.GONE);
//            stylist_option.setVisibility(View.GONE);
//        }

//        nav_home.setOnClickListener(this);
        nav_store_locator.setOnClickListener(this);
//        nav_celebrity_stylists.setOnClickListener(this);
//        nav_game_winners.setOnClickListener(this);
        nav_my_profile.setOnClickListener(this);
        nav_apply_to_become_a_stylist.setOnClickListener(this);
        nav_edit_stylist_profile.setOnClickListener(this);
        nav_post_to_stylist_page.setOnClickListener(this);
        nav_logout.setOnClickListener(this);


        if (Session.getUsers() != null && Session.getUsers().getUser() != null && Session.getUsers().getUser().getUsertype() != null && Session.getUsers().getUser().getUsertype().equals(Constants.STYLIST)) {
            showStylistMenu();
        } else {
            showUserMenu();
        }
    }

    private void showUserMenu() {
        LinearLayout user_option_menu = (LinearLayout) findViewById(R.id.user_option_menu);
        user_option_menu.setVisibility(View.VISIBLE);
        hideStylistMenu();
    }

    private void hideUserMenu() {
        LinearLayout user_option_menu = (LinearLayout) findViewById(R.id.user_option_menu);
        user_option_menu.setVisibility(View.GONE);
    }

    public void showStylistMenu() {
        hideUserMenu();
        LinearLayout stylist_option_menu = (LinearLayout) findViewById(R.id.stylist_option_menu);
        stylist_option_menu.setVisibility(View.VISIBLE);
    }

    private void hideStylistMenu() {
        LinearLayout stylist_option_menu = (LinearLayout) findViewById(R.id.stylist_option_menu);
        stylist_option_menu.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.layoutLayD:
                resetSelectMenu(null);
                layoutLayD.setSelected(true);
                fragment_general.setVisibility(View.GONE);

                viewPager.setCurrentItem(0, true);
                lastCurrentpage = 0;
                break;
            case R.id.layoutPayD:
                resetSelectMenu(null);
                fragment_general.setVisibility(View.GONE);

                layoutPayD.setSelected(true);
                viewPager.setCurrentItem(1, true);
                lastCurrentpage = 1;
                break;
            case R.id.layoutWhatHot:
                Log.d(TAG, "layoutWhatHot");
                resetSelectMenu(null);
                fragment_general.setVisibility(View.GONE);

                layoutWhatHot.setSelected(true);
                viewPager.setCurrentItem(2, true);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                break;
            case R.id.layoutShop:
                Log.d(TAG, "layoutShop");
                resetSelectMenu(null);
                fragment_general.setVisibility(View.GONE);

                layoutShop.setSelected(true);
                viewPager.setCurrentItem(3, true);
                lastCurrentpage = 3;
                break;

            case R.id.nav_store_locator:
                resetSelectMenu(nav_store_locator);
                lastCurrentpage = viewPager.getCurrentItem();

                fragment_general.setVisibility(View.VISIBLE);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_general, StoreLocatorFragment.newInstance(), String.valueOf(Constants.FRAGMENT_GENERAL_STORE_LOCATOR));
                fragmentTransaction.commit();

                DrawerLayout drawer0 = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer0.closeDrawer(GravityCompat.START);
                break;


            case R.id.nav_my_profile:
                resetSelectMenu(nav_my_profile);
                lastCurrentpage = viewPager.getCurrentItem();

                fragment_general.setVisibility(View.VISIBLE);
                FragmentTransaction fragmentTransaction11 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction11.replace(R.id.fragment_general, MyProfileFragment.newInstance(), String.valueOf(Constants.FRAGMENT_GENERAL_MY_PROFILE));
                fragmentTransaction11.commit();

                DrawerLayout drawer2 = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer2.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_apply_to_become_a_stylist:
                resetSelectMenu(nav_apply_to_become_a_stylist);
                lastCurrentpage = viewPager.getCurrentItem();
                fragment_general.setVisibility(View.VISIBLE);
                FragmentTransaction fragmentTransactiontemp = getSupportFragmentManager().beginTransaction();
                fragmentTransactiontemp.replace(R.id.fragment_general, ApplyToStylistFragment.newInstance(), String.valueOf(Constants.FRAGMENT_GENERAL_APPLY_STYLIST));
                fragmentTransactiontemp.commit();

                DrawerLayout drawer01 = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer01.closeDrawer(GravityCompat.START);

                break;
            case R.id.nav_edit_stylist_profile:
                resetSelectMenu(nav_edit_stylist_profile);

                fragment_general.setVisibility(View.VISIBLE);
                FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction1.replace(R.id.fragment_general, MyProfileFragment.newInstance(), String.valueOf(Constants.FRAGMENT_GENERAL_MY_PROFILE));
                fragmentTransaction1.commit();

                lastCurrentpage = viewPager.getCurrentItem();

                DrawerLayout drawer3 = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer3.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_post_to_stylist_page:
                resetSelectMenu(nav_post_to_stylist_page);
//                if (viewPager.getCurrentItem() != 4)
                lastCurrentpage = viewPager.getCurrentItem();
//                viewPager.setCurrentItem(4, false);
//                updateFragmentGeneral(Constants.FRAGMENT_GENERAL_NEW_POST, 0);
                fragment_general.setVisibility(View.VISIBLE);

                FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction2.replace(R.id.fragment_general, NewPostFragment.newInstance(), String.valueOf(Constants.FRAGMENT_GENERAL_NEW_POST));
                fragmentTransaction2.commit();

                DrawerLayout drawer33 = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer33.closeDrawer(GravityCompat.START);

                break;
            case R.id.nav_logout:
                resetSelectMenu(nav_logout);
                LoginManager.getInstance().logOut();
                CognitoSyncClientManager.getCredentialsProvider().clear();

                Intent intent = new Intent(NewHomeActivity.this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }


    public void selectWhatsHot() {
        Log.d(TAG, "layoutWhatHot");
        resetSelectMenu(null);
        layoutWhatHot.setSelected(true);
        viewPager.setCurrentItem(2, false);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }


    public void openNavMenu() {
        Log.d(TAG, "click to open drawable menu");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (!drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    private void resetSelectMenu(@Nullable View view) {
        // reset select bottom  bar menu
        resetSelectClickItemBottomBar();

        nav_my_profile.setSelected(false);
        nav_apply_to_become_a_stylist.setSelected(false);
        nav_edit_stylist_profile.setSelected(false);
        nav_post_to_stylist_page.setSelected(false);
        nav_logout.setSelected(false);
        if (view != null)
            view.setSelected(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void resetSelectClickItemBottomBar() {
        layoutPayD.setSelected(false);
        layoutLayD.setSelected(false);
        layoutWhatHot.setSelected(false);
        layoutShop.setSelected(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;

        if (requestCode == Constants.REQUEST_CODE_GET_IMAGE) {
            try {
                //mFileTemp = new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
                Uri selectedImageUri = data.getData();
                InputStream imageStream = getContentResolver().openInputStream(selectedImageUri);
                //BitmapFactory.Options options=new BitmapFactory.Options();
                //options.inSampleSize = 8;
                GlobalVariable.tempUserPhoto = BitmapFactory.decodeStream(imageStream);

                Matrix matrix = new Matrix();
                if (GlobalVariable.tempUserPhoto != null && GlobalVariable.tempUserPhoto.getHeight() < GlobalVariable.tempUserPhoto.getWidth())
                    matrix.preRotate(270);
                else
                    matrix.preRotate(0);
                //Bitmap thumbnail = BitmapFactory.decodeFile(mFileTemp.getPath());
                if (GlobalVariable.tempUserPhoto.getHeight() > 1200) {
                    GlobalVariable.tempUserPhoto = Bitmap.createScaledBitmap(GlobalVariable.tempUserPhoto, GlobalVariable.tempUserPhoto.getWidth() * 1200 / GlobalVariable.tempUserPhoto.getHeight(), 1200, false);
                    GlobalVariable.tempUserPhoto = Bitmap.createBitmap(GlobalVariable.tempUserPhoto, 0, 0, GlobalVariable.tempUserPhoto.getWidth(), GlobalVariable.tempUserPhoto.getHeight(), matrix, true);
                } else {
                    GlobalVariable.tempUserPhoto = Bitmap.createBitmap(GlobalVariable.tempUserPhoto, 0, 0, GlobalVariable.tempUserPhoto.getWidth(), GlobalVariable.tempUserPhoto.getHeight(), matrix, true);
                }
                System.gc();

                startActivity(new Intent(this, UserImageActivity.class));
                return;
            } catch (FileNotFoundException localFileNotFoundException) {
                localFileNotFoundException.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (fragment_general.isShown()) {
                backToLastTab();
                return;
            }

            FragmentManager fm = getSupportFragmentManager();

            for (Fragment frag : fm.getFragments()) {
                if (frag != null && frag.isVisible()) {

                    adapter.getItem(viewPager.getCurrentItem());

                    FragmentManager childFm = frag.getChildFragmentManager();
                    if (childFm.getBackStackEntryCount() > 0) {
                        childFm.popBackStack();
                        return;
                    }
                }
            }
            super.onBackPressed();
        }
    }

    /**
     * Adding fragments to ViewPager
     *
     * @param viewPager
     */
    private ViewPagerAdapter adapter;

    private void setupViewPager(final ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        viewPager.setOffscreenPageLimit(5);

        selectWhatsHot();// what hot default

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.e("position fragment", position + "");

                if (onChangeFragment != null)
                    onChangeFragment.onHideMediaController();

                if (onChangeFragment == null) {
                    onChangeFragment = (onChangeFragment) adapter.getItem(position);
                    onChangeFragment.onHideMediaController();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    /**
     * call by fragment viewpager 4 and child
     */
    public void backToLastTab() {

        fragment_general.setVisibility(View.GONE);
        viewPager.setCurrentItem(lastCurrentpage, false);

        resetSelectMenu(null);
        switch (lastCurrentpage) {
            case 0:
                layoutLayD.setSelected(true);

                break;
            case 1:
                layoutPayD.setSelected(true);

                break;
            case 2:
                layoutWhatHot.setSelected(true);

                break;
            case 3:
                layoutShop.setSelected(true);

                break;
        }
        return;
    }

    /**
     * call by fragment viewpager 4 and child
     */
    public void backToScreenGetlaydDefault() {
        layoutLayD.setSelected(true);
    }

    private GetlaydDefaultFragment getlaydDefaultFragment;

    private WebviewFragment webviewWhatHot, webViewShop;

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    getlaydDefaultFragment = GetlaydDefaultFragment.newInstance();
                    return getlaydDefaultFragment;
                case 1:
                    return DefaultFragment.Instance();
                case 2:
                    String url = Constants.URL_WHAT_HOT;
                    if (Session.getJsonResource()!=null &&
                            Session.getJsonResource().getUrls()!=null &&
                            Session.getJsonResource().getUrls().getUrlsObj()!=null &&
                            Session.getJsonResource().getUrls().getUrlsObj().getWhatsHot() != null) {
                        url = Session.getJsonResource().getUrls().getUrlsObj().getWhatsHot();
                    }
                    webviewWhatHot = WebviewFragment.newInstance(url);
                    return webviewWhatHot;
                case 3:
                    webViewShop = WebviewFragment.newInstance(Constants.URL_WHAT_HOT);
                    return webViewShop;

            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }


    private void requestGetNewResource(String url) {

        PreferenceHelpers.setPreference(NewHomeActivity.this, "json_resource", "");

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        File httpCacheDirectory = this.getCacheDir();

        Cache cache = new Cache(httpCacheDirectory, 20 * 1024 * 1024);
        OkHttpClient client = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(interceptor)
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_AWS_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<JsonResource> response = service.getResource(url);

        response.enqueue(new Callback<JsonResource>() {
            @Override
            public void onResponse(Call<JsonResource> call, Response<JsonResource> response) {
                try {

                    PreferenceHelpers.setPreference(NewHomeActivity.this, "resource_version", Session.getUsers().getResourcelocation().getLastupdate() + "");
                    JsonResource jsonResource = response.body();
                    Session.setJsonResource(jsonResource);

                    Gson gson = new Gson();
                    PreferenceHelpers.setPreference(NewHomeActivity.this, "json_resource", gson.toJson(jsonResource).toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonResource> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                t.printStackTrace();
            }
        });
    }


    boolean mergeInProgress = false;

    private void synchronize() {
        datasetUser.synchronize(new Dataset.SyncCallback() {
            @Override
            public void onSuccess(Dataset dataset, final List<Record> newRecords) {
                Log.i("Sync", "success");
                if (mergeInProgress) return;
            }

            @Override
            public void onFailure(final DataStorageException dse) {
                Log.i("Sync", "failure: ", dse);
            }

            @Override
            public boolean onConflict(final Dataset dataset,
                                      final List<SyncConflict> conflicts) {
                Log.i("Sync", "conflict: " + conflicts);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, String.format("%s records in conflict",
                                conflicts.size()));
                        List<Record> resolvedRecords = new ArrayList<Record>();
                        for (SyncConflict conflict : conflicts) {
                            Log.i(TAG, String.format("remote: %s; local: %s",
                                    conflict.getRemoteRecord(),
                                    conflict.getLocalRecord()));
                            /* resolve by taking remote records */
                            resolvedRecords.add(conflict
                                    .resolveWithRemoteRecord());

                            /* resolve by taking local records */
                            // resolvedRecords.add(conflict.resolveWithLocalRecord());

                            /*
                             * resolve with customized logic, e.g. concatenate
                             * strings
                             */
                            // String newValue =
                            // conflict.getRemoteRecord().getValue()
                            // + conflict.getLocalRecord().getValue();
                            // resolvedRecords.add(conflict.resolveWithValue(newValue));
                        }
                        dataset.resolve(resolvedRecords);

                    }
                });
                return true;
            }

            @Override
            public boolean onDatasetDeleted(Dataset dataset, String datasetName) {
                Log.i("Sync", "delete: " + datasetName);
                return true;
            }

            @Override
            public boolean onDatasetsMerged(Dataset dataset, List<String> mergedDatasetNames) {

                mergeInProgress = true;
                Log.i("Sync", "merge: " + dataset.getDatasetMetadata().getDatasetName());

                CognitoSyncManager client = CognitoSyncClientManager.getInstance();
                for (final String name : mergedDatasetNames) {
                    Log.i("Merge", "syncing merged: " + name);
                    final Dataset d = client.openOrCreateDataset(name);
                    d.synchronize(new Dataset.SyncCallback() {
                        @Override
                        public void onSuccess(Dataset dataset, List<Record> records) {

                            //This is the actual merge code, in this sample we will just join fields in both datasets into a single one
                            Log.i("Merge", "joining records");
                            NewHomeActivity.this.datasetUser.putAll(dataset.getAll());

                            //To finish and resolve the merge, we have to delete the merged dataset
                            Log.e("Merge", "deleting merged: " + name);
                            dataset.delete();
                            dataset.synchronize(new Dataset.SyncCallback() {
                                @Override
                                public void onSuccess(Dataset dataset, List<Record> records) {
                                    Log.i("Merge", "merged dataset deleted");

                                    //And finally we should sync back the new merged dataset
                                    Log.i("Merge", "now syncing the resulting new dataset");
                                    NewHomeActivity.this.datasetUser.synchronize(new Dataset.SyncCallback() {
                                        @Override
                                        public void onSuccess(Dataset dataset, List<Record> newRecords) {
                                            Log.i("Merge", "merge completed");
                                            mergeInProgress = false;

                                        }

                                        @Override
                                        public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                            Log.e("Merge", "Unhandled onConflict");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetDeleted(Dataset dataset, String s) {
                                            Log.e("Merge", "Unhandled onDatasetDeleted");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                            Log.e("Merge", "Unhandled onDatasetMerged");
                                            return false;
                                        }

                                        @Override
                                        public void onFailure(DataStorageException e) {
                                            e.printStackTrace();
                                            Log.e("Merge", "Exception");
                                        }
                                    });
                                }

                                @Override
                                public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                    Log.e("Merge", "Unhandled onConflict");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetDeleted(Dataset dataset, String s) {
                                    Log.e("Merge", "Unhandled onDatasetDeleted");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                    Log.e("Merge", "Unhandled onDatasetMerged");
                                    return false;
                                }

                                @Override
                                public void onFailure(DataStorageException e) {
                                    e.printStackTrace();
                                    Log.e("Merge", "Exception");
                                }
                            });
                        }

                        @Override
                        public boolean onDatasetDeleted(Dataset dataset, String s) {

                            //This will trigger in the scenario were we had a local dataset that was not present on the identity we are merging

                            Log.i("Merge", "onDatasetDeleted");
                            final Dataset previous = dataset;

                            //Sync the local dataset
                            NewHomeActivity.this.datasetUser.synchronize(new Dataset.SyncCallback() {
                                @Override
                                public void onSuccess(Dataset dataset, final List<Record> newRecords) {

                                    // Delete the local dataset from the old identity, now it's merged into the new one
                                    Log.i("Merge", "local dataset synced to the new identity");
                                    previous.delete();
                                    previous.synchronize(new Dataset.SyncCallback() {
                                        @Override
                                        public void onSuccess(Dataset dataset, List<Record> deletedRecords) {
                                            mergeInProgress = false;

                                        }

                                        @Override
                                        public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                            Log.e("Merge", "Unhandled onConflict");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetDeleted(Dataset dataset, String s) {
                                            Log.e("Merge", "Unhandled onDatasetDeleted");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                            Log.e("Merge", "Unhandled onDatasetMerged");
                                            return false;
                                        }

                                        @Override
                                        public void onFailure(DataStorageException e) {
                                            e.printStackTrace();
                                            Log.e("Merge", "Exception");
                                        }
                                    });
                                }

                                @Override
                                public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                    Log.e("Merge", "Unhandled onConflict");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetDeleted(Dataset dataset, String s) {
                                    Log.e("Merge", "Unhandled onDatasetDeleted");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                    Log.e("Merge", "Unhandled onDatasetMerged");
                                    return false;
                                }

                                @Override
                                public void onFailure(DataStorageException e) {
                                    e.printStackTrace();
                                    Log.e("Merge", "Exception");
                                }
                            });
                            return false;
                        }

                        @Override
                        public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                            Log.e("Merge", "Unhandled onConflict");
                            return false;
                        }

                        @Override
                        public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                            Log.e("Merge", "Unhandled onDatasetMerged");
                            return false;
                        }

                        @Override
                        public void onFailure(DataStorageException e) {
                            e.printStackTrace();
                            Log.e("Merge", "Exception");
                        }
                    });
                }
                return true;
            }
        });
    }


}
