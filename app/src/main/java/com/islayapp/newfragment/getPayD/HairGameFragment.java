package com.islayapp.newfragment.getPayD;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;

import com.core.APIService;
import com.dajodi.clock.ui.FlipClock;
import com.dajodi.clock.ui.FlipClockFinish;
import com.google.android.exoplayer.AspectRatioFrameLayout;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecTrackRenderer;
import com.google.android.exoplayer.MediaCodecUtil;
import com.google.android.exoplayer.audio.AudioCapabilities;
import com.google.android.exoplayer.audio.AudioCapabilitiesReceiver;
import com.google.android.exoplayer.drm.UnsupportedDrmException;
import com.google.android.exoplayer.util.Util;
import com.google.gson.Gson;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.user.JsonResource;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Session;
import com.player.DashRendererBuilder;
import com.player.DemoPlayer;
import com.player.ExtractorRendererBuilder;
import com.player.HlsRendererBuilder;
import com.player.SmoothStreamingRendererBuilder;
import com.player.SmoothStreamingTestMediaDrmCallback;
import com.player.WidevineTestMediaDrmCallback;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DINO on 25/04/2016.
 */
public class HairGameFragment extends BaseFragment implements DefaultFragment.TimmerChanger, SurfaceHolder.Callback, View.OnClickListener,
        DemoPlayer.Listener, AudioCapabilitiesReceiver.Listener, FlipClockFinish {

    private DefaultFragment defaultFragment;
    public static HairGameFragment Instance() {
        HairGameFragment fragment = new HairGameFragment();
        return fragment;
    }

    private FlipClock flipClock;
    private static final String TAG = EarnPointFragment.class.getName();
    private JsonResource resource;

    private AspectRatioFrameLayout videoFrame;
    private SurfaceView surfaceView;
//    private MediaController mediaController;
    private DemoPlayer player;

    private boolean playerNeedsPrepare;
    private long playerPosition;

    private Uri contentUri;
    private int contentType;
    private String contentId;
    private String provider;
    private AudioCapabilitiesReceiver audioCapabilitiesReceiver;
    private View shutterView;
    private TextViewPlus click_go_to_game;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String string = PreferenceHelpers.getPreference(getContext(), "json_resource");

        String currentVersionResource = PreferenceHelpers.getPreference(getContext(), "resource_version");
        if (!String.valueOf(Session.getUsers().getResourcelocation().getLastupdate()).equals(currentVersionResource) || string.equals("")) {
            requestGetNewResource(Session.getUsers().getResourcelocation().getLocation());
        } else {
            Gson gson = new Gson();
            resource = gson.fromJson(string, JsonResource.class);
            Session.setJsonResource(resource);
        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_payd_hair_game, container, false);

        flipClock = (FlipClock) rootView.findViewById(R.id.cwClock);
        flipClock.setFlipClockFinish(this);

//        View root = rootView.findViewById(R.id.video_frame);
//        root.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//                    toggleControlsVisibility();
//                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                    view.performClick();
//                }
//                return true;
//            }
//        });
//        root.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE
//                        || keyCode == KeyEvent.KEYCODE_MENU) {
//                    return false;
//                }
////                return mediaController.dispatchKeyEvent(event);
//            }
//        });

        shutterView = rootView.findViewById(R.id.shutter);

        videoFrame = (AspectRatioFrameLayout) rootView.findViewById(R.id.video_frame);
        surfaceView = (SurfaceView) rootView.findViewById(R.id.surface_view);
        surfaceView.getHolder().addCallback(this);

//        mediaController = new KeyCompatibleMediaController(getContext());
//        mediaController.setAnchorView(root);

        audioCapabilitiesReceiver = new AudioCapabilitiesReceiver(getContext(), this);
        audioCapabilitiesReceiver.register();

        click_go_to_game = (TextViewPlus) rootView.findViewById(R.id.click_go_to_game);
        click_go_to_game.setOnClickListener(this);

        long currentTime = System.currentTimeMillis();
        if (resource != null && resource.getGames() != null &&
                resource.getGames().getGamesItemList() != null &&
                resource.getGames().getGamesItemList().size() > 0 && maxTimeGlobal == 0 && positionItem >= 0 && currentTime > resource.getGames().getGamesItemList().get(positionItem).getEndDate()) {
            click_go_to_game.setEnabled(true);
        }
        return rootView;
    }

    @Override
    protected void initTopbar() {
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText(getResources().getString(R.string.get_payd));
        flipClock.setCurrentTime(hours, min, sec);
        rootView.findViewById(R.id.backOnQuoteFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                defaultFragment.popback();
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.click_go_to_game:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Session.getJsonResource().getGames().getGamesItemList().get(0).getUrl()));
                startActivity(browserIntent);
                break;
            default:
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            onShown();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Util.SDK_INT <= 23 || player == null) {
            onShown();
        }
    }

    private void onShown() {

        String proxyUrl = Session.getJsonResource().getGames().getGamesItemList().get(0).getVideoUrl();
        contentUri = Uri.parse(proxyUrl);
        contentType = Util.TYPE_OTHER;
        contentId = "Video play game";
        provider = "";

        Log.d(TAG, "link video: " + Session.getJsonResource().getGames().getGamesItemList().get(0).getVideoUrl());
        if (player == null) {
            if (!maybeRequestPermission()) {
                preparePlayer(true);
            }
        } else {
            player.setBackgrounded(false);
        }
    }

    @Override
    public void onPause() {

        Log.d(TAG, "onPause");

        super.onPause();
        if (Util.SDK_INT <= 23) {
            onHidden();
        }
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");

        super.onStop();
        if (Util.SDK_INT > 23) {
            onHidden();
        }
    }

    public void onHidden() {
        releasePlayer();
        shutterView.setVisibility(View.VISIBLE);
        Log.e("onHidden", "onHidden");

    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");

        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");

        super.onDestroy();
        audioCapabilitiesReceiver.unregister();
        releasePlayer();
    }

    @Override
    public void onAudioCapabilitiesChanged(AudioCapabilities audioCapabilities) {
        if (player == null) {
            return;
        }
        boolean backgrounded = player.getBackgrounded();
        boolean playWhenReady = player.getPlayWhenReady();
        releasePlayer();
        preparePlayer(playWhenReady);
        player.setBackgrounded(backgrounded);
    }

    // Permission request listener method

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            preparePlayer(true);
        } else {
            Toast.makeText(getContext(), R.string.storage_permission_denied, Toast.LENGTH_LONG).show();

        }
    }

    /**
     * Checks whether it is necessary to ask for permission to read storage. If necessary, it also
     * requests permission.
     *
     * @return true if a permission request is made. False if it is not necessary.
     */
    @TargetApi(23)
    private boolean maybeRequestPermission() {
        if (requiresPermission(contentUri)) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            return true;
        } else {
            return false;
        }
    }

    @TargetApi(23)
    private boolean requiresPermission(Uri uri) {
        return Util.SDK_INT >= 23
                && Util.isLocalFileUri(uri)
                && getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED;
    }

    private void preparePlayer(boolean playWhenReady) {
        if (player == null) {
            player = new DemoPlayer(getRendererBuilder());
            player.addListener(this);
            player.seekTo(playerPosition);
            playerNeedsPrepare = true;
//            mediaController.setMediaPlayer(player.getPlayerControl());
//            mediaController.setEnabled(true);

        }
        if (playerNeedsPrepare) {
            player.prepare();
            playerNeedsPrepare = false;
        }
        player.setSurface(surfaceView.getHolder().getSurface());
        player.setPlayWhenReady(playWhenReady);
    }


    // Internal methods

    private DemoPlayer.RendererBuilder getRendererBuilder() {
        String userAgent = Util.getUserAgent(getContext(), "ExoPlayerDemo");
        switch (contentType) {
            case Util.TYPE_SS:
                return new SmoothStreamingRendererBuilder(getContext(), userAgent, contentUri.toString(),
                        new SmoothStreamingTestMediaDrmCallback());
            case Util.TYPE_DASH:
                return new DashRendererBuilder(getContext(), userAgent, contentUri.toString(),
                        new WidevineTestMediaDrmCallback(contentId, provider));
            case Util.TYPE_HLS:
                return new HlsRendererBuilder(getContext(), userAgent, contentUri.toString());
            case Util.TYPE_OTHER:
                return new ExtractorRendererBuilder(getContext(), userAgent, contentUri);
            default:
                throw new IllegalStateException("Unsupported type: " + contentType);
        }
    }


    private void releasePlayer() {
        if (player != null) {

            playerPosition = player.getCurrentPosition();
//            if (mediaController.isShowing()) {
//                mediaController.hide();
//                mediaController.removeAllViews();
//            }
            player.release();
            player = null;

        }
    }

    // DemoPlayer.Listener implementation

    @Override
    public void onStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_ENDED) {
//            showControls();
        }
        String text = "playWhenReady=" + playWhenReady + ", playbackState=";
        switch (playbackState) {
            case ExoPlayer.STATE_BUFFERING:
                text += "buffering";
                break;
            case ExoPlayer.STATE_ENDED:
                text += "ended";
                break;
            case ExoPlayer.STATE_IDLE:
                text += "idle";
                break;
            case ExoPlayer.STATE_PREPARING:
                text += "preparing";
                break;
            case ExoPlayer.STATE_READY:
                text += "ready";
                break;
            default:
                text += "unknown";
                break;
        }

    }

    @Override
    public void onError(Exception e) {
        String errorString = null;
        if (e instanceof UnsupportedDrmException) {
            // Special case DRM failures.
            UnsupportedDrmException unsupportedDrmException = (UnsupportedDrmException) e;
            errorString = getString(Util.SDK_INT < 18 ? R.string.error_drm_not_supported
                    : unsupportedDrmException.reason == UnsupportedDrmException.REASON_UNSUPPORTED_SCHEME
                    ? R.string.error_drm_unsupported_scheme : R.string.error_drm_unknown);
        } else if (e instanceof ExoPlaybackException
                && e.getCause() instanceof MediaCodecTrackRenderer.DecoderInitializationException) {
            // Special case for decoder initialization failures.
            MediaCodecTrackRenderer.DecoderInitializationException decoderInitializationException =
                    (MediaCodecTrackRenderer.DecoderInitializationException) e.getCause();
            if (decoderInitializationException.decoderName == null) {
                if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                    errorString = getString(R.string.error_querying_decoders);
                } else if (decoderInitializationException.secureDecoderRequired) {
                    errorString = getString(R.string.error_no_secure_decoder,
                            decoderInitializationException.mimeType);
                } else {
                    errorString = getString(R.string.error_no_decoder,
                            decoderInitializationException.mimeType);
                }
            } else {
                errorString = getString(R.string.error_instantiating_decoder,
                        decoderInitializationException.decoderName);
            }
        }
        if (errorString != null) {
            Toast.makeText(getContext(), errorString, Toast.LENGTH_LONG).show();
        }
        playerNeedsPrepare = true;
//        showControls();
    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthAspectRatio) {
        shutterView.setVisibility(View.GONE);
        videoFrame.setAspectRatio(
                height == 0 ? 1 : (width * pixelWidthAspectRatio) / height);
    }

    // SurfaceHolder.Callback implementation

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (player != null) {
            player.setSurface(holder.getSurface());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Do nothing.
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (player != null) {
            player.blockingClearSurface();
        }
    }


    public void setDefaultFragment(DefaultFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }

    @Override
    public void TimerChanger() {
        ((FlipClock) rootView.findViewById(R.id.cwClock)).onTimeChanged();
    }

    int hours, min, sec, positionItem;
    long maxTimeGlobal;

    public void setTimer(long maxtime, long position) {
        maxTimeGlobal = maxtime;
        positionItem = (int) position;
        hours = (int) maxtime / (60 * 60000);
        min = (int) (maxtime - hours * 60 * 60000) / 60000;
        sec = (int) (maxtime - hours * 60 * 60000 - min * 60000) / 1000;
    }


    private void requestGetNewResource(String url) {

        PreferenceHelpers.setPreference(getContext(), "json_resource", "");

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        File httpCacheDirectory = getContext().getCacheDir();
//
        Cache cache = new Cache(httpCacheDirectory, 20 * 1024 * 1024);
        OkHttpClient client = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(interceptor)
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_AWS_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<JsonResource> response = service.getResource(url);

        response.enqueue(new Callback<JsonResource>() {
            @Override
            public void onResponse(Call<JsonResource> call, Response<JsonResource> response) {
                try {

                    PreferenceHelpers.setPreference(getContext(), "resource_version", Session.getUsers().getResourcelocation().getLastupdate() + "");
                    resource = response.body();
                    Session.setJsonResource(resource);

                    Gson gson = new Gson();
                    PreferenceHelpers.setPreference(getContext(), "json_resource", gson.toJson(resource).toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonResource> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                t.printStackTrace();
            }
        });
    }

//    private void toggleControlsVisibility() {
//        if (mediaController.isShowing()) {
//            mediaController.hide();
//        } else {
//            showControls();
//        }
//    }

//    private void showControls() {
//        mediaController.show(0);
//    }

    @Override
    public void onFlipClockFinish() {
        maxTimeGlobal = 0;
        click_go_to_game.setEnabled(true);
    }

    private static final class KeyCompatibleMediaController extends MediaController {

        private MediaController.MediaPlayerControl playerControl;

        public KeyCompatibleMediaController(Context context) {
            super(context);
        }

        @Override
        public void setMediaPlayer(MediaController.MediaPlayerControl playerControl) {
            super.setMediaPlayer(playerControl);
            this.playerControl = playerControl;
        }

        @Override
        public boolean dispatchKeyEvent(KeyEvent event) {
            int keyCode = event.getKeyCode();
            if (playerControl.canSeekForward() && (keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD
                    || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT)) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    playerControl.seekTo(playerControl.getCurrentPosition() + 15000); // milliseconds
                    show();
                }
                return true;
            } else if (playerControl.canSeekBackward() && (keyCode == KeyEvent.KEYCODE_MEDIA_REWIND
                    || keyCode == KeyEvent.KEYCODE_DPAD_LEFT)) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    playerControl.seekTo(playerControl.getCurrentPosition() - 5000); // milliseconds
                    show();
                }
                return true;
            }
            return super.dispatchKeyEvent(event);
        }
    }


}
