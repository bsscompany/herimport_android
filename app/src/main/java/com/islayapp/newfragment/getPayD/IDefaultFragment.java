package com.islayapp.newfragment.getPayD;

import android.support.annotation.Nullable;

/**
 * Created by DINO on 21/04/2016.
 */
public interface IDefaultFragment {
    void popback(@Nullable String keycode);
    void popback();
    void addFragment(Object type, Object value);
}
