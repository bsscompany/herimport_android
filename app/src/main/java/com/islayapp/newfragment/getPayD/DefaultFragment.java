package com.islayapp.newfragment.getPayD;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.LambdaInterface;
import com.amazonaws.models.CommonRequest;
import com.amazonaws.models.LambdaJsonBinder;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.core.APIService;
import com.google.gson.Gson;
import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.PointsGetResponse;
import com.islayapp.models.awsmodels.user.JsonResource;
import com.islayapp.models.awsmodels.user.RewardsObj;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.newfragment.getPayD.Views.onChangeFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Session;
import com.islayapp.util.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DINO on 20/04/2016.
 */


public class DefaultFragment extends BaseFragment implements View.OnClickListener, IDefaultFragment, onChangeFragment {


    public enum GetPayD {
        EarnPoint, RedeemPoint, WHERESHIP, HairGame
    }

    View rootView;

    public static DefaultFragment Instance() {
        DefaultFragment fragment = new DefaultFragment();
        return fragment;
    }

    private static final String TAG = DefaultFragment.class.getName();
    private CountDownTimer countDownTimer;
    private TimmerChanger timmerChanger;
    private TextViewPlus timer_coundown;
    private JsonResource resource;
    public static EarnPointFragment earnPointFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String string = PreferenceHelpers.getPreference(getContext(), "json_resource");

        String currentVersionResource = PreferenceHelpers.getPreference(getContext(), "resource_version");
        if (string.equals("") || !Utility.getLastUpdate().equals(currentVersionResource)) {
            if (Session.getUsers() != null && Session.getUsers().getResourcelocation() != null)
                requestGetNewResource(Session.getUsers().getResourcelocation().getLocation());
        } else {
            Gson gson = new Gson();
            resource = gson.fromJson(string, JsonResource.class);
            Session.setJsonResource(resource);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_payd_default, container, false);
        rootView.findViewById(R.id.earnpoint_view).setOnClickListener(this);
        rootView.findViewById(R.id.redeempoints_view).setOnClickListener(this);
        rootView.findViewById(R.id.hairgameview).setOnClickListener(this);

        ImageView share = (ImageView) rootView.findViewById(R.id.share);
        share.setOnClickListener(this);


        ImageView coppy_to_clipboard = (ImageView) rootView.findViewById(R.id.coppy_to_clipboard);
        coppy_to_clipboard.setOnClickListener(this);


        timer_coundown = (TextViewPlus) rootView.findViewById(R.id.timer_coundown);

        View imgMenu = rootView.findViewById(R.id.imgMenu);
        if (imgMenu != null)
            imgMenu.setOnClickListener(this);

        return rootView;
    }

    @Override
    protected void initTopbar() {
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText(getResources().getString(R.string.get_payd));

    }

    long tempObset = -1;

    @Override
    public void onStart() {
        super.onStart();

        if (resource != null && resource.getGames() != null &&
                resource.getGames().getGamesItemList() != null &&
                resource.getGames().getGamesItemList().size() > 0) {
            try {
                long current = System.currentTimeMillis();
                long minTime = System.currentTimeMillis();


                for (int i = 0; i < resource.getGames().getGamesItemList().size(); i++) {
                    long timeRanger = resource.getGames().getGamesItemList().get(i).getStartDate() - current;
                    if (timeRanger > 0) {
                        minTime = Math.min(minTime, timeRanger);
                        tempObset = i;
                    }
                }

                if (tempObset != -1) {
                    maxtime = minTime;
                    int hours = (int) maxtime / (60 * 60000);
                    int min = (int) (maxtime - hours * 60 * 60000) / 60000;
                    int sec = (int) (maxtime - hours * 60 * 60000 - min * 60000) / 1000;
                    startCountDown(hours, min, sec);

                } else
                    startCountDown(00, 00, 00);


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG + "onStop");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG + "onPause");
    }


    @Override
    public void onResume() {
        super.onResume();

        TextViewPlus point = (TextViewPlus) rootView.findViewById(R.id.point);
        point.setText(Session.getCurrentPoint() + "");

        CommonRequest request = new CommonRequest();
        request.setPagesize(Constants.PAGE_SIZE);
        request.setPagenumber(1);
        request.setUserid(Session.getUsers().getUser().getId());
        requestUserPoints(request, false);

    }

    /**
     * @param hours
     * @param minutes
     * @param second
     */
    long maxtime;

    public void startCountDown(int hours, int minutes, int second) {

        maxtime = second * 1000 + minutes * 60 * 1000 + hours * 60 * 60 * 1000;
        if (countDownTimer != null)
            countDownTimer.onFinish();
        Log.e("countDownTimer", "countDownTimer");
        countDownTimer = null;
        countDownTimer = new CountDownTimer(maxtime, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                maxtime -= 1000;
                if (timmerChanger != null)
                    timmerChanger.TimerChanger();

                if (timer_coundown != null) {
                    int hours = (int) maxtime / (60 * 60000);
                    int min = (int) (maxtime - hours * 60 * 60000) / 60000;
                    int sec = (int) (maxtime - hours * 60 * 60000 - min * 60000) / 1000;

                    String shours = String.format("%02d", hours);
                    String smin = String.format("%02d", min);
                    String ssec = String.format("%02d", sec);

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(shours).append(":").append(smin).append(":").append(ssec);

                    timer_coundown.setText(stringBuilder);
                }
            }

            @Override
            public void onFinish() {

            }
        };
        countDownTimer.start();
    }

    public interface TimmerChanger {
        void TimerChanger();
    }

    private Dialog dialog;

    private void requestUserPoints(CommonRequest request, boolean isShowDialog) {

        Gson gson = new Gson();
        Log.d(TAG, " Request get point:  " + gson.toJson(request).toString());

        if (isShowDialog)
            dialog = DialogUtil.showDialog(getContext());
        Log.d(TAG, "LAMBDA: Sending Data");
        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        final LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());
        final LambdaInterface myInterface = factory.build(LambdaInterface.class, new LambdaJsonBinder());
        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<CommonRequest, Void, PointsGetResponse>() {

            @Override
            protected PointsGetResponse doInBackground(CommonRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.PointsGet(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(PointsGetResponse result) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    return;
                } else {
                    Log.d(TAG, "LAMBDA: Response from request have data");
                    Log.d(TAG, "response point® " + result.getCountof().getAvailablepoints());

                    updateData(result);
                    Session.setCurrentPoint(result.getCountof().getAvailablepoints());

                    if (result.getValid().equals("Y")) {

                    }
                }
            }
        }.execute(request);
    }

    private void updateData(PointsGetResponse response) {
        TextViewPlus point = (TextViewPlus) rootView.findViewById(R.id.point);
        point.setText(response.getCountof().getAvailablepoints() + "");
    }

    @Override
    public void onHideMediaController() {
        Log.d("onHideMediaController", "onHideMediaController");

        if (earnPointFragment != null) {
            Log.d("onHideMediaController", "fragmetn not bnull");
            earnPointFragment.onHidden();
//            earnPointFragment = null;
        }
    }


    private void ShowEarnPoint() {
        earnPointFragment = EarnPointFragment.Instance();
        earnPointFragment.setDefaultFragment(this);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.root_view_get_payd, earnPointFragment, String.valueOf(DefaultFragment.GetPayD.EarnPoint));
        fragmentTransaction.addToBackStack(String.valueOf(DefaultFragment.GetPayD.EarnPoint));
        fragmentTransaction.commit();

        if (earnPointFragment == null)
            Log.e("ShowEarnPoint", "null");
        else Log.e("ShowEarnPoint", "!= null");

    }

    private void ShowRedeemPoints(String value) {
        RedeemPointsFragment fragment = RedeemPointsFragment.Instance(value);
        fragment.setDefaultFragment(this);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.root_view_get_payd, fragment);
        fragmentTransaction.addToBackStack(String.valueOf(DefaultFragment.GetPayD.RedeemPoint));
        fragmentTransaction.commit();

    }

    private void ShowHairGame() {
        HairGameFragment fragment = HairGameFragment.Instance();
        fragment.setDefaultFragment(this);
        timmerChanger = fragment;
        fragment.setTimer(maxtime, tempObset);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.root_view_get_payd, fragment);
        fragmentTransaction.addToBackStack(String.valueOf(DefaultFragment.GetPayD.HairGame));
        fragmentTransaction.commit();

    }

    private void ShowWhereShip(RewardsObj rewardsObj) {
        WhereShipFragment fragment = WhereShipFragment.Instance(rewardsObj);
        fragment.setDefaultFragment(this);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.root_view_get_payd, fragment);
        fragmentTransaction.addToBackStack(String.valueOf(DefaultFragment.GetPayD.WHERESHIP));
        fragmentTransaction.commit();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgMenu:
                ((NewHomeActivity) getActivity()).openNavMenu();
                break;
            case R.id.earnpoint_view:
                ShowEarnPoint();
                break;
            case R.id.redeempoints_view:
                ShowRedeemPoints(((TextViewPlus) rootView.findViewById(R.id.point)).getText().toString());
                break;
            case R.id.hairgameview:
                ShowHairGame();
                break;

            case R.id.share:
                Animation animScale = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale);
                v.startAnimation(animScale);
                shareTextUrl();
                break;
            case R.id.coppy_to_clipboard:
                Animation animScale1 = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale);
                v.startAnimation(animScale1);
                coppyShareLinkToClipboard();
                break;
        }
    }


    private void coppyShareLinkToClipboard() {
        String string = PreferenceHelpers.getPreference(getContext(), "json_resource");

        if (string.equals("")) {
            requestGetNewResource(Session.getUsers().getResourcelocation().getLocation());
        } else {
            Gson gson = new Gson();
            resource = gson.fromJson(string, JsonResource.class);
        }
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Herimports", getShareLink(resource));
        clipboard.setPrimaryClip(clip);

        Toast.makeText(getContext(), "Copied link to clipboard", Toast.LENGTH_SHORT).show();
    }


    // Method to share either text or URL.
    private void shareTextUrl() {


        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        share.putExtra(Intent.EXTRA_TEXT, getShareLink(resource));

        startActivity(Intent.createChooser(share, "Share link!"));
    }


    private String getShareLink(JsonResource resource) {

        return resource.getUrls().getUrlsObj().getShareLink() + "/@" + Session.getUsers().getUser().getNickname();
    }

    /**
     * request get resource
     *
     * @param url
     */
    private void requestGetNewResource(String url) {

        PreferenceHelpers.setPreference(getContext(), "json_resource", "");

        APIService service = NetworkConnectionUtil.createApiService(getContext());

        Call<JsonResource> response = service.getResource(url);

        response.enqueue(new Callback<JsonResource>() {
            @Override
            public void onResponse(Call<JsonResource> call, Response<JsonResource> response) {
                try {

                    PreferenceHelpers.setPreference(getContext(), "resource_version", Session.getUsers().getResourcelocation().getLastupdate() + "");
                    resource = response.body();

                    Session.setJsonResource(resource);
                    Gson gson = new Gson();
                    PreferenceHelpers.setPreference(getContext(), "json_resource", gson.toJson(resource).toString());

                    if (resource != null && resource.getGames() != null &&
                            resource.getGames().getGamesItemList() != null &&
                            resource.getGames().getGamesItemList().size() > 0) {
                        try {
                            long current = System.currentTimeMillis();
                            long minTime = System.currentTimeMillis();


                            for (int i = 0; i < resource.getGames().getGamesItemList().size(); i++) {
                                long timeRanger = resource.getGames().getGamesItemList().get(i).getStartDate() - current;
                                if (timeRanger > 0) {
                                    minTime = Math.min(minTime, timeRanger);
                                    tempObset = i;
                                }
                            }

                            if (tempObset != -1) {
                                maxtime = minTime;
                                int hours = (int) maxtime / (60 * 60000);
                                int min = (int) (maxtime - hours * 60 * 60000) / 60000;
                                int sec = (int) (maxtime - hours * 60 * 60000 - min * 60000) / 1000;
                                startCountDown(hours, min, sec);

                            } else
                                startCountDown(00, 00, 00);


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonResource> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                t.printStackTrace();
            }
        });
    }


    @Override
    public void popback(@Nullable String keycode) {

    }

    @Override
    public void popback() {

        if (earnPointFragment != null)
            earnPointFragment = null;

        getChildFragmentManager().popBackStack();
    }

    @Override
    public void addFragment(Object type, Object value) {
        if (type instanceof GetPayD)
            switch ((GetPayD) type) {
                case WHERESHIP:
                    ShowWhereShip((RewardsObj) value);
                    break;
            }
    }
}
