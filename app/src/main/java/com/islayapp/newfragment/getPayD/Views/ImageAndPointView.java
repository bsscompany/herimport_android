package com.islayapp.newfragment.getPayD.Views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.islayapp.R;

/**
 * Created by DINO on 29/04/2016.
 */
public class ImageAndPointView extends RelativeLayout {
    ImageView imageView;
    TextView pointview;
    int width, height, points;

    public ImageAndPointView(Context context) {
        super(context);
        init(context);
    }

    public ImageAndPointView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ImageAndPointView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflate.inflate(R.layout.layout_imageview_and_point, this, true);
        imageView = (ImageView) view.findViewById(R.id.avatar);
        pointview = (TextView) view.findViewById(R.id.points);
        // arrangementChild();
    }

    private void arrangementChild() {
        if (imageView.getDrawable() != null) {

            int imgw = imageView.getDrawable().getIntrinsicWidth();
            int imgh = imageView.getDrawable().getIntrinsicHeight();
            Log.e("w - h", width + " " + height);
            Log.e("iw - ih", imgw + " " + imgh);
            if (width < height) {
                int h = width * imgh / imgw;
                Log.e("new height", h + "");
                RelativeLayout.LayoutParams layout = new RelativeLayout.LayoutParams(width, h);
                layout.addRule(RelativeLayout.CENTER_HORIZONTAL);
                imageView.setLayoutParams(layout);
            } else {
                int w = height * imgw / imgh;
                Log.e("new width", w + "");
                RelativeLayout.LayoutParams layout = new RelativeLayout.LayoutParams(w, height);
                layout.addRule(RelativeLayout.CENTER_HORIZONTAL);
                imageView.setLayoutParams(layout);
            }
        }
    }

    public void setPoints(int points) {
        this.points = points;
        this.pointview.setText(String.valueOf(points) + "\npoints");
    }

    public int getPoints() {
        return points;
    }

    public void setImageResource(int id) {
        imageView.setImageResource(id);
        arrangementChild();
    }

    public void setImageBitmap(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
        arrangementChild();
    }

    public void setImageDrawable(Drawable drawable) {
        imageView.setImageDrawable(drawable);
        arrangementChild();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        Log.e("onLayout", l + " " + t + " " + r + " " + b);
        if (width != (r - l) && height != (b - t)){
            width = r - l;
        height = b - t;
        arrangementChild();
    }
    }
}

