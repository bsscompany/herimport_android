package com.islayapp.newfragment.getPayD.Views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.islayapp.R;

/**
 * Created by DINO on 22/04/2016.
 */
public class PointLeftToRedeemView extends RelativeLayout {
    ProgressBar progress;
    TextView points, btnClaimPrize;
    View img, text;

    public PointLeftToRedeemView(Context context) {
        super(context);
        init(context);
    }

    public PointLeftToRedeemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        getProgress(context, attrs);
    }

    public PointLeftToRedeemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        getProgress(context, attrs);
    }

    private void getProgress(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PointLeftToRedeemView);
        int p = a.getInt(R.styleable.PointLeftToRedeemView_progressRedeem, 0);
        a.recycle();
        setProgress(p);
    }

    private void init(Context context) {
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflate.inflate(R.layout.layout_point_left_to_redeem, this, true);
        progress = (ProgressBar) view.findViewById(R.id.progress_bar);
        points = (TextView) view.findViewById(R.id.point_left);
        img = view.findViewById(R.id.img_tick);
        text = view.findViewById(R.id.text_ready);
        btnClaimPrize = (TextView) view.findViewById(R.id.btnClaimPrize);
        btnClaimPrize.setEnabled(false);
    }

    public void setMax(int point) {
        progress.setMax(point);
    }

    public void setProgress(int current) {
        progress.setProgress(current);
        if (current >= progress.getMax()) {
            btnClaimPrize.setEnabled(true);
            progress.setVisibility(INVISIBLE);
            points.setVisibility(INVISIBLE);
            img.setVisibility(VISIBLE);
            text.setVisibility(VISIBLE);
        } else {
            btnClaimPrize.setEnabled(false);
            progress.setVisibility(VISIBLE);
            points.setVisibility(VISIBLE);
            img.setVisibility(INVISIBLE);
            text.setVisibility(INVISIBLE);

            int temp = Math.abs(current -  progress.getMax());

            points.setText(temp + " " + getResources().getString(R.string.points_left_to_redeem));
        }
    }

    public void setOnClickClaimPizze(OnClickListener onClickClaimPizze) {
        btnClaimPrize.setOnClickListener(onClickClaimPizze);
    }

    public int getProgress() {
        return progress.getProgress();
    }

    public int getMax() {
        return progress.getMax();
    }
}
