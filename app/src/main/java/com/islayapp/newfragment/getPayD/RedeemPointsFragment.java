package com.islayapp.newfragment.getPayD;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.LambdaInterface;
import com.amazonaws.models.CommonRequest;
import com.amazonaws.models.LambdaJsonBinder;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.core.APIService;
import com.google.gson.Gson;
import com.islayapp.R;
import com.islayapp.adapter.PointLogAdapter;
import com.islayapp.adapter.RedeemPointUserAdapter;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.PointsGetResponse;
import com.islayapp.models.awsmodels.user.JsonResource;
import com.islayapp.models.awsmodels.user.RewardsObj;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.newfragment.getPayD.Views.PagerSlidingTabStrip;
import com.islayapp.util.Constants;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Session;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DINO on 21/04/2016.
 */
public class RedeemPointsFragment extends BaseFragment {

    private DefaultFragment defaultFragment;

    public static RedeemPointsFragment Instance(String value) {
        RedeemPointsFragment fragment = new RedeemPointsFragment();
        Bundle args = new Bundle();
        args.putString("total_point", value);
        fragment.setArguments(args);
        return fragment;
    }

    private static final String TAG = DefaultFragment.class.getName();
    private List<PointsGetResponse.Datum> data = new ArrayList<PointsGetResponse.Datum>();
    private RedeemPointFragmentAdapter adapterViewPager;
    private String totalPoint = "0";
    private JsonResource resource;
    List<RewardsObj> rewardList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        totalPoint = getArguments().getString("total_point");

        String string = PreferenceHelpers.getPreference(getContext(), "json_resource");

        if (string.equals("")) {
            requestGetNewResource(Session.getUsers().getResourcelocation().getLocation());
        } else {
            Gson gson = new Gson();
            resource = gson.fromJson(string, JsonResource.class);
            rewardList.clear();
            rewardList.addAll(resource.getRewards().getRewardsObjList());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_payd_redeem_points, container, false);

        final ViewPager pager = (ViewPager) rootView.findViewById(R.id.pager);

        adapterViewPager = new RedeemPointFragmentAdapter(getContext(), defaultFragment, data, rewardList);
        pager.setAdapter(adapterViewPager);

        PagerSlidingTabStrip slidig = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        slidig.setViewPager(pager);
        slidig.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    adapterViewPager.updatePointLog(pager.getChildAt(1), data);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TextViewPlus point = (TextViewPlus) rootView.findViewById(R.id.point);
        point.setText(totalPoint);


        ImageView share = (ImageView) rootView.findViewById(R.id.share);
        share.setOnClickListener(this);


        ImageView coppy_to_clipboard = (ImageView) rootView.findViewById(R.id.coppy_to_clipboard);
        coppy_to_clipboard.setOnClickListener(this);

        return rootView;
    }

    @Override
    protected void initTopbar() {
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText(getResources().getString(R.string.get_payd));
        rootView.findViewById(R.id.backOnQuoteFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                defaultFragment.popback();
            }
        });
    }

    public void setDefaultFragment(DefaultFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }

    class RedeemPointFragmentAdapter extends PagerAdapter {
        private Context context;
        private List<PointsGetResponse.Datum> mListPointLog = new ArrayList<>();
        private List<RewardsObj> mRewardList = new ArrayList<>();
        private DefaultFragment defaultFragment;

        public RedeemPointFragmentAdapter(Context context, DefaultFragment defaultFragment, List<PointsGetResponse.Datum> listPointLog, List<RewardsObj> rewardList) {
            this.context = context;
            mListPointLog = listPointLog;
            mRewardList = rewardList;
            this.defaultFragment = defaultFragment;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            LayoutInflater inflater = LayoutInflater.from(context);
            ViewGroup layout = null;
            if (position == 0) {

                layout = (ViewGroup) inflater.inflate(R.layout.fragment_get_payd_redeem_points_default, collection, false);

                collection.addView(layout);

                RedeemPointUserAdapter adapter = new RedeemPointUserAdapter(getContext(), defaultFragment, mRewardList);

                ViewPager paper = (ViewPager) layout.findViewById(R.id.pager);
                paper.setAdapter(adapter);

                CirclePageIndicator titleIndicator = (CirclePageIndicator) layout.findViewById(R.id.paper_indicator);
                titleIndicator.setViewPager(paper);

            } else {
                layout = (ViewGroup) inflater.inflate(R.layout.fragment_get_payd_redeem_points_log, collection, false);
                updatePointLog(layout, mListPointLog);
                collection.addView(layout);
            }
            return layout;
        }


        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();


        }

        /**
         * update point log
         */
        private void updatePointLog(View viewgroup, List<PointsGetResponse.Datum> mListPointLog) {
            ListView listView5 = (ListView) viewgroup.findViewById(R.id.listView5);
            PointLogAdapter adapter = new PointLogAdapter(getContext(), R.layout.item_get_payd_list_points_log, mListPointLog);
            listView5.setAdapter(adapter);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "REDEEM POINTS";
                case 1:
                    return "POINTS LOG";
            }
            return "";
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        CommonRequest request = new CommonRequest();
        request.setPagesize(Constants.PAGE_SIZE);
        request.setPagenumber(1);
        request.setUserid(Session.getUsers().getUser().getId());
        requestUserPoints(request, false);

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.share:
                Animation animScale = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale);
                v.startAnimation(animScale);
                shareTextUrl();
                break;
            case R.id.coppy_to_clipboard:
                Animation animScale1 = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale);
                v.startAnimation(animScale1);
                coppyShareLinkToClipboard();
                break;
        }
    }


    private void coppyShareLinkToClipboard() {
        String string = PreferenceHelpers.getPreference(getContext(), "json_resource");

        if (string.equals("")) {
            requestGetNewResource(Session.getUsers().getResourcelocation().getLocation());
        } else {
            Gson gson = new Gson();
            resource = gson.fromJson(string, JsonResource.class);
        }
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Herimports", getShareLink(resource));
        clipboard.setPrimaryClip(clip);

        Toast.makeText(getContext(), "Copied link to clipboard", Toast.LENGTH_SHORT).show();
    }


    // Method to share either text or URL.
    private void shareTextUrl() {

        String string = PreferenceHelpers.getPreference(getContext(), "json_resource");

        if (string.equals("")) {
            requestGetNewResource(Session.getUsers().getResourcelocation().getLocation());
        } else {
            Gson gson = new Gson();
            resource = gson.fromJson(string, JsonResource.class);
            Session.setJsonResource(resource);

        }

        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        share.putExtra(Intent.EXTRA_TEXT, getShareLink(resource));

        startActivity(Intent.createChooser(share, "Share link!"));
    }


    private String getShareLink(JsonResource resource) {

        return resource.getUrls().getUrlsObj().getShareLink() + "/@" + Session.getUsers().getUser().getNickname();
    }


    private Dialog dialog;

    private void requestUserPoints(final CommonRequest request, boolean isShowDialog) {

        Gson gson = new Gson();
        Log.d(TAG, " Request get point:  " + gson.toJson(request).toString());

        if (isShowDialog)
            dialog = DialogUtil.showDialog(getContext());
        Log.d(TAG, "LAMBDA: Sending Data");
        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        final LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());
        final LambdaInterface myInterface = factory.build(LambdaInterface.class, new LambdaJsonBinder());
        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<CommonRequest, Void, PointsGetResponse>() {

            @Override
            protected PointsGetResponse doInBackground(CommonRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.PointsGet(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(PointsGetResponse result) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    return;
                } else {
                    Log.d(TAG, "LAMBDA: Response from request have data");
                    Log.d(TAG, "response point® " + result.getCountof().getAvailablepoints());

                    data.clear();
                    data.addAll(result.getData());
                    adapterViewPager.notifyDataSetChanged();

                    updateData(result);
                    Session.setCurrentPoint(result.getCountof().getAvailablepoints());

                    TextViewPlus point = (TextViewPlus) rootView.findViewById(R.id.point);
                    point.setText(Session.getCurrentPoint() + "");
                    if (result.getValid().equals("Y")) {

                    }
                }
            }
        }.execute(request);
    }

    private void updateData(PointsGetResponse response) {
        TextViewPlus point = (TextViewPlus) rootView.findViewById(R.id.point);
        point.setText(response.getCountof().getAvailablepoints() + "");
    }

    /**
     * request get resource
     *
     * @param url
     */
    private void requestGetNewResource(String url) {

        PreferenceHelpers.setPreference(getContext(), "json_resource", "");

        APIService service = NetworkConnectionUtil.createApiService(getContext());

        Call<JsonResource> response = service.getResource(url);

        response.enqueue(new Callback<JsonResource>() {
            @Override
            public void onResponse(Call<JsonResource> call, Response<JsonResource> response) {
                try {

                    PreferenceHelpers.setPreference(getContext(), "resource_version", Session.getUsers().getResourcelocation().getLastupdate() + "");
                    JsonResource jsonResource = response.body();
                    Session.setJsonResource(jsonResource);

                    rewardList.clear();
                    rewardList.addAll(jsonResource.getRewards().getRewardsObjList());

                    adapterViewPager.notifyDataSetChanged();

                    Gson gson = new Gson();
                    PreferenceHelpers.setPreference(getContext(), "json_resource", gson.toJson(jsonResource).toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonResource> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                t.printStackTrace();
            }
        });
    }


}

