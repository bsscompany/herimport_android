package com.islayapp.newfragment.getPayD.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.islayapp.R;

/**
 * Created by DINO on 20/04/2016.
 */
public class Points_Earned_View extends View {
    int height;
    int width;
    float padding;

    public Points_Earned_View(Context context) {
        super(context);
        padding = getResources().getDimension(R.dimen.get_payd_radius);
    }

    public Points_Earned_View(Context context, AttributeSet attrs) {
        super(context, attrs);
        padding = getResources().getDimension(R.dimen.get_payd_radius);
    }

    public Points_Earned_View(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        padding = getResources().getDimension(R.dimen.get_payd_radius);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawRadius(canvas, 0);
        drawRadius(canvas, (int) padding);
        drawRadius(canvas, (int) padding * 2);
    }

    private void drawRadius(Canvas canvas, int x) {
        GradientDrawable mDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[]{ContextCompat.getColor(getContext(), R.color.start_color), ContextCompat.getColor(getContext(), R.color.end_color)});
        mDrawable.setShape(GradientDrawable.RECTANGLE);
        Rect mRect = new Rect(0, 0, width - x, height);
        mDrawable.setBounds(mRect);
        mDrawable.setCornerRadii(new float[]{padding, padding,
                padding, height / 2, padding, height / 2, padding, padding});
        mDrawable.draw(canvas);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {width = r - l;
        height = b - t;
        invalidate();
        super.onLayout(changed, l, t, r, b);
    }
}
