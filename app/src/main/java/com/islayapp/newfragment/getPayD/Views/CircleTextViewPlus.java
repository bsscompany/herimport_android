package com.islayapp.newfragment.getPayD.Views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;

/**
 * Created by DINO on 22/04/2016.
 */
public class CircleTextViewPlus extends TextViewPlus {

    private int dis;
    private Paint paint;

    public CircleTextViewPlus(Context context) {
        super(context);
    }

    public CircleTextViewPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        getColor(context, attrs);
    }

    public CircleTextViewPlus(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getColor(context, attrs);
    }

    private void getColor(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleTextViewPlus);
        int color = a.getColor(R.styleable.CircleTextViewPlus_colorCircleTVP, Color.TRANSPARENT);
        a.recycle();
        paint = new Paint();
        paint.setColor(color);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (paint != null)
            canvas.drawCircle(dis / 2, dis / 2, dis / 2, paint);
        super.onDraw(canvas);
    }


    @Override
    protected void onMeasure(int width, int height) {
        super.onMeasure(width, height);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (measuredWidth > measuredHeight) {
            dis = measuredWidth;
            setMeasuredDimension(measuredWidth, measuredWidth);
        } else {
            dis = measuredHeight;
            setMeasuredDimension(measuredHeight, measuredHeight);

        }

    }
}
