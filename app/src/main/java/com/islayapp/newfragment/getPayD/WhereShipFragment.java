package com.islayapp.newfragment.getPayD;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.models.RedeemPointsResponse;
import com.amazonaws.models.RequestReward;
import com.amazonaws.models.Shiptoaddress;
import com.core.APIService;
import com.google.gson.Gson;
import com.islayapp.R;
import com.islayapp.adapter.CountrySpinnerAdapter;
import com.islayapp.adapter.StateSpinnerAdapter;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.user.RewardsObj;
import com.islayapp.models.shop.CountryItem;
import com.islayapp.models.shop.CountryModel;
import com.islayapp.models.shop.StateItem;
import com.islayapp.models.shop.StateModel;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.Session;

import org.apache.commons.codec.binary.Base64;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DINO on 27/04/2016.
 */
public class WhereShipFragment extends BaseFragment {
    private static final String TAG = WhereShipFragment.class.getName();
    private DefaultFragment defaultFragment;

    public static WhereShipFragment Instance(RewardsObj rewardsObj) {
        WhereShipFragment fragment = new WhereShipFragment();
        Bundle args = new Bundle();
        args.putSerializable("rewardsObj", rewardsObj);
        fragment.setArguments(args);
        return fragment;
    }

    private List<CountryItem> countryItemList = new ArrayList<>();
    private List<StateItem> stateItemList = new ArrayList<>();
    private Spinner countrySpinner, spinnerState;
    private CountrySpinnerAdapter countrySpinnerAdapter;
    private EditText edittext_state;
    private RewardsObj rewardsObj;
    private TextViewPlus btn_submit;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rewardsObj = (RewardsObj) getArguments().getSerializable("rewardsObj");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_payd_where_ship, container, false);

        countrySpinner = (Spinner) rootView.findViewById(R.id.spinner_country);
        spinnerState = (Spinner) rootView.findViewById(R.id.state_spinner);
        edittext_state = (EditText) rootView.findViewById(R.id.edittext_state);

        btn_submit = (TextViewPlus) rootView.findViewById(R.id.view2);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getShippingAddress();

            }
        });

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryItem item = countryItemList.get(position);
                getStateByCountryCode(item.getCountryCode());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return rootView;
    }

    @Override
    protected void initTopbar() {
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText(getResources().getString(R.string.get_payd));
        rootView.findViewById(R.id.backOnQuoteFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                defaultFragment.popback();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        getCountryList();

    }


    public void setDefaultFragment(DefaultFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }


    private void getShippingAddress() {

        //first name
        EditText shipping_name = (EditText) rootView.findViewById(R.id.shipping_name);
        String name = shipping_name.getText().toString();
        if (name.length() == 0) {
            shipping_name.setError("Name is required!");
            return;
        }

        // address
        EditText shipping_address = (EditText) rootView.findViewById(R.id.shipping_address);
        String address = shipping_address.getText().toString();
        if (address.length() == 0) {
            shipping_address.setError(getResources().getString(R.string.address1_hint) + " is required!");
            return;
        }

        // city
        EditText shipping_city = (EditText) rootView.findViewById(R.id.shipping_city);
        String city = shipping_city.getText().toString();
        if (city.length() == 0) {
            shipping_city.setError(getResources().getString(R.string.city_hint) + " is required!");
            return;
        }


        String countryCode = countryItemList.get(countrySpinner.getSelectedItemPosition()).getCountryCode();

        String stateCode = "";
        //State
        if (spinnerState.isShown()) {
            stateCode = stateItemList.get(spinnerState.getSelectedItemPosition()).getStateCode();
        }

        EditText edittext_state = (EditText) rootView.findViewById(R.id.edittext_state);
        if (edittext_state.isShown()) {
            stateCode = edittext_state.getText().toString();
            if (stateCode.length() == 0) {
                edittext_state.setError(getResources().getString(R.string.state_hint) + " is required!");
                return;
            }

        }

        //zipcode
        EditText shipping_zipcode = (EditText) rootView.findViewById(R.id.shipping_zipcode);
        String zipcode = shipping_zipcode.getText().toString();
        if (zipcode.length() == 0) {
            shipping_zipcode.setError(getResources().getString(R.string.postal_zip_code_hint) + " is required!");
            return;
        }

        EditText shipping_phone = (EditText) rootView.findViewById(R.id.shipping_phone);
        String telephone = shipping_phone.getText().toString();
        if (telephone.length() == 0) {
            shipping_phone.setError(getResources().getString(R.string.telephone_hint) + " is required!");
            return;
        }

        RequestReward requestReward = new RequestReward();
        requestReward.setItemid(rewardsObj.getId());
        requestReward.setUserid(Session.getUsers().getUser().getId());

        Shiptoaddress shiptoaddress = new Shiptoaddress();

        shiptoaddress.setName(name);
        shiptoaddress.setAddress1(address);
        shiptoaddress.setAddress2("");
        shiptoaddress.setCity(city);
        shiptoaddress.setState(stateCode);
        shiptoaddress.setZip(zipcode);
//        shiptoaddress.setPhone(telephone);
        requestReward.setShiptoaddress(shiptoaddress);

        Gson gson = new Gson();
        String json = gson.toJson(requestReward);
        requestRedeemPoint(json);

    }


    /**
     * get list country
     */
    private void getCountryList() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<CountryModel> response = service.getListCountryData();
        response.enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(Call<CountryModel> call, Response<CountryModel> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    CountryModel countryModel = response.body();
                    if (countryModel.getStatus() == 1) {
                        countryItemList = countryModel.getCountryItems();
                        countrySpinnerAdapter = new CountrySpinnerAdapter(getContext(), R.layout.layout_spinner_country_state, countryItemList);
                        countrySpinner.setAdapter(countrySpinnerAdapter);

                        for (int i = 0; i < countryItemList.size(); i++) {
                            if (countryItemList.get(i).getCountryCode().equals("US")) {
                                countrySpinner.setSelection(i);
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CountryModel> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMess(getContext(), "Error request");
            }
        });
    }


    /**
     * get state
     *
     * @param countryCode
     */
    private void getStateByCountryCode(String countryCode) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<StateModel> response = service.getListState(countryCode);
        response.enqueue(new Callback<StateModel>() {
            @Override
            public void onResponse(Call<StateModel> call, Response<StateModel> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    StateModel stateModel = response.body();
                    if (stateModel.getFieldType() == 1) {
                        stateItemList = stateModel.getStateItemList();
                        StateSpinnerAdapter stateSpinnerAdapter = new StateSpinnerAdapter(getContext(), R.layout.layout_spinner_country_state, stateItemList);
                        spinnerState.setAdapter(stateSpinnerAdapter);
                        spinnerState.setVisibility(View.VISIBLE);
                        edittext_state.setVisibility(View.GONE);
                        ImageView imgv = (ImageView) rootView.findViewById(R.id.dropdown_spinner);
                        imgv.setVisibility(View.VISIBLE);
                    } else {
                        spinnerState.setVisibility(View.GONE);
                        edittext_state.setVisibility(View.VISIBLE);
                        ImageView imgv = (ImageView) rootView.findViewById(R.id.dropdown_spinner);
                        imgv.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<StateModel> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMess(getContext(), "Error request");
            }
        });
    }


    /**
     * @param requestRedeem
     */
    private void requestRedeemPoint(String requestRedeem) {

        final Dialog progress = DialogUtil.showDialog(getContext());

        Log.d(TAG, "get list hostest product" + requestRedeem.toString());
        APIService service = NetworkConnectionUtil.createApiService(getContext());

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), requestRedeem);

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<RedeemPointsResponse> response = service.requestRedeemPoint(String.format(basic, string), body);

        response.enqueue(new Callback<RedeemPointsResponse>() {
            @Override
            public void onResponse(Call<RedeemPointsResponse> call, Response<RedeemPointsResponse> response) {
                if (progress != null)
                    progress.dismiss();
                try {
                    RedeemPointsResponse redeemPointsResponse = response.body();

                    if (redeemPointsResponse.getSuccess()) {
                        Session.setCurrentPoint(redeemPointsResponse.getRemainingpoints());
                        Log.d(TAG, "response redeem: " + redeemPointsResponse.getRemainingpoints());
                        Log.d(TAG, "response redeem: " + redeemPointsResponse.getSuccess());
                        defaultFragment.popback();
                        Toast.makeText(getContext(), "Redeem Success!", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getContext(), redeemPointsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

//                    if (responseUsers.getValid().equals("Y")) {
//
//                    } else {
////                        Toast.makeText(getBaseContext(), "Login failed!", Toast.LENGTH_SHORT).show();
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RedeemPointsResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                if (progress != null)
                    progress.dismiss();
                t.printStackTrace();
            }
        });
    }
}
