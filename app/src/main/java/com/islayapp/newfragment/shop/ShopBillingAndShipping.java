package com.islayapp.newfragment.shop;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.core.APIService;
import com.islayapp.R;
import com.islayapp.adapter.CountrySpinnerAdapter;
import com.islayapp.adapter.StateSpinnerAdapter;
import com.islayapp.customview.CircleView;
import com.islayapp.customview.EditTextPlus;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.shop.CountryItem;
import com.islayapp.models.shop.CountryModel;
import com.islayapp.models.shop.ResponseAddress;
import com.islayapp.models.shop.SaveObjectResult;
import com.islayapp.models.shop.StateItem;
import com.islayapp.models.shop.StateModel;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.PreferenceHelpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_SHIPPING_METHOD_AND_PAYMENT;

/**
 * Created by maxo on 3/4/16.
 */
public class ShopBillingAndShipping extends BaseFragment {
    // newInstance constructor for creating fragment with arguments
    public static ShopBillingAndShipping newInstance() {
        ShopBillingAndShipping fragmentFirst = new ShopBillingAndShipping();
        Bundle args = new Bundle();
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = ShopBillingAndShipping.class.getName();
    private RadioGroup radioGroup;
    private RadioButton yes, no;

    private List<CountryItem> countryItemList = new ArrayList<>();
    private List<StateItem> stateItemList = new ArrayList<>();
    private List<StateItem> stateItemListShipping = new ArrayList<>();

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        productID = getArguments().getInt("product_id", 0);

    }

    // Inflate the view for the fragment based on layout_try_hair_bottombar XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shop_billing_and_shipping, container, false);

        CircleView myCartCircleView = (CircleView) rootView.findViewById(R.id.my_cart_circle_my_cart);
        myCartCircleView.setSelect(true);

        TextViewPlus shipping_and_billing = (TextViewPlus) rootView.findViewById(R.id.shipping_and_billing);
        shipping_and_billing.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));

        CircleView shippingAndPaymentCircleView = (CircleView) rootView.findViewById(R.id.circle_shipping_and_billing);
        shippingAndPaymentCircleView.setSelect(true);

        Button btn_billing_and_shipping_continue = (Button) rootView.findViewById(R.id.btn_billing_and_shipping_continue);
        btn_billing_and_shipping_continue.setOnClickListener(this);

        radioGroup = (RadioGroup) rootView.findViewById(R.id.group_shipping_yes_no);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                LinearLayout shipping_infomation_layout = (LinearLayout) rootView.findViewById(R.id.shipping_infomation_layout);
                if (checkedId == R.id.yes) {
                    shipping_infomation_layout.setVisibility(View.GONE);
                } else if (checkedId == R.id.no) {
                    shipping_infomation_layout.setVisibility(View.VISIBLE);
                }
            }

        });

        initBillingInfomation();
        initShippingInfomation();

        yes = (RadioButton) rootView.findViewById(R.id.yes);
        no = (RadioButton) rootView.findViewById(R.id.no);

        return rootView;
    }

    @Override
    protected void initTopbar() {
        ImageButton backOnQuoteFragment = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        ImageButton shop_card = (ImageButton) rootView.findViewById(R.id.shop_card);
        backOnQuoteFragment.setOnClickListener(this);
        title_topbar.setText(getString(R.string.billing_infomation));
        shop_card.setVisibility(View.INVISIBLE);
    }


    private Spinner countrySpinnerBilling, spinnerStateBilling;
    private CountrySpinnerAdapter countrySpinnerAdapter;
    private EditTextPlus billing_zipcode, billing_first_name, billing_last_name, billing_address, billing_address_2, billing_city, billing_email,
            billing_telephone, billing_state;

    private void initBillingInfomation() {
        countrySpinnerBilling = (Spinner) rootView.findViewById(R.id.spinner_country_billing);
        countrySpinnerBilling.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryItem item = countryItemList.get(position);
                getStateByCountryCode(item.getCountryCode(), 0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerStateBilling = (Spinner) rootView.findViewById(R.id.billing_spinner_state);
        billing_zipcode = (EditTextPlus) rootView.findViewById(R.id.billing_zipcode);
        billing_first_name = (EditTextPlus) rootView.findViewById(R.id.billing_first_name);
        billing_last_name = (EditTextPlus) rootView.findViewById(R.id.billing_last_name);
        billing_address = (EditTextPlus) rootView.findViewById(R.id.billing_address);
        billing_address_2 = (EditTextPlus) rootView.findViewById(R.id.billing_address_2);
        billing_city = (EditTextPlus) rootView.findViewById(R.id.billing_city);
        billing_email = (EditTextPlus) rootView.findViewById(R.id.billing_email);
        billing_telephone = (EditTextPlus) rootView.findViewById(R.id.billing_telephone);
        billing_state = (EditTextPlus) rootView.findViewById(R.id.billing_state);

        billing_zipcode.setOnEditorActionListener(createOnEditorActionListenerBilling());
        billing_first_name.setOnEditorActionListener(createOnEditorActionListenerBilling());
        billing_last_name.setOnEditorActionListener(createOnEditorActionListenerBilling());
        billing_address.setOnEditorActionListener(createOnEditorActionListenerBilling());
        billing_address_2.setOnEditorActionListener(createOnEditorActionListenerBilling());
        billing_city.setOnEditorActionListener(createOnEditorActionListenerBilling());
        billing_email.setOnEditorActionListener(createOnEditorActionListenerBilling());
        billing_telephone.setOnEditorActionListener(createOnEditorActionListenerBilling());
        billing_state.setOnEditorActionListener(createOnEditorActionListenerBilling());


        billing_zipcode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (billing_zipcode.getText().toString().length() == 5)
                        requestZipcodeAddress(billing_zipcode.getText().toString(), ((CountryItem) countrySpinnerBilling.getSelectedItem()).getCountryCode());
                }
            }
        });


    }


    private TextView.OnEditorActionListener createOnEditorActionListenerBilling() {
        return new EditTextPlus.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    getValueBilling();
                }
                return false;
            }
        };
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        // get country list
        if (countryItemList.size() == 0)
            getCountryList();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continue_shopping:
            case R.id.backOnQuoteFragment:
                defaultFragment.popback();
                break;
            case R.id.btn_billing_and_shipping_continue:
                Map<String, String> param = getValueBilling();
                if (yes.isChecked())
                    saveBilingAddress(param, null, true);
                else {
                    Map<String, String> param1 = getValueShipping();
                    saveBilingAddress(param, param1, false);

                }
                break;
        }
    }

    private Map<String, String> getValueBilling() {
        Map<String, String> param = new HashMap<>();

        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
        param.put("quoteId", quoteId);

        String countryCode = countryItemList.get(countrySpinnerBilling.getSelectedItemPosition()).getCountryCode();
        param.put("countryCode", countryCode);

        //zipcode
        String zipcode = billing_zipcode.getText().toString();
        if (zipcode.length() == 0) {
            billing_zipcode.setError(getResources().getString(R.string.postal_zip_code_hint) + " is required!");
            return null;
        }
        param.put("postCode", zipcode);

        //first name
        String firstName = billing_first_name.getText().toString();
        if (firstName.length() == 0) {
            billing_first_name.setError(getResources().getString(R.string.first_name_hint) + " is required!");
            return null;
        }
        param.put("firstName", firstName);

        //last name
        String lastName = billing_last_name.getText().toString();
        if (lastName.length() == 0) {
            billing_last_name.setError(getResources().getString(R.string.last_name_hint) + " is required!");
            return null;
        }
        param.put("lastName", lastName);

        //last name
        String address = billing_address.getText().toString();
        if (address.length() == 0) {
            billing_address.setError(getResources().getString(R.string.address1_hint) + " is required!");
            return null;
        }
        param.put("street", address);

        //last name
        String address2 = billing_address_2.getText().toString();
        param.put("street2", address2);

        //last name
        String city = billing_city.getText().toString();
        if (city.length() == 0) {
            billing_city.setError(getResources().getString(R.string.city_hint) + " is required!");
            return null;
        }
        param.put("city", city);

        if (spinnerStateBilling.isShown()) {
            param.put("stateCode", stateItemList.get(spinnerStateBilling.getSelectedItemPosition()).getStateCode());
        }

        //last name
        if (billing_state.isShown()) {
            String state = billing_state.getText().toString();
            if (state.length() == 0) {
                billing_state.setError(getResources().getString(R.string.state_hint) + " is required!");
                return null;
            }
            param.put("state", state);
        }

        String email = billing_email.getText().toString();
        if (email.length() == 0) {
            billing_email.setError(getResources().getString(R.string.email_address_hint) + " is required!");
            return null;
        }
        param.put("email", email);

        String telephone = billing_telephone.getText().toString();
        if (telephone.length() == 0) {
            billing_telephone.setError(getResources().getString(R.string.telephone_hint) + " is required!");
            return null;
        }
        param.put("telephone", telephone);

        return param;
    }


    private void saveBilingAddress(final Map<String, String> paramBilling, final Map<String, String> paramShipping, final boolean isShippingSameBilling) {
        if (paramBilling == null)
            return;
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());

        Log.d(TAG, "content paramBilling : " + paramBilling.toString());

        APIService service = NetworkConnectionUtil.createShopApiService(getContext());

        Call<SaveObjectResult> response = service.saveBillingAddress(paramBilling);
        response.enqueue(new Callback<SaveObjectResult>() {
            @Override
            public void onResponse(Call<SaveObjectResult> call, Response<SaveObjectResult> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    SaveObjectResult countryModel = response.body();
                    if (countryModel.getStatus() == 1) {
                        if (isShippingSameBilling)
                            saveShippingAddress(paramBilling);
                        else
                            saveShippingAddress(paramShipping);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<SaveObjectResult> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMess(getContext(), "Save billing error");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    private void requestZipcodeAddress(String zipcode, String countryCode) {

        APIService service = NetworkConnectionUtil.createGmapApiService(getContext());

        Call<ResponseAddress> response = service.requestGetAddressByZipcode(zipcode, countryCode);

        response.enqueue(new Callback<ResponseAddress>() {
            @Override
            public void onResponse(Call<ResponseAddress> call, Response<ResponseAddress> response) {

                ResponseAddress address = response.body();

                if (address.getStatus().equals("OK")) {
                    for (int i = 0; i < stateItemList.size(); i++) {

                        String shortName = "";
                        if (address.getResults().get(0).getAddressComponents().size() > 2)
                            shortName = address.getResults().get(0).getAddressComponents().get(address.getResults().get(0).getAddressComponents().size() - 2).getLongName();
                        else
                            return;

                        if (stateItemList.get(i).getStateLabel().toLowerCase().equals(shortName.toLowerCase())) {
                            spinnerStateBilling.setSelection(i);
                            shipping_spinner_state.setSelection(i);
                        }

                        billing_city.setText(address.getResults().get(0).getAddressComponents().get(address.getResults().get(0).getAddressComponents().size() - 3).getLongName());
                        shipping_city.setText(address.getResults().get(0).getAddressComponents().get(address.getResults().get(0).getAddressComponents().size() - 3).getLongName());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseAddress> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                t.printStackTrace();
            }
        });
    }


    private Spinner shipping_spinner_country, shipping_spinner_state;
    private EditTextPlus shipping_pos_code, shipping_first_name, shipping_last_name, shipping_address, shipping_address_2, shipping_city, shipping_email, shipping_telephone,
            shipping_state;

    private void initShippingInfomation() {
        shipping_spinner_country = (Spinner) rootView.findViewById(R.id.shipping_spinner_country);
        shipping_spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryItem item = countryItemList.get(position);
                getStateByCountryCode(item.getCountryCode(), 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        shipping_spinner_state = (Spinner) rootView.findViewById(R.id.shipping_spinner_state);

        shipping_pos_code = (EditTextPlus) rootView.findViewById(R.id.shipping_pos_code);
        shipping_first_name = (EditTextPlus) rootView.findViewById(R.id.shipping_first_name);
        shipping_last_name = (EditTextPlus) rootView.findViewById(R.id.shipping_last_name);
        shipping_address = (EditTextPlus) rootView.findViewById(R.id.shipping_address);
        shipping_address_2 = (EditTextPlus) rootView.findViewById(R.id.shipping_address_2);
        shipping_city = (EditTextPlus) rootView.findViewById(R.id.shipping_city);
        shipping_email = (EditTextPlus) rootView.findViewById(R.id.shipping_email);
        shipping_telephone = (EditTextPlus) rootView.findViewById(R.id.shipping_telephone);
        shipping_state = (EditTextPlus) rootView.findViewById(R.id.shipping_state);

        shipping_pos_code.setOnEditorActionListener(createOnEditorActionListenerShipping());
        shipping_first_name.setOnEditorActionListener(createOnEditorActionListenerShipping());
        shipping_last_name.setOnEditorActionListener(createOnEditorActionListenerShipping());
        shipping_address.setOnEditorActionListener(createOnEditorActionListenerShipping());
        shipping_address_2.setOnEditorActionListener(createOnEditorActionListenerShipping());
        shipping_city.setOnEditorActionListener(createOnEditorActionListenerShipping());
        shipping_email.setOnEditorActionListener(createOnEditorActionListenerShipping());
        shipping_telephone.setOnEditorActionListener(createOnEditorActionListenerShipping());
        shipping_state.setOnEditorActionListener(createOnEditorActionListenerShipping());

    }

    private TextView.OnEditorActionListener createOnEditorActionListenerShipping() {
        return new EditTextPlus.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    getValueShipping();
                }
                return false;
            }
        };
    }

    private Map<String, String> getValueShipping() {
        Map<String, String> param = new HashMap<>();

        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
        param.put("quoteId", quoteId);

        String countryCode = countryItemList.get(shipping_spinner_country.getSelectedItemPosition()).getCountryCode();
        param.put("countryCode", countryCode);

        //zipcode
        String zipcode = shipping_pos_code.getText().toString();
        if (zipcode.length() == 0) {
            shipping_pos_code.setError(getResources().getString(R.string.postal_zip_code_hint) + " is required!");
            return null;
        }
        param.put("postCode", zipcode);

        //first name
        String firstName = shipping_first_name.getText().toString();
        if (firstName.length() == 0) {
            shipping_first_name.setError(getResources().getString(R.string.first_name_hint) + " is required!");
            return null;
        }
        param.put("firstName", firstName);

        //last name
        String lastName = shipping_last_name.getText().toString();
        if (lastName.length() == 0) {
            shipping_last_name.setError(getResources().getString(R.string.last_name_hint) + " is required!");
            return null;
        }
        param.put("lastName", lastName);

        //last name
        String address = shipping_address.getText().toString();
        if (address.length() == 0) {
            shipping_address.setError(getResources().getString(R.string.address1_hint) + " is required!");
            return null;
        }
        param.put("street", address);

        //last name
        String address2 = shipping_address_2.getText().toString();
        param.put("street2", address2);

        //last name
        String city = shipping_city.getText().toString();
        if (city.length() == 0) {
            shipping_city.setError(getResources().getString(R.string.city_hint) + " is required!");
            return null;
        }
        param.put("city", city);

        if (shipping_spinner_state.isShown()) {
            param.put("stateCode", stateItemListShipping.get(shipping_spinner_state.getSelectedItemPosition()).getStateCode());
        }

        //last name
        if (shipping_state.isShown()) {
            String state = shipping_state.getText().toString();
            if (state.length() == 0) {
                shipping_state.setError(getResources().getString(R.string.state_hint) + " is required!");
                return null;
            }
            param.put("state", state);
        }

        String email = shipping_email.getText().toString();
        if (email.length() == 0) {
            shipping_email.setError(getResources().getString(R.string.email_address_hint) + " is required!");
            return null;
        }
        param.put("email", email);

        String telephone = shipping_telephone.getText().toString();
        if (telephone.length() == 0) {
            shipping_telephone.setError(getResources().getString(R.string.telephone_hint) + " is required!");
            return null;
        }
        param.put("telephone", telephone);

        return param;
    }

    private void saveShippingAddress(Map<String, String> param) {
        if (param == null)
            return;
        Log.d(TAG, "content param shipping : " + param.toString());

        final Dialog dialog = DialogUtil.showProgressDialog(getContext());

        APIService service = NetworkConnectionUtil.createShopApiService(getContext());


        Call<SaveObjectResult> response = service.saveShippingddress(param);
        response.enqueue(new Callback<SaveObjectResult>() {
            @Override
            public void onResponse(Call<SaveObjectResult> call, Response<SaveObjectResult> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    SaveObjectResult countryModel = response.body();
                    if (countryModel.getStatus() == 1) {
                        defaultFragment.addNewFragment(FRAGMENT_SHOP_SHIPPING_METHOD_AND_PAYMENT, "");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<SaveObjectResult> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMess(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    /**
     * get list country
     */
    private void getCountryList() {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());

        APIService service = NetworkConnectionUtil.createShopApiService(getContext());

        Call<CountryModel> response = service.getListCountryData();
        response.enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(Call<CountryModel> call, Response<CountryModel> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    CountryModel countryModel = response.body();
                    if (countryModel.getStatus() == 1) {
                        countryItemList = countryModel.getCountryItems();
                        countrySpinnerAdapter = new CountrySpinnerAdapter(getContext(), R.layout.layout_spinner_country_state, countryItemList);
                        countrySpinnerBilling.setAdapter(countrySpinnerAdapter);
                        shipping_spinner_country.setAdapter(countrySpinnerAdapter);
                        for (int i = 0; i < countryItemList.size(); i++) {
                            if (countryItemList.get(i).getCountryCode().equals("US")) {
                                countrySpinnerBilling.setSelection(i);
                                shipping_spinner_country.setSelection(i);
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<CountryModel> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMess(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    /**
     * get state
     *
     * @param countryCode
     */
    private void getStateByCountryCode(String countryCode, final int code) {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());

        APIService service = NetworkConnectionUtil.createShopApiService(getContext());

        Call<StateModel> response = service.getListState(countryCode);
        response.enqueue(new Callback<StateModel>() {
            @Override
            public void onResponse(Call<StateModel> call, Response<StateModel> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    StateModel stateModel = response.body();
                    if (stateModel.getStatus() == 1) {
                        switch (code) {
                            case 0: // state for billing
                                if (stateModel.getFieldType() == 1) {
                                    stateItemList = stateModel.getStateItemList();
                                    StateSpinnerAdapter stateSpinnerAdapter = new StateSpinnerAdapter(getContext(), R.layout.layout_spinner_country_state, stateItemList);
                                    spinnerStateBilling.setAdapter(stateSpinnerAdapter);
                                    spinnerStateBilling.setVisibility(View.VISIBLE);
                                    billing_state.setVisibility(View.GONE);
                                    ImageView imgv = (ImageView) rootView.findViewById(R.id.dropdown_spinner);
                                    imgv.setVisibility(View.VISIBLE);
                                } else {
                                    spinnerStateBilling.setVisibility(View.GONE);
                                    billing_state.setVisibility(View.VISIBLE);
                                    ImageView imgv = (ImageView) rootView.findViewById(R.id.dropdown_spinner);
                                    imgv.setVisibility(View.GONE);
                                }

                                break;
                            case 1: // state for shipping
                                if (stateModel.getFieldType() == 1) {
                                    stateItemListShipping = stateModel.getStateItemList();
                                    StateSpinnerAdapter stateSpinnerAdapter = new StateSpinnerAdapter(getContext(), R.layout.layout_spinner_country_state, stateItemListShipping);
                                    shipping_spinner_state.setAdapter(stateSpinnerAdapter);
                                    shipping_spinner_state.setVisibility(View.VISIBLE);
                                    shipping_state.setVisibility(View.GONE);
                                    ImageView imgv = (ImageView) rootView.findViewById(R.id.shipping_dropdown_img);
                                    imgv.setVisibility(View.VISIBLE);
                                } else {
                                    shipping_spinner_state.setVisibility(View.GONE);
                                    shipping_state.setVisibility(View.VISIBLE);
                                    ImageView imgv = (ImageView) rootView.findViewById(R.id.shipping_dropdown_img);
                                    imgv.setVisibility(View.GONE);
                                }
                                break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<StateModel> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMess(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private ShopFragment defaultFragment;

    public void setDefaultFragment(ShopFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }
}
