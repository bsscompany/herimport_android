package com.islayapp.newfragment.shop;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.core.APIService;
import com.islayapp.R;
import com.islayapp.adapter.ProductSpinnerAdapter;
import com.islayapp.adapter.ProductSpinnerSecondAdapter;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.shop.AddToCart;
import com.islayapp.models.shop.CartItem;
import com.islayapp.models.shop.GetCartModel;
import com.islayapp.models.shop.ProductDetails;
import com.islayapp.models.shop.RemoveItemCart;
import com.islayapp.models.shop.SuperProductDetails;
import com.islayapp.models.shop.Value;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.DpiToPixels;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.ScreenUtils;
import com.islayapp.util.Utility;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_MY_CART;


/**
 * Created by maxo on 3/4/16.
 */
public class ShopProductDetailsFragment extends BaseFragment {
    // newInstance constructor for creating fragment with arguments
    public static ShopProductDetailsFragment newInstance(String productID) {
        ShopProductDetailsFragment fragmentFirst = new ShopProductDetailsFragment();
        Bundle args = new Bundle();
        args.putString("product_id", productID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private ShopFragment defaultFragment;

    private static final String TAG = ShopProductDetailsFragment.class.getName();
    private LayoutInflater inflaterRoot;
    private int heightPixels = 500;

    private String productID = "";
    //cart
    private View shoppingCartView;
    private ImageButton imgbtn;
    private CartAdapter mMyAnimListAdapter;
    private ListView myListViewCartItem;
    private LinearLayout linearLayoutThumbmail;

    private PopupWindow mPopupWindow;
    private SparseArray<View> views = new SparseArray<View>();
    private Point size = new Point();
    private GetCartModel cartContent;
    private ProductDetails currentProduct;
    private DecimalFormat df;

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productID = getArguments().getString("product_id");
        df = new DecimalFormat("0.00");

    }

    // Inflate the view for the fragment based on layout_try_hair_bottombar XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shop_new_product_details, container, false);
        inflaterRoot = (LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(size);

        initProductLayout();

        if (currentProduct != null) {
            LinearLayout root_product_detail = (LinearLayout) rootView.findViewById(R.id.root_product_detail);
            root_product_detail.setVisibility(View.VISIBLE);
            FrameLayout loading_product_details_layout = (FrameLayout) rootView.findViewById(R.id.loading_product_details_layout);
            loading_product_details_layout.setVisibility(View.GONE);
            showProductDetails(currentProduct);

        } else {
            requestProducts(productID);
        }

        initShoppingCartLayout();

        return rootView;
    }

    @Override
    protected void initTopbar() {
        ImageButton backOnQuoteFragment = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        imgbtn = (ImageButton) rootView.findViewById(R.id.shop_card);
        backOnQuoteFragment.setOnClickListener(this);
        imgbtn.setOnClickListener(this);
        title_topbar.setText(getString(R.string.shop_name));

    }

    private void initProductLayout() {
        TextViewPlus shop_product_hostest_details_quanlity_subtract1 = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_quanlity_subtract1);
        shop_product_hostest_details_quanlity_subtract1.setOnClickListener(this);

        TextViewPlus shop_product_hostest_details_quanlity_summation1 = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_quanlity_summation1);
        shop_product_hostest_details_quanlity_summation1.setOnClickListener(this);

        Button btnAddToCart = (Button) rootView.findViewById(R.id.add_to_cart_product_details);
        btnAddToCart.setOnClickListener(this);

    }

    /**
     * get hostest product
     */
    private void requestProducts(final String productID) {

        final LinearLayout root_product_detail = (LinearLayout) rootView.findViewById(R.id.root_product_detail);
        root_product_detail.setVisibility(View.GONE);
        final FrameLayout loading_product_details_layout = (FrameLayout) rootView.findViewById(R.id.loading_product_details_layout);
        loading_product_details_layout.setVisibility(View.VISIBLE);

        Log.d(TAG, "get list hostest product");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<SuperProductDetails> response = service.getProductDetails(productID);
        response.enqueue(new Callback<SuperProductDetails>() {
            @Override
            public void onResponse(Call<SuperProductDetails> call, Response<SuperProductDetails> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    SuperProductDetails hostestProductList = response.body();
                    currentProduct = hostestProductList.getProductDetails();
                    showProductDetails(currentProduct);

                    root_product_detail.setVisibility(View.VISIBLE);
                    loading_product_details_layout.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SuperProductDetails> call, Throwable t) {
                Log.d(TAG, "content error ");
                Utility.showLoadingFail(getContext(), loading_product_details_layout);

                ImageButton imgbtn = (ImageButton) loading_product_details_layout.findViewById(R.id.id_frame_fail);
                imgbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestProducts(productID);
                    }
                });
            }
        });
    }


    private void showProductDetails(final ProductDetails product) {

        // update title
        TextViewPlus title_product = (TextViewPlus) rootView.findViewById(R.id.title_product);
        title_product.setText(product.getName());

        final ImageView imgage_view_product_main = (ImageView) rootView.findViewById(R.id.imgage_view_product_main);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(size.x / 2, (int) (size.x * 0.75));
        imgage_view_product_main.setLayoutParams(layoutParams);

        if (product != null && product.getImage().size() > 0)
            Glide.with(getActivity())
                    .load(product.getImage().get(0))
                    .into(imgage_view_product_main);

        // update price
        TextViewPlus price_number = (TextViewPlus) rootView.findViewById(R.id.price_number);
        price_number.setText(getString(R.string.price, product.getPrice()));

        // update descrition content
        TextViewPlus des_content = (TextViewPlus) rootView.findViewById(R.id.des_content);
        des_content.setText(Html.fromHtml(product.getDescription()));

        // list image
        linearLayoutThumbmail = (LinearLayout) rootView.findViewById(R.id.container);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                ScreenUtils.convertDIPToPixels(getActivity(), getResources().getDimension(R.dimen.width_item_horizon_scroll_view)),
                ScreenUtils.convertDIPToPixels(getActivity(), getResources().getDimension(R.dimen.height_horizon_scroll_view)));

        param.rightMargin = 15;
        for (int i = 0; i < product.getImage().size(); i++) {
            View view = inflaterRoot.inflate(R.layout.fragment_shop_item_image_select, null);

            ImageView imgView = (ImageView) view.findViewById(R.id.img_product_thumbnail);
            Glide.with(getContext())
                    .load(product.getImage().get(i))
                    .placeholder(R.drawable.logo)
                    .into(imgView);
            imgView.setLayoutParams(param);
            views.put(i, view);

            linearLayoutThumbmail.addView(view);

            LinearLayout layout = (LinearLayout) view.findViewById(R.id.item_image_root);
            if (i == 0)
                layout.setSelected(true);
            layout.setTag(i);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int numClick = (int) v.getTag();
                    for (int j = 0; j < product.getImage().size(); j++) {
                        if (j == numClick) {
                            views.get(j).setSelected(true);
                            Glide.with(getContext())
                                    .load(product.getImage().get(j))
                                    .placeholder(R.drawable.logo)
                                    .into(imgage_view_product_main);
                        } else
                            views.get(j).setSelected(false);
                    }
                }
            });
        }

        final EditText editTextQuanlity22 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
        editTextQuanlity22.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                TextViewPlus price_number = (TextViewPlus) rootView.findViewById(R.id.price_number);
                if (s.equals("")) {
                    price_number.setText(getString(R.string.price_justsymbol) + df.format(0));
                    return;
                }

                if (product.getOptions() == null || product.getOptions().size() == 0) {
                    try {
                        int price = Integer.valueOf(s.toString()) * Integer.valueOf(product.getPrice());
                        price_number.setText(getString(R.string.price_justsymbol) + df.format(price));
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                } else if (product.getOptions().size() == 1) {
                    Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);

                    int position = spinner.getSelectedItemPosition();
                    if (position == 0) {
                        price_number.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }
                    if (!s.toString().equals("")) {
                        int price = Integer.valueOf(s.toString()) *
                                product.getOptions().get(0).getValues().get(position).getPriceMatrix().get(0).getPrice();
                        price_number.setText(getString(R.string.price_justsymbol) + df.format(price));
                    } else {
                        editTextQuanlity22.setText("1");
                    }
                } else if (product.getOptions().size() == 2) {
                    Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_second);
                    int position = spinnerSecond.getSelectedItemPosition();
                    if (position == 0) {
                        price_number.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }

                    View v = spinnerSecond.getAdapter().getView(spinnerSecond.getSelectedItemPosition(), null, null);
                    int price = Integer.valueOf(s.toString()) * (int) v.getTag();
                    price_number.setText(getString(R.string.price_justsymbol) + df.format(price));

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        showOptionSelect(product);
    }

    /**
     * @param productDetails
     */
    public void showOptionSelect(final ProductDetails productDetails) {
        LinearLayout linearlayout_hostest_option = (LinearLayout) rootView.findViewById(R.id.linearlayout_hostest_option);
        if (productDetails.getOptions() == null || productDetails.getOptions().size() == 0) {
            linearlayout_hostest_option.setVisibility(View.GONE);
            return;
        }

        if (productDetails.getOptions().size() == 1) {
            RelativeLayout rlt_spinner_select_option_first = (RelativeLayout) rootView.findViewById(R.id.rlt_spinner_select_option_first);
            RelativeLayout rlt_spinner_select_option_second = (RelativeLayout) rootView.findViewById(R.id.rlt_spinner_select_option_second);

            Value value1 = new Value(productDetails.getOptions().get(0).getLabel(), "-1");
            productDetails.getOptions().get(0).getValues().add(0, value1);

            rlt_spinner_select_option_first.setVisibility(View.VISIBLE);
            rlt_spinner_select_option_second.setVisibility(View.GONE);
            Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);

            ProductSpinnerAdapter spinnerAdapter = new ProductSpinnerAdapter(getActivity(), R.layout.layout_spinner_select_option_item, productDetails.getOptions().get(0).getValues());
            spinner.setAdapter(spinnerAdapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    TextViewPlus price_number = (TextViewPlus) rootView.findViewById(R.id.price_number);
                    if (position == 0) {
                        price_number.setText(getString(R.string.price, "0"));
                        return;
                    }

                    EditText quanlity1 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
                    if (!quanlity1.getText().equals("")) {
                        int price = Integer.valueOf(quanlity1.getText().toString()) *
                                productDetails.getOptions().get(0).getValues().get(position).getPriceMatrix().get(0).getPrice();
                        price_number.setText(getString(R.string.price, price));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else if (productDetails.getOptions().size() == 2) {
            RelativeLayout rlt_spinner_select_option_first = (RelativeLayout) rootView.findViewById(R.id.rlt_spinner_select_option_first);
            RelativeLayout rlt_spinner_select_option_second = (RelativeLayout) rootView.findViewById(R.id.rlt_spinner_select_option_second);

            rlt_spinner_select_option_first.setVisibility(View.VISIBLE);
            rlt_spinner_select_option_second.setVisibility(View.VISIBLE);

            Value value1 = new Value(productDetails.getOptions().get(0).getLabel(), "-1");
            productDetails.getOptions().get(0).getValues().add(0, value1);

            Value value2 = new Value(productDetails.getOptions().get(1).getLabel(), "-1");
            productDetails.getOptions().get(1).getValues().add(0, value2);

            //================================================================================================================================================
            Spinner spinnerFirst = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);
            ProductSpinnerAdapter spinnerAdapter = new ProductSpinnerAdapter(getActivity(), R.layout.layout_spinner_select_option_item,
                    productDetails.getOptions().get(0).getValues());
            spinnerFirst.setAdapter(spinnerAdapter);

            final Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_second);

            ProductSpinnerSecondAdapter spinnerAdapterSecond =
                    new ProductSpinnerSecondAdapter(getActivity(),
                            R.layout.layout_spinner_select_option_item,
                            productDetails.getOptions().get(1).getValues(), productDetails.getOptions().get(0).getValues().get(0));

            spinnerSecond.setAdapter(spinnerAdapterSecond);

            spinnerFirst.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Value item = productDetails.getOptions().get(0).getValues().get(position);
                    String name = "";
                    TextViewPlus price_number = (TextViewPlus) rootView.findViewById(R.id.price_number);

                    if (position == 0) {
                        price_number.setText(getString(R.string.price, "0"));
                        spinnerSecond.setSelection(0);
                        spinnerSecond.setEnabled(false);
                        return;
                    } else {
                        spinnerSecond.setEnabled(true);
                    }

                    if (item.getPriceMatrix().size() == 1)
                        name = item.getPriceMatrix().get(0).getPrice() + "";
                    else if (item.getPriceMatrix().size() == 2)
                        name = item.getPriceMatrix().get(0).getPrice() + ".00 - $" + item.getPriceMatrix().get(1).getPrice();

                    price_number.setText(getString(R.string.price, name));


                    ProductSpinnerSecondAdapter spinnerAdapterSecond =
                            new ProductSpinnerSecondAdapter(getActivity(),
                                    R.layout.layout_spinner_select_option_item,
                                    productDetails.getOptions().get(1).getValues(), item);

                    spinnerSecond.setAdapter(spinnerAdapterSecond);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //================================================================================================================================================
            spinnerSecond.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    TextViewPlus price_number = (TextViewPlus) rootView.findViewById(R.id.price_number);

                    if (position == 0) {
                        price_number.setText(getString(R.string.price, "0"));
                        return;
                    }

                    EditText quanlity1 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
                    if (!quanlity1.getText().equals("")) {
//                        int price = Integer.valueOf(quanlity1.getText().toString()) *
//                                productDetails.getOptions().get(1).getValues().get(position).getPriceMatrix().get(0).getPrice();
//                        price_number.setText(getString(R.string.price, price));

                        Integer price1 = (Integer) view.getTag();
                        int price = Integer.valueOf(quanlity1.getText().toString()) * price1;
                        price_number.setText(getString(R.string.price, price));

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }


    private Map<String, String> getValueProductHostest(final ProductDetails productDetails) {
        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
        // get quaility
        EditText quanlity2 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
        int quanlitynum2 = Integer.valueOf(quanlity2.getText().toString());

        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("product", productDetails.getId());
        fieldMap.put("qty", quanlitynum2 + "");

        //=============================get option select ======================================
        if (productDetails.getOptions() != null && productDetails.getOptions().size() == 1) {
            Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);

            int position = spinner.getSelectedItemPosition();
            if (position == 0) {
                DialogUtil.showAlertMessage(getActivity(), getResources().getString(R.string.pls_select_option));
                return null;
            }
            String super_attributenode = "super_attribute[" + productDetails.getOptions().get(0).getAttributeId() + "]";
            fieldMap.put(super_attributenode, productDetails.getOptions().get(0).getValues().get(position).getValueIndex());

        } else if (productDetails.getOptions() != null && productDetails.getOptions().size() == 2) {
            Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);
            int position1 = spinner.getSelectedItemPosition();
            if (position1 == 0) {
                DialogUtil.showAlertMessage(getActivity(), getResources().getString(R.string.pls_select_option));
                return null;
            }

            String super_attributenode = "super_attribute[" + productDetails.getOptions().get(0).getAttributeId() + "]";
            fieldMap.put(super_attributenode, productDetails.getOptions().get(0).getValues().get(position1).getValueIndex());

            Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_second);
            int position2 = spinnerSecond.getSelectedItemPosition();
            if (position2 == 0) {
                DialogUtil.showAlertMessage(getActivity(), getResources().getString(R.string.pls_select_option));
                return null;
            }
            String super_attributenode1 = "super_attribute[" + productDetails.getOptions().get(1).getAttributeId() + "]";
            fieldMap.put(super_attributenode1, productDetails.getOptions().get(1).getValues().get(position2).getValueIndex());
        }

        if (!quoteId.equals(""))
            fieldMap.put("quoteId", quoteId);

        return fieldMap;
    }


    // add product to cart
    private void addProductToCart(Map<String, String> field) {
        if (field == null)
            return;
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<AddToCart> response = service.hostestAddToCart(field);
        response.enqueue(new Callback<AddToCart>() {
            @Override
            public void onResponse(Call<AddToCart> call, Response<AddToCart> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    AddToCart addToCart = response.body();
                    if (addToCart.getStatus() == 1) {
                        String quoteId = addToCart.getQuoteId();
                        PreferenceHelpers.setPreference(getContext(), "quoteId", quoteId);
//                        getCart(quoteId, false);
                        DialogUtil.toastMess(getContext(), addToCart.getMessage());
                    } else if (addToCart.getStatus() == 0) {
                        DialogUtil.toastMess(getContext(), addToCart.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddToCart> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMess(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    /**
     * init shopping cart data
     */
    private void initShoppingCartLayout() {
        TextViewPlus home_tab = (TextViewPlus) rootView.findViewById(R.id.home_tab);
        home_tab.setOnClickListener(this);


        shoppingCartView = inflaterRoot.inflate(R.layout.fragment_shop_popup_cart, (ViewGroup) rootView.findViewById(R.id.popupRoot));
        myListViewCartItem = (ListView) shoppingCartView.findViewById(R.id.listView);

        Button btncheckout = (Button) shoppingCartView.findViewById(R.id.btn_check_out);
        btncheckout.setOnClickListener(this);

    }

    // get cart info
    private void getCart(String quoteId, boolean isShowProgress) {

        final Dialog dialog = DialogUtil.showProgressDialog(getContext(), isShowProgress);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<GetCartModel> response = service.getCartContent(quoteId);

        response.enqueue(new Callback<GetCartModel>() {
            @Override
            public void onResponse(Call<GetCartModel> call, Response<GetCartModel> response) {
                try {
                    cartContent = response.body();
                    if (cartContent.getStatus() == 1) {
                        mMyAnimListAdapter = new CartAdapter(getContext(), R.layout.fragment_shop_popup_cart_item, cartContent.getItems());
                        myListViewCartItem.setAdapter(mMyAnimListAdapter);
                        myListViewCartItem.setSelection(mMyAnimListAdapter.getCount() - 1);

                    }
                    initiatePopupWindow();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<GetCartModel> call, Throwable t) {
                Log.d(TAG, "content error ");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private void initiatePopupWindow() {
        try {
            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            heightPixels = metrics.heightPixels;

            if (cartContent != null && cartContent.getItems() != null && cartContent.getItems().size() > 0) {
                TextViewPlus lblTotalNumber = (TextViewPlus) shoppingCartView.findViewById(R.id.lblTotalNumber);
                lblTotalNumber.setVisibility(View.VISIBLE);
                myListViewCartItem.setVisibility(View.VISIBLE);
                lblTotalNumber.setText("$" + df.format(cartContent.getTotal()));
            } else {
                TextViewPlus lblTotalNumber = (TextViewPlus) shoppingCartView.findViewById(R.id.lblTotalNumber);
                lblTotalNumber.setVisibility(View.GONE);
                myListViewCartItem.setVisibility(View.GONE);
            }

            mPopupWindow = new PopupWindow(shoppingCartView, ViewGroup.LayoutParams.MATCH_PARENT, heightPixels / 2, true);

            mPopupWindow.setAnimationStyle(R.style.Daily_Popup_DialogAnimation);
            // Closes the popup window when touch outside.
            mPopupWindow.setOutsideTouchable(true);
            mPopupWindow.setFocusable(true);
            // Removes default background.
            mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));

            mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    imgbtn.setSelected(false);
                }
            });

            // get height topbar
            RelativeLayout topbar = (RelativeLayout) rootView.findViewById(R.id.shop_topbar);
            topbar.invalidate();
            int[] location = new int[2];
            topbar.getLocationInWindow(location);
            int y = location[1] + topbar.getMeasuredHeight() + 2;
            mPopupWindow.showAtLocation(shoppingCartView, Gravity.TOP, 0, y);
            calculatorHeightPopUp();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calculatorHeightPopUp() {

        if (mMyAnimListAdapter != null && mMyAnimListAdapter.getCount() > 0) {
            View viewTvNoItem = (View) shoppingCartView.findViewById(R.id.no_item_in_cart_title);
            viewTvNoItem.setVisibility(View.GONE);

            View totalAddtocat = (View) shoppingCartView.findViewById(R.id.bottomCartLayout);
            totalAddtocat.setVisibility(View.VISIBLE);

            if (DpiToPixels.convertDpToPixel(mMyAnimListAdapter.getCount() * 116, getActivity()) > heightPixels / 2) {
                int heightPopup = heightPixels / 2;
                mPopupWindow.update(ViewGroup.LayoutParams.MATCH_PARENT, heightPopup);
            } else {
                float height = DpiToPixels.convertDpToPixel(mMyAnimListAdapter.getCount() * 116, getActivity()) + DpiToPixels.convertDpToPixel(100, getActivity()) + 20;
                mPopupWindow.update(ViewGroup.LayoutParams.MATCH_PARENT, (int) height);
            }
        } else {
            View viewTvNoItem = (View) shoppingCartView.findViewById(R.id.no_item_in_cart_title);
            viewTvNoItem.setVisibility(View.VISIBLE);
            View totalAddtocat = (View) shoppingCartView.findViewById(R.id.bottomCartLayout);
            totalAddtocat.setVisibility(View.GONE);
            mPopupWindow.update(ViewGroup.LayoutParams.MATCH_PARENT, (int) DpiToPixels.convertDpToPixel(100, getActivity()));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shop_card:
                if (imgbtn.isSelected() || (mPopupWindow != null && mPopupWindow.isShowing())) {
                    imgbtn.setSelected(false);
                    if (mPopupWindow != null)
                        mPopupWindow.dismiss();
                } else {
                    imgbtn.setSelected(true);
                    String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                    if (!quoteId.equals("")) {
                        getCart(quoteId, true);
                    } else
                        initiatePopupWindow();
                }
                break;
            case R.id.add_to_cart_product_details:
                Map<String, String> field = getValueProductHostest(currentProduct);
                addProductToCart(field);
                break;

            case R.id.backOnQuoteFragment:
            case R.id.home_tab:
                defaultFragment.popback();
                break;
            case R.id.btn_check_out:
                if (mPopupWindow != null)
                    mPopupWindow.dismiss();
                Log.d(TAG, "click to product details");
                defaultFragment.addNewFragment(FRAGMENT_SHOP_MY_CART, "");
                break;
            case R.id.shop_product_hostest_details_quanlity_subtract1:
                EditText quanlity1 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
                int quanlity = 1;
                if (!quanlity1.getText().toString().equals(""))
                    quanlity = Integer.valueOf(quanlity1.getText().toString());

                if (quanlity > 1) {
                    quanlity--;
                    quanlity1.setText(quanlity + "");
                } else
                    quanlity1.setText(1 + "");
                break;
            case R.id.shop_product_hostest_details_quanlity_summation1:
                EditText quanlity2 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
                int quanlitynum2 = 1;
                if (!quanlity2.getText().toString().equals(""))
                    quanlitynum2 = Integer.valueOf(quanlity2.getText().toString());

                quanlitynum2++;
                quanlity2.setText(quanlitynum2 + "");
                break;
        }
    }

    // get cart info
    private void removeItemInCart(String quoteId, String itemId, final View v, final int index) {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<RemoveItemCart> response = service.removeItemInCart(quoteId, itemId);

        response.enqueue(new Callback<RemoveItemCart>() {
            @Override
            public void onResponse(Call<RemoveItemCart> call, Response<RemoveItemCart> response) {
                try {
                    RemoveItemCart removeItemCart = response.body();
                    if (removeItemCart.getStatus() == 1) {

                        Animation.AnimationListener al = new Animation.AnimationListener() {
                            @Override
                            public void onAnimationEnd(Animation arg0) {

                                Double cartTotal = cartContent.getTotal() - cartContent.getItems().get(index).getItemPrice();
                                TextViewPlus lblTotalNumber = (TextViewPlus) shoppingCartView.findViewById(R.id.lblTotalNumber);
                                lblTotalNumber.setVisibility(View.VISIBLE);
                                lblTotalNumber.setText("$" + df.format(cartTotal));

                                cartContent.getItems().remove(index);
                                ViewHolder vh = (ViewHolder) v.getTag();
                                vh.needInflate = true;
                                mMyAnimListAdapter.notifyDataSetChanged();
                                calculatorHeightPopUp();
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }

                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                        };

                        Utility.collapse(v, al);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<RemoveItemCart> call, Throwable t) {
                Log.d(TAG, "content error ");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    private class ViewHolder {
        public boolean needInflate;
        public TextViewPlus textTitleProduct, textQuanlityNumber, textPriceNumber;
        public ImageView img_product;
        public ImageButton img_button_close;
    }

    /**
     * cart adapter
     */
    public class CartAdapter extends ArrayAdapter<CartItem> {
        private LayoutInflater mInflater;
        private int resId;
        private DecimalFormat df;

        public CartAdapter(Context context, int textViewResourceId, List<CartItem> objects) {
            super(context, textViewResourceId, objects);
            this.resId = textViewResourceId;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            df = new DecimalFormat("0.00");

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final View view;
            final ViewHolder vh;
            final CartItem cell = (CartItem) getItem(position);

            if (convertView == null) {
                view = mInflater.inflate(R.layout.fragment_shop_popup_cart_item, parent, false);
                setViewHolder(view);
            } else if (((ViewHolder) convertView.getTag()).needInflate) {
                view = mInflater.inflate(R.layout.fragment_shop_popup_cart_item, parent, false);
                setViewHolder(view);
            } else {
                view = convertView;
            }

            vh = (ViewHolder) view.getTag();
            vh.textTitleProduct.setText(cell.getItemName());
            vh.textQuanlityNumber.setText(String.valueOf(cell.getItemQty()));
            vh.textPriceNumber.setText("Price: $" + df.format(cell.getItemPrice()));
            vh.img_button_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                    String itemId = getItem(position).getItemId();
                    removeItemInCart(quoteId, itemId, view, position);
                }
            });

            Glide.with(getContext())
                    .load(cell.getItemImage().get(0))
                    .into(vh.img_product);

            vh.textTitleProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestProducts(cell.getProductId());
                    if (mPopupWindow != null)
                        mPopupWindow.dismiss();
                }
            });

            vh.img_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestProducts(cell.getProductId());
                    if (mPopupWindow != null)
                        mPopupWindow.dismiss();
                }
            });


            return view;
        }

        private void setViewHolder(View view) {
            ViewHolder vh = new ViewHolder();
            vh.textTitleProduct = (TextViewPlus) view.findViewById(R.id.title_product_in_cart);
            vh.textQuanlityNumber = (TextViewPlus) view.findViewById(R.id.quanlity_cart_number);
            vh.textPriceNumber = (TextViewPlus) view.findViewById(R.id.price_cart_number);
            vh.img_button_close = (ImageButton) view.findViewById(R.id.iconClose);
            vh.img_product = (ImageView) view.findViewById(R.id.img_product);
            vh.needInflate = false;
            view.setTag(vh);
        }
    }

    public void setDefaultFragment(ShopFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }

}
