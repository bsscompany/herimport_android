package com.islayapp.newfragment.shop;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.core.APIService;
import com.islayapp.R;
import com.islayapp.adapter.MonthYearSpinnerAdapter;
import com.islayapp.adapter.PaymentMycartAdapter;
import com.islayapp.adapter.PaymentSpinnerAdapter;
import com.islayapp.customview.CircleView;
import com.islayapp.customview.EditTextPlus;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.shop.PaymentMethodItem;
import com.islayapp.models.shop.SaveObjectResult;
import com.islayapp.models.shop.SaveShippingResult;
import com.islayapp.models.shop.ShippingMethod;
import com.islayapp.models.shop.ShippingMethodItem;
import com.islayapp.models.shop.SubmitOrder;
import com.islayapp.models.shop.order.OrderReview;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Utility;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_BILLING_AND_SHIPPING;
import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_THANK_YOU;
import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_START_PAYPAL;

/**
 * Created by maxo on 3/4/16.
 */
public class ShopShippingMethodAndPaymentFragment extends BaseFragment {
    // newInstance constructor for creating fragment with arguments
    public static ShopShippingMethodAndPaymentFragment newInstance() {
        ShopShippingMethodAndPaymentFragment fragmentFirst = new ShopShippingMethodAndPaymentFragment();
        Bundle args = new Bundle();
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = ShopShippingMethodAndPaymentFragment.class.getName();
    private ImageButton imgbtn;
    private RadioGroup radioGroupPaymentMethod;
    private List<PaymentMethodItem> paymentMethodItemList = new ArrayList<>();
    private RadioButton credit_debit_card, paypal;
    private ShippingMethodItem curentItemMethod;
    private EditTextPlus cc_number, cc_id_number;
    private Spinner spinner_payment_credit_card;
    private List<String> stringMonthList = Arrays.asList("Select Month", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
    private List<String> stringYearList = Arrays.asList("Select Year", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030");
    private Spinner spinnerMonth, spinnerYear;
    private ListView listOrderReview;
    private OrderReview orderReview;
    private LayoutInflater inflaterRoot;
    private CheckBox checkBox23;
    private ShippingMethod shippingMethod;
    private Button btn_apply_coupon;

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paymentMethodItemList.clear();
        paymentMethodItemList.add(new PaymentMethodItem("NO_CODE", "Select credit card", ""));
        paymentMethodItemList.add(new PaymentMethodItem("VI", "Visa", ""));
        paymentMethodItemList.add(new PaymentMethodItem("MC", "MasterCard", ""));

        inflaterRoot = (LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // Inflate the view for the fragment based on layout_try_hair_bottombar XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shop_shipping_method_and_payment, container, false);
        initView();
        initTopbar();
        return rootView;
    }

    @Override
    protected void initTopbar() {
        ImageButton backOnQuoteFragment = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        ImageButton shop_card = (ImageButton) rootView.findViewById(R.id.shop_card);
        backOnQuoteFragment.setOnClickListener(this);
        title_topbar.setText(getString(R.string.payment));
        shop_card.setVisibility(View.INVISIBLE);
    }

    private void initView() {
        CircleView myCartCircleView = (CircleView) rootView.findViewById(R.id.my_cart_circle_my_cart);
        myCartCircleView.setSelect(true);

        CircleView shippingAndPaymentCircleView = (CircleView) rootView.findViewById(R.id.circle_shipping_and_billing);
        shippingAndPaymentCircleView.setSelect(true);

        CircleView circle_shipping_method_and_payment = (CircleView) rootView.findViewById(R.id.circle_shipping_method_and_payment);
        circle_shipping_method_and_payment.setSelect(true);


        TextViewPlus shipping_method_and_payment = (TextViewPlus) rootView.findViewById(R.id.shipping_method_and_payment);
        shipping_method_and_payment.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));

        Button btn_shipping_method_and_payment_continue = (Button) rootView.findViewById(R.id.btn_shipping_method_and_payment_continue);
        btn_shipping_method_and_payment_continue.setOnClickListener(this);

        imgbtn = (ImageButton) rootView.findViewById(R.id.shop_card);
        imgbtn.setVisibility(View.INVISIBLE);

        radioGroupPaymentMethod = (RadioGroup) rootView.findViewById(R.id.group_payment_method);
        radioGroupPaymentMethod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                LinearLayout linear_form_credit_card = (LinearLayout) rootView.findViewById(R.id.linear_form_credit_card);
                if (checkedId == R.id.credit_debit_card) {
                    linear_form_credit_card.setVisibility(View.VISIBLE);
                } else if (checkedId == R.id.paypal) {
                    linear_form_credit_card.setVisibility(View.GONE);
                }
            }
        });

        spinner_payment_credit_card = (Spinner) rootView.findViewById(R.id.spinner_payment_credit_card);
        spinner_payment_credit_card.setAdapter(new PaymentSpinnerAdapter(getContext(), R.layout.layout_spinner_select_option_item, paymentMethodItemList));
        spinner_payment_credit_card.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerMonth.setSelection(0);
                spinnerYear.setSelection(0);
                cc_number.setText("");
                cc_id_number.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerMonth = (Spinner) rootView.findViewById(R.id.spinnerMonth);
        spinnerMonth.setAdapter(new MonthYearSpinnerAdapter(getContext(), R.layout.layout_spinner_select_option_item, stringMonthList));

        spinnerYear = (Spinner) rootView.findViewById(R.id.spinnerYear);
        spinnerYear.setAdapter(new MonthYearSpinnerAdapter(getContext(), R.layout.layout_spinner_select_option_item, stringYearList));

        credit_debit_card = (RadioButton) rootView.findViewById(R.id.credit_debit_card);
        paypal = (RadioButton) rootView.findViewById(R.id.paypal);

        cc_number = (EditTextPlus) rootView.findViewById(R.id.cc_number);
        cc_id_number = (EditTextPlus) rootView.findViewById(R.id.cc_id_number);

        listOrderReview = (ListView) rootView.findViewById(R.id.listViewProductReview);

        checkBox23 = (CheckBox) rootView.findViewById(R.id.checkBox23);

        TextViewPlus agree_term_and_condition = (TextViewPlus) rootView.findViewById(R.id.agree_term_and_condition);
        agree_term_and_condition.setMovementMethod(LinkMovementMethod.getInstance());

        btn_apply_coupon = (Button) rootView.findViewById(R.id.btn_apply_coupon);
        btn_apply_coupon.setOnClickListener(this);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (shippingMethod == null) {
            String quoteID = PreferenceHelpers.getPreference(getActivity(), "quoteId");
            getShippingMethod(quoteID);
        }

        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
        getOrderReview(quoteId, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_apply_coupon:
                String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                if (btn_apply_coupon.isSelected())
                    removeCoupon(quoteId);
                else {
                    EditTextPlus edt_coupon = (EditTextPlus) rootView.findViewById(R.id.edt_coupon);
                    if (edt_coupon.length() > 0)
                        applyCoupon(quoteId, edt_coupon.getText().toString());
                    else
                        edt_coupon.setError("Enter coupon code!");
                }
                break;
            case R.id.backOnQuoteFragment:
                defaultFragment.popback(String.valueOf(FRAGMENT_SHOP_BILLING_AND_SHIPPING));
                break;
            case R.id.btn_shipping_method_and_payment_continue:
                if (curentItemMethod == null) {
                    DialogUtil.toastMessNoIcon(getContext(), "Please select shipping method");
                    return;
                }

                if (checkBox23.isChecked()) {
                    if (credit_debit_card.isChecked()) {
                        Map<String, String> param = getCreditCardParam();
                        submitOrder(param);
                    } else if (paypal.isChecked()) {
                        Map<String, String> param = new HashMap<>();
                        param.put("method", "paypal_express");
                        String quoteId1 = PreferenceHelpers.getPreference(getContext(), "quoteId");
                        param.put("quoteId", quoteId1);

                        String url = Constants.BASE_URL + "mobileapp/checkout/submitOrder" + "?method=paypal_express&quoteId=" + quoteId1;
                        if (paypal.isChecked()) {
                            defaultFragment.addNewFragment(FRAGMENT_START_PAYPAL, url);
                        }
                    }
                } else {
                    DialogUtil.toastMessNoIcon(getContext(), "Please accept term and conditions");
                }
                break;
        }
    }

    //radio button shipping method
    public void addRadioButtons(List<ShippingMethodItem> shippingMethodItemList) {
        RadioGroup group_shipping_method_option = (RadioGroup) rootView.findViewById(R.id.group_shipping_method_option);
        group_shipping_method_option.removeAllViews();

        for (int i = 0; i < shippingMethodItemList.size(); i++) {

            ShippingMethodItem item = shippingMethodItemList.get(i);
            RadioButton rdbtn = (RadioButton) inflaterRoot.inflate(R.layout.radiobutton_item, null);

            rdbtn.setText(item.getCarrierTitle() + " - " + item.getMethodTitle());
            rdbtn.setTag(item);
            rdbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    curentItemMethod = (ShippingMethodItem) v.getTag();
                    Log.d(TAG, curentItemMethod.getCarrierTitle());
                    String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                    saveShippingMethod(quoteId, curentItemMethod.getCode());
                }
            });

            group_shipping_method_option.addView(rdbtn);
        }
    }


    private void getShippingMethod(String quoteId) {

        final Dialog dialog = DialogUtil.showProgressDialog(getContext());

        APIService service = NetworkConnectionUtil.createShopApiService(getContext());

        Call<ShippingMethod> response = service.getShippingMethod(quoteId);
        response.enqueue(new Callback<ShippingMethod>() {
            @Override
            public void onResponse(Call<ShippingMethod> call, Response<ShippingMethod> response) {
                try {
                    shippingMethod = response.body();
                    if (shippingMethod.getStatus() == 1) {
                        if (shippingMethod.getShippingMethodItemList() == null || shippingMethod.getShippingMethodItemList().size() == 0) {
                            TextViewPlus tv_there_are_no_shipping_method = (TextViewPlus) rootView.findViewById(R.id.tv_there_are_no_shipping_method);
                            tv_there_are_no_shipping_method.setText(getString(R.string.no_shipping_method));
                            tv_there_are_no_shipping_method.setTextColor(ContextCompat.getColor(getContext(), R.color.label_red));
                        } else
                            addRadioButtons(shippingMethod.getShippingMethodItemList());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ShippingMethod> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMessNoIcon(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    private void saveShippingMethod(String quoteId, String codeMethodShipping) {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());

        APIService service = NetworkConnectionUtil.createShopApiService(getContext());

        Call<SaveShippingResult> response = service.saveShippingMethod(quoteId, codeMethodShipping);
        response.enqueue(new Callback<SaveShippingResult>() {
            @Override
            public void onResponse(Call<SaveShippingResult> call, Response<SaveShippingResult> response) {
                try {
                    SaveShippingResult saveShippngMethod = response.body();
                    if (saveShippngMethod.getStatus() == 1) {
                        Log.d(TAG, "save shipping success ");

                        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                        getOrderReview(quoteId, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<SaveShippingResult> call, Throwable t) {
                Log.d(TAG, "save shipping error ");

                t.printStackTrace();
                DialogUtil.toastMessNoIcon(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private Map<String, String> getCreditCardParam() {
        Map<String, String> param = new HashMap<>();
        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");

        param.put("quoteId", quoteId);
        param.put("method", "firstdataglobalgateway");

        int position = spinner_payment_credit_card.getSelectedItemPosition();
        if (position == 0) {
            DialogUtil.toastMess(getContext(), "Please select credit card!");
            return null;
        } else
            param.put("cc_type", paymentMethodItemList.get(spinner_payment_credit_card.getSelectedItemPosition()).getCode());

        //ccNumber
        String ccNumber = cc_number.getText().toString();
        if (ccNumber.length() == 0) {
            cc_number.setError(getResources().getString(R.string.credit_card_number_hint) + " is required!");
            return null;
        }
        param.put("cc_number", ccNumber);

        int position1 = spinnerMonth.getSelectedItemPosition();
        if (position1 == 0)
            DialogUtil.toastMess(getContext(), "Please select month");
        else
            param.put("cc_exp_month", stringMonthList.get(position1));
        int position2 = spinnerYear.getSelectedItemPosition();
        if (position2 == 0)
            DialogUtil.toastMess(getContext(), "Please select month");
        else
            param.put("cc_exp_year", stringYearList.get(position2));

        String ccIdNumber = cc_id_number.getText().toString();
        if (ccIdNumber.length() == 0) {
            cc_id_number.setError(getResources().getString(R.string.card_verify_number) + " is required!");
            return null;
        }
        param.put("cc_cid", ccIdNumber);

        return param;
    }


    private void getOrderReview(String quoteId, boolean showDialog) {

        final Dialog dialog = DialogUtil.showProgressDialog(getContext(), showDialog);
        Log.d(TAG, "getOrderReview start ");

        APIService service = NetworkConnectionUtil.createShopApiService(getContext());

        Call<OrderReview> response = service.getOrderReview(quoteId);
        response.enqueue(new Callback<OrderReview>() {
            @Override
            public void onResponse(Call<OrderReview> call, Response<OrderReview> response) {
                try {
                    orderReview = null;
                    orderReview = response.body();

                    if (orderReview.getStatus() == 1) {
                        Log.d(TAG, "getOrderReview success ");

                        updateOrderReview(orderReview);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<OrderReview> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMessNoIcon(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    private void updateOrderReview(OrderReview orderReview) {
        listOrderReview.setAdapter(new PaymentMycartAdapter(getContext(), R.layout.fragment_shop_shippingmethod_and_payment_my_cart_item, orderReview.getItems()));
        Utility.setListViewHeightBasedOnChildren(listOrderReview);

        RelativeLayout rlt_sub_total = (RelativeLayout) rootView.findViewById(R.id.rlt_sub_total);
        RelativeLayout shipping_layout_review = (RelativeLayout) rootView.findViewById(R.id.shipping_layout_review);
        RelativeLayout rlt_total = (RelativeLayout) rootView.findViewById(R.id.rlt_total);

        RelativeLayout coupon_layout_review = (RelativeLayout) rootView.findViewById(R.id.coupon_layout_review);
        RelativeLayout tax_layout_review = (RelativeLayout) rootView.findViewById(R.id.tax_layout_review);

        TextViewPlus shipping_method_title = (TextViewPlus) rootView.findViewById(R.id.shipping_method_title);
        shipping_method_title.setText(orderReview.getShippingMethod().getLabel());

        shipping_layout_review.setVisibility(View.VISIBLE);
        TextViewPlus shipping_price_number = (TextViewPlus) rootView.findViewById(R.id.shipping_price_number);

        DecimalFormat df = new DecimalFormat("0.00");
        shipping_price_number.setText("$" + df.format(orderReview.getShippingMethod().getValue()));

        if (orderReview.getDiscountInfo() != null && orderReview.getDiscountInfo().getValue() != null) {
            coupon_layout_review.setVisibility(View.VISIBLE);
            TextViewPlus discount_title = (TextViewPlus) rootView.findViewById(R.id.discount_title);
            TextViewPlus coupon_discount_number = (TextViewPlus) rootView.findViewById(R.id.coupon_discount_number);
            discount_title.setText(orderReview.getDiscountInfo().getLabel());
            coupon_discount_number.setText("$" + orderReview.getDiscountInfo().getValue());

        } else
            coupon_layout_review.setVisibility(View.GONE);

        if (orderReview.getTax() != null && orderReview.getTax() != 0) {
            tax_layout_review.setVisibility(View.VISIBLE);
            TextViewPlus tax_number = (TextViewPlus) rootView.findViewById(R.id.tax_number);
            tax_number.setText("$" + orderReview.getTax());
        }

        TextViewPlus subtotal_number = (TextViewPlus) rootView.findViewById(R.id.subtotal_number);
        TextViewPlus total_number = (TextViewPlus) rootView.findViewById(R.id.total_number);

        rlt_sub_total.setVisibility(View.VISIBLE);
        rlt_total.setVisibility(View.VISIBLE);
        subtotal_number.setText("$" + df.format(orderReview.getSubTotal()));
        total_number.setText("$" + df.format(orderReview.getGrandTotal()));
    }

    private void submitOrder(Map<String, String> param) {

        if (param == null)
            return;
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());
        APIService service = NetworkConnectionUtil.createShopApiService(getContext());

        Call<SubmitOrder> response = service.submitOrder(param);
        response.enqueue(new Callback<SubmitOrder>() {
            @Override
            public void onResponse(Call<SubmitOrder> call, Response<SubmitOrder> response) {
                try {
                    SubmitOrder body = response.body();
                    if (body.getStatus() == 1) {

                        FragmentManager fm = defaultFragment.getChildFragmentManager();
                        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();
                        }

                        // reser cart ID
                        PreferenceHelpers.setPreference(getContext(), "quoteId", "");
                        defaultFragment.addNewFragment(FRAGMENT_SHOP_THANK_YOU, body.getData().getOrderId());
                        Log.d(TAG, "Order ID: " + body.getData().getOrderId());
                    } else {
                        DialogUtil.toastMessNoIcon(getContext(), body.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<SubmitOrder> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMessNoIcon(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private void applyCoupon(String quoteId, String couponCode) {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());

        APIService service = NetworkConnectionUtil.createShopApiService(getContext());
        Call<SaveObjectResult> response = service.applyCoupon(quoteId, couponCode);
        response.enqueue(new Callback<SaveObjectResult>() {
            @Override
            public void onResponse(Call<SaveObjectResult> call, Response<SaveObjectResult> response) {
                try {
                    SaveObjectResult body = response.body();
                    if (body.getStatus() == 1) {

                        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                        getOrderReview(quoteId, true);

                        btn_apply_coupon.setSelected(true);
                        btn_apply_coupon.setText(getString(R.string.remove));

                    }

                    DialogUtil.toastMessNoIcon(getContext(), body.getMessage());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<SaveObjectResult> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMessNoIcon(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private void removeCoupon(String quoteId) {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());
        APIService service = NetworkConnectionUtil.createShopApiService(getContext());

        Call<SaveObjectResult> response = service.removeCoupon(quoteId, "");
        response.enqueue(new Callback<SaveObjectResult>() {
            @Override
            public void onResponse(Call<SaveObjectResult> call, Response<SaveObjectResult> response) {
                try {
                    SaveObjectResult body = response.body();
                    if (body.getStatus() == 1) {
                        btn_apply_coupon.setSelected(false);
                        btn_apply_coupon.setText("Apply");
                        EditTextPlus edt_coupon = (EditTextPlus) rootView.findViewById(R.id.edt_coupon);
                        edt_coupon.setText("");
                        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                        getOrderReview(quoteId, true);
                    }

                    DialogUtil.toastMessNoIcon(getContext(), body.getMessage());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<SaveObjectResult> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMessNoIcon(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private ShopFragment defaultFragment;

    public void setDefaultFragment(ShopFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }
}
