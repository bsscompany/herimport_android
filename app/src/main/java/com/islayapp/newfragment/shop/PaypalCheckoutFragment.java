package com.islayapp.newfragment.shop;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.Toast;

import com.amazonaws.models.OrderIdResponse;
import com.core.APIService;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.PreferenceHelpers;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_THANK_YOU;

/**
 * Created by DINO on 07/04/2016.
 */
public class PaypalCheckoutFragment extends BaseFragment {
    public static PaypalCheckoutFragment newInstance(String url) {
        PaypalCheckoutFragment fragmentFirst = new PaypalCheckoutFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = PaypalCheckoutFragment.class.getName();

    private String url = "";
    private WebView webView;
    private ImageButton imgbtn;
    private boolean isSuccess = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString("url");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.checkout_paypal, container, false);
        webView = (WebView) rootView.findViewById(R.id.paypal_wv);
        startWebView(url);

        return rootView;
    }

    @Override
    protected void initTopbar() {
        ImageButton backOnQuoteFragment = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        imgbtn = (ImageButton) rootView.findViewById(R.id.shop_card);
        imgbtn.setVisibility(View.INVISIBLE);
        backOnQuoteFragment.setOnClickListener(this);
        title_topbar.setText(getString(R.string.paypal_title));

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                defaultFragment.popback();
                break;
        }
    }

    boolean isRun = false;

    private void startWebView(String url) {
        Log.d("url", "first url: " + url);
        isRun = false;

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                Log.d("url", "url shouldOverrideUrlLoading: " + url);

                return true;
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {
                Log.d("url", "url onLoadResource: " + url);

                if (webView.getUrl() != null) {
                    if (webView.getUrl().contains("checkout/onepage/success")) {
                        if (!isSuccess) {
                            String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                            requestOrderID(quoteId);
                            isSuccess = true;
                        }
                    }
                }
            }

            public void onPageFinished(WebView view, String url) {
                Log.d("url", "url onPageFinished: " + url);
            }

        });


        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);

        //Load url in webview
        webView.loadUrl(url);


    }

    /**
     * @param quoteId
     */
    private void requestOrderID(String quoteId) {

        final Dialog progress = DialogUtil.showDialog(getContext());
        APIService service = NetworkConnectionUtil.createShopApiService(getContext());


        Call<OrderIdResponse> response = service.getOrderID(quoteId);

        response.enqueue(new Callback<OrderIdResponse>() {
            @Override
            public void onResponse(Call<OrderIdResponse> call, Response<OrderIdResponse> response) {
                if (progress != null)
                    progress.dismiss();
                try {
                    OrderIdResponse orderIdResponse = response.body();
                    if (orderIdResponse.getStatus() == 1) {
                        defaultFragment.addNewFragment(FRAGMENT_SHOP_THANK_YOU, orderIdResponse.getOrderId());

                        Log.d(TAG, "Get order success");
                    } else {
                        Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<OrderIdResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                if (progress != null)
                    progress.dismiss();
                t.printStackTrace();
            }
        });
    }


    private ShopFragment defaultFragment;

    public void setDefaultFragment(ShopFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }
}
