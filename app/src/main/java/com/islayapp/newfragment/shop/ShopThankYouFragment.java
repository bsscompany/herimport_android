package com.islayapp.newfragment.shop;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.islayapp.R;
import com.islayapp.customview.CircleView;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.PreferenceHelpers;

/**
 * Created by maxo on 3/4/16.
 */
public class ShopThankYouFragment extends BaseFragment {
    // newInstance constructor for creating fragment with arguments
    public static ShopThankYouFragment newInstance(String orderID) {
        ShopThankYouFragment fragmentFirst = new ShopThankYouFragment();
        Bundle args = new Bundle();
        args.putString("orderID", orderID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = ShopThankYouFragment.class.getName();
    private TextViewPlus continue_shopping_thank_you, tv_order_id;
    private String orderID;

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderID = getArguments().getString("orderID", "");
        PreferenceHelpers.setPreference(getContext(), "quoteId", "");

    }

    // Inflate the view for the fragment based on layout_try_hair_bottombar XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shop_thank_you, container, false);
        initView();
        return rootView;
    }

    @Override
    protected void initTopbar() {
        ImageButton imgMenu = (ImageButton) rootView.findViewById(R.id.imgMenu);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        ImageButton shop_card = (ImageButton) rootView.findViewById(R.id.shop_card);
        imgMenu.setVisibility(View.INVISIBLE);
        title_topbar.setText(getString(R.string.thank_you_title));
        shop_card.setVisibility(View.INVISIBLE);
    }

    private void initView() {
        CircleView myCartCircleView = (CircleView) rootView.findViewById(R.id.my_cart_circle_my_cart);
        myCartCircleView.setSelect(true);

        CircleView shippingAndPaymentCircleView = (CircleView) rootView.findViewById(R.id.circle_shipping_and_billing);
        shippingAndPaymentCircleView.setSelect(true);

        CircleView circle_shipping_method_and_payment = (CircleView) rootView.findViewById(R.id.circle_shipping_method_and_payment);
        circle_shipping_method_and_payment.setSelect(true);

        CircleView circle_thank_you = (CircleView) rootView.findViewById(R.id.circle_thank_you);
        circle_thank_you.setSelect(true);

        TextViewPlus thank_you = (TextViewPlus) rootView.findViewById(R.id.thank_you);
        thank_you.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));

        tv_order_id = (TextViewPlus) rootView.findViewById(R.id.tv_order_id);
        tv_order_id.setText("#" + orderID);
        continue_shopping_thank_you = (TextViewPlus) rootView.findViewById(R.id.continue_shopping_thank_you);
        continue_shopping_thank_you.setOnClickListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continue_shopping_thank_you:
                PreferenceHelpers.setPreference(getContext(), "quoteId", "");
//                defaultFragment.popback(String.valueOf(FRAGMENT_SHOP));
                FragmentManager fm = defaultFragment.getChildFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }

                // reser cart ID

                break;
        }
    }

    private ShopFragment defaultFragment;

    public void setDefaultFragment(ShopFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }
}
