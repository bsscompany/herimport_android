package com.islayapp.newfragment.shop;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.core.APIService;
import com.islayapp.R;
import com.islayapp.customview.CircleView;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.shop.CartItem;
import com.islayapp.models.shop.GetCartModel;
import com.islayapp.models.shop.RemoveItemCart;
import com.islayapp.models.shop.UpdateItemCart;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Utility;

import java.text.DecimalFormat;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_BILLING_AND_SHIPPING;
import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_PRODUCT_DETAILS;

/**
 * Created by maxo on 3/4/16.
 */
public class ShopMyCartFragment extends BaseFragment {
    // newInstance constructor for creating fragment with arguments
    public static ShopMyCartFragment newInstance() {
        ShopMyCartFragment fragmentFirst = new ShopMyCartFragment();
        Bundle args = new Bundle();
//        args.putInt("product_id", productID);
//        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = ShopMyCartFragment.class.getName();

    private CartAdapter mMyAnimListAdapter;
    private ListView myListViewCartItem;
    private Button btn_check_out;

    private GetCartModel cartContent;
    private TextViewPlus subtotal, my_cart_number_item;
    private RelativeLayout rlt_cart_content_list_item;
    private Call<GetCartModel> responseCart;
    private DecimalFormat df;

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        df = new DecimalFormat("0.00");
    }

    // Inflate the view for the fragment based on layout_try_hair_bottombar XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shop_my_cart, container, false);
        initLayout();

        return rootView;
    }

    @Override
    protected void initTopbar() {
        ImageButton backOnQuoteFragment = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        ImageButton shop_card = (ImageButton) rootView.findViewById(R.id.shop_card);
        backOnQuoteFragment.setOnClickListener(this);
        shop_card.setOnClickListener(this);
        title_topbar.setText(getString(R.string.my_cart));
        shop_card.setVisibility(View.INVISIBLE);

    }

    private void initLayout() {

        CircleView myCartCircleView = (CircleView) rootView.findViewById(R.id.my_cart_circle_my_cart);
        myCartCircleView.setSelect(true);
        TextViewPlus title_my_cart = (TextViewPlus) rootView.findViewById(R.id.title_my_cart);
        title_my_cart.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));

        rlt_cart_content_list_item = (RelativeLayout) rootView.findViewById(R.id.rlt_cart_content_list_item);
        rlt_cart_content_list_item.setVisibility(View.GONE);
        FrameLayout loadingHostest = (FrameLayout) rootView.findViewById(R.id.loading_my_cart);
        loadingHostest.setVisibility(View.VISIBLE);

        TextViewPlus tvcontinueShopping = (TextViewPlus) rootView.findViewById(R.id.continue_shopping);
        my_cart_number_item = (TextViewPlus) rootView.findViewById(R.id.my_cart_number_item);
        my_cart_number_item.setText(getString(R.string.my_cart_number_item, 0));

        String htmlString = getString(R.string.continue_shopping);
        tvcontinueShopping.setText(Html.fromHtml("<u>" + htmlString + "</u>"));
        tvcontinueShopping.setOnClickListener(this);

        btn_check_out = (Button) rootView.findViewById(R.id.btn_check_out);
        btn_check_out.setOnClickListener(this);

        myListViewCartItem = (ListView) rootView.findViewById(R.id.listView);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onStart() {
        super.onStart();

        if (cartContent != null && cartContent.getItems() != null) {
            rlt_cart_content_list_item.setVisibility(View.VISIBLE);
            FrameLayout loadingHostest = (FrameLayout) rootView.findViewById(R.id.loading_my_cart);
            loadingHostest.setVisibility(View.GONE);
            showCartInfo(cartContent.getItems());
        } else {
            String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
            getCart(quoteId, true);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    // get cart info
    private void getCart(String quoteId, boolean isShowLoading) {
        final FrameLayout loadingHostest = (FrameLayout) rootView.findViewById(R.id.loading_my_cart);
        if (isShowLoading) {
            rlt_cart_content_list_item.setVisibility(View.GONE);
            loadingHostest.setVisibility(View.VISIBLE);
        } else {
            rlt_cart_content_list_item.setVisibility(View.VISIBLE);
            loadingHostest.setVisibility(View.GONE);
        }

        APIService service = NetworkConnectionUtil.createShopApiService(getContext());
        if (responseCart != null)
            responseCart.cancel();
        responseCart = service.getCartContent(quoteId);

        responseCart.enqueue(new Callback<GetCartModel>() {
            @Override
            public void onResponse(Call<GetCartModel> call, Response<GetCartModel> response) {
                try {
                    cartContent = response.body();
                    if (cartContent.getStatus() == 1) {
                        showCartInfo(cartContent.getItems());
                        rlt_cart_content_list_item.setVisibility(View.VISIBLE);
                        loadingHostest.setVisibility(View.GONE);
                    } else {

                        showCartInfo(cartContent.getItems());
                        rlt_cart_content_list_item.setVisibility(View.GONE);
                        loadingHostest.setVisibility(View.GONE);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetCartModel> call, Throwable t) {
                Log.d(TAG, "content error ");
                Utility.showLoadingFail(getContext(), loadingHostest);
            }
        });
    }


    private void showCartInfo(List<CartItem> cartItems) {
        mMyAnimListAdapter = new CartAdapter(getContext(), R.layout.fragment_shop_popup_cart_item, cartItems);
        myListViewCartItem.setAdapter(mMyAnimListAdapter);
        Utility.setListViewHeightBasedOnChildren(myListViewCartItem);


        subtotal = (TextViewPlus) rootView.findViewById(R.id.subtotal_number);
        subtotal.setText("$" + df.format(cartContent.getTotal()));

        my_cart_number_item.setText(getString(R.string.my_cart_number_item, cartContent.getItems().size()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_check_out:
                Log.d(TAG, "click to product details");
                defaultFragment.addNewFragment(FRAGMENT_SHOP_BILLING_AND_SHIPPING, "");
                break;
            case R.id.continue_shopping:
            case R.id.backOnQuoteFragment:
                defaultFragment.popback();
        }
    }

    // get cart info
    private void removeItemInCart(String quoteId, String itemId, final View v, final int index) {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());
        APIService service = NetworkConnectionUtil.createShopApiService(getContext());

        Call<RemoveItemCart> response = service.removeItemInCart(quoteId, itemId);

        response.enqueue(new Callback<RemoveItemCart>() {
            @Override
            public void onResponse(Call<RemoveItemCart> call, Response<RemoveItemCart> response) {
                try {
                    RemoveItemCart removeItemCart = response.body();
                    if (removeItemCart.getStatus() == 1) {

                        Animation.AnimationListener al = new Animation.AnimationListener() {
                            @Override
                            public void onAnimationEnd(Animation arg0) {
                                double cartTotal = cartContent.getTotal() - cartContent.getItems().get(index).getItemPrice();
                                cartContent.getItems().remove(index);
                                cartContent.setTotal(cartTotal);
                                showCartInfo(cartContent.getItems());
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }

                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                        };

                        Utility.collapse(v, al);
                    } else {
                        showCartInfo(cartContent.getItems());
                        rlt_cart_content_list_item.setVisibility(View.GONE);
                        FrameLayout loadingHostest = (FrameLayout) rootView.findViewById(R.id.loading_my_cart);
                        loadingHostest.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<RemoveItemCart> call, Throwable t) {
                Log.d(TAG, "content error ");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    // get cart info
    private void updateItemInCart(String quoteId, String itemId, String qty) {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext(), false);
        APIService service = NetworkConnectionUtil.createShopApiService(getContext());
        Call<UpdateItemCart> response = service.updateItemInCart(quoteId, itemId, qty);

        response.enqueue(new Callback<UpdateItemCart>() {
            @Override
            public void onResponse(Call<UpdateItemCart> call, Response<UpdateItemCart> response) {
                try {
                    UpdateItemCart updateItemCart = response.body();
                    if (updateItemCart.getStatus() == 1) {
                        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                        getCart(quoteId, false);
                    } else if (updateItemCart.getStatus() == 0)
                        DialogUtil.toastMess(getContext(), updateItemCart.getMessage());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<UpdateItemCart> call, Throwable t) {
                Log.d(TAG, "content error ");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    private class ViewHolder {
        public boolean needInflate;
        public TextViewPlus textTitleProduct, textPriceNumber, sku_my_cart, quanlity_subtract2, quanlity_summation2;
        public EditText textQuanlityNumber;
        public ImageView img_product;
        public ImageButton img_button_close;
    }

    /**
     * cart adapter
     */
    public class CartAdapter extends ArrayAdapter<CartItem> {
        private LayoutInflater mInflater;
        private int resId;
        private DecimalFormat df;

        public CartAdapter(Context context, int textViewResourceId, List<CartItem> objects) {
            super(context, textViewResourceId, objects);
            this.resId = textViewResourceId;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            df = new DecimalFormat("0.00");
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final View view;
            final ViewHolder vh;
//            final CartItem cell =  getItem(position);

            if (convertView == null) {
                view = mInflater.inflate(R.layout.fragment_shop_my_cart_item, parent, false);
                setViewHolder(view);
            } else if (((ViewHolder) convertView.getTag()).needInflate) {
                view = mInflater.inflate(R.layout.fragment_shop_my_cart_item, parent, false);
                setViewHolder(view);
            } else {
                view = convertView;
            }

            vh = (ViewHolder) view.getTag();
            vh.textTitleProduct.setText(getItem(position).getItemName());
            vh.textPriceNumber.setText(getString(R.string.price_justsymbol) + df.format(getItem(position).getItemPrice() * getItem(position).getItemQty()));
            vh.textQuanlityNumber.setText(String.valueOf(getItem(position).getItemQty()));

            vh.sku_my_cart.setText(getItem(position).getItemSku());
            vh.img_button_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                    String itemId = getItem(position).getItemId();
                    removeItemInCart(quoteId, itemId, view, position);
                }
            });

            Glide.with(getContext())
                    .load(getItem(position).getItemImage().get(0))
                    .into(vh.img_product);

            vh.textTitleProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    defaultFragment.addNewFragment(FRAGMENT_SHOP_PRODUCT_DETAILS, (getItem(position)).getProductId());
                }
            });

            vh.img_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    defaultFragment.addNewFragment(FRAGMENT_SHOP_PRODUCT_DETAILS, (getItem(position)).getProductId());
                }
            });

            vh.quanlity_subtract2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int quanlity = Integer.valueOf(vh.textQuanlityNumber.getText().toString());
                    if (quanlity > 1) {
                        quanlity--;
                        vh.textQuanlityNumber.setText(quanlity + "");
                    } else {
                        quanlity = 1;
                        vh.textQuanlityNumber.setText(1 + "");
                    }
                    Double price = quanlity * Double.valueOf(getItem(position).getItemPrice());
                    vh.textPriceNumber.setText(getString(R.string.price_justsymbol) + df.format(price));


                    String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                    String itemId = getItem(position).getItemId();

                    if (vh.textQuanlityNumber.getText().length() > 0)
                        updateItemInCart(quoteId, itemId, vh.textQuanlityNumber.getText().toString());


                }
            });

            vh.quanlity_summation2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int quanlity = Integer.valueOf(vh.textQuanlityNumber.getText().toString());
                    quanlity++;
                    vh.textQuanlityNumber.setText(quanlity + "");

                    Double price = quanlity * Double.valueOf(getItem(position).getItemPrice());
                    vh.textPriceNumber.setText(getString(R.string.price_justsymbol) + df.format(price));

                    String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                    String itemId = getItem(position).getItemId();

                    if (vh.textQuanlityNumber.getText().length() > 0)
                        updateItemInCart(quoteId, itemId, vh.textQuanlityNumber.getText().toString());

                }
            });

            vh.textQuanlityNumber.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.toString().equals("")) {
                        vh.textPriceNumber.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }

                    try {
                        Double price = Integer.valueOf(s.toString()) * Double.valueOf(getItem(position).getItemPrice());
                        vh.textPriceNumber.setText(getString(R.string.price_justsymbol) + df.format(price));

                        String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                        String itemId = getItem(position).getItemId();

                        if (vh.textQuanlityNumber.getText().length() > 0)
                            updateItemInCart(quoteId, itemId, vh.textQuanlityNumber.getText().toString());

                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            return view;
        }

        private void setViewHolder(View view) {
            ViewHolder vh = new ViewHolder();
            vh.textTitleProduct = (TextViewPlus) view.findViewById(R.id.title_product_in_cart);
            vh.textQuanlityNumber = (EditText) view.findViewById(R.id.editTextQuanlity2);
            vh.quanlity_subtract2 = (TextViewPlus) view.findViewById(R.id.quanlity_subtract2);
            vh.quanlity_summation2 = (TextViewPlus) view.findViewById(R.id.quanlity_summation2);

            vh.textPriceNumber = (TextViewPlus) view.findViewById(R.id.price_cart_number);
            vh.sku_my_cart = (TextViewPlus) view.findViewById(R.id.sku_my_cart);
            vh.img_button_close = (ImageButton) view.findViewById(R.id.iconClose);
            vh.img_product = (ImageView) view.findViewById(R.id.img_product);
            vh.needInflate = false;
            view.setTag(vh);
        }
    }

    private ShopFragment defaultFragment;

    public void setDefaultFragment(ShopFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }
}