package com.islayapp.newfragment.shop;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.core.APIService;
import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.adapter.EndlessPagerAdapter;
import com.islayapp.adapter.ProductImageAdapter;
import com.islayapp.adapter.ProductSpinnerAdapter;
import com.islayapp.adapter.ProductSpinnerSecondAdapter;
import com.islayapp.adapter.ShopAccessoriesProductAdapter;
import com.islayapp.adapter.ShopHostestProductAdapter;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.customview.WrappingViewPager;
import com.islayapp.models.shop.AddToCart;
import com.islayapp.models.shop.CartItem;
import com.islayapp.models.shop.GetCartModel;
import com.islayapp.models.shop.HostestProduct;
import com.islayapp.models.shop.ProductDetails;
import com.islayapp.models.shop.RemoveItemCart;
import com.islayapp.models.shop.Value;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.newfragment.getPayD.IDefaultFragment;
import com.islayapp.newfragment.getPayD.Views.onChangeFragment;
import com.islayapp.tranform.ZoomOutTransformer;
import com.islayapp.util.AddFragment;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.DpiToPixels;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Utility;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.islayapp.util.Constants.BASE_URL;
import static com.islayapp.util.Constants.FragmentEnum;
import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_BILLING_AND_SHIPPING;
import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_MY_CART;
import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_PRODUCT_DETAILS;
import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_SHIPPING_METHOD_AND_PAYMENT;
import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_SHOP_THANK_YOU;
import static com.islayapp.util.Constants.FragmentEnum.FRAGMENT_START_PAYPAL;


public class ShopFragment extends BaseFragment implements AddFragment, IDefaultFragment, onChangeFragment {

    // newInstance constructor for creating fragment with arguments
    public static ShopFragment newInstance() {
        ShopFragment fragmentFirst = new ShopFragment();
        return fragmentFirst;
    }

    private static final String TAG = ShopFragment.class.getName();
    private WrappingViewPager viewPager, viewpager_accessories;
    private List<ProductDetails> productHostestDetailsList = new ArrayList<>();
    private List<ProductDetails> productAccessoriesProductList = new ArrayList<>();
    private LayoutInflater inflaterRoot;
    private int heightPixels = 500;
    private View shoppingCartView;
    private Point size = new Point();

    //cart
    private CartAdapter mMyAnimListAdapter;
    private ListView myListViewCartItem;
    private ImageButton imgbtn;
    private ProductDetails currentHostestProductGlobal = new ProductDetails();
    private ProductDetails currentAccessoriesProductGlobal;
    private ProductImageAdapter productImageAdapter;
    private ListView listViewProduct1;
    private int lastPosition = 0, lastAccessoriesPosition = 0;
    private ImageView shop_product_info_right_img;
    private GetCartModel cartContent;
    private String quoteId = "";
    private int positionFirstHostest, positionSecondHostest, positionFirstAccessories, positionSecondAccessories, positionHostestSlider, positionAccessoriesSlider;
    private PopupWindow mPopupWindow;
    private DecimalFormat df;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG + "onCreate");
        df = new DecimalFormat("0.00");

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, TAG + "onCreateView");
//        onRestoreInstanceStae(savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_shop_new, container, false);
        inflaterRoot = (LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View imgMenu = rootView.findViewById(R.id.imgMenu);
        if (imgMenu != null)
            imgMenu.setOnClickListener(this);

        initShoppingCartLayout();

        initViewHostestProducts();

        initViewAccessoriesProduct();
//
//        if (productHostestDetailsList.size() != 0) {
//            LinearLayout hostest_root = (LinearLayout) rootView.findViewById(R.id.hostest_root);
//            hostest_root.setVisibility(View.VISIBLE);
//            FrameLayout loadingHostest = (FrameLayout) rootView.findViewById(R.id.loading_hostest_layout);
//            loadingHostest.setVisibility(View.GONE);
//
//            showHostestProductSlider(productHostestDetailsList);
//            viewPager.setCurrentItem(positionHostestSlider);
//
//            int position1 = ((EndlessPagerAdapter) viewPager.getAdapter()).getVirtualPosition(positionHostestSlider);
//            View currentView = (View) viewPager.findViewWithTag("slider_hostet_tag" + position1);
//            if (currentView != null)
//                updateSliderHostestLayout(currentView, currentHostestProductGlobal);
//
//        }
//
//        if (productAccessoriesProductList.size() != 0) {
//            LinearLayout accessories_root = (LinearLayout) rootView.findViewById(R.id.accessories_root);
//            accessories_root.setVisibility(View.VISIBLE);
//            FrameLayout loading_accessories = (FrameLayout) rootView.findViewById(R.id.loading_accessories);
//            loading_accessories.setVisibility(View.GONE);
//            showAccessoriesProductSlider(productAccessoriesProductList);
//            viewpager_accessories.setCurrentItem(positionAccessoriesSlider);
//
//            int position1 = ((EndlessPagerAdapter) viewpager_accessories.getAdapter()).getVirtualPosition(positionAccessoriesSlider);
//            View currentView = (View) viewpager_accessories.findViewWithTag("slider_accessories_tag" + position1);
//            if (currentView != null)
//                updateLayoutSliderAccessories(currentView, currentAccessoriesProductGlobal);
//
//        }

        return rootView;
    }

    @Override
    protected void initTopbar() {
        ImageButton imgMenu = (ImageButton) rootView.findViewById(R.id.imgMenu);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        imgbtn = (ImageButton) rootView.findViewById(R.id.shop_card);
        imgMenu.setOnClickListener(this);
        imgbtn.setOnClickListener(this);
        title_topbar.setText(getString(R.string.shop_name));
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, TAG + "onStart");
        // request get list hostest product
        if (productHostestDetailsList == null || productHostestDetailsList.size() == 0)
            requestGetHostestProducts();
        if (productAccessoriesProductList == null || productAccessoriesProductList.size() == 0)
            requestGetAccessoriesProducts();
    }

//    @Override
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        super.onSaveInstanceState(savedInstanceState);
//
//        Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);
//        Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_second);
//        Spinner spinneraccessories = (Spinner) rootView.findViewById(R.id.spinner_accessories_product_details_first);
//        Spinner spinnerSecondaccessories = (Spinner) rootView.findViewById(R.id.spinner_accessories_product_details_second);
//
//        savedInstanceState.putInt("spinner1_hostest", spinner.getSelectedItemPosition());
//        savedInstanceState.putInt("spinner2_hostest", spinnerSecond.getSelectedItemPosition());
//        savedInstanceState.putInt("spinner1_accessories", spinneraccessories.getSelectedItemPosition());
//        savedInstanceState.putInt("spinner2_accessories", spinnerSecondaccessories.getSelectedItemPosition());
//        savedInstanceState.putInt("positionHostestSlider", positionHostestSlider);
//        savedInstanceState.putInt("positionAccessoriesSlider", positionAccessoriesSlider);
//    }
//
//    //Here you can restore saved data in onSaveInstanceState Bundle
//    private void onRestoreInstanceStae(Bundle savedInstanceState) {
//        if (savedInstanceState != null) {
//            positionFirstHostest = savedInstanceState.getInt("spinner1_hostest", 0);
//            positionSecondHostest = savedInstanceState.getInt("spinner2_hostest", 0);
//            positionFirstAccessories = savedInstanceState.getInt("spinner1_accessories", 0);
//            positionSecondAccessories = savedInstanceState.getInt("spinner2_accessories", 0);
//
//            positionHostestSlider = savedInstanceState.getInt("positionHostestSlider", 0);
//            positionAccessoriesSlider = savedInstanceState.getInt("positionAccessoriesSlider", 0);
//        }
//    }

    /**
     * init shopping cart data
     */
    private void initShoppingCartLayout() {
        shoppingCartView = inflaterRoot.inflate(R.layout.fragment_shop_popup_cart, (ViewGroup) rootView.findViewById(R.id.popupRoot));
        myListViewCartItem = (ListView) shoppingCartView.findViewById(R.id.listView);

        Button btncheckout = (Button) shoppingCartView.findViewById(R.id.btn_check_out);
        btncheckout.setOnClickListener(this);
    }


    private ViewPager.OnPageChangeListener createOnPageChangeListener() {
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                positionHostestSlider = position;
                int position1 = ((EndlessPagerAdapter) viewPager.getAdapter()).getVirtualPosition(position);
                //View currentView = (View) viewPager.findViewWithTag("slider_hostet_tag" + position1);
                ///View lastView = (View) viewPager.findViewWithTag("slider_hostet_tag" + lastPosition);

                productHostestDetailsList.get(position1).setSelect(true);
                productHostestDetailsList.get(lastPosition).setSelect(false);

                /*if (currentView != null)
                    updateSliderHostestLayout(currentView, productHostestDetailsList.get(position1));
                if (lastView != null)
                    updateSliderHostestLayout(lastView, productHostestDetailsList.get(lastPosition));*/

                currentHostestProductGlobal = null;
                currentHostestProductGlobal = new ProductDetails(productHostestDetailsList.get(position1));
                lastPosition = position1;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showHostestProductDetails(currentHostestProductGlobal);
                    }
                });
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    private void updateSliderHostestLayout(View view, ProductDetails item) {

        LinearLayout layoutBounaryTextView = (LinearLayout) view.findViewById(R.id.slider_linearlayout_caption);
        ImageView slider_linearlayout_caption_narrow = (ImageView) view.findViewById(R.id.slider_linearlayout_caption_narrow);
        TextView tv = (TextView) view.findViewById(R.id.tv_hottestStyles);
        View ocpatibg = view.findViewById(R.id.ocpatibg);

        if (item.isSelect()) {
            layoutBounaryTextView.setBackgroundResource(R.color.bg_label_slider);
            slider_linearlayout_caption_narrow.setVisibility(View.VISIBLE);
            tv.setTextColor(ContextCompat.getColor(getActivity(), R.color.white_color));
            ocpatibg.setVisibility(View.INVISIBLE);
        } else {
            layoutBounaryTextView.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent));
            slider_linearlayout_caption_narrow.setVisibility(View.INVISIBLE);
            tv.setTextColor(ContextCompat.getColor(getActivity(), R.color.bg_label_slider));
            ocpatibg.setVisibility(View.VISIBLE);
        }
    }

    //init hostest product
    private void initViewHostestProducts() {
        viewPager = (WrappingViewPager) rootView.findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(createOnPageChangeListener());
        viewPager.setPageTransformer(true, new ZoomOutTransformer());
        ImageButton btnNext1 = (ImageButton) rootView.findViewById(R.id.shop_slider_next1);
        ImageButton btnPrev1 = (ImageButton) rootView.findViewById(R.id.shop_slider_prev1);
        btnNext1.setOnClickListener(this);
        btnPrev1.setOnClickListener(this);

        ImageView shop_product_info_left_img = (ImageView) rootView.findViewById(R.id.shop_product_info_left_img);
        shop_product_info_left_img.setOnClickListener(this);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(size);

        //Necessary or the viewPager will only have one extra page to show
        // make this at least however many pages you can see
        viewPager.setClipToPadding(false);
        viewPager.setPadding(size.x / 4, 0, size.x / 4, 0);

        TextViewPlus tvtitle = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_title);
        tvtitle.setOnClickListener(this);

        TextViewPlus shop_product_hostest_details_quanlity_subtract1 = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_quanlity_subtract1);
        shop_product_hostest_details_quanlity_subtract1.setOnClickListener(this);

        TextViewPlus shop_product_hostest_details_quanlity_summation1 = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_quanlity_summation1);
        shop_product_hostest_details_quanlity_summation1.setOnClickListener(this);

        shop_product_info_right_img = (ImageView) rootView.findViewById(R.id.shop_product_info_left_img);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(size.x / 2, (int) (size.x * 0.75));
        shop_product_info_right_img.setLayoutParams(layoutParams);

        Button btn_hostest_add_to_cart = (Button) rootView.findViewById(R.id.hostest_add_to_cart);
        btn_hostest_add_to_cart.setOnClickListener(this);

        EditText quanlity1 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
        quanlity1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                TextViewPlus shop_product_hostest_details_price = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_price);

                if (s.equals("")) {
                    shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                    return;
                }

                if (currentHostestProductGlobal.getOptions() == null || currentHostestProductGlobal.getOptions().size() == 0) {
                    try {
                        int price = Integer.valueOf(s.toString()) * Integer.valueOf(currentHostestProductGlobal.getPrice());
                        shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(price));
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                } else if (currentHostestProductGlobal.getOptions().size() == 1) {
                    if (positionFirstHostest == 0) {
                        shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }

                    if (!s.equals("")) {
                        int price = Integer.valueOf(s.toString()) *
                                currentHostestProductGlobal.getOptions().get(0).getValues().get(positionFirstHostest).getPriceMatrix().get(0).getPrice();
                        shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(price));
                    }
                } else if (currentHostestProductGlobal.getOptions().size() == 2) {
                    Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_second);
                    positionSecondHostest = spinnerSecond.getSelectedItemPosition();
                    if (positionSecondHostest == 0) {
                        shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }


                    View v = spinnerSecond.getAdapter().getView(spinnerSecond.getSelectedItemPosition(), null, null);
                    int price = Integer.valueOf(s.toString()) * (int) v.getTag();
                    shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(price));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        listViewProduct1 = (ListView) rootView.findViewById(R.id.listView2);
        listViewProduct1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int j = 0; j < listViewProduct1.getChildCount(); j++) {
                    if (j == position)
                        parent.getChildAt(j).findViewById(R.id.item_image_root).setSelected(true);
                    else
                        parent.getChildAt(j).findViewById(R.id.item_image_root).setSelected(false);
                }

                Glide.with(getActivity())
                        .load(currentHostestProductGlobal.getImage().get(position))
                        .into(((ImageView) rootView.findViewById(R.id.shop_product_info_left_img)));

                Log.d(TAG, "Position: " + position);
                view.setSelected(true);
            }
        });
    }

    /**
     * get hostest product
     */
    private void requestGetHostestProducts() {

        final LinearLayout hostest_root = (LinearLayout) rootView.findViewById(R.id.hostest_root);
        hostest_root.setVisibility(View.GONE);
        final FrameLayout loadingHostest = (FrameLayout) rootView.findViewById(R.id.loading_hostest_layout);
        loadingHostest.setVisibility(View.VISIBLE);


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        Log.d(TAG, "get list hostest product");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<HostestProduct> response = service.getHostlesProduct();

        response.enqueue(new Callback<HostestProduct>() {
            @Override
            public void onResponse(Call<HostestProduct> call, Response<HostestProduct> response) {

                try {
                    HostestProduct hostestProductList = response.body();
                    showHostestProductSlider(hostestProductList.getProductDetailsList());
                    hostest_root.setVisibility(View.VISIBLE);
                    loadingHostest.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<HostestProduct> call, Throwable t) {
                Log.d(TAG, "content error ");

                Utility.showLoadingFail(getContext(), loadingHostest);
                ImageButton imgbtn = (ImageButton) loadingHostest.findViewById(R.id.id_frame_fail);
                imgbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestGetHostestProducts();
                    }
                });
            }
        });
    }


    public void showHostestProductSlider(List<ProductDetails> productHostestDetailsList1) {
        productHostestDetailsList = productHostestDetailsList1;
        ShopHostestProductAdapter mShopHostestProductAdapter = new ShopHostestProductAdapter(getActivity(), productHostestDetailsList, size.x);
        EndlessPagerAdapter mAdapater = new EndlessPagerAdapter(mShopHostestProductAdapter);
        viewPager.setAdapter(mAdapater);
        viewPager.setCurrentItem(100);
        lastPosition = ((EndlessPagerAdapter) viewPager.getAdapter()).getVirtualPosition(100);
        positionHostestSlider = 100;
    }


    /**
     * update product child of slider one info
     */
    private void showHostestProductDetails(final ProductDetails currentHostestProduct) {

        if (currentHostestProduct != null && currentHostestProduct.getImage().size() > 0)
            Glide.with(getActivity())
                    .load(currentHostestProduct.getImage().get(0))
                    .into(shop_product_info_right_img);

        productImageAdapter = new ProductImageAdapter(this.getActivity(), currentHostestProductGlobal.getImage(), size.x);
        listViewProduct1.setAdapter(productImageAdapter);

        listViewProduct1.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        // update title
        TextViewPlus shop_product_hostest_details_title = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_title);
        shop_product_hostest_details_title.setText(currentHostestProduct.getName());

        // update description
        TextViewPlus shop_product_hostest_details_description = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_description);
        shop_product_hostest_details_description.setText(Html.fromHtml(currentHostestProduct.getDescription()));

        // update price
        TextViewPlus shop_product_hostest_details_price = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_price);
        shop_product_hostest_details_price.setText(getString(R.string.price, currentHostestProduct.getPrice()));

        //get list option
        initHostestOptionSelect(currentHostestProduct);

    }

    public void initHostestOptionSelect(final ProductDetails currentHostestProduct) {
        LinearLayout linearlayout_hostest_option = (LinearLayout) rootView.findViewById(R.id.linearlayout_hostest_option);
        if (currentHostestProduct.getOptions().size() == 0) {
            linearlayout_hostest_option.setVisibility(View.GONE);
            return;
        } else
            linearlayout_hostest_option.setVisibility(View.VISIBLE);

        if (currentHostestProduct.getOptions().size() == 1) {
            Value value1 = new Value(currentHostestProduct.getOptions().get(0).getLabel(), "-1");

            if (currentHostestProduct.getOptions().get(0).getValues().size() > 0 && (
                    currentHostestProduct.getOptions().get(0).getValues().get(0).getValueId() == null ||
                            !currentHostestProduct.getOptions().get(0).getValues().get(0).getValueId().equals("-1")))
                currentHostestProduct.getOptions().get(0).getValues().add(0, value1);

            RelativeLayout rlt_spinner_select_option_first = (RelativeLayout) rootView.findViewById(R.id.rlt_spinner_select_option_first);
            RelativeLayout rlt_spinner_select_option_second = (RelativeLayout) rootView.findViewById(R.id.rlt_spinner_select_option_second);

            rlt_spinner_select_option_first.setVisibility(View.VISIBLE);
            rlt_spinner_select_option_second.setVisibility(View.GONE);
            Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);

            ProductSpinnerAdapter spinnerAdapter = new ProductSpinnerAdapter(getActivity(), R.layout.layout_spinner_select_option_item, currentHostestProduct.getOptions().get(0).getValues());
            spinner.setAdapter(spinnerAdapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    positionFirstHostest = position;

                    TextViewPlus shop_product_hostest_details_price = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_price);
                    if (position == 0) {
                        shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }

                    EditText quanlity1 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
                    if (!quanlity1.getText().equals("")) {
                        int price = Integer.valueOf(quanlity1.getText().toString()) *
                                currentHostestProduct.getOptions().get(0).getValues().get(position).getPriceMatrix().get(0).getPrice();
                        shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(price));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else if (currentHostestProduct.getOptions().size() == 2) {
            RelativeLayout rlt_spinner_select_option_first = (RelativeLayout) rootView.findViewById(R.id.rlt_spinner_select_option_first);
            RelativeLayout rlt_spinner_select_option_second = (RelativeLayout) rootView.findViewById(R.id.rlt_spinner_select_option_second);
            rlt_spinner_select_option_first.setVisibility(View.VISIBLE);
            rlt_spinner_select_option_second.setVisibility(View.VISIBLE);


            Value value1 = new Value(currentHostestProduct.getOptions().get(0).getLabel(), "-1");
            Value value2 = new Value(currentHostestProduct.getOptions().get(1).getLabel(), "-1");


            if (currentHostestProduct.getOptions().get(0).getValues().size() > 0 && (
                    currentHostestProduct.getOptions().get(0).getValues().get(0).getValueId() == null ||
                            !currentHostestProduct.getOptions().get(0).getValues().get(0).getValueId().equals("-1")))
                currentHostestProduct.getOptions().get(0).getValues().add(0, value1);


            if (currentHostestProduct.getOptions().get(1).getValues().size() > 0 && (
                    currentHostestProduct.getOptions().get(1).getValues().get(0).getValueId() == null ||
                            !currentHostestProduct.getOptions().get(1).getValues().get(0).getValueId().equals("-1")))
                currentHostestProduct.getOptions().get(1).getValues().add(0, value2);

            //================================================================================================================================================
            final Spinner spinnerFirst = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);
            ProductSpinnerAdapter spinnerAdapter = new ProductSpinnerAdapter(getActivity(), R.layout.layout_spinner_select_option_item,
                    currentHostestProduct.getOptions().get(0).getValues());
            spinnerFirst.setAdapter(spinnerAdapter);

            final Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_second);

            ProductSpinnerSecondAdapter spinnerAdapterSecond =
                    new ProductSpinnerSecondAdapter(getActivity(),
                            R.layout.layout_spinner_select_option_item,
                            currentHostestProduct.getOptions().get(1).getValues(), currentHostestProduct.getOptions().get(0).getValues().get(0));

            spinnerSecond.setAdapter(spinnerAdapterSecond);

            spinnerFirst.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Value item = currentHostestProduct.getOptions().get(0).getValues().get(position);
                    String name = "";
                    TextViewPlus shop_product_hostest_details_price = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_price);

                    positionFirstHostest = position;
                    if (position == 0) {
                        shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        spinnerSecond.setSelection(0);
                        spinnerSecond.setEnabled(false);
                        return;
                    } else {
                        spinnerSecond.setEnabled(true);
                    }

                    if (item.getPriceMatrix().size() == 1)
                        name = item.getPriceMatrix().get(0).getPrice() + "";
                    else if (item.getPriceMatrix().size() == 2)
                        name = item.getPriceMatrix().get(0).getPrice() + ".00 - $" + item.getPriceMatrix().get(1).getPrice() + ".00";

                    shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + name);

                    ProductSpinnerSecondAdapter spinnerAdapterSecond =
                            new ProductSpinnerSecondAdapter(getActivity(),
                                    R.layout.layout_spinner_select_option_item,
                                    currentHostestProduct.getOptions().get(1).getValues(), item);

                    spinnerSecond.setAdapter(spinnerAdapterSecond);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //================================================================================================================================================
            spinnerSecond.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    TextViewPlus shop_product_hostest_details_price = (TextViewPlus) rootView.findViewById(R.id.shop_product_hostest_details_price);
                    positionSecondHostest = position;
                    if (position == 0) {
                        shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }

                    EditText quanlity1 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);

                    if (!quanlity1.getText().equals("")) {
                        Integer price1 = (Integer) view.getTag();
                        int price = Integer.valueOf(quanlity1.getText().toString()) * price1;
                        shop_product_hostest_details_price.setText(getString(R.string.price_justsymbol) + df.format(price));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }


    // init Accessories Product
    private void initViewAccessoriesProduct() {

        ImageButton shop_slider_next2 = (ImageButton) rootView.findViewById(R.id.shop_slider_next2);
        ImageButton shop_slider_prev2 = (ImageButton) rootView.findViewById(R.id.shop_slider_prev2);
        shop_slider_next2.setOnClickListener(this);
        shop_slider_prev2.setOnClickListener(this);

        ImageView accessories_image_product_details = (ImageView) rootView.findViewById(R.id.accessories_image_product_details);
        accessories_image_product_details.setOnClickListener(this);


        viewpager_accessories = (WrappingViewPager) rootView.findViewById(R.id.viewpager_accessories);
        viewpager_accessories.setClipToPadding(false);
        viewpager_accessories.setPadding(size.x / 4, 0, size.x / 4, 0);

        viewpager_accessories.addOnPageChangeListener(createAccessoriesOnPageChangeListener());

        TextViewPlus accessories_image_product_details_title = (TextViewPlus) rootView.findViewById(R.id.accessories_image_product_details_title);
        accessories_image_product_details_title.setOnClickListener(this);

        TextViewPlus tvtitle = (TextViewPlus) rootView.findViewById(R.id.accessories_image_product_details_title);
        tvtitle.setOnClickListener(this);

        TextViewPlus quanlity_subtract2 = (TextViewPlus) rootView.findViewById(R.id.quanlity_subtract2);
        quanlity_subtract2.setOnClickListener(this);

        TextViewPlus quanlity_summation2 = (TextViewPlus) rootView.findViewById(R.id.quanlity_summation2);
        quanlity_summation2.setOnClickListener(this);


        Button accessories_add_to_cart = (Button) rootView.findViewById(R.id.accessories_add_to_cart);
        accessories_add_to_cart.setOnClickListener(this);

    }


    private ViewPager.OnPageChangeListener createAccessoriesOnPageChangeListener() {
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                int position1 = ((EndlessPagerAdapter) viewpager_accessories.getAdapter()).getVirtualPosition(position);
                View currentView = (View) viewpager_accessories.findViewWithTag("slider_accessories_tag" + position1);
                View lastView = (View) viewpager_accessories.findViewWithTag("slider_accessories_tag" + lastAccessoriesPosition);

                productAccessoriesProductList.get(position1).setSelect(true);
                productAccessoriesProductList.get(lastAccessoriesPosition).setSelect(false);

                if (currentView != null)
                    updateLayoutSliderAccessories(currentView, productAccessoriesProductList.get(position1));
                if (lastView != null)
                    updateLayoutSliderAccessories(lastView, productAccessoriesProductList.get(lastAccessoriesPosition));

                currentAccessoriesProductGlobal = null;
                currentAccessoriesProductGlobal = new ProductDetails(productAccessoriesProductList.get(position1));

                showAccessoriesProductDetails(currentAccessoriesProductGlobal);
                lastAccessoriesPosition = position1;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    private void updateLayoutSliderAccessories(View view, ProductDetails item) {
        View ocpatibg = view.findViewById(R.id.ocpatibg);
        if (item.isSelect()) {
            ocpatibg.setVisibility(View.GONE);
        } else {
            ocpatibg.setVisibility(View.VISIBLE);
        }
    }

    /**
     * get accessories product
     */
    private void requestGetAccessoriesProducts() {

        final LinearLayout accessories_root = (LinearLayout) rootView.findViewById(R.id.accessories_root);
        accessories_root.setVisibility(View.GONE);
        final FrameLayout loading_accessories = (FrameLayout) rootView.findViewById(R.id.loading_accessories);
        loading_accessories.setVisibility(View.VISIBLE);

        Log.d(TAG, "get list hostest product");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<HostestProduct> response = service.getAccessoriesProduct();

        response.enqueue(new Callback<HostestProduct>() {
            @Override
            public void onResponse(Call<HostestProduct> call, Response<HostestProduct> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    HostestProduct hostestProductList = response.body();
                    productAccessoriesProductList = hostestProductList.getProductDetailsList();

                    showAccessoriesProductSlider(productAccessoriesProductList);
                    accessories_root.setVisibility(View.VISIBLE);
                    loading_accessories.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<HostestProduct> call, Throwable t) {
                Log.d(TAG, "content error ");
                Utility.showLoadingFail(getContext(), loading_accessories);
                ImageButton imgbtn = (ImageButton) loading_accessories.findViewById(R.id.id_frame_fail);
                imgbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestGetHostestProducts();
                    }
                });
            }
        });
    }


    private void showAccessoriesProductSlider(List<ProductDetails> productAccessoriesProductList) {
        ShopAccessoriesProductAdapter mShopHostestProductAdapter = new ShopAccessoriesProductAdapter(getActivity(), productAccessoriesProductList, size.x);
        EndlessPagerAdapter mAdapater = new EndlessPagerAdapter(mShopHostestProductAdapter);
        viewpager_accessories.setAdapter(mAdapater);
        viewpager_accessories.setCurrentItem(100);
        lastAccessoriesPosition = ((EndlessPagerAdapter) viewpager_accessories.getAdapter()).getVirtualPosition(100);
        positionAccessoriesSlider = 100;
    }


    /**
     * update product child of slider one info
     */
    private void showAccessoriesProductDetails(final ProductDetails currentAccessoriesProduct) {
        ImageView accessories_image_product_details = (ImageView) rootView.findViewById(R.id.accessories_image_product_details);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(size.x / 2, (int) (size.x * 0.75));
        accessories_image_product_details.setLayoutParams(layoutParams);

        if (currentAccessoriesProduct != null && currentAccessoriesProduct.getImage().size() > 0)
            Glide.with(getActivity())
                    .load(currentAccessoriesProduct.getImage().get(0))
                    .into(accessories_image_product_details);

        // update title
        TextViewPlus tvTitleAccessoriesProduct = (TextViewPlus) rootView.findViewById(R.id.tvTitleAccessoriesProduct);
        tvTitleAccessoriesProduct.setText(currentAccessoriesProduct.getName());

        // update title
        TextViewPlus accessories_image_product_details_title = (TextViewPlus) rootView.findViewById(R.id.accessories_image_product_details_title);
        accessories_image_product_details_title.setText(currentAccessoriesProduct.getName());

        // update description
        TextViewPlus accessories_image_product_details_title_des = (TextViewPlus) rootView.findViewById(R.id.accessories_image_product_details_title_des);
        accessories_image_product_details_title_des.setText(Html.fromHtml(currentAccessoriesProduct.getDescription()));

        // update price
        TextViewPlus accessories_product_price = (TextViewPlus) rootView.findViewById(R.id.accessories_product_price);
        accessories_product_price.setText(getString(R.string.price, currentAccessoriesProduct.getPrice()));

        EditText editTextQuanlity22 = (EditText) rootView.findViewById(R.id.editTextQuanlity2);
        editTextQuanlity22.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                TextViewPlus accessories_product_price = (TextViewPlus) rootView.findViewById(R.id.accessories_product_price);
                if (s.equals("")) {
                    accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                    return;
                }

                if (currentAccessoriesProduct.getOptions() == null || currentAccessoriesProduct.getOptions().size() == 0) {
                    try {
                        int price = Integer.valueOf(s.toString()) * Integer.valueOf(currentAccessoriesProduct.getPrice());
                        accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(price));
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                } else if (currentAccessoriesProduct.getOptions().size() == 1) {
                    if (positionFirstAccessories == 0) {
                        accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }
                    if (!s.equals("")) {
                        int price = Integer.valueOf(s.toString()) *
                                currentAccessoriesProduct.getOptions().get(0).getValues().get(positionFirstAccessories).getPriceMatrix().get(0).getPrice();
                        accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(price));
                    }
                } else if (currentAccessoriesProduct.getOptions().size() == 2) {
                    if (positionSecondAccessories == 0) {
                        accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }


                    Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_accessories_product_details_second);


                    View v = spinnerSecond.getAdapter().getView(spinnerSecond.getSelectedItemPosition(), null, null);
                    int price = Integer.valueOf(s.toString()) * (int) v.getTag();
                    accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(price));

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //get list option
        initAccessoriesOptionSelect(currentAccessoriesProduct);

    }

    public void initAccessoriesOptionSelect(final ProductDetails currentAccessoriesProduct) {
        LinearLayout linearlayout_accessories_option = (LinearLayout) rootView.findViewById(R.id.linearlayout_accessories_option);
        if (currentAccessoriesProduct.getOptions().size() == 0) {
            linearlayout_accessories_option.setVisibility(View.GONE);
            return;
        } else
            linearlayout_accessories_option.setVisibility(View.VISIBLE);


        if (currentAccessoriesProduct.getOptions().size() == 1) {
            Value value1 = new Value(currentAccessoriesProduct.getOptions().get(0).getLabel(), "-1");

            if (currentAccessoriesProduct.getOptions().get(0).getValues().size() > 0 && (
                    currentAccessoriesProduct.getOptions().get(0).getValues().get(0).getValueId() == null ||
                            !currentAccessoriesProduct.getOptions().get(0).getValues().get(0).getValueId().equals("-1")))
                currentAccessoriesProduct.getOptions().get(0).getValues().add(0, value1);

            RelativeLayout rlt_accessories_spinner_select_option_first = (RelativeLayout) rootView.findViewById(R.id.rlt_accessories_spinner_select_option_first);
            RelativeLayout rlt_accessories_spinner_select_option_second = (RelativeLayout) rootView.findViewById(R.id.rlt_accessories_spinner_select_option_second);

            rlt_accessories_spinner_select_option_first.setVisibility(View.VISIBLE);
            rlt_accessories_spinner_select_option_second.setVisibility(View.GONE);
            Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_accessories_product_details_first);

            ProductSpinnerAdapter spinnerAdapter = new ProductSpinnerAdapter(getActivity(), R.layout.layout_spinner_select_option_item, currentAccessoriesProduct.getOptions().get(0).getValues());
            spinner.setAdapter(spinnerAdapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    TextViewPlus accessories_product_price = (TextViewPlus) rootView.findViewById(R.id.accessories_product_price);
                    positionFirstAccessories = position;
                    if (position == 0) {
                        accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }

                    if (currentAccessoriesProduct.getOptions().size() > 0 &&
                            currentAccessoriesProduct.getOptions().get(0).getValues().size() > 0 &&
                            currentAccessoriesProduct.getOptions().get(0).getValues().get(position).getPriceMatrix().size() > 0) {

                        EditText quanlity1 = (EditText) rootView.findViewById(R.id.editTextQuanlity2);
                        int price = Integer.valueOf(quanlity1.getText().toString()) * currentAccessoriesProduct.getOptions().get(0).getValues().get(position).getPriceMatrix().get(0).getPrice();
                        accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(price));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else if (currentAccessoriesProduct.getOptions().size() == 2) {
            RelativeLayout rlt_accessories_spinner_select_option_first = (RelativeLayout) rootView.findViewById(R.id.rlt_accessories_spinner_select_option_first);
            RelativeLayout rlt_accessories_spinner_select_option_second = (RelativeLayout) rootView.findViewById(R.id.rlt_accessories_spinner_select_option_second);

            rlt_accessories_spinner_select_option_first.setVisibility(View.VISIBLE);
            rlt_accessories_spinner_select_option_second.setVisibility(View.VISIBLE);

            Value value1 = new Value(currentAccessoriesProduct.getOptions().get(0).getLabel(), "-1");
            Value value2 = new Value(currentAccessoriesProduct.getOptions().get(1).getLabel(), "-1");

            if (currentAccessoriesProduct.getOptions().get(0).getValues().size() > 0 && (
                    currentAccessoriesProduct.getOptions().get(0).getValues().get(0).getValueId() == null ||
                            !currentAccessoriesProduct.getOptions().get(0).getValues().get(0).getValueId().equals("-1")))
                currentAccessoriesProduct.getOptions().get(0).getValues().add(0, value1);

            if (currentAccessoriesProduct.getOptions().get(1).getValues().size() > 0 && (
                    currentAccessoriesProduct.getOptions().get(1).getValues().get(0).getValueId() == null ||
                            !currentAccessoriesProduct.getOptions().get(1).getValues().get(0).getValueId().equals("-1")))
                currentAccessoriesProduct.getOptions().get(1).getValues().add(0, value2);

            //================================================================================================================================================
            Spinner spinnerFirst = (Spinner) rootView.findViewById(R.id.spinner_accessories_product_details_first);
            ProductSpinnerAdapter spinnerAdapter = new ProductSpinnerAdapter(getActivity(), R.layout.layout_spinner_select_option_item,
                    currentAccessoriesProduct.getOptions().get(0).getValues());
            spinnerFirst.setAdapter(spinnerAdapter);

            final Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_accessories_product_details_second);

            ProductSpinnerSecondAdapter spinnerAdapterSecond =
                    new ProductSpinnerSecondAdapter(getActivity(),
                            R.layout.layout_spinner_select_option_item,
                            currentAccessoriesProduct.getOptions().get(1).getValues(), currentAccessoriesProduct.getOptions().get(0).getValues().get(0));

            spinnerSecond.setAdapter(spinnerAdapterSecond);

            spinnerFirst.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    positionFirstAccessories = position;

                    Value item = currentAccessoriesProduct.getOptions().get(0).getValues().get(position);
                    String name = "";
                    TextViewPlus accessories_product_price = (TextViewPlus) rootView.findViewById(R.id.accessories_product_price);

                    if (position == 0) {
                        accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        spinnerSecond.setSelection(0);
                        spinnerSecond.setEnabled(false);
                        return;
                    } else {
                        spinnerSecond.setEnabled(true);
                    }

                    if (item.getPriceMatrix().size() == 1)
                        name = name + item.getPriceMatrix().get(0).getPrice() + "";
                    else if (item.getPriceMatrix().size() == 2)
                        name = name + "$" + item.getPriceMatrix().get(0).getPrice() + ".00 - $" + item.getPriceMatrix().get(1).getPrice() + ".00";

                    accessories_product_price.setText(name);

                    ProductSpinnerSecondAdapter spinnerAdapterSecond =
                            new ProductSpinnerSecondAdapter(getActivity(),
                                    R.layout.layout_spinner_select_option_item,
                                    currentAccessoriesProduct.getOptions().get(1).getValues(), item);

                    spinnerSecond.setAdapter(spinnerAdapterSecond);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //================================================================================================================================================
            spinnerSecond.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    positionSecondAccessories = position;

                    TextViewPlus accessories_product_price = (TextViewPlus) rootView.findViewById(R.id.accessories_product_price);

                    if (position == 0) {
                        accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(0));
                        return;
                    }
                    if (currentAccessoriesProduct.getOptions().size() > 0 &&
                            currentAccessoriesProduct.getOptions().get(1).getValues().size() > 0 &&
                            currentAccessoriesProduct.getOptions().get(1).getValues().get(position).getPriceMatrix().size() > 0) {

                        EditText quanlity2 = (EditText) rootView.findViewById(R.id.editTextQuanlity2);
                        Integer price1 = (Integer) view.getTag();
                        int price = Integer.valueOf(quanlity2.getText().toString()) * price1;
                        accessories_product_price.setText(getString(R.string.price_justsymbol) + df.format(price));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imgMenu:
                ((NewHomeActivity) getActivity()).openNavMenu();
                break;
            case R.id.shop_slider_next1:
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                break;
            case R.id.shop_slider_prev1:
                if (viewPager.getCurrentItem() >= 1)
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
                break;
            case R.id.shop_card:
                if (imgbtn.isSelected() || (mPopupWindow != null && mPopupWindow.isShowing())) {
                    imgbtn.setSelected(false);
                    if (mPopupWindow != null)
                        mPopupWindow.dismiss();
                } else {
                    imgbtn.setSelected(true);
                    quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                    if (!quoteId.equals("")) {
                        getCart(quoteId, true);
                    } else
                        showCartPopupWindow();
                }
                break;

            case R.id.shop_product_info_left_img:
            case R.id.shop_product_hostest_details_title:
                Log.d(TAG, "click to product details");
                addNewFragment(FRAGMENT_SHOP_PRODUCT_DETAILS, currentHostestProductGlobal.getId());

                break;

            case R.id.accessories_image_product_details:
            case R.id.accessories_image_product_details_title:
                Log.d(TAG, "click to product details");
                addNewFragment(FRAGMENT_SHOP_PRODUCT_DETAILS, currentAccessoriesProductGlobal.getId());

                break;
            case R.id.btn_check_out:
                if (mPopupWindow != null)
                    mPopupWindow.dismiss();
                Log.d(TAG, "click to product details");
                addNewFragment(FRAGMENT_SHOP_MY_CART, "");

                break;
            case R.id.shop_product_hostest_details_quanlity_subtract1:
                EditText quanlity1 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
                int quanlity = 1;
                if (!quanlity1.getText().toString().equals(""))
                    quanlity = Integer.valueOf(quanlity1.getText().toString());

                if (quanlity > 1) {
                    quanlity--;
                    quanlity1.setText(quanlity + "");
                } else
                    quanlity1.setText(1 + "");
                break;
            case R.id.shop_product_hostest_details_quanlity_summation1:
                EditText quanlity2 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
                int quanlitynum2 = 1;
                if (!quanlity2.getText().toString().equals(""))
                    quanlitynum2 = Integer.valueOf(quanlity2.getText().toString());


                quanlitynum2++;
                quanlity2.setText(quanlitynum2 + "");
                break;

            case R.id.quanlity_subtract2:
                EditText editTextQuanlity22 = (EditText) rootView.findViewById(R.id.editTextQuanlity2);
                int numAccessori1 = 1;
                if (!editTextQuanlity22.getText().toString().equals(""))
                    numAccessori1 = Integer.valueOf(editTextQuanlity22.getText().toString());

                if (numAccessori1 > 1) {
                    numAccessori1--;
                    editTextQuanlity22.setText(numAccessori1 + "");
                } else
                    editTextQuanlity22.setText(1 + "");
                break;
            case R.id.quanlity_summation2:
                EditText editTextQuanlity23 = (EditText) rootView.findViewById(R.id.editTextQuanlity2);
                int numAccessori = 1;
                if (!editTextQuanlity23.getText().toString().equals(""))
                    numAccessori = Integer.valueOf(editTextQuanlity23.getText().toString());

                numAccessori++;
                editTextQuanlity23.setText(numAccessori + "");
                break;

            case R.id.hostest_add_to_cart:
                Log.d(TAG, "click to hostest add to cart");
                Map<String, String> filed = getValueProductHostest(currentHostestProductGlobal);
                addProductToCart(filed);
                break;
            case R.id.accessories_add_to_cart:
                Log.d(TAG, "click to accessories add to cart");
                Map<String, String> filed1 = getValueProductAccessories(currentAccessoriesProductGlobal);
                addProductToCart(filed1);
                break;

            case R.id.shop_slider_next2:
                Log.d(TAG, "click to accessories add to cart");
                viewpager_accessories.setCurrentItem(viewpager_accessories.getCurrentItem() + 1, true);
                break;
            case R.id.shop_slider_prev2:
                Log.d(TAG, "click to accessories add to cart");
                if (viewpager_accessories.getCurrentItem() >= 1) {
                    viewpager_accessories.setCurrentItem(viewpager_accessories.getCurrentItem() - 1, true);
                }
                break;
        }
    }

    private Map<String, String> getValueProductHostest(ProductDetails currentHostestProduct) {
        quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
        // get quaility
        EditText quanlity2 = (EditText) rootView.findViewById(R.id.editText_shop_prodct_hostest_details_quanlity_quanlity1);
        int quanlitynum2 = Integer.valueOf(quanlity2.getText().toString());

        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("product", currentHostestProductGlobal.getId());
        fieldMap.put("qty", quanlitynum2 + "");

        //=============================get option select ======================================
        if (currentHostestProduct.getOptions() != null && currentHostestProduct.getOptions().size() == 1) {
            Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);

            int position = spinner.getSelectedItemPosition();
            if (position == 0) {
                DialogUtil.showAlertMessage(getActivity(), getResources().getString(R.string.pls_select_option));
                return null;
            }
            String super_attributenode = "super_attribute[" + currentHostestProduct.getOptions().get(0).getAttributeId() + "]";
            fieldMap.put(super_attributenode, currentHostestProduct.getOptions().get(0).getValues().get(position).getValueIndex());

        } else if (currentHostestProduct.getOptions().size() == 2) {
            Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_first);
            int position1 = spinner.getSelectedItemPosition();
            if (position1 == 0) {
                DialogUtil.showAlertMessage(getActivity(), getResources().getString(R.string.pls_select_option));
                return null;
            }

            String super_attributenode = "super_attribute[" + currentHostestProduct.getOptions().get(0).getAttributeId() + "]";
            fieldMap.put(super_attributenode, currentHostestProduct.getOptions().get(0).getValues().get(position1).getValueIndex());

            Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_hostest_product_details_second);
            int position2 = spinnerSecond.getSelectedItemPosition();
            if (position2 == 0) {
                DialogUtil.showAlertMessage(getActivity(), getResources().getString(R.string.pls_select_option));
                return null;
            }
            String super_attributenode1 = "super_attribute[" + currentHostestProduct.getOptions().get(1).getAttributeId() + "]";
            fieldMap.put(super_attributenode1, currentHostestProduct.getOptions().get(1).getValues().get(position2).getValueIndex());
        }

        if (!quoteId.equals(""))
            fieldMap.put("quoteId", quoteId);

        return fieldMap;
    }

    private Map<String, String> getValueProductAccessories(ProductDetails currentAccessoriesProduct) {
        quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
        // get quaility
        EditText editTextQuanlity22 = (EditText) rootView.findViewById(R.id.editTextQuanlity2);
        int quanlitynum2 = Integer.valueOf(editTextQuanlity22.getText().toString());

        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("product", currentAccessoriesProduct.getId());
        fieldMap.put("qty", quanlitynum2 + "");

        //=============================get option select ======================================
        if (currentAccessoriesProduct.getOptions() != null && currentAccessoriesProduct.getOptions().size() == 1) {
            Spinner spinnerFirst = (Spinner) rootView.findViewById(R.id.spinner_accessories_product_details_first);

            int position = spinnerFirst.getSelectedItemPosition();
            if (position == 0) {
                DialogUtil.showAlertMessage(getActivity(), getResources().getString(R.string.pls_select_option));
                return null;
            }
            String super_attributenode = "super_attribute[" + currentAccessoriesProduct.getOptions().get(0).getAttributeId() + "]";
            fieldMap.put(super_attributenode, currentAccessoriesProduct.getOptions().get(0).getValues().get(position).getValueIndex());

        } else if (currentAccessoriesProduct.getOptions().size() == 2) {
            Spinner spinnerFirst = (Spinner) rootView.findViewById(R.id.spinner_accessories_product_details_first);
            int position1 = spinnerFirst.getSelectedItemPosition();
            if (position1 == 0) {
                DialogUtil.showAlertMessage(getActivity(), getResources().getString(R.string.pls_select_option));
                return null;
            }

            String super_attributenode = "super_attribute[" + currentAccessoriesProduct.getOptions().get(0).getAttributeId() + "]";
            fieldMap.put(super_attributenode, currentAccessoriesProduct.getOptions().get(0).getValues().get(position1).getValueIndex());

            Spinner spinnerSecond = (Spinner) rootView.findViewById(R.id.spinner_accessories_product_details_second);
            int position2 = spinnerSecond.getSelectedItemPosition();
            if (position2 == 0) {
                DialogUtil.showAlertMessage(getActivity(), getResources().getString(R.string.pls_select_option));
                return null;
            }
            String super_attributenode1 = "super_attribute[" + currentAccessoriesProduct.getOptions().get(1).getAttributeId() + "]";
            fieldMap.put(super_attributenode1, currentAccessoriesProduct.getOptions().get(1).getValues().get(position2).getValueIndex());
        }

        if (!quoteId.equals(""))
            fieldMap.put("quoteId", quoteId);

        return fieldMap;
    }

    // add product to cart
    private void addProductToCart(Map<String, String> field) {
        if (field == null)
            return;
        final Dialog dialog = DialogUtil.showDialog(getContext());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<AddToCart> response = service.hostestAddToCart(field);
        response.enqueue(new Callback<AddToCart>() {
            @Override
            public void onResponse(Call<AddToCart> call, Response<AddToCart> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    AddToCart addToCart = response.body();
                    if (addToCart.getStatus() == 1) {
                        quoteId = addToCart.getQuoteId();
                        Log.d(TAG, "quoteId: " + quoteId);
                        PreferenceHelpers.setPreference(getContext(), "quoteId", quoteId);
//                        getCart(quoteId, false);
                        DialogUtil.toastMess(getContext(), addToCart.getMessage());
                    } else if (addToCart.getStatus() == 0) {
                        DialogUtil.toastMess(getContext(), addToCart.getMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddToCart> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMess(getContext(), "Error request");
                if (dialog != null)
                    dialog.dismiss();
            }

        });
    }

    // get cart info
    private void getCart(String quoteId, final boolean isShowProgress) {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext(), isShowProgress);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<GetCartModel> response = service.getCartContent(quoteId);

        response.enqueue(new Callback<GetCartModel>() {
            @Override
            public void onResponse(Call<GetCartModel> call, Response<GetCartModel> response) {
                try {
                    cartContent = response.body();
                    if (cartContent.getStatus() == 1) {
                        mMyAnimListAdapter = new CartAdapter(getContext(), R.layout.fragment_shop_popup_cart_item, cartContent.getItems());
                        myListViewCartItem.setAdapter(mMyAnimListAdapter);
                        myListViewCartItem.setSelection(mMyAnimListAdapter.getCount() - 1);
                    }
                    showCartPopupWindow();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<GetCartModel> call, Throwable t) {
                Log.d(TAG, "content error ");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }


    // get cart info
    private void removeItemInCart(String quoteId, String itemId, final View v, final int index) {
        final Dialog dialog = DialogUtil.showProgressDialog(getContext());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<RemoveItemCart> response = service.removeItemInCart(quoteId, itemId);

        response.enqueue(new Callback<RemoveItemCart>() {
            @Override
            public void onResponse(Call<RemoveItemCart> call, Response<RemoveItemCart> response) {
                try {
                    RemoveItemCart removeItemCart = response.body();
                    if (removeItemCart.getStatus() == 1) {
                        Animation.AnimationListener al = new Animation.AnimationListener() {
                            @Override
                            public void onAnimationEnd(Animation arg0) {
                                double cartTotal = cartContent.getTotal() - cartContent.getItems().get(index).getItemPrice();
                                TextViewPlus lblTotalNumber = (TextViewPlus) shoppingCartView.findViewById(R.id.lblTotalNumber);
                                lblTotalNumber.setVisibility(View.VISIBLE);
                                lblTotalNumber.setText("$" + df.format(cartTotal));

                                cartContent.getItems().remove(index);
                                ViewHolder vh = (ViewHolder) v.getTag();
                                vh.needInflate = true;
                                mMyAnimListAdapter.notifyDataSetChanged();

                                myListViewCartItem.setVisibility(View.VISIBLE);
                                cartContent.setTotal(cartTotal);
                                calculatorHeightPopUp();
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }

                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                        };

                        Utility.collapse(v, al);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<RemoveItemCart> call, Throwable t) {
                Log.d(TAG, "content error ");
                dialog.dismiss();
            }
        });
    }


    private void showCartPopupWindow() {
        try {
            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

            if (cartContent != null && cartContent.getItems() != null && cartContent.getItems().size() > 0) {
                TextViewPlus lblTotalNumber = (TextViewPlus) shoppingCartView.findViewById(R.id.lblTotalNumber);
                lblTotalNumber.setVisibility(View.VISIBLE);
                myListViewCartItem.setVisibility(View.VISIBLE);
                lblTotalNumber.setText("$" + df.format(cartContent.getTotal()));
            } else {
                TextViewPlus lblTotalNumber = (TextViewPlus) shoppingCartView.findViewById(R.id.lblTotalNumber);
                lblTotalNumber.setVisibility(View.GONE);
                myListViewCartItem.setVisibility(View.GONE);
            }

            heightPixels = metrics.heightPixels;

            mPopupWindow = new PopupWindow(shoppingCartView, ViewGroup.LayoutParams.MATCH_PARENT, heightPixels / 2, true);
            mPopupWindow.setAnimationStyle(R.style.Daily_Popup_DialogAnimation);
            // Closes the popup window when touch outside.
            mPopupWindow.setOutsideTouchable(true);
            mPopupWindow.setFocusable(true);
            // Removes default background.
            mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    imgbtn.setSelected(false);
                }
            });
            // get height topbar
            RelativeLayout topbar = (RelativeLayout) rootView.findViewById(R.id.shop_topbar);
            topbar.invalidate();
            int[] location = new int[2];
            topbar.getLocationInWindow(location);
            int y = location[1] + topbar.getMeasuredHeight() + 2;
            mPopupWindow.showAtLocation(shoppingCartView, Gravity.TOP, 0, y);
            calculatorHeightPopUp();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void calculatorHeightPopUp() {

        if (mMyAnimListAdapter != null && mMyAnimListAdapter.getCount() > 0) {
            View viewTvNoItem = shoppingCartView.findViewById(R.id.no_item_in_cart_title);
            viewTvNoItem.setVisibility(View.GONE);

            View totalAddtocat = shoppingCartView.findViewById(R.id.bottomCartLayout);
            totalAddtocat.setVisibility(View.VISIBLE);

            if (DpiToPixels.convertDpToPixel(mMyAnimListAdapter.getCount() * 116, getActivity()) > heightPixels / 2) {
                int heightPopup = heightPixels / 2;
                mPopupWindow.update(ViewGroup.LayoutParams.MATCH_PARENT, heightPopup);
            } else {
                float height = DpiToPixels.convertDpToPixel(mMyAnimListAdapter.getCount() * 116, getActivity()) + DpiToPixels.convertDpToPixel(100, getActivity()) + 20;
                mPopupWindow.update(ViewGroup.LayoutParams.MATCH_PARENT, (int) height);
            }
        } else {
            View viewTvNoItem = shoppingCartView.findViewById(R.id.no_item_in_cart_title);
            viewTvNoItem.setVisibility(View.VISIBLE);
            View totalAddtocat = shoppingCartView.findViewById(R.id.bottomCartLayout);
            totalAddtocat.setVisibility(View.GONE);
            mPopupWindow.update(ViewGroup.LayoutParams.MATCH_PARENT, (int) DpiToPixels.convertDpToPixel(100, getActivity()));
        }
    }

    @Override
    public void onHideMediaController() {

    }

    private class ViewHolder {
        public boolean needInflate;
        public TextViewPlus textTitleProduct, textQuanlityNumber, textPriceNumber;
        public ImageView img_product;
        public ImageButton img_button_close;
    }

    /**
     * cart adapter
     */
    public class CartAdapter extends ArrayAdapter<CartItem> {
        private LayoutInflater mInflater;
        private int resId;
        private Context mContext;
        private DecimalFormat df;

        public CartAdapter(Context context, int textViewResourceId, List<CartItem> objects) {
            super(context, textViewResourceId, objects);
            this.resId = textViewResourceId;
            this.mContext = context;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            df = new DecimalFormat("0.00");

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final View view;
            final ViewHolder vh;
            final CartItem cell = getItem(position);

            if (convertView == null) {
                view = mInflater.inflate(R.layout.fragment_shop_popup_cart_item, parent, false);
                setViewHolder(view);
            } else if (((ViewHolder) convertView.getTag()).needInflate) {
                view = mInflater.inflate(R.layout.fragment_shop_popup_cart_item, parent, false);
                setViewHolder(view);
            } else {
                view = convertView;
            }

            vh = (ViewHolder) view.getTag();
            vh.textTitleProduct.setText(cell.getItemName());
            vh.textQuanlityNumber.setText(String.valueOf(cell.getItemQty()));
            vh.textPriceNumber.setText("Price: $" + df.format(cell.getItemPrice()));
            vh.img_button_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String quoteId = PreferenceHelpers.getPreference(getContext(), "quoteId");
                    String itemId = cell.getItemId();
                    removeItemInCart(quoteId, itemId, view, position);
                }
            });

            vh.textTitleProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addNewFragment(FRAGMENT_SHOP_PRODUCT_DETAILS, cell.getProductId());

                    if (mPopupWindow != null)
                        mPopupWindow.dismiss();
                }
            });


            vh.img_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addNewFragment(FRAGMENT_SHOP_PRODUCT_DETAILS, cell.getProductId());
                    if (mPopupWindow != null)
                        mPopupWindow.dismiss();
                }
            });

            Glide.with(mContext)
                    .load(cell.getItemImage().get(0))
                    .into(vh.img_product);

            return view;
        }

        private void setViewHolder(View view) {
            ViewHolder vh = new ViewHolder();
            vh.textTitleProduct = (TextViewPlus) view.findViewById(R.id.title_product_in_cart);
            vh.textQuanlityNumber = (TextViewPlus) view.findViewById(R.id.quanlity_cart_number);
            vh.textPriceNumber = (TextViewPlus) view.findViewById(R.id.price_cart_number);
            vh.img_button_close = (ImageButton) view.findViewById(R.id.iconClose);
            vh.img_product = (ImageView) view.findViewById(R.id.img_product);
            vh.needInflate = false;
            view.setTag(vh);
        }
    }

    @Override
    public void addNewFragment(@Nullable FragmentEnum code, @Nullable String value) {

        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        switch (code) {
//            case FRAGMENT_SHOP:
//                Fragment shopFragment1 = ShopFragment.newInstance();
//                fragmentTransaction.replace(R.id.parent, shopFragment1);
//                fragmentTransaction.addToBackStack(String.valueOf(FRAGMENT_SHOP));
//                break;
            case FRAGMENT_SHOP_PRODUCT_DETAILS:
                ShopProductDetailsFragment fragmentProductDetails = ShopProductDetailsFragment.newInstance(value);
                fragmentTransaction.add(R.id.parent, fragmentProductDetails);
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(String.valueOf(FRAGMENT_SHOP_PRODUCT_DETAILS));
                fragmentProductDetails.setDefaultFragment(this);

                break;
            case FRAGMENT_SHOP_MY_CART:
                ShopMyCartFragment shopMyCartFragment = ShopMyCartFragment.newInstance();
                fragmentTransaction.add(R.id.parent, shopMyCartFragment);
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(String.valueOf(FRAGMENT_SHOP_MY_CART));
                shopMyCartFragment.setDefaultFragment(this);

                break;
            case FRAGMENT_SHOP_BILLING_AND_SHIPPING:
                ShopBillingAndShipping shopBillingAndShipping = ShopBillingAndShipping.newInstance();
                fragmentTransaction.add(R.id.parent, shopBillingAndShipping);
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(String.valueOf(FRAGMENT_SHOP_BILLING_AND_SHIPPING));
                shopBillingAndShipping.setDefaultFragment(this);

                break;
            case FRAGMENT_SHOP_SHIPPING_METHOD_AND_PAYMENT:
                ShopShippingMethodAndPaymentFragment shopShopShippingAndPaymentFragment = ShopShippingMethodAndPaymentFragment.newInstance();
                fragmentTransaction.add(R.id.parent, shopShopShippingAndPaymentFragment);
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(String.valueOf(FRAGMENT_SHOP_SHIPPING_METHOD_AND_PAYMENT));
                shopShopShippingAndPaymentFragment.setDefaultFragment(this);

                break;
            case FRAGMENT_SHOP_THANK_YOU:
                ShopThankYouFragment shopThankYouFragment = ShopThankYouFragment.newInstance(value);
                fragmentTransaction.replace(R.id.parent, shopThankYouFragment);
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(String.valueOf(FRAGMENT_SHOP_THANK_YOU));
                shopThankYouFragment.setDefaultFragment(this);

                break;

            case FRAGMENT_START_PAYPAL:
                PaypalCheckoutFragment paypalCheckoutFragment = PaypalCheckoutFragment.newInstance(value);
                fragmentTransaction.add(R.id.parent, paypalCheckoutFragment);
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(String.valueOf(FRAGMENT_START_PAYPAL));
                paypalCheckoutFragment.setDefaultFragment(this);

                break;

        }
        fragmentTransaction.commit();
    }


    @Override
    public void popback(@Nullable String keycode) {
        getChildFragmentManager().popBackStack(keycode, 0);
    }

    @Override
    public void popback() {
        getChildFragmentManager().popBackStack();
    }

    @Override
    public void addFragment(Object type, Object value) {

    }


}
