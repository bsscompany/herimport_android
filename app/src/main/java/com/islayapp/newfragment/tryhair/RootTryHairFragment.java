//package com.islayapp.newfragment.tryhair;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.islayapp.R;
//import com.islayapp.util.Constants;
//
//public class RootTryHairFragment extends Fragment {
//
//    private static final String TAG = RootTryHairFragment.class.getName();
//    private View rootView;
//    private UserPhotoFragment userPhotoFragment;
//    private HairStyleFragment hairStyleFragment;
//    private HairFilterFragment hairFilterFragment;
//    private HairCoverFragment hairCoverFragment;
//    private FinalFragment finalFragment;
//    private ProfileImageFragment profileImageFragment;
//    private FragmentManager fm;
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        fm = getChildFragmentManager();
//    }
//
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.root_fragment_whats_hot, container, false);
//
//        FragmentManager fm = getChildFragmentManager();
//        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//
//        fragmentTransaction.replace(R.id.fragmentRootWhatHot, new UserPhotoFragment(), "whats_hot");
//        fragmentTransaction.commit();
//
//        return rootView;
//    }
//
//    public void replaceFragmentWhatsHot(int fragmentCode, int value) {
//        Log.d(TAG, "update fragment");
//        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//
//        switch (fragmentCode) {
//            case Constants.FRAGMENT_WHAT_HOT_USER_PHOTO:
//                if (userPhotoFragment == null)
//                    userPhotoFragment = new UserPhotoFragment();
//
//                if (fm.getBackStackEntryCount() != 0) {
//                    Log.d(TAG, "back stack ok");
//                    fm.popBackStack();
//                } else {
//                    fragmentTransaction.replace(R.id.fragmentRootWhatHot, userPhotoFragment).addToBackStack("vsnkj").commit();
//                }
//                break;
//            case Constants.FRAGMENT_WHAT_HOT_PROFILE_IMAGE:
//                if (profileImageFragment == null)
//                    profileImageFragment = new ProfileImageFragment();
//                fragmentTransaction.replace(R.id.fragmentRootWhatHot, profileImageFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_PROFILE_IMAGE));
//                fragmentTransaction.commit();
//                break;
//            case Constants.FRAGMENT_WHAT_HOT_HAIR_STYLE:
//                if (hairStyleFragment == null)
//                    hairStyleFragment = new HairStyleFragment();
//
//
//                fragmentTransaction.replace(R.id.fragmentRootWhatHot, hairStyleFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_HAIR_STYLE));
//                fragmentTransaction.commit();
//                break;
//            case Constants.FRAGMENT_WHAT_HOT_HAIR_FILTER:
//
//                if (hairStyleFragment != null) {
//                    hairStyleFragment.getOriginBmp();
//                    hairStyleFragment.getOriginBeforeBmp();
//                }
//
//                if (hairFilterFragment == null)
//                    hairFilterFragment = new HairFilterFragment();
//                fragmentTransaction.replace(R.id.fragmentRootWhatHot, hairFilterFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_HAIR_FILTER));
//                fragmentTransaction.commit();
//
//                break;
//            case Constants.FRAGMENT_WHAT_HOT_HAIR_COVER:
//                if (hairCoverFragment == null)
//                    hairCoverFragment = new HairCoverFragment();
//                fragmentTransaction.replace(R.id.fragmentRootWhatHot, hairCoverFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_HAIR_COVER));
//                fragmentTransaction.commit();
//
//                break;
//            case Constants.FRAGMENT_WHAT_HOT_HAIR_FINAL:
//                if (finalFragment == null)
//                    finalFragment = new FinalFragment();
//                fragmentTransaction.replace(R.id.fragmentRootWhatHot, finalFragment, String.valueOf(Constants.FRAGMENT_WHAT_HOT_HAIR_FINAL));
//                fragmentTransaction.commit();
//
//                break;
//            case Constants.FRAGMENT_WHAT_HOT_HAIR_SHARE:
//                if (finalFragment != null) {
//                    finalFragment.ShareImage();
//                }
//                break;
//            case Constants.FRAGMENT_WHAT_HOT_HAIR_SAVE:
//                if (finalFragment != null) {
//                    finalFragment.SaveImage();
//                }
//                break;
//            case Constants.FRAGMENT_WHAT_HOT_HAIR_SUBMIT:
//                if (finalFragment != null) {
//                    finalFragment.SubmitToWinImage();
//                }
//                break;
//            case Constants.FRAGMENT_WHAT_HOT_HOME:
//                if (userPhotoFragment == null)
//                    userPhotoFragment = new UserPhotoFragment();
//
//                fragmentTransaction.replace(R.id.fragmentRootWhatHot, userPhotoFragment).addToBackStack("vsnkj").commit();
//                break;
//        }
//    }
//}
