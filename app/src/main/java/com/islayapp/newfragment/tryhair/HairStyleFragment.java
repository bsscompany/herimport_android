package com.islayapp.newfragment.tryhair;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.islayapp.R;
import com.islayapp.TryHairNewActivity;
import com.islayapp.models.shop.GlobalVariable;
import com.islayapp.util.BitmapUtils;

import java.util.Locale;

import change.look.face.DragController;
import change.look.face.DragLayer;
import change.look.face.WarpView;

@SuppressLint("NewApi")
public class HairStyleFragment extends Fragment implements View.OnClickListener {
    private TryHairNewActivity activity;
    private DragLayer mDragHairLayer;
    private ImageView imgPerson;
    private WarpView imgHair;
    private LinearLayout scrollViewLayout;
    private LinearLayout prevButton;

    private RelativeLayout dragLayout;
    private DragController mDragController;
    private View v;
    private RelativeLayout layoutTip;
    private SharedPreferences preferences;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_hairstyle, container, false);

        activity = (TryHairNewActivity) getActivity();
        preferences = activity.getSharedPreferences("com.islayapp", Context.MODE_PRIVATE);
        Typeface font = Typeface.createFromAsset(activity.getAssets(), "FuturaLT.ttf");

        //init layout_try_hair_bottombar tool tip
        initTip();

        dragLayout = (RelativeLayout) v.findViewById(R.id.hair_draglayout);
        imgPerson = (ImageView) v.findViewById(R.id.imgPerson);
        imgHair = (WarpView) v.findViewById(R.id.imgHair);
        scrollViewLayout = (LinearLayout) v.findViewById(R.id.scrollViewLayout);
        imgPerson.setImageBitmap(GlobalVariable.userPhoto);

//        getOriginBmp();
//        getOriginBeforeBmp();

        if (GlobalVariable.selectHairIndex == 0) {
            imgHair.setImageBitmap(null);
        } else {
            String path = String.format(Locale.getDefault(), "%s/hairstyle_%d.png", GlobalVariable.HairStylesDir, GlobalVariable.selectHairIndex);
            imgHair.setImageBitmap(BitmapUtils.loadBitmapFromAssets(activity, path));
            //imgHair.setImageResource(R.raw.hairstyle_01 + GlobalVariable.selectHairIndex);
        }

        mDragController = new DragController(activity);
        DragController localDragController = mDragController;

        mDragHairLayer = (DragLayer) v.findViewById(R.id.hairdrag_layer);
        mDragHairLayer.setDragController(localDragController);
        localDragController.addDropTarget(mDragHairLayer);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        txtTitle.setTypeface(font);

        prevButton = null;
        redrawHairStyles();

        return v;
    }

    private void redrawHairStyles() {
        scrollViewLayout.removeAllViews();

        for (int i = 0; i < GlobalVariable.HAIRSTYLES.length; i++) {
            LinearLayout btnHairStyle = new LinearLayout(activity);
            //btnHairStyle.setBackgroundResource(R.raw.models_00 + GlobalVariable.HAIRSTYLES[i]);
            String path = String.format(Locale.getDefault(), "%s/thumb_%d.png", GlobalVariable.HairStylesDir, i);
            btnHairStyle.setBackground(BitmapUtils.loadDrawableFromAssets(activity, path));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int) (60 * activity.getResources().getDisplayMetrics().density), (int) (80 * activity.getResources().getDisplayMetrics().density));
            lp.leftMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
            lp.topMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
            lp.bottomMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
            btnHairStyle.setLayoutParams(lp);
            btnHairStyle.setTag(GlobalVariable.HAIRSTYLES[i]);
            btnHairStyle.setOnClickListener(this);
            scrollViewLayout.addView(btnHairStyle);

            ImageView imageFrame = new ImageView(activity);
            imageFrame.setImageResource(R.raw.color_up);
            LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams((int) (60 * activity.getResources().getDisplayMetrics().density), (int) (80 * activity.getResources().getDisplayMetrics().density));
            lp1.leftMargin = 0;
            lp1.topMargin = 0;
            imageFrame.setLayoutParams(lp1);
            imageFrame.setVisibility(View.GONE);
            imageFrame.setScaleType(ScaleType.FIT_XY);
            btnHairStyle.addView(imageFrame);

            //if (GlobalVariable.selectHairIndex >= 1) {
            if (GlobalVariable.selectHairIndex == i) {
                imageFrame.setVisibility(View.VISIBLE);
                prevButton = btnHairStyle;
            }
            //}
        }
    }

    @Override
    public void onClick(View v) {
        if (GlobalVariable.selectHairIndex == Integer.parseInt(v.getTag().toString()))
            return;
        GlobalVariable.selectHairIndex = Integer.parseInt(v.getTag().toString());

        if (GlobalVariable.selectHairIndex == 0) {
            imgHair.setImageBitmap(null);
        } else {
            String path = String.format(Locale.getDefault(), "%s/hairstyle_%d.png", GlobalVariable.HairStylesDir, GlobalVariable.selectHairIndex);
            imgHair.setImageBitmap(BitmapUtils.loadBitmapFromAssets(activity, path));
            //imgHair.setImageResource(R.raw.hairstyle_01 + GlobalVariable.selectHairIndex-1);
            SharedPreferences preferences = activity.getSharedPreferences("com.islayapp", Context.MODE_PRIVATE);
            if (preferences.getInt("showPopup", 1) == 1) {
                showTip();
            }
        }

        if (prevButton != null) {
            ImageView imageFrame = (ImageView) prevButton.getChildAt(0);
            imageFrame.setVisibility(View.GONE);
        }

        GlobalVariable.selectHairIndex = Integer.parseInt(v.getTag().toString());

        //if (GlobalVariable.selectHairIndex >= 1) {
        ImageView imageFrame = (ImageView) ((LinearLayout) v).getChildAt(0);
        imageFrame.setVisibility(View.VISIBLE);

        prevButton = ((LinearLayout) v);
        //}
    }

    public void showTip() {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(500);

        layoutTip.setAnimation(fadeIn);
        layoutTip.setVisibility(View.VISIBLE);
        //layoutTip.startAnimation(fadeIn);
        //layoutTip.setVisibility(View.VISIBLE);
    }


    private void initTip() {
        Typeface font = Typeface.createFromAsset(activity.getAssets(), "FuturaLT.ttf");

        // tips layout_try_hair_bottombar
        layoutTip = (RelativeLayout) v.findViewById(R.id.layoutTip);
        TextView txtTip1 = (TextView) v.findViewById(R.id.txtTip1);
        txtTip1.setTypeface(font);
        TextView txtTip2 = (TextView) v.findViewById(R.id.txtTip2);
        txtTip2.setTypeface(font);
        TextView lblTipTitle = (TextView) v.findViewById(R.id.lblTipTitle);
        lblTipTitle.setTypeface(font);

        layoutTip.setVisibility(View.GONE);
        Button btnDisable = (Button) v.findViewById(R.id.btnDisable);
        btnDisable.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("showPopup", 0);
                editor.commit();

                //layoutTip.setVisibility(View.GONE);
                Animation fadeOut = new AlphaAnimation(1, 0);
                fadeOut.setInterpolator(new AccelerateInterpolator()); //add this
                fadeOut.setDuration(500);

                layoutTip.setAnimation(fadeOut);
                layoutTip.setVisibility(View.GONE);
            }
        });

        Button btnDismiss = (Button) v.findViewById(R.id.btnDismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //layoutTip.setVisibility(View.GONE);
                Animation fadeOut = new AlphaAnimation(1, 0);
                fadeOut.setInterpolator(new AccelerateInterpolator()); //add this
                fadeOut.setDuration(500);

                layoutTip.setAnimation(fadeOut);
                layoutTip.setVisibility(View.GONE);
            }
        });

        layoutTip.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

    }

    public void getOriginBmp() {
        GlobalVariable.styleBitmap = null;
        dragLayout.setDrawingCacheEnabled(true);
        while (GlobalVariable.styleBitmap == null) {
            GlobalVariable.styleBitmap = dragLayout.getDrawingCache();
        }

        GlobalVariable.styleBitmap = Bitmap.createScaledBitmap(GlobalVariable.styleBitmap, GlobalVariable.styleBitmap.getWidth() / 2, GlobalVariable.styleBitmap.getHeight() / 2, true);
    }

    public void getOriginBeforeBmp() {
        imgHair.setVisibility(View.GONE);
        GlobalVariable.originBitmap = null;
        dragLayout.setDrawingCacheEnabled(true);
        while (GlobalVariable.originBitmap == null) {
            GlobalVariable.originBitmap = dragLayout.getDrawingCache();
        }

        GlobalVariable.originBitmap = Bitmap.createScaledBitmap(GlobalVariable.originBitmap, GlobalVariable.originBitmap.getWidth() / 2, GlobalVariable.originBitmap.getHeight() / 2, true);
        imgHair.setVisibility(View.VISIBLE);
    }
}