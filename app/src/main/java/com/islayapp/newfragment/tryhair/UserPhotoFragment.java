package com.islayapp.newfragment.tryhair;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.islayapp.R;
import com.islayapp.models.shop.GlobalVariable;

public class UserPhotoFragment extends Fragment {
    private ImageView imgPerson;
    private TextView txtView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_userphoto, container, false);

        imgPerson = (ImageView) v.findViewById(R.id.imageView1);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "FuturaLT.ttf");

        txtView = (TextView) v.findViewById(R.id.textView1);
        txtView.setTypeface(font);
        txtView.setText(getActivity().getResources().getString(R.string.get_start));

        redrawPerson();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        redrawPerson();
    }

    public void redrawPerson() {
        if (GlobalVariable.userPhoto != null) {
            imgPerson.setImageBitmap(GlobalVariable.userPhoto);
            txtView.setVisibility(View.GONE);
        } else {
            txtView.setVisibility(View.VISIBLE);
        }
    }
}