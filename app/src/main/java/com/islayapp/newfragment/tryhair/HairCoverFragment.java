//package com.islayapp.newfragment.tryhair;
//
//import android.annotation.SuppressLint;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.ImageView.ScaleType;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.islayapp.R;
//import com.islayapp.TryHairNewActivity;
//import com.islayapp.models.shop.GlobalVariable;
//import com.islayapp.util.BitmapUtils;
//
//import java.util.Locale;
//
//@SuppressLint("NewApi")
//public class HairCoverFragment extends Fragment implements View.OnClickListener {
//
//	private TryHairNewActivity activity;
//	private ImageView imgPerson;
//	private LinearLayout scrollViewLayout, prevButton;
//	private ImageView imgCover;
//
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		View v = inflater.inflate(R.layout.fragment_haircover, container, false);
//
//		activity = (TryHairNewActivity) getActivity();
//
//		Typeface font = Typeface.createFromAsset(activity.getAssets(), "FuturaLT.ttf");
//
//		imgPerson = (ImageView) v.findViewById(R.id.imgPerson);
//		imgCover = (ImageView) v.findViewById(R.id.imgCover);
//		scrollViewLayout = (LinearLayout) v.findViewById(R.id.scrollViewLayout);
//
//		imgPerson.setImageBitmap(GlobalVariable.renderFilter(activity));
//
//		if (GlobalVariable.selectCoverIndex <= 0)
//			imgCover.setImageBitmap(null);
//		else{
//			//imgCover.setImageResource(R.raw.logocover_01 + (GlobalVariable.selectCoverIndex - 1));
//			String path = String.format(Locale.getDefault(), "%s/logocover_%d.png", GlobalVariable.CoversDir, GlobalVariable.selectCoverIndex);
//			imgCover.setImageDrawable(BitmapUtils.loadDrawableFromAssets(activity,path));
//		}
//
//		TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
//		txtTitle.setTypeface(font);
//		redrawHairCovers();
//
//		return v;
//	}
//
//	private void redrawHairCovers() {
//		scrollViewLayout.removeAllViews();
//
//		for (int i = 0; i < GlobalVariable.HAIRCOVERS.length; i++) {
//			LinearLayout btnCover = new LinearLayout(activity);
//			//btnCover.setBackgroundResource(R.raw.covers_00 + GlobalVariable.HAIRCOVERS[i]);
//			String path = String.format(Locale.getDefault(), "%s/thumb_%d.png", GlobalVariable.CoversDir, i);
//			btnCover.setBackground(BitmapUtils.loadDrawableFromAssets(activity,path));
//
//			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int)(60 * activity.getResources().getDisplayMetrics().density), (int)(80 * activity.getResources().getDisplayMetrics().density));
//			lp.leftMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
//			lp.topMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
//			lp.bottomMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
//			btnCover.setLayoutParams(lp);
//			btnCover.setTag(GlobalVariable.HAIRCOVERS[i]);
//			btnCover.setOnClickListener(this);
//			scrollViewLayout.addView(btnCover);
//
//			ImageView imageFrame = new ImageView(activity);
//			imageFrame.setImageResource(R.raw.color_up);
//			LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams((int)(60 * activity.getResources().getDisplayMetrics().density), (int)(80 * activity.getResources().getDisplayMetrics().density));
//			lp1.leftMargin = 0;
//			lp1.topMargin = 0;
//			imageFrame.setLayoutParams(lp1);
//			imageFrame.setVisibility(View.GONE);
//			imageFrame.setScaleType(ScaleType.FIT_XY);
//			btnCover.addView(imageFrame);
//
//			//if (GlobalVariable.selectCoverIndex >= 1) {
//				if (GlobalVariable.selectCoverIndex == i) {
//					imageFrame.setVisibility(View.VISIBLE);
//					prevButton = btnCover;
//				}
//			//}
//		}
//
//	}
//
//	@Override
//	public void onClick(View v) {
//		if (GlobalVariable.selectCoverIndex == Integer.parseInt(v.getTag().toString()))
//			return;
//
//		if (prevButton != null) {
//			ImageView imageFrame = (ImageView) prevButton.getChildAt(0);
//			imageFrame.setVisibility(View.GONE);
//		}
//
//		GlobalVariable.selectCoverIndex = Integer.parseInt(v.getTag().toString());
//
//		if (GlobalVariable.selectCoverIndex <= 0)
//			imgCover.setImageBitmap(null);
//		else {
//			//imgCover.setImageResource(R.raw.logocover_01 + (GlobalVariable.selectCoverIndex - 1));
//			String path = String.format(Locale.getDefault(), "%s/logocover_%d.png", GlobalVariable.CoversDir, GlobalVariable.selectCoverIndex);
//			imgCover.setImageDrawable(BitmapUtils.loadDrawableFromAssets(activity,path));
//		}
//
//		ImageView imageFrame = (ImageView) ((LinearLayout)v).getChildAt(0);
//		imageFrame.setVisibility(View.VISIBLE);
//
//		prevButton = ((LinearLayout)v);
//
//	}
//}