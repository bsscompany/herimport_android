package com.islayapp.newfragment.tryhair;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.islayapp.R;
import com.islayapp.TryHairNewActivity;
import com.islayapp.models.shop.GlobalVariable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

//import jp.co.cyberagent.android.gpuimage.GPUImageView;

@SuppressLint("NewApi")
public class FinalFragment extends Fragment {

    private TryHairNewActivity activity;
    private ImageView imgBefore;
    private ImageView imgPerson, imgAfter;
//    private ImageView imgCover;
    private RelativeLayout dragLayout, layoutCompare, layoutSlider;
    private Bitmap bmp;
    private TextView lblFinal, lblName, lblColor, lblOption, lblTotal, lblPrice, lblFinal2, lblFinal3;
    private HorizontalScrollView scroll;
    private boolean isMovingSlider;
    private int screenWidth, downPosition, layoutBeforeLeftMargin;
    private float density;
    private LinearLayout layoutBefore;
    private SharedPreferences preferences;
    private ProgressDialog progressDialog;

    private File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/herimports");

    private String fileName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_final, container, false);


        preferences = getActivity().getSharedPreferences("com.islayapp", Context.MODE_PRIVATE);


        Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();

        screenWidth = (int) (config.screenWidthDp * dm.density);

        //int topDp = (int) (((float)290 / (float)config.screenWidthDp) * (float)40);

        density = dm.density;

        layoutBeforeLeftMargin = (int) (screenWidth - 290 * density) / 2;

        activity = (TryHairNewActivity) getActivity();

        isMovingSlider = false;

        Typeface font = Typeface.createFromAsset(activity.getAssets(), "FuturaLT.ttf");
        Typeface fontObl = Typeface.createFromAsset(activity.getAssets(), "FuturaLT-Oblique.ttf");

        imgPerson = (ImageView) v.findViewById(R.id.imgPerson);
//        imgCover = (ImageView) v.findViewById(R.id.imgCover);
        dragLayout = (RelativeLayout) v.findViewById(R.id.draglayout);
        layoutCompare = (RelativeLayout) v.findViewById(R.id.layoutCompare);
        imgAfter = (ImageView) v.findViewById(R.id.imgAfter);
        imgBefore = (ImageView) v.findViewById(R.id.imgBefore);
        layoutSlider = (RelativeLayout) v.findViewById(R.id.layoutSlider);
        layoutBefore = (LinearLayout) v.findViewById(R.id.layoutBefore);

        lblFinal = (TextView) v.findViewById(R.id.lblFinal);
        lblFinal2 = (TextView) v.findViewById(R.id.lblFinal2);
        lblFinal3 = (TextView) v.findViewById(R.id.lblFinal3);
        lblName = (TextView) v.findViewById(R.id.lblName);
        lblColor = (TextView) v.findViewById(R.id.lblColor);
        lblOption = (TextView) v.findViewById(R.id.lblOption);
        lblTotal = (TextView) v.findViewById(R.id.lblTotal);
        lblPrice = (TextView) v.findViewById(R.id.lblPrice);

        lblFinal.setTypeface(fontObl);
        lblFinal2.setTypeface(fontObl);
        lblFinal3.setTypeface(fontObl);
        lblName.setTypeface(font);
        lblColor.setTypeface(font);
        lblOption.setTypeface(font);
        lblTotal.setTypeface(font);
        lblPrice.setTypeface(font);

        TextView lblBefore = (TextView) v.findViewById(R.id.lblBefore);
        lblBefore.setTypeface(font);
        TextView lblAfter = (TextView) v.findViewById(R.id.lblAfter);
        lblAfter.setTypeface(font);

        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        txtTitle.setTypeface(font);

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) dragLayout.getLayoutParams();
        lp.width = screenWidth;
        dragLayout.setLayoutParams(lp);

        RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) layoutSlider.getLayoutParams();
        lp1.leftMargin = (int) (screenWidth - 40 * dm.density) / 2;
        layoutSlider.setLayoutParams(lp1);

        imgBefore.setImageBitmap(GlobalVariable.originBitmap);

        lp = (LinearLayout.LayoutParams) layoutCompare.getLayoutParams();
        lp.width = screenWidth;
        layoutCompare.setLayoutParams(lp);

        imgPerson.setImageBitmap(GlobalVariable.renderFilter(activity));

        imgAfter.setImageBitmap(GlobalVariable.renderFilter(activity));
        /*
        lp1 = (RelativeLayout.LayoutParams) imgBefore.getLayoutParams();
	    lp1.topMargin = 0 - (int) (topDp * dm.density);
	    lp1.height = (int) ((384 + topDp * 2) * dm.density);
	    lp1.width = (int) (290 * dm.density);
	    lp1.bottomMargin = 0 - (int) (topDp * dm.density);
	    imgBefore.setLayoutParams(lp1);
	    */

//        if (GlobalVariable.selectCoverIndex <= 0)
//            imgCover.setImageBitmap(null);
//        else {
//            //imgCover.setImageResource(R.raw.logocover_01 + (GlobalVariable.selectCoverIndex - 1));
//            String path = String.format(Locale.getDefault(), "%s/logocover_%d.png", GlobalVariable.CoversDir, GlobalVariable.selectCoverIndex);
//            imgCover.setImageDrawable(BitmapUtils.loadDrawableFromAssets(getActivity(), path));
//        }

        bmp = null;

        ImageView imgView1 = (ImageView) v.findViewById(R.id.imageView1);
        imgView1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.herimports.com"));
                startActivity(intent);
            }
        });

        scroll = (HorizontalScrollView) v.findViewById(R.id.horizontalScrollView1);
        scroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (isMovingSlider == true) {
                        isMovingSlider = false;
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutSlider.getLayoutParams();
                        lp.leftMargin = (int) (event.getX() - 20 * density);
                        layoutSlider.setLayoutParams(lp);

                        LinearLayout.LayoutParams lp1 = (LinearLayout.LayoutParams) layoutBefore.getLayoutParams();
                        if ((int) (event.getX()) - layoutBeforeLeftMargin >= 0)
                            lp1.width = (int) (event.getX()) - layoutBeforeLeftMargin;
                        else
                            lp1.width = 0;
                        layoutBefore.setLayoutParams(lp1);

                        return true;
                    }
                   /* int currentPosition = scroll.getScrollX();
                    Log.e("Wang", "up: " + currentPosition);
	                if (currentPosition > downPosition) {
	                	scroll.scrollTo(screenWidth, 0);
	                } else {
	                	scroll.scrollTo(0, 0);
	                }*/
                }

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    downPosition = scroll.getScrollX();
                    Log.e("Wang", "down: " + downPosition);
                }

                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    if (isMovingSlider == true) {
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutSlider.getLayoutParams();
                        lp.leftMargin = (int) (event.getX() - 20 * density);
                        layoutSlider.setLayoutParams(lp);

                        LinearLayout.LayoutParams lp1 = (LinearLayout.LayoutParams) layoutBefore.getLayoutParams();
                        if ((int) (event.getX()) - layoutBeforeLeftMargin >= 0)
                            lp1.width = (int) (event.getX()) - layoutBeforeLeftMargin;
                        else
                            lp1.width = 0;
                        layoutBefore.setLayoutParams(lp1);

                        return true;
                    }

                }

                return true;
            }
        });

        layoutSlider.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    isMovingSlider = true;
                } /*else if (event.getAction() == MotionEvent.ACTION_UP) {
                    Log.e("Wang", "asdf qwerwq : " + event.getX());
					isMovingSlider = false;
				} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
					if (isMovingSlider == true)
						Log.e("Wang", "SDK : " + event.getX());
				}*/
                return false;
            }
        });

        return v;
    }


    public void ShareImage() {
        ShareTask task = new ShareTask();
        task.execute(new String[]{""});
    }

    public void SaveImage() {
        SaveTask task = new SaveTask();
        task.execute(new String[]{""});
    }

//    public void SubmitToWinImage() {
//        SubmitPhotoTask task = new SubmitPhotoTask();
//        task.execute(new String[]{""});
//    }

    public Bitmap getFinalImage() {
        if (bmp == null) {
            dragLayout.setDrawingCacheEnabled(true);
            bmp = dragLayout.getDrawingCache();

            bmp = Bitmap.createScaledBitmap(bmp, bmp.getWidth() / 2, bmp.getHeight() / 2, true);
        }

        return bmp;
    }

    private void initDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getResources().getString(R.string.saving));
        }
    }

    public void save(Bitmap bmp, int flag) {
        Calendar localCalendar = Calendar.getInstance();
        if (flag == 1 || flag == 2)
            fileName = new SimpleDateFormat("yyyyMMddhhmmss").format(localCalendar.getTime());
        else
            fileName = "share";

        try {
            FileOutputStream inout = new FileOutputStream(new File(root.getAbsolutePath(), fileName + ".png"));
            bmp.compress(Bitmap.CompressFormat.PNG, 90, inout);
            inout.flush();
            inout.close();

            return;
        } catch (FileNotFoundException localFileNotFoundException) {
            while (true) {
                localFileNotFoundException.printStackTrace();
                Toast.makeText(getActivity(), "Image not saved", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException localIOException) {
            while (true) {
                localIOException.printStackTrace();
                Toast.makeText(getActivity(), "Image not saved", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void share() {
        Intent localIntent = new Intent("android.intent.action.SEND");
        localIntent.setType("image/png");
        localIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(root, fileName + ".png")));
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "";
        localIntent.putExtra("android.intent.extra.EMAIL", arrayOfString);
        localIntent.putExtra("android.intent.extra.SUBJECT", "See This New Hair Style");
        startActivity(Intent.createChooser(localIntent, "Share Via"));
    }

//    private class SubmitPhotoTask extends AsyncTask<String, Void, String> {
//
//
//        public SubmitPhotoTask() {
//            initDialog();
//            progressDialog.setMessage(getResources().getString(R.string.submitting));
//            progressDialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... urls) {
//            Bitmap bmp = FinalFragment.this.getFinalImage();
//            save(bmp, 2);
//
//
//            //setup params
//            Map<String, String> params = new HashMap<String, String>(2);
//            params.put("user_id", preferences.getString("ID", ""));
//            String url = GlobalVariable.API_URL + GlobalVariable.UPLOAD_PHOTO;
//            String path = new File(root, fileName + ".png").toString();
//
//            String result = Utility.multipartRequest(url, params, path, "uploaded_file", "image/png");
//
//            return result;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            Log.d("TAG", "Result upload file: " + result);
//            progressDialog.hide();
//            if (result.equals("success"))
//                Toast.makeText(getActivity(), "Successfully submitted!", Toast.LENGTH_SHORT).show();
//        }
//    }

    private class SaveTask extends AsyncTask<String, Void, String> {

        public SaveTask() {
            initDialog();
            progressDialog.setMessage(getResources().getString(R.string.saving));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            Bitmap bmp = FinalFragment.this.getFinalImage();
            save(bmp, 1);

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.hide();
            Toast.makeText(getActivity(), "Image Saved Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    private class ShareTask extends AsyncTask<String, Void, String> {


        public ShareTask() {
            initDialog();
            progressDialog.setMessage(getResources().getString(R.string.saving));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            Bitmap bmp = FinalFragment.this.getFinalImage();
            save(bmp, 0);

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.hide();

            share();
        }
    }


}