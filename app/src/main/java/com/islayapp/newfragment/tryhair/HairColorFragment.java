//package com.islayapp.fragments;
//
//import java.util.Locale;
//
//import android.annotation.SuppressLint;
//import android.app.Fragment;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.ImageView.ScaleType;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.islayapp.HomeActivity;
//import com.islayapp.R;
//import com.islayapp.model.GlobalVariable;
//
//@SuppressLint("NewApi")
//public class HairColorFragment extends Fragment implements View.OnClickListener {
//	private HomeActivity activity;
//	private ImageView imgPerson;//, imgHair;
//	private LinearLayout scrollViewLayout;
//	private LinearLayout prevButton;
//
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		View v = inflater.inflate(R.layout_try_hair_bottombar.fragment_haircolor, container, false);
//
//		activity = (HomeActivity) getActivity();
//
//		Typeface font = Typeface.createFromAsset(activity.getAssets(), "FuturaLT.ttf");
//
//		imgPerson = (ImageView) v.findViewById(R.id.imgPerson);
//		scrollViewLayout = (LinearLayout) v.findViewById(R.id.scrollViewLayout);
//
//		//imgPerson.setImageBitmap(GlobalVariable.userPhoto);
//		imgPerson.setImageBitmap(GlobalVariable.styleBitmap);
//
//		TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
//		txtTitle.setTypeface(font);
//
//		prevButton = null;
//		redrawHairColors();
//
//		return v;
//	}
//
//	private void redrawHairColors() {
//		scrollViewLayout.removeAllViews();
//
//		for (int i = 0; i < GlobalVariable.HAIRCOLORS.length; i++) {
//			LinearLayout btnHairColor = new LinearLayout(activity);
//			//btnHairColor.setBackgroundResource(R.raw.color_00 + GlobalVariable.HAIRCOLORS[i]);
//			String path = String.format(Locale.getDefault(), "%s/thumb_%d.png", GlobalVariable.HairColorsDir, i);
//			btnHairColor.setBackground(activity.loadDrawableFromAssets(path));
//			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int)(60 * activity.getResources().getDisplayMetrics().density), (int)(80 * activity.getResources().getDisplayMetrics().density));
//			lp.leftMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
//			lp.topMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
//			lp.bottomMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
//			btnHairColor.setLayoutParams(lp);
//			btnHairColor.setTag(GlobalVariable.HAIRCOLORS[i]);
//			btnHairColor.setOnClickListener(this);
//			scrollViewLayout.addView(btnHairColor);
//
//			ImageView imageFrame = new ImageView(activity);
//			imageFrame.setImageResource(R.raw.color_up);
//			LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams((int)(60 * activity.getResources().getDisplayMetrics().density), (int)(80 * activity.getResources().getDisplayMetrics().density));
//			lp1.leftMargin = 0;
//			lp1.topMargin = 0;
//			imageFrame.setLayoutParams(lp1);
//			imageFrame.setVisibility(View.GONE);
//			imageFrame.setScaleType(ScaleType.FIT_XY);
//			btnHairColor.addView(imageFrame);
//
//			//if (GlobalVariable.selectHairColorIndex >= 1) {
//				if (GlobalVariable.selectHairColorIndex == i) {
//					imageFrame.setVisibility(View.VISIBLE);
//					prevButton = btnHairColor;
//				}
//			//}
//		}
//
//	}
//
//	@Override
//	public void onClick(View v) {
//		if (GlobalVariable.selectHairColorIndex == Integer.parseInt(v.getTag().toString()))
//			return;
//		if (prevButton != null) {
//			ImageView imageFrame = (ImageView) prevButton.getChildAt(0);
//			imageFrame.setVisibility(View.GONE);
//		}
//
//		GlobalVariable.selectHairColorIndex = Integer.parseInt(v.getTag().toString());
//
//		//if (GlobalVariable.selectHairColorIndex >= 1) {
//			ImageView imageFrame = (ImageView) ((LinearLayout)v).getChildAt(0);
//			imageFrame.setVisibility(View.VISIBLE);
//
//			prevButton = ((LinearLayout)v);
//		//}
//		/*
//		if (GlobalVariable.selectHairColorIndex == -1) {
//			imgHair.setImageBitmap(null);
//		} else {
//			imgHair.setImageResource(R.raw.hairstyle_01 + GlobalVariable.selectHairIndex);
//		}*/
//	}
//}