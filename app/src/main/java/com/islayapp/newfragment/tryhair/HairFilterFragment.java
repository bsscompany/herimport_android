package com.islayapp.newfragment.tryhair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.islayapp.R;
import com.islayapp.models.shop.GlobalVariable;
import com.islayapp.util.BitmapUtils;

import java.util.Locale;

@SuppressLint("NewApi")
public class HairFilterFragment extends Fragment implements View.OnClickListener {
	private Activity activity;
	private ImageView imgPerson;
	private LinearLayout scrollViewLayout;
	private RelativeLayout prevButton;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_hairfilter, container, false);
		
		activity = getActivity();
		
		Typeface font = Typeface.createFromAsset(activity.getAssets(), "FuturaLT.ttf");  
		
		imgPerson = (ImageView) v.findViewById(R.id.imgPerson);
		scrollViewLayout = (LinearLayout) v.findViewById(R.id.scrollViewLayout);
		
		//imgPerson.setImageBitmap(GlobalVariable.userPhoto);
		imgPerson.setImageBitmap(GlobalVariable.styleBitmap);

		TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
		txtTitle.setTypeface(font);
		
		prevButton = null;
		redrawHairFilters();
		
		//GlobalVariable.renderFilter(imgPerson);
		return v;
	}
	
	private void redrawHairFilters() {
		scrollViewLayout.removeAllViews();
		
		Typeface font = Typeface.createFromAsset(activity.getAssets(), "FuturaLT-Light.ttf");  
		
		for (int i = 0; i < GlobalVariable.HAIRFILTERS.length; i++) {
			RelativeLayout btnHairFilter = new RelativeLayout(activity);
			//btnHairFilter.setBackgroundResource(R.raw.filter_00 + GlobalVariable.HAIRFILTERS[i]);
			String path = String.format(Locale.getDefault(), "%s/thumb_%d.png", GlobalVariable.FiltersDir, i);
			btnHairFilter.setBackground(BitmapUtils.loadDrawableFromAssets(activity,path));
			
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int)(60 * activity.getResources().getDisplayMetrics().density), (int)(80 * activity.getResources().getDisplayMetrics().density));
			lp.leftMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
			lp.topMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
			lp.bottomMargin = (int) (1 * activity.getResources().getDisplayMetrics().density);
			btnHairFilter.setLayoutParams(lp);
			btnHairFilter.setTag(GlobalVariable.HAIRFILTERS[i]);
			btnHairFilter.setOnClickListener(this);
			scrollViewLayout.addView(btnHairFilter);
			
			if (i >= 1) {
				TextView txtBack = new TextView(activity);
				txtBack.setBackgroundColor(Color.BLACK);
				txtBack.setAlpha(0.6f);
				RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams((int)(60 * activity.getResources().getDisplayMetrics().density), (int)(15 * activity.getResources().getDisplayMetrics().density));
				lp1.leftMargin = 0;
				lp1.topMargin = (int) (65 * activity.getResources().getDisplayMetrics().density);
				txtBack.setLayoutParams(lp1);
				btnHairFilter.addView(txtBack);
				
				TextView txtBack1 = new TextView(activity);
				txtBack1.setTextColor(Color.WHITE);
				txtBack1.setTypeface(font);
				txtBack1.setTextSize(12);
				txtBack1.setText(GlobalVariable.HAIRFILTERNAMES[i].toUpperCase(Locale.getDefault()));
				txtBack1.setGravity(Gravity.CENTER);
				RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams((int)(60 * activity.getResources().getDisplayMetrics().density), (int)(15 * activity.getResources().getDisplayMetrics().density));
				lp2.leftMargin = 0;
				lp2.topMargin = (int) (65 * activity.getResources().getDisplayMetrics().density);
				txtBack1.setLayoutParams(lp2);
				btnHairFilter.addView(txtBack1);
			}
			ImageView imageFrame = new ImageView(activity);
			imageFrame.setImageResource(R.raw.color_up);
			LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams((int)(60 * activity.getResources().getDisplayMetrics().density), (int)(80 * activity.getResources().getDisplayMetrics().density));
			lp1.leftMargin = 0;
			lp1.topMargin = 0;
			imageFrame.setLayoutParams(lp1);
			imageFrame.setVisibility(View.GONE);
			imageFrame.setScaleType(ScaleType.FIT_XY);
			btnHairFilter.addView(imageFrame);
			//if (GlobalVariable.selectFilterIndex >= 1) {
				if (GlobalVariable.selectFilterIndex == i) {
					imageFrame.setVisibility(View.VISIBLE);
					prevButton = btnHairFilter;
				}
			//}
			
		}
		
		if (GlobalVariable.selectFilterIndex > 0) {
			imgPerson.setImageBitmap(GlobalVariable.renderFilter(activity));
		}
	}
	
	@Override
	public void onClick(View v) {
		int currentIndex = Integer.parseInt(v.getTag().toString());
		ImageView imageFrame;
		if (GlobalVariable.selectFilterIndex == currentIndex)
			return;
		if (prevButton != null && GlobalVariable.selectFilterIndex>=1) {
			imageFrame = (ImageView) prevButton.getChildAt(2);
			imageFrame.setVisibility(View.GONE);
		} else {
			imageFrame = (ImageView) prevButton.getChildAt(0);
			imageFrame.setVisibility(View.GONE);
		}
		GlobalVariable.selectFilterIndex = currentIndex;
		
		if (currentIndex >= 1) {
			imageFrame = (ImageView) ((RelativeLayout)v).getChildAt(2);
		} else {
			imageFrame = (ImageView) ((RelativeLayout)v).getChildAt(0);
		}
		imageFrame.setVisibility(View.VISIBLE);
		prevButton = ((RelativeLayout)v);
		
	    imgPerson.setImageBitmap(GlobalVariable.renderFilter(activity));
		/*
		if (GlobalVariable.selectFilterIndex == -1) {
			imgHair.setImageBitmap(null);
		} else {
			imgHair.setImageResource(R.raw.hairstyle_01 + GlobalVariable.selectHairIndex);
		}*/
	}
}