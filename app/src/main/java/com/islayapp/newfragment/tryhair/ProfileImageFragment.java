package com.islayapp.newfragment.tryhair;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.media.MediaActionSound;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.islayapp.R;
import com.islayapp.models.shop.CameraPreview;
import com.islayapp.models.shop.GlobalVariable;

import java.io.IOException;
import java.util.List;

@SuppressLint("NewApi")
public class ProfileImageFragment extends Fragment implements View.OnClickListener {

    private Camera mCamera;
    private CameraPreview mPreview;
    private LinearLayout cameraPreview;

    private Button captureButton;
    private Button flashButton;
    private Button switchCameraButton;

    boolean flashFlag = false;
    boolean cameraFront = false;
    boolean recordedFlag = false;
    private View v;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_profileimage, container, false);

        captureButton = (Button) v.findViewById(R.id.RecordButton);
        captureButton.setOnClickListener(this);

        cameraPreview = (LinearLayout) v.findViewById(R.id.camera_preview);
        mPreview = new CameraPreview(getActivity(), mCamera);
        cameraPreview.addView(mPreview);
        return v;
    }


    @Override
    public void onStart() {
        super.onStart();

        openCamera();


    }

    private void openCamera() {
        if (!hasCamera(getActivity())) {
            Toast toast = Toast.makeText(getActivity(), "Sorry, your phone does not have a camera!", Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            if (mCamera == null) {
                // if the front facing camera does not exist
                if (findFrontFacingCamera() < 0) {
                    Toast.makeText(getActivity(), "No front facing camera found.", Toast.LENGTH_LONG).show();
//				switchCameraButton.setVisibility(View.GONE);
                }

                mCamera = Camera.open(findFrontFacingCamera());

                Resources resources = getResources();
                Configuration config = resources.getConfiguration();
                DisplayMetrics dm = resources.getDisplayMetrics();
                int width = (int) (config.screenWidthDp * dm.density);
                int height = width * dm.heightPixels / dm.widthPixels;

                Camera.Parameters parameters = mCamera.getParameters();

                List sizes = parameters.getSupportedPictureSizes();
                Camera.Size result = null;
                for (int i = 0; i < sizes.size(); i++) {
                    result = (Size) sizes.get(i);
                }

                Camera.Size size = getOptimalSize(sizes, height, width);
                //if (width > height)
                //	size = getOptimalSize(sizes, width, height);
                if (size != null) {
                    parameters.setPreviewSize(size.width, size.height);

                    mCamera.setParameters(parameters);
                    //cameraConfigured=true;
                }

                mPreview.refreshCamera(mCamera);
            }


            if (mCamera == null) {
                mCamera = Camera.open();
            }


        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.RecordButton:
                if (mCamera != null) {
                    try {
                        mCamera.stopPreview();
                        MediaActionSound sound = new MediaActionSound();
                        sound.play(MediaActionSound.SHUTTER_CLICK);
                        Resources resources = getResources();
                        Configuration config = resources.getConfiguration();
                        DisplayMetrics dm = resources.getDisplayMetrics();
                        int width = (int) (config.screenWidthDp * dm.density);
                        int height = width * dm.heightPixels / dm.widthPixels;

                        Bitmap bmp = mPreview.getBitmap();

                        bmp = Bitmap.createScaledBitmap(bmp, bmp.getWidth(), bmp.getHeight(), true);
                        Log.e("Wang", "" + bmp.getWidth() + " : " + bmp.getHeight() + " : " + width + " : " + height);
                        Matrix matrix = new Matrix();
                        if (cameraFront) {
                            matrix.preRotate(270);
                        } else {
                            matrix.preRotate(90);
                        }

                        Bitmap adjustedBitmap;

                        if (height == bmp.getWidth()) {
                            adjustedBitmap = Bitmap.createBitmap(bmp, (int) (30 * dm.density), 0, (int) (bmp.getWidth() - 60 * dm.density), bmp.getHeight(), matrix, true);
                        } else {
                            float yRatio = (float) (height - 60 * dm.density) / (float) bmp.getWidth();
                            float xRatio = (float) width / (float) bmp.getHeight();
                            float startX, startY, bmpWid, bmpHei;

                            if (yRatio >= xRatio) {
                                startX = 0;
                                startY = ((float) bmp.getHeight() - ((float) width / yRatio)) / 2;
                                bmpWid = bmp.getWidth();
                                bmpHei = ((float) width / yRatio);

                                if (startY < 0)
                                    startY = 0;
                                if (bmpHei > bmp.getHeight())
                                    bmpHei = bmp.getHeight();

                                adjustedBitmap = Bitmap.createBitmap(bmp, (int) startX, (int) startY, (int) bmpWid, (int) bmpHei, matrix, true);
                            } else {
                                xRatio = (float) (width - 60 * dm.density) / (float) bmp.getHeight();
                                startY = 0;
                                startX = bmp.getWidth() - bmp.getHeight() * ((float) (height - 60 * dm.density) / (float) width) - 5;
                                bmpHei = bmp.getHeight();
                                bmpWid = bmp.getHeight() * ((float) (height - 60 * dm.density) / (float) width);

                                if (startX < 0)
                                    startX = 0;
                                if (bmpWid > bmp.getWidth())
                                    bmpWid = bmp.getWidth();

                                adjustedBitmap = Bitmap.createBitmap(bmp, (int) startX, (int) startY, (int) bmpWid, (int) bmpHei, matrix, true);
                            }
                        }

                        //adjustedBitmap.setConfig(Bitmap.Config.ARGB_8888);
                    /*adjustedBitmap.setHasAlpha(true);

			        Log.e("Wang", "" + bmp.getWidth() + " : " + bmp.getHeight());
			        Bitmap maskBmp = BitmapFactory.decodeResource(getResources(), R.raw.cameraimagemask1);
			        maskBmp = Bitmap.createScaledBitmap(maskBmp, bmp.getHeight(), bmp.getWidth(), true);

			        Canvas canvas = new Canvas(adjustedBitmap);
			        Paint paint = new Paint();
			        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
			        canvas.drawBitmap(maskBmp, 0, 0, paint);*/
                        // We do not need the mask bitmap anymore.
                        //maskBmp.recycle();

                        GlobalVariable.userPhoto = adjustedBitmap;

                        GlobalVariable.writeUserPhoto();

                        releaseCamera();

//					((NewHomeActivity) getActivity()).updateFragmentWhatsHot(Constants.FRAGMENT_WHAT_HOT_USER_PHOTO, 0);

                    } catch (IOException e) {
                    }
                } else {
                    openCamera();
                }
                break;
            default:
        }

    }


    public void chooseCamera() {
        // if the camera preview is the front
        if (cameraFront) {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                mCamera = Camera.open(cameraId);
                mPreview.refreshCamera(mCamera);
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                mCamera = Camera.open(cameraId);
                mPreview.refreshCamera(mCamera);
            }
        }
    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        // Search for the back facing camera
        // get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        // for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            CameraInfo info = new CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;
            }
        }
        return cameraId;
    }

    @SuppressLint("NewApi")
    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            CameraInfo info = new CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;
    }

    private boolean hasCamera(Context context) {
        // check if the device has camera
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    private void releaseCamera() {

        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mPreview.getHolder().removeCallback(mPreview);
            mCamera.release();
        }
        // stop and release camera
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }


    }

    /**
     * Calculate the optimal size of camera preview
     *
     * @param sizes
     * @param w
     * @param h
     * @return
     */
    private Size getOptimalSize(List<Size> sizes, int w, int h) {

        final double ASPECT_TOLERANCE = 0.2;
        double targetRatio = (double) w / h;
        if (sizes == null)
            return null;
        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        // Try to find an size match aspect ratio and size         
        for (Size size : sizes) {
//          Log.d("CameraActivity", "Checking size " + size.width + "w " + size.height + "h");            
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        // Cannot find the one match the aspect ratio, ignore the requirement     

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        Log.e("Wang", "Size: " + optimalSize.width + " : " + optimalSize.height);
/*
        SharedPreferences previewSizePref;
        if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            previewSizePref = getSharedPreferences("PREVIEW_PREF", MODE_PRIVATE);
        } else {
            previewSizePref = getSharedPreferences("FRONT_PREVIEW_PREF",MODE_PRIVATE);
        }

        SharedPreferences.Editor prefEditor = previewSizePref.edit();
        prefEditor.putInt("width", optimalSize.width);
        prefEditor.putInt("height", optimalSize.height);

        prefEditor.commit();*/

//      Log.d("CameraActivity", "Using size: " + optimalSize.width + "w " + optimalSize.height + "h");            
        return optimalSize;
    }


}
