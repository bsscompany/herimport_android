package com.islayapp.newfragment.general;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.amazonaws.AmazonClientException;
import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.Util;
import com.amazonaws.mobileconnectors.kinesis.kinesisrecorder.KinesisRecorder;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.bumptech.glide.Glide;
import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.newfragment.getlayd.Views.EmojiconEditTextPlus;
import com.islayapp.util.BitmapUtils;
import com.islayapp.util.Constants;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.Session;
import com.islayapp.util.Utility;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import me.iwf.photopicker.PhotoPickerActivity;
import me.iwf.photopicker.utils.PhotoPickerIntent;
import uk.co.senab.photoview.PhotoView;

public class NewPostFragment extends BaseFragment implements View.OnClickListener {
    public static NewPostFragment newInstance() {
        NewPostFragment fragmentFirst = new NewPostFragment();
        return fragmentFirst;
    }

    private static final String TAG = NewPostFragment.class.getName();
    public final static int REQUEST_CODE = 1;

    ArrayList<String> selectedPhotos = new ArrayList<>();
    private LinearLayout layout_select_image;
    private RelativeLayout layout_show_image;
    private int screenSize = 500;
    private File file;
    // The TransferUtility is the primary class for managing transfer to S3
    private TransferUtility transferUtility;

    //    private ArrayList<HashMap<String, Object>> transferRecordMaps;
    // A List of all transfers
    private List<TransferObserver> observers;
    private KinesisRecorder recorder;
    private EmojiconEditTextPlus edittext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initializes TransferUtility, always do this before using it.
        transferUtility = Util.getTransferUtility(getContext());
//        transferRecordMaps = new ArrayList<HashMap<String, Object>>();
        // Use TransferUtility to get all upload transfers.
        observers = transferUtility.getTransfersWithType(TransferType.UPLOAD);
//        TransferListener listener = new UploadListener();


        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.READ_EXTERNAL_STORAGE);
        } else {
//            callMethod();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_general_new_post, container, false);

        edittext = (EmojiconEditTextPlus) rootView.findViewById(R.id.edit);
        EmojIconActions emojIcon = new EmojIconActions(getContext(), rootView, edittext, (ImageView) rootView.findViewById(R.id.emojiconbutton));
        emojIcon.ShowEmojIcon();


        initView();

        return rootView;
    }

    @Override
    protected void initTopbar() {
        ImageButton backOnQuoteFragment = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        ImageButton shop_card = (ImageButton) rootView.findViewById(R.id.shop_card);
        backOnQuoteFragment.setOnClickListener(this);
        shop_card.setOnClickListener(this);
        title_topbar.setText(getString(R.string.post_to_stylist));
        shop_card.setVisibility(View.INVISIBLE);

    }

    private void initView() {
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        screenSize = metrics.widthPixels;
        layout_select_image = (LinearLayout) rootView.findViewById(R.id.layout_select_image);
        layout_select_image.setOnClickListener(this);

        layout_show_image = (RelativeLayout) rootView.findViewById(R.id.layout_show_image);

        ImageView button_close = (ImageView) rootView.findViewById(R.id.button_close);
        button_close.setOnClickListener(this);
        TextViewPlus new_post = (TextViewPlus) rootView.findViewById(R.id.new_post);
        new_post.setOnClickListener(this);

    }

    @Override
    public boolean shouldShowRequestPermissionRationale(String permission) {
        switch (permission) {
            case Manifest.permission.READ_EXTERNAL_STORAGE:
                // No need to explain to user as it is obvious
                return false;

            default:
                return true;
        }
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        List<String> photos = null;
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE) {
            if (data != null) {
                photos = data.getStringArrayListExtra(PhotoPickerActivity.KEY_SELECTED_PHOTOS);
                Log.d(TAG, "path outside if: " + photos.get(0));

                if (photos.size() > 0 && !photos.get(0).equals("")) {
                    file = new File(photos.get(0));
                    Log.d(TAG, "path: " + photos.get(0));

                    if (file.exists()) {


                        layout_show_image.setVisibility(View.VISIBLE);
                        layout_select_image.setVisibility(View.GONE);

                        Uri uri = Uri.fromFile(new File(photos.get(0)));
                        Glide.with(getContext())
                                .load(uri)
                                .into((PhotoView) rootView.findViewById(R.id.image_upload));

                    } else {
                        DialogUtil.toastMessNoIcon(getContext(), "Photo error");
                        layout_show_image.setVisibility(View.GONE);
                        layout_select_image.setVisibility(View.VISIBLE);
                    }
                } else {
                    DialogUtil.toastMessNoIcon(getContext(), "Photo error");
                    layout_show_image.setVisibility(View.GONE);
                    layout_select_image.setVisibility(View.VISIBLE);
                }
            }
            selectedPhotos.clear();

            if (photos != null) {
                selectedPhotos.addAll(photos);
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToLastTab();
                break;
            case R.id.new_post:

                newPost();

                break;
            case R.id.layout_select_image:
                Log.d(TAG, "click to btn select image");
                PhotoPickerIntent intent = new PhotoPickerIntent(getContext());
                intent.setPhotoCount(1);
                intent.setShowCamera(false);
                startActivityForResult(intent, REQUEST_CODE);

                break;

            case R.id.button_close:
                layout_show_image.setVisibility(View.GONE);
                layout_select_image.setVisibility(View.VISIBLE);

                break;

        }
    }

    private Dialog dialog;

    private void newPost() {

        if (edittext.getText().length() != 0 || (file != null && file.exists())) {

            if (edittext.getText().length() > 3000) {
                Toast.makeText(getContext(), getString(R.string.max_length_alert), Toast.LENGTH_SHORT).show();
                return;
            }

            String kinesisDirectory = "sewinAppStream";
            dialog = DialogUtil.showDialog(getContext());
            ContextWrapper cw = new ContextWrapper(getContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir(kinesisDirectory, Context.MODE_PRIVATE);

            recorder = new KinesisRecorder(
                    directory, /* An empty directory KinesisRecorder can use for storing requests */
                    CognitoSyncClientManager.REGION,  /* Region that this Recorder should save and send requests to */
                    CognitoSyncClientManager.getCredentialsProvider()); /* The credentials provider to use when making requests to AWS */

            String id = Utility.generalUUID();
            Log.d(TAG, "NEW Post ID : " + id);
            String userID = Session.getUsers().getUser().getId();
            String timestampe = System.currentTimeMillis() + "";
            String comment = edittext.getText().toString();

            StringBuilder stringPost = new StringBuilder();
            stringPost.append("\"").append(id).append("\",\"").append(userID).append("\",\"").append(timestampe).append("\",\"").append(comment).append("\"");

            byte ptext[] = new byte[0];
            try {
                ptext = stringPost.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "String post in utf8: " + stringPost.toString());
            recorder.saveRecord(ptext, CognitoSyncClientManager.STREAM_NAME);
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... v) {
                    try {
                        recorder.submitAllRecords();
                    } catch (AmazonClientException ace) {
                        // handle error
                    }
                    return "";
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                }
            }.execute();


            if (file != null && file.exists()) {
                Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
                if (bm.getWidth() > 1024 || bm.getHeight() > 1024) {
                    bm = BitmapUtils.resizeImageToMaxSize(bm, 1024);
                    new SaveData(getContext(), id).execute(bm);
                } else
                    beginUpload(file.getAbsolutePath(), id);
            }
        }
    }

    /*
     * Begins to upload the file specified by the file path.
     */
    private void beginUpload(String filePath, String id) {
        if (filePath == null) {
            Toast.makeText(getContext(), "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath);

        ObjectMetadata myObjectMetadata = new ObjectMetadata();
        //create a map to store user metadata
        Map<String, String> userMetadata = new HashMap<String, String>();
        userMetadata.put("userId", Session.getUsers().getUser().getId());
        userMetadata.put("type", "post");
        userMetadata.put("id", id);
        //call setUserMetadata on our ObjectMetadata object, passing it our map
        myObjectMetadata.setUserMetadata(userMetadata);


        TransferObserver observer = transferUtility.upload(CognitoSyncClientManager.BUCKET_NAME, id + ".jpg", file, myObjectMetadata);
        observers.add(observer);
        observer.setTransferListener(new UploadListener());
    }


    /*
    * A TransferListener class that can listen to a upload task and be notified
    * when the status changes.
    */
    private class UploadListener implements TransferListener {

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "Error during upload: " + id, e);
            if (dialog != null)
                dialog.dismiss();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.d(TAG, "onStateChanged: " + id + ", " + newState);
            if (newState == TransferState.COMPLETED) {
                if (dialog != null)
                    dialog.dismiss();
                file = null;
            }
        }
    }

    private class SaveData extends AsyncTask<Bitmap, Void, String> {
        private Context context;
        private String mId;

        public SaveData(Context mContext, String id) {
            context = mContext;
            mId = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Bitmap... bm) {
            return Utility.saveProfileToInternalStorage(context, bm[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("CropImage", " file save to:  " + result);

            beginUpload(result, mId);
        }
    }

}