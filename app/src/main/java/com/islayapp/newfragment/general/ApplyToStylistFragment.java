package com.islayapp.newfragment.general;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.amazonaws.models.googlemap.AddressResponse;
import com.amazonaws.models.googlemap.ResponseGetLocationByAddress;
import com.core.APIService;
import com.google.gson.Gson;
import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.adapter.AutocompletedSearchAdapter;
import com.islayapp.customview.AutoCompleteTextViewPlus;
import com.islayapp.customview.EditTextPlus;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.Salon;
import com.islayapp.models.awsmodels.user.Geo;
import com.islayapp.models.awsmodels.user.User;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.Session;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by maxo on 3/4/16.
 */
public class ApplyToStylistFragment extends BaseFragment {
    // newInstance constructor for creating fragment with arguments
    public static ApplyToStylistFragment newInstance() {
        ApplyToStylistFragment fragmentFirst = new ApplyToStylistFragment();
        Bundle args = new Bundle();
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = ApplyToStylistFragment.class.getName();

    private AutoCompleteTextViewPlus apply_to_stylist_street_address_edt;
    private AutocompletedSearchAdapter adapterAddressResult;
    private ArrayList<String> newArr;
    String state = "", city = "", zip = "";
    AddressResponse body;

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // Inflate the view for the fragment based on layout_try_hair_bottombar XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_general_apply_to_stylist, container, false);

        initLayout();
        return rootView;
    }

    @Override
    protected void initTopbar() {
        ImageButton backOnQuoteFragment = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        ImageButton shop_card = (ImageButton) rootView.findViewById(R.id.shop_card);
        backOnQuoteFragment.setOnClickListener(this);
        title_topbar.setText(getString(R.string.titile_apply_stylist));
        shop_card.setVisibility(View.INVISIBLE);
    }

    private EditTextPlus apply_to_stylist_salon_name_edt,
            apply_to_stylist_suite_edt, apply_to_stylist_phone_edt, apply_to_stylist_email_edt, apply_to_stylist_website_edt,
            apply_to_stylist_facebook_edt, apply_to_stylist_twitter_edt, apply_to_stylist_igname_edt, apply_to_stylist_year_experience_edt,
            apply_to_stylist_description_edt;
    private Button btn_apply_submit;
    private TextViewPlus apply_to_stylist_price_range1, apply_to_stylist_price_range2,
            apply_to_stylist_price_range3, apply_to_stylist_price_range4;

    private void initLayout() {

        apply_to_stylist_salon_name_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_salon_name_edt);
        apply_to_stylist_suite_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_suite_edt);
        apply_to_stylist_phone_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_phone_edt);
        apply_to_stylist_email_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_email_edt);
        apply_to_stylist_website_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_website_edt);
        apply_to_stylist_facebook_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_facebook_edt);
        apply_to_stylist_twitter_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_twitter_edt);
        apply_to_stylist_igname_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_igname_edt);
        apply_to_stylist_year_experience_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_year_experience_edt);
        apply_to_stylist_description_edt = (EditTextPlus) rootView.findViewById(R.id.apply_to_stylist_description_edt);

        apply_to_stylist_price_range1 = (TextViewPlus) rootView.findViewById(R.id.apply_to_stylist_price_range1);
        apply_to_stylist_price_range2 = (TextViewPlus) rootView.findViewById(R.id.apply_to_stylist_price_range2);
        apply_to_stylist_price_range3 = (TextViewPlus) rootView.findViewById(R.id.apply_to_stylist_price_range3);
        apply_to_stylist_price_range4 = (TextViewPlus) rootView.findViewById(R.id.apply_to_stylist_price_range4);

        btn_apply_submit = (Button) rootView.findViewById(R.id.btn_apply_submit);
        btn_apply_submit.setOnClickListener(this);

        apply_to_stylist_price_range1.setOnClickListener(this);
        apply_to_stylist_price_range2.setOnClickListener(this);
        apply_to_stylist_price_range3.setOnClickListener(this);
        apply_to_stylist_price_range4.setOnClickListener(this);


        apply_to_stylist_street_address_edt = (AutoCompleteTextViewPlus) rootView.findViewById(R.id.apply_to_stylist_street_address_edt);
        apply_to_stylist_street_address_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (apply_to_stylist_street_address_edt.isPerformingCompletion()) {
                    // An item has been selected from the list. Ignore.
                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(apply_to_stylist_street_address_edt.getWindowToken(), 0);
                    return;
                }

                if (s.length() >= 3) {
                    fetchAutocompletePlaces(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        newArr = new ArrayList<String>();

        adapterAddressResult = new AutocompletedSearchAdapter(getContext(), R.layout.layout_autocompleted_search_stylist, newArr);
        apply_to_stylist_street_address_edt.setAdapter(adapterAddressResult);
        apply_to_stylist_street_address_edt.showDropDown();


        apply_to_stylist_street_address_edt.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Log.d(TAG, apply_to_stylist_street_address_edt.getText().toString());

                    return true;
                }
                return false;
            }
        });


        apply_to_stylist_street_address_edt.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(arg1.getWindowToken(), 0);
                String item = adapterAddressResult.getItem(arg2);
                Log.d(TAG, item);

                if (body != null && body.getPredictions().get(arg2).getTerms().size() > 0) {

                    Gson gson = new Gson();
                    String json = gson.toJson(body);
                    Log.d(TAG, "content json : " + json);

                    getLocationByAddress(body.getPredictions().get(arg2).getDescription());
                    if (body.getPredictions().get(arg2).getTerms().size() >= 3)
                        city = body.getPredictions().get(arg2).getTerms().get(body.getPredictions().get(arg2).getTerms().size() - 3).getValue();
                    else
                        city = body.getPredictions().get(arg2).getTerms().get(0).getValue();

                    if (body.getPredictions().get(arg2).getTerms().size() >= 2)
                        state = body.getPredictions().get(arg2).getTerms().get(body.getPredictions().get(arg2).getTerms().size() - 2).getValue();
                    else
                        state = body.getPredictions().get(arg2).getTerms().get(body.getPredictions().get(arg2).getTerms().size()).getValue();
                }

                apply_to_stylist_street_address_edt.setText(item);

            }

        });
    }


    private void applyToPriceRanger(View selectView) {
        apply_to_stylist_price_range1.setSelected(false);
        apply_to_stylist_price_range2.setSelected(false);
        apply_to_stylist_price_range3.setSelected(false);
        apply_to_stylist_price_range4.setSelected(false);
        selectView.setSelected(true);
    }


    /**
     * autocompleted
     *
     * @param input
     */
    private void fetchAutocompletePlaces(String input) {
        if (input == null || input.equals(""))
            return;

        Log.d(TAG, "content input : " + input.toString());

        APIService service = NetworkConnectionUtil.createGmapApiService(getContext());

        Call<AddressResponse> response = service.searchAutocompletedAddress(Constants.GOOGLE_MAPS_KEY, input);
        response.enqueue(new Callback<AddressResponse>() {
            @Override
            public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                Log.d(TAG, "content onResponse : " + response.body().toString());
                try {
                    body = response.body();
                    ArrayList<String> newArr = new ArrayList<String>();
                    for (int i = 0; i < body.getPredictions().size(); i++) {
                        newArr.add(body.getPredictions().get(i).getDescription());
                    }

                    Log.d(TAG, "Length: " + body.getPredictions().size());
                    adapterAddressResult = new AutocompletedSearchAdapter(getContext(), R.layout.layout_autocompleted_search_stylist, newArr);
                    apply_to_stylist_street_address_edt.setAdapter(adapterAddressResult);
                    apply_to_stylist_street_address_edt.showDropDown();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddressResponse> call, Throwable t) {
            }
        });
    }


    private void getLocationByAddress(String address) {

        APIService service = NetworkConnectionUtil.createGmapApiService(getContext());

        try {
            String paramemcode = URLEncoder.encode(address, "UTF-8");
            Call<ResponseGetLocationByAddress> response = service.requestGetLocationByAddress(paramemcode);
            response.enqueue(new Callback<ResponseGetLocationByAddress>() {
                @Override
                public void onResponse(Call<ResponseGetLocationByAddress> call, Response<ResponseGetLocationByAddress> response) {
                    Log.d(TAG, "content onResponse : " + response.body().toString());
                    try {
                        ResponseGetLocationByAddress body = response.body();
                        if (body.getStatus().equalsIgnoreCase("OK") && body.getResults().size() > 0) {
                            Geo geo = new Geo();
                            List<Double> geoArr = new ArrayList<Double>();
                            geoArr.add(body.getResults().get(0).getGeometry().getLocation().getLat());
                            geoArr.add(body.getResults().get(0).getGeometry().getLocation().getLng());
                            Session.getUsers().getUser().setGeo(geo);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseGetLocationByAddress> call, Throwable t) {
                }
            });

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToLastTab();
                break;
            case R.id.apply_to_stylist_price_range1:
                applyToPriceRanger(apply_to_stylist_price_range1);
                break;
            case R.id.apply_to_stylist_price_range2:
                applyToPriceRanger(apply_to_stylist_price_range2);
                break;
            case R.id.apply_to_stylist_price_range3:
                applyToPriceRanger(apply_to_stylist_price_range3);
                break;
            case R.id.apply_to_stylist_price_range4:
                applyToPriceRanger(apply_to_stylist_price_range4);
                break;
            case R.id.btn_apply_submit:
                Salon param = getValueSalon();
                saveSalon(param);
                break;
        }
    }

    private Salon getValueSalon() {
        Salon salon = new Salon();

        //first name
        String salonName = apply_to_stylist_salon_name_edt.getText().toString();
        if (salonName.length() == 0) {
            apply_to_stylist_salon_name_edt.setError(getResources().getString(R.string.apply_to_stylist_salon_name_hint) + " is required!");
            return null;
        }
        salon.setName(salonName);

        //last name
        String address = apply_to_stylist_street_address_edt.getText().toString();
        if (address.length() == 0) {
            apply_to_stylist_street_address_edt.setError(getResources().getString(R.string.apply_to_stylist_street_address_hint) + " is required!");
            return null;
        }
        salon.setAddress1(address);

        //last name
        String phone = apply_to_stylist_phone_edt.getText().toString();
        if (phone.length() == 0) {
            apply_to_stylist_phone_edt.setError(getResources().getString(R.string.apply_to_stylist_phone_hint) + " is required!");
            return null;
        }
        salon.setPhone(phone);

        //last name
        String email = apply_to_stylist_email_edt.getText().toString();
        if (email.length() == 0) {
            apply_to_stylist_email_edt.setError(getResources().getString(R.string.apply_to_stylist_email_hint) + " is required!");
            return null;
        }
        salon.setEmail(email);

        //last name
        String website = apply_to_stylist_website_edt.getText().toString();
        if (website.length() == 0) {
            apply_to_stylist_website_edt.setError(getResources().getString(R.string.apply_to_stylist_website_hint) + " is required!");
            return null;
        }
        salon.setWebsite(website);

        //last name
        String facbeook = apply_to_stylist_facebook_edt.getText().toString();
        if (facbeook.length() == 0) {
            apply_to_stylist_facebook_edt.setError(getResources().getString(R.string.apply_to_stylist_facebook_hint) + " is required!");
            return null;
        }
        salon.setFacebook(facbeook);

        //last name
        String twitter = apply_to_stylist_twitter_edt.getText().toString();
        if (twitter.length() == 0) {
            apply_to_stylist_twitter_edt.setError(getResources().getString(R.string.apply_to_stylist_twitter_hint) + " is required!");
            return null;
        }
        salon.setTwitter(twitter);

        //last name
        String igname = apply_to_stylist_igname_edt.getText().toString();
        if (igname.length() == 0) {
            apply_to_stylist_igname_edt.setError(getResources().getString(R.string.apply_to_stylist_igname_hint) + " is required!");
            return null;
        }
        salon.setIgname(igname);

        //last name
        String year_exp = apply_to_stylist_year_experience_edt.getText().toString();
        if (year_exp.length() == 0) {
            apply_to_stylist_year_experience_edt.setError(getResources().getString(R.string.apply_to_stylist_year_experience_hint) + " is required!");
            return null;
        }
        salon.setYearExperience(year_exp);

        //last name
        String description = apply_to_stylist_description_edt.getText().toString();
        if (description.length() == 0) {
            apply_to_stylist_description_edt.setError(getResources().getString(R.string.apply_to_stylist_description_hint) + " is required!");
            return null;
        }
        salon.setDescription(description);

        if (apply_to_stylist_price_range1.isSelected())
            salon.setPricerange("1");
        else if (apply_to_stylist_price_range2.isSelected())
            salon.setPricerange("2");
        else if (apply_to_stylist_price_range3.isSelected())
            salon.setPricerange("3");
        else if (apply_to_stylist_price_range4.isSelected())
            salon.setPricerange("4");

        salon.setCity(city);
        salon.setState(state);

        try {
            if (Session.getUsers() != null && Session.getUsers().getUser() != null && Session.getUsers().getUser().getGeo() != null && Session.getUsers().getUser().getGeo().getSalon() != null) {
                ArrayList<Double> arrayList = new ArrayList<>();
                arrayList.add(0, Session.getUsers().getUser().getGeo().getSalon().get(1));
                arrayList.add(1, Session.getUsers().getUser().getGeo().getSalon().get(0));
                salon.setLoc(arrayList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return salon;
    }


    private void saveSalon(Salon salon) {
        if (salon == null)
            return;
        final Dialog dialog = DialogUtil.showDialog(getContext());
        Log.d(TAG, "content paramBilling : " + salon.toString());
        APIService service = NetworkConnectionUtil.createApiService(getContext());

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Gson gson = new Gson();

        User user = Session.getUsers().getUser();

        if (Session.getUsers() != null && Session.getUsers().getUser() != null && Session.getUsers().getUser().getGeo() != null && Session.getUsers().getUser().getGeo().getSalon() != null) {
            if (Session.getUsers().getUser().getGeo().getSalon().size() > 1) {

                Log.d(TAG, "Lat: " + Session.getUsers().getUser().getGeo().getSalon().get(0));
                Log.d(TAG, "Long: " + Session.getUsers().getUser().getGeo().getSalon().get(1));
            } else {
                ArrayList<Double> arrayList = new ArrayList<>();
                arrayList.add(0, (double) 0);
                arrayList.add(1, (double) 0);
                salon.setLoc(arrayList);

                Geo geo = new Geo();
                geo.setSalon(arrayList);
                user.setGeo(geo);
            }
        } else {
            ArrayList<Double> arrayList = new ArrayList<>();
            arrayList.add(0, (double) 0);
            arrayList.add(1, (double) 0);
            salon.setLoc(arrayList);

            Geo geo = new Geo();
            geo.setSalon(arrayList);
            user.setGeo(geo);
        }
        user.setSalon(salon);

        final String jsonObject = gson.toJson(user);
        Log.d(TAG, "content jsonObject send apply to stylist: " + jsonObject);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject);
        Call<ResponseBody> response = service.requestApplyToStylist(String.format(basic, string), body);

        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (dialog != null)
                    dialog.dismiss();

                if (response.body() != null) {
                    Log.d(TAG, "content onResponse : " + response.body().toString());
                    String value = "";

                    try {
                        value = response.body().string();
                        JSONObject jsonObject1 = new JSONObject(value);
                        if (!jsonObject1.get("applicationid").toString().equals("")) {
                            DialogUtil.toastMess(getContext(), "Apply to stylist success!");
                            ((NewHomeActivity) getActivity()).showStylistMenu();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        DialogUtil.toastMess(getContext(), value);
                    }
                } else {
                    DialogUtil.toastMess(getContext(), "False");
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMess(getContext(), "Save salon error");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }
}
