package com.islayapp.newfragment.general;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import com.amazonaws.models.StoresLocatorResponse;
import com.core.APIService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.Stores;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.SortAlphabe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreLocatorFragment extends BaseFragment implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static StoreLocatorFragment newInstance() {
        StoreLocatorFragment fragmentFirst = new StoreLocatorFragment();
        Bundle args = new Bundle();
//        args.putInt("product_id", productID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = StoreLocatorFragment.class.getName();

    private View rootView;
    private List<Stores> storeLocatorList = new ArrayList<>();
    private StoreLocatorAdapter adapter;
    private TextViewPlus sort_distance, sort_alphabe;

    public double longitude = -1;
    public double latitude = -1;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_general_store_locator, container, false);
        sort_distance = (TextViewPlus) rootView.findViewById(R.id.sort_distance);
        sort_distance.setSelected(false);
        sort_distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (latitude != -1 && longitude != -1) {
//                    Collections.sort(storeLocatorList, new SortPlaces(latitude, longitude));
//                    for (Stores p : storeLocatorList) {
//                        Log.i("Places after sorting", "Place: " + p.getName());
//                    }
//                    adapter.notifyDataSetChanged();
//                } else {
//                    getLocation(getContext());
//                }

                mGoogleApiClient.connect();

            }
        });

        sort_alphabe = (TextViewPlus) rootView.findViewById(R.id.sort_alphabe);
        sort_alphabe.setSelected(true);
        sort_alphabe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sort_distance.setSelected(false);
                sort_alphabe.setSelected(true);
                mGoogleApiClient.disconnect();

                Comparator<Stores> comp = new SortAlphabe();
                Collections.sort(storeLocatorList, comp);
                adapter.notifyDataSetChanged();
            }
        });

        getListStores();

        init();
        return rootView;
    }


//    public void getLocation(Context context) {
//        // when you need location
//        // if inside activity context = this;
//        Log.d("Location", "getLocation calling ");
//
//        SingleShotLocationProvider.requestSingleUpdate(context,
//                new SingleShotLocationProvider.LocationCallback() {
//                    @Override
//                    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
//                        Log.d("Location", "my location is " + location.latitude + "," + location.longitude);
//                        longitude = location.longitude;
//                        latitude = location.latitude;
//                        if (sort_distance.isSelected()) {
//                            Collections.sort(storeLocatorList, new SortPlaces(latitude, longitude));
//                            for (Stores p : storeLocatorList) {
//                                Log.i("Places after sorting", "Place: " + p.getName());
//                            }
//                            adapter.notifyDataSetChanged();
//                        }
//                    }
//                });
//    }


    @Override
    protected void initTopbar() {
        ImageButton backOnQuoteFragment = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        ImageButton shop_card = (ImageButton) rootView.findViewById(R.id.shop_card);
        backOnQuoteFragment.setOnClickListener(this);
        shop_card.setOnClickListener(this);
        title_topbar.setText(getString(R.string.store_locator));
        shop_card.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void init() {
        ListView listView = (ListView) rootView.findViewById(R.id.listViewStoreLocator);
        adapter = new StoreLocatorAdapter(getActivity(), R.layout.fragment_general_store_locator_item, storeLocatorList);
        listView.setAdapter(adapter);

        int permissionFineLocation = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCoarseLocation = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permissionFineLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.ACCESS_FINE_LOCATION);
        }
        if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, Constants.ACCESS_COARSE_LOCATION);

        }
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(StoreLocatorFragment.this)
                    .addOnConnectionFailedListener(StoreLocatorFragment.this)
                    .addApi(LocationServices.API)
                    .build();
        }


    }


    @Override
    public void onStart() {
//        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToLastTab();
                break;
        }
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(String permission) {
        switch (permission) {
            case Manifest.permission.ACCESS_COARSE_LOCATION:
                // No need to explain to user as it is obvious
                return false;
            case Manifest.permission.ACCESS_FINE_LOCATION:
                // No need to explain to user as it is obvious
                return false;

            default:
                return true;
        }
    }

    private void getListStores() {

        final Dialog dialog = DialogUtil.showDialog(getContext());
        APIService service = NetworkConnectionUtil.createApiService(getContext());

        Call<StoresLocatorResponse> response = service.requestStoresLocator();

        response.enqueue(new Callback<StoresLocatorResponse>() {
            @Override
            public void onResponse(Call<StoresLocatorResponse> call, Response<StoresLocatorResponse> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {
                    StoresLocatorResponse storesLocatorResponse = response.body();
                    for (int i = 0; i < storesLocatorResponse.getStores().size(); i++) {
                        storesLocatorResponse.getStores().get(i).getOpenhours().setWeekday1(timeOpenClose(storesLocatorResponse.getStores().get(i).getOpenhours().getWeekday1()));
                        storesLocatorResponse.getStores().get(i).getOpenhours().setWeekday2(timeOpenClose(storesLocatorResponse.getStores().get(i).getOpenhours().getWeekday2()));
                        storesLocatorResponse.getStores().get(i).getOpenhours().setWeekday3(timeOpenClose(storesLocatorResponse.getStores().get(i).getOpenhours().getWeekday3()));
                        storesLocatorResponse.getStores().get(i).getOpenhours().setWeekday4(timeOpenClose(storesLocatorResponse.getStores().get(i).getOpenhours().getWeekday4()));
                        storesLocatorResponse.getStores().get(i).getOpenhours().setWeekday5(timeOpenClose(storesLocatorResponse.getStores().get(i).getOpenhours().getWeekday5()));
                        storesLocatorResponse.getStores().get(i).getOpenhours().setWeekday6(timeOpenClose(storesLocatorResponse.getStores().get(i).getOpenhours().getWeekday6()));
                        storesLocatorResponse.getStores().get(i).getOpenhours().setWeekday7(timeOpenClose(storesLocatorResponse.getStores().get(i).getOpenhours().getWeekday7()));
                    }

                    storeLocatorList.clear();
                    storeLocatorList.addAll(storesLocatorResponse.getStores());

                    if (sort_distance.isSelected()) {
                        for (Stores p : storeLocatorList) {
                            double lat1 = p.getLoc().get(1);
                            double lon1 = p.getLoc().get(0);
                            double distanceToPlace2 = distance(latitude, longitude, lat1, lon1);
                            Log.i("Places after sorting", "Place: " + p.getName() + " distance:  " + distanceToPlace2);
                            p.setDistance(distanceToPlace2);
                        }

                        Collections.sort(storeLocatorList, new Comparator<Stores>() {
                            public int compare(Stores p1, Stores p2) {
                                return Double.compare(p1.getDistance(), p2.getDistance());
                            }
                        });

                        adapter.notifyDataSetChanged();
                    } else {
                        Comparator<Stores> comp = new SortAlphabe();
                        Collections.sort(storeLocatorList, comp);
                        adapter.notifyDataSetChanged();
                    }

//                    mGoogleApiClient.connect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dialog != null)
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<StoresLocatorResponse> call, Throwable t) {
                Log.d(TAG, "content error ");
                DialogUtil.toastMessNoIcon(getContext(), "Error");
                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

    private String timeOpenClose(String timeNormal) {
        String temp = null;
        if (timeNormal != null && !timeNormal.equals("")) {
            try {
                String[] parts = timeNormal.split("-");
                String part1 = parts[0].trim();
                String[] parts1am = part1.split(":");
                if (Integer.parseInt(parts1am[0]) < 12)
                    part1 = part1 + "am";

                String part2 = parts[1].trim();
                String[] parts1pm = part2.split(":");
                if (Integer.parseInt(parts1pm[0]) >= 12) part2 = part2 + "pm";

                Log.d(TAG, "temp: " + part1 + "   " + part2);
                return part1 + "-" + part2;

            } catch (Exception ex) {
                ex.printStackTrace();
                return temp;
            }
        }

        return temp;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
//            mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));
//            mLongitudeText.setText(String.valueOf(mLastLocation.getLongitude()));

            sort_distance.setSelected(true);
            sort_alphabe.setSelected(false);

            Log.d("Location", "my location is " + mLastLocation.getLatitude() + "," + mLastLocation.getLongitude());
            longitude = mLastLocation.getLongitude();
            latitude = mLastLocation.getLatitude();
            if (sort_distance.isSelected()) {
//                Collections.sort(storeLocatorList, new SortPlaces(latitude, longitude));
                for (Stores p : storeLocatorList) {
                    double lat1 = p.getLoc().get(1);
                    double lon1 = p.getLoc().get(0);
                    double distanceToPlace2 = distance(latitude, longitude, lat1, lon1);
                    Log.i("Places after sorting", "Place: " + p.getName() + " distance:  " + distanceToPlace2);
                    p.setDistance(distanceToPlace2);

                }

                Collections.sort(storeLocatorList, new Comparator<Stores>() {
                    public int compare(Stores p1, Stores p2) {
                        return Double.compare(p1.getDistance(), p2.getDistance());
                    }
                });

                adapter.notifyDataSetChanged();
            }
        }
    }

    public double distance(double fromLat, double fromLon, double toLat, double toLon) {
        double radius = 6378137;   // approximate Earth radius, *in meters*
        double deltaLat = toLat - fromLat;
        double deltaLon = toLon - fromLon;
        double angle = 2 * Math.asin(Math.sqrt(
                Math.pow(Math.sin(deltaLat / 2), 2) +
                        Math.cos(fromLat) * Math.cos(toLat) *
                                Math.pow(Math.sin(deltaLon / 2), 2)));
        return radius * angle;
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private class StoreLocatorAdapter extends ArrayAdapter<Stores> {
        private Context mContext;

        private List<Stores> mStoreLocator = new ArrayList<>();

        public StoreLocatorAdapter(Context context, int resourceID, List<Stores> storeLocator) {
            super(context, resourceID, storeLocator);
            mContext = context;
            mStoreLocator = storeLocator;
        }

        @Override
        public int getCount() {
            return mStoreLocator.size();
        }

        @Override
        public Stores getItem(int pos) {
            return mStoreLocator.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return pos;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;

            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.fragment_general_store_locator_item, null);

                ViewHolder viewHolder = new ViewHolder();
                viewHolder.storeLocatorName = (TextViewPlus) view.findViewById(R.id.store_locator_name);
                viewHolder.storeLocatorPhone = (TextViewPlus) view.findViewById(R.id.store_locator_phone);
                viewHolder.storeLocatorAddr1 = (TextViewPlus) view.findViewById(R.id.store_locator_addr1);
                viewHolder.storeLocatorAddr2 = (TextViewPlus) view.findViewById(R.id.store_locator_addr2);
                viewHolder.storeLocatorAddr3 = (TextViewPlus) view.findViewById(R.id.store_locator_addr3);
                viewHolder.storeLocatorDirection = (TextViewPlus) view.findViewById(R.id.store_locator_direction);
                viewHolder.storeLocatorDay1 = (TextViewPlus) view.findViewById(R.id.store_locator_day1);
                viewHolder.storeLocatorDay2 = (TextViewPlus) view.findViewById(R.id.store_locator_day2);
                viewHolder.storeLocatorDay3 = (TextViewPlus) view.findViewById(R.id.store_locator_day3);
                viewHolder.storeLocatorDay4 = (TextViewPlus) view.findViewById(R.id.store_locator_day4);
                viewHolder.storeLocatorDay5 = (TextViewPlus) view.findViewById(R.id.store_locator_day5);
                viewHolder.storeLocatorDay6 = (TextViewPlus) view.findViewById(R.id.store_locator_day6);
                viewHolder.storeLocatorDay7 = (TextViewPlus) view.findViewById(R.id.store_locator_day7);
                view.setTag(viewHolder);
            }
            final Stores item = getItem(position);
            ViewHolder holder = (ViewHolder) view.getTag();
            holder.storeLocatorName.setText(item.getName());
            holder.storeLocatorPhone.setText(item.getPhone());
            holder.storeLocatorPhone.setTag(item.getPhone());
            holder.storeLocatorPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String phoneNumber = "tel:" + (String) v.getTag();
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse(phoneNumber));
                        startActivity(callIntent);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }


            });
            if (item.getOpenhours().getWeekday1() != null && !item.getOpenhours().getWeekday1().equals(""))
                holder.storeLocatorDay1.setText(getString(R.string.sunday, item.getOpenhours().getWeekday1()));
            else
                holder.storeLocatorDay1.setVisibility(View.GONE);

            if (item.getOpenhours().getWeekday2() != null && !item.getOpenhours().getWeekday2().equals(""))
                holder.storeLocatorDay2.setText(getString(R.string.monday, item.getOpenhours().getWeekday2()));
            else
                holder.storeLocatorDay2.setVisibility(View.GONE);

            if (item.getOpenhours().getWeekday3() != null && !item.getOpenhours().getWeekday3().equals(""))
                holder.storeLocatorDay3.setText(getString(R.string.tuesday, item.getOpenhours().getWeekday3()));
            else
                holder.storeLocatorDay3.setVisibility(View.GONE);

            if (item.getOpenhours().getWeekday4() != null && !item.getOpenhours().getWeekday4().equals(""))
                holder.storeLocatorDay4.setText(getString(R.string.wednesday, item.getOpenhours().getWeekday4()));
            else
                holder.storeLocatorDay4.setVisibility(View.GONE);

            if (item.getOpenhours().getWeekday5() != null && !item.getOpenhours().getWeekday5().equals(""))
                holder.storeLocatorDay5.setText(getString(R.string.thursday, item.getOpenhours().getWeekday5()));
            else
                holder.storeLocatorDay5.setVisibility(View.GONE);

            if (item.getOpenhours().getWeekday6() != null && !item.getOpenhours().getWeekday6().equals(""))
                holder.storeLocatorDay6.setText(getString(R.string.firday, item.getOpenhours().getWeekday6()));
            else
                holder.storeLocatorDay6.setVisibility(View.GONE);

            if (item.getOpenhours().getWeekday7() != null && !item.getOpenhours().getWeekday7().equals(""))
                holder.storeLocatorDay7.setText(getString(R.string.satuday, item.getOpenhours().getWeekday7()));
            else
                holder.storeLocatorDay7.setVisibility(View.GONE);

            holder.storeLocatorDirection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String uriBegin = "geo:" + getItem(position).getLoc().get(1) + "," + getItem(position).getLoc().get(0);
                    String query = getItem(position).getLoc().get(1) + "," + getItem(position).getLoc().get(0) + "(" + item.getName() + ")";
                    String encodedQuery = Uri.encode(query);

                    String string = uriBegin + "?q=" + query;
                    if (isGoogleMapsInstalled()) {
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(string));
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivity(mapIntent);
                        }
                    } else {
                        String url = "http://maps.google.com/maps?daddr=" + getItem(position).getLoc().get(1) + "," + getItem(position).getLoc().get(0);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    }
                }
            });

            if (item.getAddress1() != null && !item.getAddress1().equals(""))
                holder.storeLocatorAddr1.setText(item.getAddress1());
            else
                holder.storeLocatorAddr1.setVisibility(View.GONE);

            if (item.getAddress2() != null && !item.getAddress2().equals(""))
                holder.storeLocatorAddr2.setText(item.getAddress2());
            else
                holder.storeLocatorAddr2.setVisibility(View.GONE);


            String address3 = "";
            if (item.getCity() != null && !item.getCity().equals(""))
                address3 = item.getCity();

            if (item.getState() != null && !item.getState().equals(""))
                address3 = address3 + ", " + item.getState();

            if (item.getZipcode() != null && !item.getZipcode().equals(""))
                address3 = address3 + " " + item.getZipcode();//+ " " + item.getCountry();

            if (address3 != null && !address3.equals(""))
                holder.storeLocatorAddr3.setText(address3);
            else
                holder.storeLocatorAddr3.setVisibility(View.GONE);

            return view;
        }

        public boolean isGoogleMapsInstalled() {
            try {
                ApplicationInfo info = getActivity().getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }
        }

        class ViewHolder {
            private TextViewPlus storeLocatorName;
            private TextViewPlus storeLocatorPhone;
            private TextViewPlus storeLocatorAddr1;
            private TextViewPlus storeLocatorAddr2;
            private TextViewPlus storeLocatorAddr3;
            private TextViewPlus storeLocatorDirection;
            private TextViewPlus storeLocatorDay1;
            private TextViewPlus storeLocatorDay2;
            private TextViewPlus storeLocatorDay3;
            private TextViewPlus storeLocatorDay4;
            private TextViewPlus storeLocatorDay5;
            private TextViewPlus storeLocatorDay6;
            private TextViewPlus storeLocatorDay7;
        }
    }
}