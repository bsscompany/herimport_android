package com.islayapp.newfragment.general;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.LambdaInterface;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.Record;
import com.amazonaws.mobileconnectors.cognito.SyncConflict;
import com.amazonaws.mobileconnectors.cognito.exceptions.DataStorageException;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.amazonaws.models.CallPhoneResponse;
import com.amazonaws.models.CheckNicknameResponse;
import com.amazonaws.models.CommonRequest;
import com.amazonaws.models.EmailStatusResponse;
import com.amazonaws.models.LambdaJsonBinder;
import com.amazonaws.models.SendEmailResponse;
import com.amazonaws.models.UpdateNicknameResponse;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.core.APIService;
import com.google.gson.Gson;
import com.islayapp.CropImageActivity;
import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.CircularImageView;
import com.islayapp.customview.EditTextPlus;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.PointsGetResponse;
import com.islayapp.models.awsmodels.SendPhoneActionSmsResponse;
import com.islayapp.models.awsmodels.user.ResponseUsers;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Session;
import com.islayapp.util.Utility;

import net.rimoto.intlphoneinput.IntlPhoneInput;

import org.apache.commons.codec.binary.Base64;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.iwf.photopicker.PhotoPickerActivity;
import me.iwf.photopicker.utils.PhotoPickerIntent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.islayapp.util.Constants.EMAIL;
import static com.islayapp.util.Constants.EMAIL_VERIFIRED;
import static com.islayapp.util.Constants.FIRST_NAME;
import static com.islayapp.util.Constants.LAST_NAME;
import static com.islayapp.util.Constants.NICK_NAME;
import static com.islayapp.util.Constants.PAGE_SIZE;
import static com.islayapp.util.Constants.PHONE;
import static com.islayapp.util.Constants.PHONE_VERIFIRED;
import static com.islayapp.util.Constants.USER_TABLE;

//import io.card.payment.CardIOActivity;

public class MyProfileFragment extends BaseFragment implements View.OnClickListener {
    public static MyProfileFragment newInstance() {
        MyProfileFragment fragmentFirst = new MyProfileFragment();
        return fragmentFirst;
    }

    public final static int REQUEST_CODE_CHOOSE_PHOTO = 10000;
    public final static int REQUEST_CODE_TAKE_PHOTO = 10001;
    public final static int REQUEST_CODE_CROP_IMAGE = 10002;

    private static final String TAG = MyProfileFragment.class.getName();
    private TextViewPlus change, points_earn_number, update_user_info, choose_photo, take_photo, btn_verify_mail, following_number, contact_info_name;
    private TextView input_phone;
    private ImageButton contact_info_dropdown;
    private LinearLayout layout_contact_info_details;
    private CircularImageView circularImageView;
    private int screenSize = 500;
    private EditText first_name, last_name, user_email;
    private EditTextPlus nickname;
    private String lastNickName = "";
    private Dataset datasetUser;

    private String inputPhoneVerifyNumber = "";
    private IntlPhoneInput phoneInputView;
    private ImageView icon_email_verify, icon_phone_verify_status;

    private String codeVerify = "";
    boolean mergeInProgress = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_general_profile, container, false);

        datasetUser = CognitoSyncClientManager.getInstance().openOrCreateDataset(USER_TABLE);

        Log.d(TAG, "datasetUser.getAll():" + datasetUser.getAll().toString());

        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        screenSize = metrics.widthPixels;


        initLayout();

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void initTopbar() {
        ImageButton backOnQuoteFragment = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        ImageButton shop_card = (ImageButton) rootView.findViewById(R.id.shop_card);
        backOnQuoteFragment.setOnClickListener(this);
        shop_card.setOnClickListener(this);
        title_topbar.setText(getString(R.string.my_profile));
        shop_card.setVisibility(View.INVISIBLE);

    }

    private void initLayout() {

        circularImageView = (CircularImageView) rootView.findViewById(R.id.circularImageView);
        circularImageView.setLayoutParams(new LinearLayout.LayoutParams(screenSize / 3, screenSize / 3));
        circularImageView.setBorderWidth(2);
        circularImageView.setBorderColorResource(R.color.label_red);

        change = (TextViewPlus) rootView.findViewById(R.id.tv_change);
        choose_photo = (TextViewPlus) rootView.findViewById(R.id.choose_photo);
        take_photo = (TextViewPlus) rootView.findViewById(R.id.take_photo);
        change.setOnClickListener(this);
        choose_photo.setOnClickListener(this);
        take_photo.setOnClickListener(this);
        contact_info_dropdown = (ImageButton) rootView.findViewById(R.id.contact_info_dropdown);
//        payment_dropdown = (ImageButton) rootView.findViewById(R.id.payment_dropdown);
        contact_info_dropdown.setOnClickListener(this);
//        payment_dropdown.setOnClickListener(this);

        nickname = (EditTextPlus) rootView.findViewById(R.id.name);
        following_number = (TextViewPlus) rootView.findViewById(R.id.following_number);
        update_user_info = (TextViewPlus) rootView.findViewById(R.id.update_user_info);
//        update_payment_info = (TextViewPlus) rootView.findViewById(R.id.update_payment_info);
        update_user_info.setOnClickListener(this);
//        update_payment_info.setOnClickListener(this);

        layout_contact_info_details = (LinearLayout) rootView.findViewById(R.id.layout_contact_info_details);
//        layout_root_payment_method = (LinearLayout) rootView.findViewById(R.id.layout_root_payment_method);

        input_phone = (TextView) rootView.findViewById(R.id.input_phone);
        input_phone.setOnClickListener(this);

//        layout_scan_card = (LinearLayout) rootView.findViewById(R.id.layout_scan_card);
//        layout_scan_card.setOnClickListener(this);

        contact_info_name = (TextViewPlus) rootView.findViewById(R.id.contact_info_name);
        contact_info_name.setText(Session.getUsers().getUser().getFirstname() + " " + Session.getUsers().getUser().getLastname());

        points_earn_number = (TextViewPlus) rootView.findViewById(R.id.points_earn_number);

        first_name = (EditText) rootView.findViewById(R.id.first_name);
        last_name = (EditText) rootView.findViewById(R.id.last_name);
        first_name.setText(Session.getUsers().getUser().getFirstname());
        last_name.setText(Session.getUsers().getUser().getLastname());

        icon_email_verify = (ImageView) rootView.findViewById(R.id.icon_email_verify);
        icon_phone_verify_status = (ImageView) rootView.findViewById(R.id.icon_phone_verify_status);

        btn_verify_mail = (TextViewPlus) rootView.findViewById(R.id.btn_verify_mail);
        btn_verify_mail.setOnClickListener(this);

        user_email = (EditText) rootView.findViewById(R.id.user_email);

        nickname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d(TAG, "forcus change: " + hasFocus);
                if (!hasFocus) {
                    if (nickname.getText().length() > 0) {

                        if (!nickname.getText().toString().equals(lastNickName))
                            requestCheckNickName(nickname.getText().toString());
                    }
                    nickname.clearFocus();
                    nickname.setEnabled(false);

                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        refreshGuiWithData(datasetUser);

        addBaseUserInfo(Session.getUsers());

        if (Session.getUsers().getUser().getMeta().getPhoneverified().equals("Y")) {
            icon_phone_verify_status.setSelected(true);
        } else
            icon_phone_verify_status.setSelected(false);


        if (Session.getUsers().getUser().getMeta().getEmailverified().equals("Y")) {
            TextView verify_mail_alert = (TextView) rootView.findViewById(R.id.verify_mail_alert);
            verify_mail_alert.setVisibility(View.GONE);
            icon_email_verify.setSelected(true);
        } else {
            icon_email_verify.setSelected(false);
        }

        // check status verify email
        String queueID = PreferenceHelpers.getPreference(getContext(), "queueID_email");
        if (queueID != null & !queueID.equals("")) {
            requestVerifyEmailStatus(null, queueID);
        }

        CommonRequest request = new CommonRequest();
        request.setPagesize(PAGE_SIZE);
        request.setPagenumber(1);
        request.setUserid(Session.getUsers().getUser().getId());
        requestUserPoints(request, false);

        // get profile info, billing info
        synchronize();

    }

    private void addBaseUserInfo(final ResponseUsers responseUsers) {
        SimpleTarget target = new SimpleTarget<GlideBitmapDrawable>() {
            @Override
            public void onResourceReady(GlideBitmapDrawable bitmap, GlideAnimation glideAnimation) {
                // do something with the bitmap
                circularImageView.setImageBitmap(bitmap.getBitmap());
            }
        };


        Glide.with(getContext())
                .load(responseUsers.getUser().getFacebook().getPicurl())
                .placeholder(R.drawable.ic_photo_black_48dp)
                .error(R.drawable.ic_broken_image_black_48dp)
                .into(target);

        if (responseUsers.getUser().getCountof().getFollowing() > 1) {
            following_number.setText(getString(R.string.number_stylists, responseUsers.getUser().getCountof().getFollowing()));
        } else
            following_number.setText(getString(R.string.number_stylist, responseUsers.getUser().getCountof().getFollowing()));
        
        points_earn_number.setText(Session.getCurrentPoint() + "");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (countDownTimer != null)
            countDownTimer.cancel();
        if (countDownTimerEmail != null)
            countDownTimerEmail.cancel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null)
            countDownTimer.cancel();
        if (countDownTimerEmail != null)
            countDownTimerEmail.cancel();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (countDownTimer != null)
            countDownTimer.cancel();
        if (countDownTimerEmail != null)
            countDownTimerEmail.cancel();
    }


    private Dialog dialog;

    private void requestUserPoints(CommonRequest request, boolean isShowDialog) {

        Gson gson = new Gson();
        Log.d(TAG, " Request get point:  " + gson.toJson(request).toString());

        if (isShowDialog)
            dialog = DialogUtil.showDialog(getContext());
        Log.d(TAG, "LAMBDA: Sending Data");
        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        final LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());
        final LambdaInterface myInterface = factory.build(LambdaInterface.class, new LambdaJsonBinder());
        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<CommonRequest, Void, PointsGetResponse>() {

            @Override
            protected PointsGetResponse doInBackground(CommonRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.PointsGet(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(PointsGetResponse result) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    return;
                } else {
                    Log.d(TAG, "LAMBDA: Response from request have data");
                    Log.d(TAG, "response point® " + result.getCountof().getAvailablepoints());
                    points_earn_number.setText(result.getCountof().getAvailablepoints() + "");
                    Session.setCurrentPoint(result.getCountof().getAvailablepoints());
                }
            }
        }.execute(request);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backOnQuoteFragment:
                if (countDownTimer != null)
                    countDownTimer.cancel();
                if (countDownTimerEmail != null)
                    countDownTimerEmail.cancel();
                ((NewHomeActivity) getActivity()).backToLastTab();
                break;
            case R.id.circularImageView:
                LinearLayout child_eidt_photo = (LinearLayout) rootView.findViewById(R.id.child_eidt_photo);
                if (circularImageView.isSelected())
                    child_eidt_photo.setVisibility(View.GONE);
                else
                    child_eidt_photo.setVisibility(View.VISIBLE);
                circularImageView.setSelected(!circularImageView.isSelected());
                break;
            case R.id.choose_photo:
                Log.d(TAG, "click to btn select image");
                PhotoPickerIntent intent = new PhotoPickerIntent(getContext());
                intent.setPhotoCount(1);
                intent.setShowCamera(false);
                startActivityForResult(intent, REQUEST_CODE_CHOOSE_PHOTO);
                break;
            case R.id.take_photo:
                Intent intent1 = new Intent("android.media.action.IMAGE_CAPTURE");
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                intent1.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(intent1, REQUEST_CODE_TAKE_PHOTO);
                break;
            case R.id.contact_info_dropdown:
                if (contact_info_dropdown.isSelected())
                    layout_contact_info_details.setVisibility(View.GONE);
                else
                    layout_contact_info_details.setVisibility(View.VISIBLE);
                contact_info_dropdown.setSelected(!contact_info_dropdown.isSelected());
                break;
//            case R.id.payment_dropdown:
//                if (payment_dropdown.isSelected())
//                    layout_root_payment_method.setVisibility(View.GONE);
//                else
//                    layout_root_payment_method.setVisibility(View.VISIBLE);
//                payment_dropdown.setSelected(!payment_dropdown.isSelected());
//                break;
            case R.id.tv_change:
                if (nickname.isEnabled()) {
                    Log.d(TAG, "disable edit nickname");
                    nickname.setEnabled(false);
                    if (nickname.getText().length() > 0) {
                        if (!nickname.getText().toString().equals(lastNickName))
                            requestCheckNickName(nickname.getText().toString());
                    }
                } else {
                    Log.d(TAG, "Enable edit nickname");
                    nickname.setEnabled(true);
                    nickname.setFocusable(true);
                    nickname.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(nickname, InputMethodManager.SHOW_IMPLICIT);
                }

                break;
            case R.id.update_user_info:
                Log.d(TAG, "Click to update_user_info");
                if (first_name.getText().length() != 0) {
                    datasetUser.put(FIRST_NAME, first_name.getText().toString());
                } else {
                    first_name.setError(getString(R.string.field_require));
                    break;
                }

                if (last_name.getText().length() != 0) {
                    datasetUser.put(LAST_NAME, last_name.getText().toString());
                } else {
                    last_name.setError(getString(R.string.field_require));
                    break;
                }

                if (user_email.getText().length() != 0) {
                    datasetUser.put(EMAIL, user_email.getText().toString());
                } else {
                    user_email.setError(getString(R.string.field_require));
                    break;
                }

                if (input_phone.getText().length() != 0) {
                    datasetUser.put(PHONE, input_phone.getText().toString());
                }

                synchronize();
                break;
//            case R.id.update_payment_info:
//                Log.d(TAG, "Click to update_payment_info");
//
//                break;
            case R.id.input_phone:
                Log.d(TAG, "Click to verify phone");
                showDialogVerifyPhone1();
                break;
            case R.id.btn_verify_mail:

                EditText user_email = (EditText) rootView.findViewById(R.id.user_email);
                if (user_email.getText().length() > 0) {
                    requestVerifyEmail(user_email.getText().toString());
                    datasetUser.put(EMAIL, user_email.getText().toString());

                }
                break;
//            case R.id.layout_scan_card:
//                Log.d(TAG, "Click to layout_scan_card");
//                onScanPress();
//                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_CHOOSE_PHOTO) {
                if (data != null) {
                    List<String> photos = data.getStringArrayListExtra(PhotoPickerActivity.KEY_SELECTED_PHOTOS);
                    Log.d(TAG, "path outside if: " + photos.get(0));

                    Intent intent = new Intent(getContext(), CropImageActivity.class);
                    intent.putExtra("path_img", photos.get(0));
                    startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
                }
            }
            if (requestCode == REQUEST_CODE_CROP_IMAGE) {
                String path = data.getStringExtra("path_image");
                if (path != null)
                    circularImageView.setImageBitmap(Utility.loadProfileImageFromStorage(path));
            }

            if (requestCode == REQUEST_CODE_TAKE_PHOTO) {
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                        + File.separator + "image.jpg");
                Intent intent = new Intent(getContext(), CropImageActivity.class);
                intent.putExtra("path_img", file.getPath());
                startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
            }
        }
    }

//    public void onScanPress() {
//        // This method is set up as an onClick handler in the layout_try_hair_bottombar xml
//        // e.g. android:onClick="onScanPress"
//
////        Intent scanIntent = new Intent(getActivity(), CardIOActivity.class);
////
////        // customize these values to suit your needs.
////        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
////        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
////        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
////        scanIntent.putExtra(CardIOActivity.EXTRA_RESTRICT_POSTAL_CODE_TO_NUMERIC_ONLY, false); // default: false
////        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, false); // default: false
////
////        // hides the manual entry button
////        // if set, developers should provide their own manual entry mechanism in the app
////        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false
////
////        // matches the theme of your application
////        scanIntent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, false); // default: false
////
////        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
////        startActivityForResult(scanIntent, NewHomeActivity.MY_SCAN_REQUEST_CODE);
//    }

    /**
     * function show input phone number
     */
    private void showDialogVerifyPhone1() {
        codeVerify = "";
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.fragment_general_profile_content_input_phone);

        TextViewPlus btn_alert_dlg_verify_phone = (TextViewPlus) dialog.findViewById(R.id.btn_alert_dlg_verify_phone);
        btn_alert_dlg_verify_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (phoneInputView.isValid()) {
                    inputPhoneVerifyNumber = phoneInputView.getNumber();
                    Log.d(TAG, "phone number: " + inputPhoneVerifyNumber);
                    requestSendPhoneNumber(inputPhoneVerifyNumber, dialog);
                } else {
                    Toast.makeText(getContext(), "Please input exactly phone number!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();

        phoneInputView = (IntlPhoneInput) dialog.findViewById(R.id.my_phone_input);
        phoneInputView.setDefault();

        if (inputPhoneVerifyNumber != null && !inputPhoneVerifyNumber.equals("")) {
            phoneInputView.setNumber(inputPhoneVerifyNumber);
        }

        ImageView button_close = (ImageView) dialog.findViewById(R.id.button_close);
        button_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void showDialogVerifyPhone2() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.fragment_general_profile_content_input_verify_code);

        TextViewPlus btn_verify_phone_code = (TextViewPlus) dialog.findViewById(R.id.btn_verify_phone_code);
        btn_verify_phone_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edticode = (EditText) dialog.findViewById(R.id.phone_verify_code);
                if (edticode.getText().toString().length() > 0) {
                    if (!codeVerify.equals("") && codeVerify.equals(edticode.getText().toString())) {
                        input_phone.setText(inputPhoneVerifyNumber);
                        dialog.dismiss();

                        datasetUser.put(PHONE, inputPhoneVerifyNumber);
                        datasetUser.put(PHONE_VERIFIRED, "Y");
                        synchronize();

                        requestChangeStatusPhone();
                    }
                } else {
                    Toast.makeText(getContext(), "Please input verification code", Toast.LENGTH_SHORT).show();
                }
            }
        });

        TextViewPlus resend_sms = (TextViewPlus) dialog.findViewById(R.id.resend_sms);
        resend_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogVerifyPhone1();
                dialog.dismiss();
                if (countDownTimer != null)
                    countDownTimer.cancel();
            }
        });

        ImageView button_close = (ImageView) dialog.findViewById(R.id.button_close);
        button_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (countDownTimer != null)
                    countDownTimer.cancel();
            }
        });


        TextViewPlus timer_alert = (TextViewPlus) dialog.findViewById(R.id.timer_alert);
        startCountDown(2, 1, timer_alert);

        dialog.show();
    }

    /**
     * @param minutes
     * @param second
     */
    int maxtime;
    private CountDownTimer countDownTimer;

    public void startCountDown(int minutes, int second, final TextViewPlus textViewPlus) {
        textViewPlus.setEnabled(false);

        maxtime = second * 1000 + minutes * 60 * 1000;
        if (countDownTimer != null)
            countDownTimer.cancel();
        Log.e("countDownTimer", "countDownTimer");
        countDownTimer = null;
        countDownTimer = new CountDownTimer(maxtime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                maxtime -= 1000;
                if (textViewPlus != null) {
                    int min = (int) (maxtime) / 60000;
                    int sec = (int) (maxtime - min * 60000) / 1000;

                    String smin = String.format("%02d", min);
                    String ssec = String.format("%02d", sec);

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(smin).append(":").append(ssec);

                    textViewPlus.setText(getString(R.string.will_call_you_in, stringBuilder));
                }
            }

            @Override
            public void onFinish() {
                if (textViewPlus != null) {
                    textViewPlus.setEnabled(true);
                    textViewPlus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestCallPhoneNumber(inputPhoneVerifyNumber);
                        }
                    });
                    textViewPlus.setText(getString(R.string.call_now));
                }
            }
        };
        countDownTimer.start();
    }

    private void showDialogCodeVerifyEmail(String requestID) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.fragment_general_profile_content_verify_email);

        TextViewPlus timmer_count_email = (TextViewPlus) dialog.findViewById(R.id.timmer_count_email);
        startEmailVeriEmailVerifyCountDown(dialog, 2, 1, timmer_count_email, requestID);

        ImageView button_close = (ImageView) dialog.findViewById(R.id.button_close);
        button_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (countDownTimerEmail != null)
                    countDownTimerEmail.cancel();
            }
        });

        dialog.show();
    }

    /**
     * @param minutes
     * @param second
     */
    int maxtimeEmail;
    int lastMaxtimeEmail;
    private CountDownTimer countDownTimerEmail;

    public void startEmailVeriEmailVerifyCountDown(final Dialog dialog, int minutes, int second, final TextViewPlus textViewPlus, final String requestID) {

        lastMaxtimeEmail = maxtimeEmail = second * 1000 + minutes * 60 * 1000;
        if (countDownTimerEmail != null) {
            countDownTimerEmail.cancel();
        }
        Log.e("countDownTimer", "countDownTimer");
        countDownTimerEmail = null;
        countDownTimerEmail = new CountDownTimer(maxtimeEmail, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                maxtimeEmail -= 1000;

                if (maxtimeEmail != 0 && lastMaxtimeEmail - maxtimeEmail > 4000) {

                    requestVerifyEmailStatus(dialog, requestID);
                    lastMaxtimeEmail = maxtimeEmail;
                }
                if (textViewPlus != null) {
                    int min = (int) (maxtimeEmail) / 60000;
                    int sec = (int) (maxtimeEmail - min * 60000) / 1000;

                    String smin = String.format("%02d", min);
                    String ssec = String.format("%02d", sec);

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(smin).append(":").append(ssec);

                    textViewPlus.setText(getString(R.string.check_your_email, stringBuilder));
                }
            }

            @Override
            public void onFinish() {
                textViewPlus.setText(getString(R.string.check_spam_box));

            }
        };
        countDownTimerEmail.start();
    }

    /**
     * @param nickname
     */

    private void requestCheckNickName(final String nickname) {

        APIService service = NetworkConnectionUtil.createApiService(getContext());

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<CheckNicknameResponse> response = service.requestCheckNickname(String.format(basic, string), nickname);

        response.enqueue(new Callback<CheckNicknameResponse>() {
            @Override
            public void onResponse(Call<CheckNicknameResponse> call, Response<CheckNicknameResponse> response) {

                try {
                    CheckNicknameResponse checkNicknameResponse = response.body();
                    if (checkNicknameResponse.isStatus()) {
//                        MyProfileFragment.this.nickname.setError(getString(R.string.nickname_avaiable), ContextCompat.getDrawable(getContext(), R.drawable.check_success));
                        requestUpdateNickName(nickname);

                    } else {
                        MyProfileFragment.this.nickname.setError(getString(R.string.nickname_not_avaiable));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CheckNicknameResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());

                t.printStackTrace();
            }
        });
    }


    /**
     * @param nick_name
     */

    private void requestUpdateNickName(final String nick_name) {

        final Dialog progress = DialogUtil.showDialog(getContext());

        APIService service = NetworkConnectionUtil.createApiService(getContext());

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<UpdateNicknameResponse> response = service.requestUpdateNickname(String.format(basic, string), nick_name);

        response.enqueue(new Callback<UpdateNicknameResponse>() {
            @Override
            public void onResponse(Call<UpdateNicknameResponse> call, Response<UpdateNicknameResponse> response) {
                if (progress != null)
                    progress.dismiss();
                try {
                    UpdateNicknameResponse updateNicknameResponse = response.body();
                    if (updateNicknameResponse.isStatus()) {
                        Toast.makeText(getContext(), getString(R.string.update_nickname_success), Toast.LENGTH_SHORT).show();
                        Session.getUsers().getUser().setNickname(nick_name);
                        MyProfileFragment.this.nickname.setEnabled(false);
                        datasetUser.put(NICK_NAME, nick_name);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UpdateNicknameResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                if (progress != null)
                    progress.dismiss();
                t.printStackTrace();
            }
        });
    }

    /**
     * @param phone
     */
    private void requestSendPhoneNumber(String phone, final Dialog dialog) {
        final Dialog progress = DialogUtil.showDialog(getContext());

        APIService service = NetworkConnectionUtil.createApiService(getContext());

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<SendPhoneActionSmsResponse> response = service.requestSendPhoneVerify(String.format(basic, string), phone);

        response.enqueue(new Callback<SendPhoneActionSmsResponse>() {
            @Override
            public void onResponse(Call<SendPhoneActionSmsResponse> call, Response<SendPhoneActionSmsResponse> response) {
                if (progress != null)
                    progress.dismiss();

                try {
                    SendPhoneActionSmsResponse sendPhoneActionSmsResponse = response.body();
                    if (sendPhoneActionSmsResponse.getSuccess()) {

                        codeVerify = sendPhoneActionSmsResponse.getCode();

                        if (dialog != null)
                            dialog.dismiss();
                        Log.d(TAG, "Send phone success");
                        showDialogVerifyPhone2();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SendPhoneActionSmsResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                if (progress != null)
                    progress.dismiss();

                t.printStackTrace();
            }
        });
    }

    /**
     * request call to number and get code verify
     *
     * @param phoneCal
     */
    private void requestCallPhoneNumber(String phoneCal) {

        final Dialog progress = DialogUtil.showDialog(getContext());

        APIService service = NetworkConnectionUtil.createApiService(getContext());

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<CallPhoneResponse> response = service.requestCallToPhone(String.format(basic, string), phoneCal);

        response.enqueue(new Callback<CallPhoneResponse>() {
            @Override
            public void onResponse(Call<CallPhoneResponse> call, Response<CallPhoneResponse> response) {
                if (progress != null)
                    progress.dismiss();

                try {
                    CallPhoneResponse sendPhoneActionSmsResponse = response.body();
                    if (sendPhoneActionSmsResponse.getSuccess()) {
                        codeVerify = sendPhoneActionSmsResponse.getCode();

                        Log.d(TAG, "Call phone success");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CallPhoneResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                if (progress != null)
                    progress.dismiss();

                t.printStackTrace();
            }
        });
    }


    private void requestChangeStatusPhone() {

        APIService service = NetworkConnectionUtil.createApiService(getContext());

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<SendPhoneActionSmsResponse> response = service.requestSendhoneVerified(String.format(basic, string));

        response.enqueue(new Callback<SendPhoneActionSmsResponse>() {
            @Override
            public void onResponse(Call<SendPhoneActionSmsResponse> call, Response<SendPhoneActionSmsResponse> response) {

                try {
                    SendPhoneActionSmsResponse sendPhoneActionSmsResponse = response.body();
                    if (sendPhoneActionSmsResponse.getStatus()) {
                        icon_phone_verify_status.setSelected(true);
//                        Session.getUsers().getUser().getMeta().setPhoneverified("Y");
                        Session.getUsers().getUser().setPhone(inputPhoneVerifyNumber);
//
//                        datasetUser.put(PHONE, inputPhoneVerifyNumber);
//                        synchronize();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SendPhoneActionSmsResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                t.printStackTrace();
            }
        });
    }


    /**
     * @param email
     */
    private void requestVerifyEmail(String email) {

        final Dialog progress = DialogUtil.showDialog(getContext());
        APIService service = NetworkConnectionUtil.createApiService(getContext());

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<SendEmailResponse> response = service.requestSendEmail(String.format(basic, string), email);

        response.enqueue(new Callback<SendEmailResponse>() {
            @Override
            public void onResponse(Call<SendEmailResponse> call, Response<SendEmailResponse> response) {
                if (progress != null)
                    progress.dismiss();
                try {
                    SendEmailResponse sendEmailResponse = response.body();
                    if (sendEmailResponse.getSuccess()) {
                        Session.getUsers().getUser().setEmail(user_email.getText().toString());
                        datasetUser.put(EMAIL, user_email.getText().toString());
                        datasetUser.put(EMAIL_VERIFIRED, "N");
                        synchronize();

                        PreferenceHelpers.setPreference(getContext(), "queueID_email", sendEmailResponse.getRequestid());

                        showDialogCodeVerifyEmail(sendEmailResponse.getRequestid());
                        Log.d(TAG, "Send email success");
                    } else {
                        Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SendEmailResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                if (progress != null)
                    progress.dismiss();
                t.printStackTrace();
            }
        });
    }


    /**
     * @param requesID
     */
    private void requestVerifyEmailStatus(final Dialog dialog, String requesID) {

        APIService service = NetworkConnectionUtil.createApiService(getContext());

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<EmailStatusResponse> response = service.requestVerifyEmailStatus(String.format(basic, string), requesID);

        response.enqueue(new Callback<EmailStatusResponse>() {
            @Override
            public void onResponse(Call<EmailStatusResponse> call, Response<EmailStatusResponse> response) {
                try {
                    EmailStatusResponse emailStatusResponse = response.body();
                    if (emailStatusResponse.getStatus().equals("VERIFIED")) {
                        PreferenceHelpers.removePreference(getContext(), "queueID_email");
                        Log.d(TAG, "VERIFIED");
                        datasetUser.put(EMAIL, user_email.getText().toString());
                        datasetUser.put(EMAIL_VERIFIRED, "Y");
                        synchronize();

                        icon_email_verify.setSelected(true);
//                        btn_verify_mail.setEnabled(false);
                        Session.getUsers().getUser().getMeta().setEmailverified("Y");
                        TextView verify_mail_alert = (TextView) rootView.findViewById(R.id.verify_mail_alert);
                        verify_mail_alert.setVisibility(View.GONE);

                        if (countDownTimerEmail != null)
                            countDownTimerEmail.cancel();
                        if (dialog != null)
                            dialog.dismiss();

                    } else if (emailStatusResponse.getStatus().equals("PENDING")) {
                        TextView verify_mail_alert = (TextView) rootView.findViewById(R.id.verify_mail_alert);
                        verify_mail_alert.setVisibility(View.VISIBLE);
                        Log.d(TAG, "PENDING");
                        user_email.setEnabled(true);
                        icon_email_verify.setSelected(false);
//                        btn_verify_mail.setEnabled(true);
                        Session.getUsers().getUser().getMeta().setEmailverified("N");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<EmailStatusResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                t.printStackTrace();
            }
        });
    }


    private void synchronize() {
        datasetUser.synchronize(new Dataset.SyncCallback() {
            @Override
            public void onSuccess(Dataset dataset, final List<Record> newRecords) {
                Log.i("Sync", "success" + dataset.toString());
                if (mergeInProgress) return;
                refreshGuiWithData(dataset);
            }

            @Override
            public void onFailure(final DataStorageException dse) {
                Log.i("Sync", "failure: ", dse);
            }

            @Override
            public boolean onConflict(final Dataset dataset,
                                      final List<SyncConflict> conflicts) {
                Log.i("Sync", "conflict: " + conflicts);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, String.format("%s records in conflict",
                                conflicts.size()));
                        List<Record> resolvedRecords = new ArrayList<Record>();
                        for (SyncConflict conflict : conflicts) {
                            Log.i(TAG, String.format("remote: %s; local: %s",
                                    conflict.getRemoteRecord(),
                                    conflict.getLocalRecord()));
                            /* resolve by taking remote records */
                            resolvedRecords.add(conflict
                                    .resolveWithRemoteRecord());

                            /* resolve by taking local records */
                            // resolvedRecords.add(conflict.resolveWithLocalRecord());

                            /*
                             * resolve with customized logic, e.g. concatenate
                             * strings
                             */
                            // String newValue =
                            // conflict.getRemoteRecord().getValue()
                            // + conflict.getLocalRecord().getValue();
                            // resolvedRecords.add(conflict.resolveWithValue(newValue));
                        }
                        dataset.resolve(resolvedRecords);
                        refreshGuiWithData(dataset);

                    }
                });
                return true;
            }

            @Override
            public boolean onDatasetDeleted(Dataset dataset, String datasetName) {
                Log.i("Sync", "delete: " + datasetName);
                return true;
            }

            @Override
            public boolean onDatasetsMerged(Dataset dataset, List<String> mergedDatasetNames) {

                mergeInProgress = true;
                Log.i("Sync", "merge: " + dataset.getDatasetMetadata().getDatasetName());

                CognitoSyncManager client = CognitoSyncClientManager.getInstance();
                for (final String name : mergedDatasetNames) {
                    Log.i("Merge", "syncing merged: " + name);
                    final Dataset d = client.openOrCreateDataset(name);
                    d.synchronize(new Dataset.SyncCallback() {
                        @Override
                        public void onSuccess(Dataset dataset, List<Record> records) {

                            //This is the actual merge code, in this sample we will just join fields in both datasets into a single one
                            Log.i("Merge", "joining records");
                            MyProfileFragment.this.datasetUser.putAll(dataset.getAll());

                            //To finish and resolve the merge, we have to delete the merged dataset
                            Log.e("Merge", "deleting merged: " + name);
                            dataset.delete();
                            dataset.synchronize(new Dataset.SyncCallback() {
                                @Override
                                public void onSuccess(Dataset dataset, List<Record> records) {
                                    Log.i("Merge", "merged dataset deleted");

                                    //And finally we should sync back the new merged dataset
                                    Log.i("Merge", "now syncing the resulting new dataset");
                                    MyProfileFragment.this.datasetUser.synchronize(new Dataset.SyncCallback() {
                                        @Override
                                        public void onSuccess(Dataset dataset, List<Record> newRecords) {
                                            Log.i("Merge", "merge completed");
                                            mergeInProgress = false;

                                            refreshGuiWithData(dataset);

                                        }

                                        @Override
                                        public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                            Log.e("Merge", "Unhandled onConflict");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetDeleted(Dataset dataset, String s) {
                                            Log.e("Merge", "Unhandled onDatasetDeleted");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                            Log.e("Merge", "Unhandled onDatasetMerged");
                                            return false;
                                        }

                                        @Override
                                        public void onFailure(DataStorageException e) {
                                            e.printStackTrace();
                                            Log.e("Merge", "Exception");
                                        }
                                    });
                                }

                                @Override
                                public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                    Log.e("Merge", "Unhandled onConflict");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetDeleted(Dataset dataset, String s) {
                                    Log.e("Merge", "Unhandled onDatasetDeleted");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                    Log.e("Merge", "Unhandled onDatasetMerged");
                                    return false;
                                }

                                @Override
                                public void onFailure(DataStorageException e) {
                                    e.printStackTrace();
                                    Log.e("Merge", "Exception");
                                }
                            });
                        }

                        @Override
                        public boolean onDatasetDeleted(Dataset dataset, String s) {

                            //This will trigger in the scenario were we had a local dataset that was not present on the identity we are merging

                            Log.i("Merge", "onDatasetDeleted");
                            final Dataset previous = dataset;

                            //Sync the local dataset
                            MyProfileFragment.this.datasetUser.synchronize(new Dataset.SyncCallback() {
                                @Override
                                public void onSuccess(Dataset dataset, final List<Record> newRecords) {

                                    // Delete the local dataset from the old identity, now it's merged into the new one
                                    Log.i("Merge", "local dataset synced to the new identity");
                                    previous.delete();
                                    previous.synchronize(new Dataset.SyncCallback() {
                                        @Override
                                        public void onSuccess(Dataset dataset, List<Record> deletedRecords) {
                                            mergeInProgress = false;

                                            refreshGuiWithData(dataset);

                                        }

                                        @Override
                                        public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                            Log.e("Merge", "Unhandled onConflict");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetDeleted(Dataset dataset, String s) {
                                            Log.e("Merge", "Unhandled onDatasetDeleted");
                                            return false;
                                        }

                                        @Override
                                        public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                            Log.e("Merge", "Unhandled onDatasetMerged");
                                            return false;
                                        }

                                        @Override
                                        public void onFailure(DataStorageException e) {
                                            e.printStackTrace();
                                            Log.e("Merge", "Exception");
                                        }
                                    });
                                }

                                @Override
                                public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                                    Log.e("Merge", "Unhandled onConflict");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetDeleted(Dataset dataset, String s) {
                                    Log.e("Merge", "Unhandled onDatasetDeleted");
                                    return false;
                                }

                                @Override
                                public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                                    Log.e("Merge", "Unhandled onDatasetMerged");
                                    return false;
                                }

                                @Override
                                public void onFailure(DataStorageException e) {
                                    e.printStackTrace();
                                    Log.e("Merge", "Exception");
                                }
                            });
                            return false;
                        }

                        @Override
                        public boolean onConflict(Dataset dataset, List<SyncConflict> syncConflicts) {
                            Log.e("Merge", "Unhandled onConflict");
                            return false;
                        }

                        @Override
                        public boolean onDatasetsMerged(Dataset dataset, List<String> strings) {
                            Log.e("Merge", "Unhandled onDatasetMerged");
                            return false;
                        }

                        @Override
                        public void onFailure(DataStorageException e) {
                            e.printStackTrace();
                            Log.e("Merge", "Exception");
                        }
                    });
                }
                return true;
            }
        });
    }


    private void refreshGuiWithData(final Dataset dataset) {
        MyProfileFragment.this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Map<String, String> userInfo = dataset.getAll();
                        Log.d(TAG, "data result: " + userInfo.toString());
                        showContactInfo(userInfo);
                    }
                });
            }
        });
    }

    private void showContactInfo(final Map<String, String> userInfo) {
        getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            if (userInfo.get(NICK_NAME) == null || userInfo.get(NICK_NAME).equals(""))
                                                nickname.setText(Session.getUsers().getUser().getNickname());
                                            else
                                                nickname.setText(userInfo.get(NICK_NAME));

                                            lastNickName = userInfo.get(NICK_NAME);

                                            String firstName = "";

                                            if (userInfo.get(FIRST_NAME) == null || userInfo.get(FIRST_NAME).equals("")) {
                                                firstName = Session.getUsers().getUser().getFirstname();
                                            } else
                                                firstName = userInfo.get(FIRST_NAME);

                                            String lastName = "";

                                            if (userInfo.get(LAST_NAME) == null || userInfo.get(LAST_NAME).equals("")) {
                                                lastName = Session.getUsers().getUser().getLastname();
                                            } else
                                                lastName = userInfo.get(LAST_NAME);

                                            contact_info_name.setText(firstName + " " + lastName);
                                            first_name.setText(firstName);
                                            last_name.setText(lastName);


                                            if (userInfo.get(PHONE_VERIFIRED) != null && userInfo.get(PHONE_VERIFIRED).equals("Y"))
                                                icon_phone_verify_status.setSelected(true);
                                            else
                                                icon_phone_verify_status.setSelected(false);

                                            if (userInfo.get(EMAIL_VERIFIRED) != null && userInfo.get(EMAIL_VERIFIRED).equals("Y"))
                                                icon_email_verify.setSelected(true);
                                            else
                                                icon_email_verify.setSelected(false);

                                            input_phone.setText(userInfo.get(PHONE));
                                            user_email.setText(userInfo.get(EMAIL));
                                            inputPhoneVerifyNumber = userInfo.get(PHONE);
                                        }
                                    }

        );
    }


}