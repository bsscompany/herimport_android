package com.islayapp.newfragment.whathot;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.newfragment.getPayD.Views.onChangeFragment;

/**
 * Created by DINO on 07/04/2016.
 */
public class WebviewFragment extends BaseFragment implements onChangeFragment{
    public static WebviewFragment newInstance(String url) {
        WebviewFragment fragmentFirst = new WebviewFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private String url = "";
    private WebView webView;
    private ImageButton imgbtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString("url");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.what_hot_webview, container, false);
        webView = (WebView) rootView.findViewById(R.id.paypal_wv);
        startWebView(url);

        return rootView;
    }


    @Override
    protected void initTopbar() {
        ImageButton imgMenu = (ImageButton) rootView.findViewById(R.id.imgMenu);
        TextViewPlus title_topbar = (TextViewPlus) rootView.findViewById(R.id.title_topbar);
        imgbtn = (ImageButton) rootView.findViewById(R.id.shop_card);
        imgbtn.setVisibility(View.INVISIBLE);
        imgMenu.setOnClickListener(this);
        title_topbar.setText(getString(R.string.try_hair));

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.imgMenu:
                ((NewHomeActivity) getActivity()).openNavMenu();
                break;
        }
    }

    boolean isRun = false;

    public void startWebView(String url) {
        Log.d("url", "first url: " + url);
        isRun = false;

        webView.setWebViewClient(new WebViewClient() {

            private int running = 0; // Could be public if you want a timer to check.
//            private Dialog dialog;

            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String urlNewString) {
//                if (dialog == null) {
//                    dialog = DialogUtil.showDialog(getContext());
//                    dialog.setCancelable(true);
//                }
                running++;
                webView.loadUrl(urlNewString);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                running = Math.max(running, 1); // First request move it to 1.
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (--running == 0) { // just "running--;" if you add a timer.
                    // TODO: finished... if you want to fire a method.
//                    if (dialog != null)
//                        dialog.dismiss();
                }
            }
        });


        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setSupportZoom(false);

        //Load url in webview
        webView.loadUrl(url);

    }

    @Override
    public void onHideMediaController() {

    }
}
