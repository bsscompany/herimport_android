package com.islayapp.newfragment.getlayd;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amazonaws.models.ItemFeed;
import com.amazonaws.models.Post;
import com.amazonaws.models.PostComment;
import com.amazonaws.models.Stylist;
import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.TryHairNewActivity;
import com.islayapp.newfragment.getPayD.IDefaultFragment;
import com.islayapp.newfragment.getPayD.Views.onChangeFragment;

import java.util.ArrayList;
import java.util.List;


public class GetlaydDefaultFragment extends Fragment implements View.OnClickListener, IDefaultFragment, onChangeFragment {
    public static GetlaydDefaultFragment newInstance() {
        GetlaydDefaultFragment fragmentFirst = new GetlaydDefaultFragment();
        return fragmentFirst;
    }

    public List<ItemFeed> itemFeedList = new ArrayList<>();
    public List<Stylist> mStylists = new ArrayList<>();

    /**
     * value for post details
     */

    public Stylist stylist = new Stylist();
    public Post post = new Post();

    @Override
    public void onHideMediaController() {


    }

    public enum GetLayDEnum {
        Profile, StyListDirectory, StylistFeed, Commnets, Image;
    }

    private static final String TAG = GetlaydDefaultFragment.class.getName();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_get_layd_get_default, container, false);
        init(rootView);
        return rootView;
    }


    private void init(View rootView) {
        rootView.findViewById(R.id.imgMenu).setOnClickListener(this);
        rootView.findViewById(R.id.rlt_stylist_directory).setOnClickListener(this);
        rootView.findViewById(R.id.rlt_stylist_feed).setOnClickListener(this);
        rootView.findViewById(R.id.frm_virtual_hair).setOnClickListener(this);
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText(getResources().getString(R.string.get_layd));
    }

    private void StyListDirectory() {
        StyListDirectoryFragment fragment = StyListDirectoryFragment.newInstance();
        fragment.setDefaultFragment(this);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.parent, fragment);
        fragmentTransaction.addToBackStack(String.valueOf(GetLayDEnum.StyListDirectory));
        fragmentTransaction.commit();

    }


    private void StylistFeedFragment() {

        StylistFeedFragment fragment = StylistFeedFragment.newInstance();
        fragment.setDefaultFragment(this);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.parent, fragment);
        fragmentTransaction.addToBackStack(String.valueOf(GetLayDEnum.StylistFeed));
        fragmentTransaction.commit();
    }

    public void PostDetailsFragment(Fragment child, Stylist stylist, Post value, boolean isFollow) {

        this.stylist = new Stylist();
        this.post = new Post();

        PostDetailsFragment fragment = PostDetailsFragment.newInstance(stylist, value, isFollow);
        fragment.setDefaultFragment(this);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.parent, fragment);
        fragmentTransaction.hide(child);
        fragmentTransaction.addToBackStack(String.valueOf(GetLayDEnum.Image));

        fragmentTransaction.commit();
    }

    public void PostDetailsFragment(Fragment child, ItemFeed itemFeed) {

        this.stylist = new Stylist();
        this.post = new Post();

        PostDetailsFragment fragment = PostDetailsFragment.newInstance(itemFeed);
        fragment.setDefaultFragment(this);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.parent, fragment);
        fragmentTransaction.hide(child);
        fragmentTransaction.addToBackStack(String.valueOf(GetLayDEnum.Image));

        fragmentTransaction.commit();
    }

    public void stylistDetailsFragment(Object value) {

        ProfileFragment fragment = ProfileFragment.newInstance((Stylist) value);
        fragment.setDefaultFragment(this);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.parent, fragment);
        fragmentTransaction.addToBackStack(String.valueOf(GetLayDEnum.Profile));
        fragmentTransaction.commit();
    }

    public void CommentsFragment(Fragment child, List<PostComment> postComments, String value) {

        CommentsFragment fragment = CommentsFragment.newInstance(postComments, value);
        fragment.setDefaultFragment(this);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.parent, fragment);
        fragmentTransaction.hide(child);
        fragmentTransaction.addToBackStack(String.valueOf(GetLayDEnum.Commnets));
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgMenu:
                ((NewHomeActivity) getActivity()).openNavMenu();
                break;
            case R.id.rlt_stylist_directory:
                StyListDirectory();
                break;
            case R.id.rlt_stylist_feed:
                StylistFeedFragment();
                break;
            case R.id.frm_virtual_hair:

                Intent intent = new Intent(getContext(), TryHairNewActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                break;
        }
    }

    @Override
    public void popback(@Nullable String keycode) {

    }

    @Override
    public void popback() {
        getChildFragmentManager().popBackStack();
    }

    @Override
    public void addFragment(Object type, Object value) {
        if (type instanceof GetLayDEnum) {
            switch ((GetLayDEnum) type) {
                case Profile:
                    stylistDetailsFragment(value);
                    break;
                case Image:
                    break;
//                case Commnets:
//                    CommentsFragment((List<PostComment> )type, (String) value);
//                    break;
            }
        }
    }


    public void popbackAll() {
        for (int i = 0; i < this.getChildFragmentManager().getBackStackEntryCount(); ++i) {
            getChildFragmentManager().popBackStack();
        }
    }

}