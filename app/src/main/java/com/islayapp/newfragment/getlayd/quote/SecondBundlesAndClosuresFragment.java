package com.islayapp.newfragment.getlayd.quote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;

public class SecondBundlesAndClosuresFragment extends BaseFragment implements  View.OnClickListener {
    public static SecondBundlesAndClosuresFragment newInstance() {
        SecondBundlesAndClosuresFragment fragmentFirst = new SecondBundlesAndClosuresFragment();
        Bundle args = new Bundle();
//        args.putInt("product_id", productID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = SecondBundlesAndClosuresFragment.class.getName();
    private TextViewPlus yes_install_closures,no_install_closures;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_bundles_and_closures, container, false);
        init();
        initBottombarCircleSelect(2);
        return rootView;
    }

    private void init() {
        ImageButton back = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        back.setOnClickListener(this);

        ImageButton nextOnChoosePhoto = (ImageButton) rootView.findViewById(R.id.nextOnChoosePhoto);
        nextOnChoosePhoto.setOnClickListener(this);

        yes_install_closures = (TextViewPlus) rootView.findViewById(R.id.yes_install_closures);
        yes_install_closures.setOnClickListener(this);


        no_install_closures = (TextViewPlus) rootView.findViewById(R.id.no_install_closures);
        no_install_closures.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToScreenGetlaydDefault();
                break;

            case R.id.nextOnChoosePhoto:
//                ((NewHomeActivity) getActivity()).updateFragmentGetlayd(Constants.FRAGMENT_GETLAYD_ADDITIONAL_DETAIL, 0);
                break;
            case R.id.yes_install_closures:
                yes_install_closures.setSelected(true);
                no_install_closures.setSelected(false);

                break;
            case R.id.no_install_closures:
                yes_install_closures.setSelected(false);
                no_install_closures.setSelected(true);

                break;
        }
    }
}