package com.islayapp.newfragment.getlayd.quote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.CircleColorView;

public class SevenVerifyAccountFragment extends BaseFragment implements View.OnClickListener {
    public static SevenVerifyAccountFragment newInstance() {
        SevenVerifyAccountFragment fragmentFirst = new SevenVerifyAccountFragment();
        Bundle args = new Bundle();
//        args.putInt("product_id", productID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = SevenVerifyAccountFragment.class.getName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_verify_account, container, false);
        init();
        return rootView;
    }

    private void init() {
        ImageButton back = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        back.setOnClickListener(this);
        Button btn_check_out = (Button) rootView.findViewById(R.id.btn_check_out);
        btn_check_out.setOnClickListener(this);

        CircleColorView dot_first = (CircleColorView) rootView.findViewById(R.id.dot_first);
        dot_first.setColor("#C12055");
        CircleColorView dot_second = (CircleColorView) rootView.findViewById(R.id.dot_second);
        dot_second.setColor("#C12055");
        CircleColorView dot_third = (CircleColorView) rootView.findViewById(R.id.dot_third);
        dot_third.setColor("#C12055");

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToScreenGetlaydDefault();
                break;
            case R.id.btn_check_out:
//                ((NewHomeActivity) getActivity()).updateFragmentGetlayd(Constants.FRAGMENT_GETLAYD_PAYMENT, 0);
                break;
        }
    }
}