package com.islayapp.newfragment.getlayd;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.models.ItemFeed;
import com.amazonaws.models.NewFeedRequest;
import com.amazonaws.models.NewFeedResponse;
import com.amazonaws.models.Stylist;
import com.core.APIService;
import com.google.gson.Gson;
import com.islayapp.R;
import com.islayapp.adapter.NewFeedAdapter;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.Session;

import org.apache.commons.codec.binary.Base64;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DINO on 07/04/2016.
 */
public class StylistFeedFragment extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    public static StylistFeedFragment newInstance() {
        StylistFeedFragment fragmentFirst = new StylistFeedFragment();
        return fragmentFirst;
    }

    private static final String TAG = StylistFeedFragment.class.getName();
    private ListView listViewFeed;
    private NewFeedAdapter feedAdapter;
    private boolean isLoading = false;
    private boolean isMoreAvaiable = false;
    private NewFeedRequest request;
    private View footerListview;
    private FrameLayout frame_preload;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_stylist_feed, container, false);

        frame_preload = (FrameLayout) rootView.findViewById(R.id.frame_preload);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        listViewFeed = (ListView) rootView.findViewById(R.id.listView3);
        footerListview = LayoutInflater.from(getContext()).inflate(R.layout.bottom_listview, null);
        footerListview.setVisibility(View.GONE);
        listViewFeed.addFooterView(footerListview);
        // listener event
        listViewFeed.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                //what is the bottom iten that is visible
                int lastInScreen = firstVisibleItem + visibleItemCount;
                //is the bottom item visible & not loading more already ? Load more !
                if ((lastInScreen == totalItemCount) && isMoreAvaiable && !isLoading) {
                    isLoading = true;
                    footerListview.setVisibility(View.VISIBLE);
                    request.setPagenumber(request.getPagenumber() + 1);
                    requestNewFeed(request, false, false);
                }
            }
        });

        feedAdapter = new NewFeedAdapter(StylistFeedFragment.this, getContext(), defaultFragment, R.layout.item_get_layd_stylist_feed, defaultFragment.itemFeedList);
        listViewFeed.setAdapter(feedAdapter);
        return rootView;
    }

    @Override
    protected void initTopbar() {
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText(getResources().getString(R.string.stylist_feed));
        rootView.findViewById(R.id.backOnQuoteFragment).setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();

        request = new NewFeedRequest();
        request.setPagenumber(1);
        request.setPagesize(Constants.PAGE_SIZE);
        if (defaultFragment.itemFeedList.size() > 0) {
            requestNewFeed(request, false, true);
        } else
            requestNewFeed(request, true, false);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        feedAdapter.notifyDataSetChanged();
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {

// showing refresh animation before making http call
        swipeRefreshLayout.setRefreshing(true);
        request = new NewFeedRequest();
        request.setPagenumber(1);
        request.setPagesize(Constants.PAGE_SIZE);

        requestNewFeed(request, false, true);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                defaultFragment.popback();
                break;
        }
    }

    private void requestNewFeed(NewFeedRequest request, boolean isShowLoading, final boolean isUpdate) {

        if (isShowLoading) {
            frame_preload.setVisibility(View.VISIBLE);
            frame_preload.findViewById(R.id.progressBarCircularIndetermininate).setVisibility(View.VISIBLE);
            footerListview.setVisibility(View.GONE);
            frame_preload.findViewById(R.id.tv_no_result).setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.GONE);

        } else
            frame_preload.setVisibility(View.GONE);

        APIService service = NetworkConnectionUtil.createApiService(getContext());

        Gson gson = new Gson();
        String json = gson.toJson(request);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Log.d(TAG, "request: " + json);

        String basic = "Basic %s";
        String authToken = Session.getUsers().getUser().getId() + ":" + Session.getUsers().getApitoken();

        // encrypt data on your side using BASE64
        byte[] bytesEncoded = Base64.encodeBase64(authToken.getBytes());
        String string = new String(bytesEncoded);

        Call<NewFeedResponse> response = service.requestNewFeed(String.format(basic, string), body);

        response.enqueue(new Callback<NewFeedResponse>() {
            @Override
            public void onResponse(Call<NewFeedResponse> call, Response<NewFeedResponse> response) {
                footerListview.setVisibility(View.GONE);
                isLoading = false;
                swipeRefreshLayout.setRefreshing(false);
                try {
                    NewFeedResponse feedResponse = response.body();
                    if (feedResponse.getItemFeedList().size() > 0) {

                        if (isUpdate)
                            defaultFragment.itemFeedList.clear();
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        frame_preload.setVisibility(View.GONE);
                        HashMap<String, Stylist> stylistHashMap = new HashMap<>();
                        for (int i = 0; i < feedResponse.getStylists().size(); i++) {
                            Stylist stylist = feedResponse.getStylists().get(i);
                            stylist.setId(stylist.getUserid());
                            stylist.setName(stylist.getFirstname() + " " + stylist.getLastname());
                            stylistHashMap.put(stylist.getUserid(), stylist);
                        }

                        List<ItemFeed> mItemFeedList = new ArrayList<ItemFeed>();
                        mItemFeedList.addAll(feedResponse.getItemFeedList());
                        for (int i = 0; i < feedResponse.getItemFeedList().size(); i++) {
                            mItemFeedList.get(i).setStylist(stylistHashMap.get(mItemFeedList.get(i).getUserid()));
                            for (int j = 0; j < mItemFeedList.get(i).getComments().size(); j++) {
                                mItemFeedList.get(i).getComments().get(j).setStylist(stylistHashMap.get(mItemFeedList.get(i).getComments().get(j).getUserid()));
                            }
                        }

                        defaultFragment.itemFeedList.addAll(mItemFeedList);
                        feedAdapter.notifyDataSetChanged();

                        // check require allow load more or not
                        if (feedResponse.getMoreavailable().equals("Y")) {
                            isMoreAvaiable = true;
                        } else {
                            isMoreAvaiable = false;
                        }
                    } else {
                        if (defaultFragment.itemFeedList.size() == 0) {
                            frame_preload.setVisibility(View.VISIBLE);
                            swipeRefreshLayout.setVisibility(View.GONE);
                            footerListview.setVisibility(View.GONE);
                            frame_preload.findViewById(R.id.progressBarCircularIndetermininate).setVisibility(View.GONE);
                            frame_preload.findViewById(R.id.tv_no_result).setVisibility(View.VISIBLE);
                        }
                        isMoreAvaiable = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<NewFeedResponse> call, Throwable t) {
                Log.d(TAG, "content error " + t.getCause());
                t.printStackTrace();

                if (t instanceof SocketTimeoutException) {
                    if (defaultFragment.itemFeedList.size() == 0) {
                        frame_preload.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        footerListview.setVisibility(View.GONE);
                        frame_preload.findViewById(R.id.progressBarCircularIndetermininate).setVisibility(View.GONE);
                        frame_preload.findViewById(R.id.tv_no_result).setVisibility(View.VISIBLE);
                    }
                    isMoreAvaiable = false;
                    Toast.makeText(getContext(), "Timeout exception", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (t instanceof ConnectException) {
                    if (defaultFragment.itemFeedList.size() == 0) {
                        frame_preload.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setVisibility(View.GONE);
                        footerListview.setVisibility(View.GONE);
                        frame_preload.findViewById(R.id.progressBarCircularIndetermininate).setVisibility(View.GONE);
                        frame_preload.findViewById(R.id.tv_no_result).setVisibility(View.VISIBLE);
                    }
                    isMoreAvaiable = false;
                    Toast.makeText(getContext(), "No connection", Toast.LENGTH_SHORT).show();

                    return;
                }
            }
        });
    }

    private GetlaydDefaultFragment defaultFragment;

    public void setDefaultFragment(GetlaydDefaultFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }

}
