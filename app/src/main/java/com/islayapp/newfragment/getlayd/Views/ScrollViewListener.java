package com.islayapp.newfragment.getlayd.Views;

/**
 * Created by maxo on 5/12/16.
 */
public interface ScrollViewListener {
    void onScrollChanged(ScrollViewExt scrollView,
                         int x, int y, int oldx, int oldy);
}