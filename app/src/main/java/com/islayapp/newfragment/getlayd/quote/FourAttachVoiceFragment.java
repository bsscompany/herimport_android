package com.islayapp.newfragment.getlayd.quote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.islayapp.NewHomeActivity;
import com.islayapp.R;

public class FourAttachVoiceFragment extends BaseFragment implements View.OnClickListener {
    public static FourAttachVoiceFragment newInstance() {
        FourAttachVoiceFragment fragmentFirst = new FourAttachVoiceFragment();
        Bundle args = new Bundle();
//        args.putInt("product_id", productID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = FourAttachVoiceFragment.class.getName();
    private RelativeLayout root_before_record_screen, root_record_screen, root_end_record_screen;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_attach_voice, container, false);
        init();
        initBottombarCircleSelect(4);
        return rootView;
    }

    private void init() {
        ImageButton back = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        back.setOnClickListener(this);

        ImageButton nextOnChoosePhoto = (ImageButton) rootView.findViewById(R.id.nextOnChoosePhoto);
        nextOnChoosePhoto.setVisibility(View.GONE);
        nextOnChoosePhoto.setOnClickListener(this);

        LinearLayout btn_skip = (LinearLayout) rootView.findViewById(R.id.btn_skip);
        btn_skip.setOnClickListener(this);

        ImageView record_voice_icon = (ImageView) rootView.findViewById(R.id.record_voice_icon);
        record_voice_icon.setOnClickListener(this);

        ImageView recording_voice_icon = (ImageView) rootView.findViewById(R.id.recording_voice_icon);
        recording_voice_icon.setOnClickListener(this);

        ImageView record_voice_done_icon = (ImageView) rootView.findViewById(R.id.record_voice_done_icon);
        record_voice_done_icon.setOnClickListener(this);

        ImageView imgbtn_close = (ImageView) rootView.findViewById(R.id.imgbtn_close);
        imgbtn_close.setOnClickListener(this);


        root_before_record_screen = (RelativeLayout) rootView.findViewById(R.id.root_before_record_screen);
        root_record_screen = (RelativeLayout) rootView.findViewById(R.id.root_record_screen);
        root_end_record_screen = (RelativeLayout) rootView.findViewById(R.id.root_end_record_screen);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToScreenGetlaydDefault();
                break;
            case R.id.btn_skip:
            case R.id.nextOnChoosePhoto:
//                ((NewHomeActivity) getActivity()).updateFragmentGetlayd(Constants.FRAGMENT_GETLAYD_CALENDER, 0);
                break;
            case R.id.record_voice_icon:
                root_before_record_screen.setVisibility(View.GONE);
                root_end_record_screen.setVisibility(View.GONE);
                root_record_screen.setVisibility(View.VISIBLE);
                break;
            case R.id.recording_voice_icon:
                root_before_record_screen.setVisibility(View.GONE);
                root_end_record_screen.setVisibility(View.VISIBLE);
                root_record_screen.setVisibility(View.GONE);
                break;
            case R.id.record_voice_done_icon:
//                root_before_record_screen.setVisibility(View.VISIBLE);
//                root_end_record_screen.setVisibility(View.GONE);
//                root_record_screen.setVisibility(View.GONE);

                ImageButton nextOnChoosePhoto = (ImageButton) rootView.findViewById(R.id.nextOnChoosePhoto);
                nextOnChoosePhoto.setVisibility(View.VISIBLE);

                break;
            case R.id.imgbtn_close:
                root_before_record_screen.setVisibility(View.VISIBLE);
                root_end_record_screen.setVisibility(View.GONE);
                root_record_screen.setVisibility(View.GONE);
                break;


        }
    }
}