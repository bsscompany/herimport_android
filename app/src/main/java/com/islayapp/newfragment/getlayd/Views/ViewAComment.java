package com.islayapp.newfragment.getlayd.Views;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.customview.Views.EmojiconTextViewPlus;

/**
 * Created by DINO on 11/04/2016.
 */
public class ViewAComment extends RelativeLayout { //implements View.OnClickListener {
//    int postion;
    TextViewPlus name;
    EmojiconTextViewPlus commnet;

    public ViewAComment(Context context, String name, String commnet, int postion) {
        super(context);
        init(context);
        SetCommnet(name, commnet, postion);
        Log.e("commnet", postion + "");
    }

    public ViewAComment(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ViewAComment(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        mInflater.inflate(R.layout.layout_a_comment, this, true);
        name = (TextViewPlus) findViewById(R.id.name);
        commnet = (EmojiconTextViewPlus) findViewById(R.id.comment);
//        setOnClickListener(this);
    }

    private void SetCommnet(String name, String cmm, int pos) {

        this.name.setText("@"+name);

//        this.postion = pos;
        String ss = "  ";
        if (name != null) {
            for (int i = 0; i < name.length() * 2.2; i++) {
                ss = ss + " ";
            }
        }
        this.commnet.setText(ss + cmm);
    }

//    @Override
//    public void onClick(View v) {
//        //Toast.makeText(getContext(), "pos " + postion, Toast.LENGTH_SHORT).show();
//        //if (v.getId() == commnet.getId()) {
//        ((ViewComment) getParent()).OnClickItemListener(this, postion);
//        // }
//    }
}
