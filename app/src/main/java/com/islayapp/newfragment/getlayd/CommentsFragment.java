package com.islayapp.newfragment.getlayd;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.LambdaInterface;
import com.amazonaws.models.Comment;
import com.amazonaws.models.CommonFindRequest;
import com.amazonaws.models.CommonResponse;
import com.amazonaws.models.PostComment;
import com.amazonaws.models.PostDetailsResponse;
import com.amazonaws.models.UserPosttNewCommentRequest;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.customview.Views.CircleImageView;
import com.islayapp.customview.Views.EmojiconTextViewPlus;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.newfragment.getlayd.Views.EmojiconEditTextPlus;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.ScreenUtils;
import com.islayapp.util.Session;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;

/**
 * Created by DINO on 11/04/2016.
 */
public class CommentsFragment extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    public static CommentsFragment newInstance(List<PostComment> postComments, String value) {
        CommentsFragment fragmentFirst = new CommentsFragment();
        Bundle args = new Bundle();
        args.putString("postID", value);
        args.putSerializable("postComments", (Serializable) postComments);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    /**
     * bundle.putSerializable("lstContact", (Serializable) lstObject);
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    private static final String TAG = CommentsFragment.class.getName();

    private List<PostComment> postComments = new ArrayList<>();
    private String postID;
    private CommentsAdapter commentsAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView listView;
    private TextViewPlus btn_send;
    private EmojiconEditTextPlus edt_comment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postComments = (List<PostComment>) getArguments().getSerializable("postComments");
        postID = getArguments().getString("postID");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_comments, container, false);


        edt_comment = (EmojiconEditTextPlus) rootView.findViewById(R.id.edit);

        commentsAdapter = new CommentsAdapter(getContext(), R.layout.layout_item_commnet, postComments);
        listView = (ListView) rootView.findViewById(R.id.listView4);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        listView.setAdapter(commentsAdapter);

        EmojIconActions emojIcon = new EmojIconActions(getContext(), rootView, (EmojiconEditTextPlus) rootView.findViewById(R.id.edit), (ImageView) rootView.findViewById(R.id.emojiconbutton));
        emojIcon.ShowEmojIcon();

        btn_send = (TextViewPlus) rootView.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);

        return rootView;
    }

    @Override
    protected void initTopbar() {
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText("COMMENTS");
        rootView.findViewById(R.id.backOnQuoteFragment).setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();

        // request get post details
        CommonFindRequest commonFindRequest = new CommonFindRequest();
        commonFindRequest.setUniqueid(postID);
        commonFindRequest.setUserid(Session.getUsers().getUser().getId());
        requestPostDetails(commonFindRequest, false);

    }


    private void requestPostDetails(CommonFindRequest request, boolean isShowDialog) {
        if (isShowDialog)
            dialog = DialogUtil.showDialog(getContext());
        Log.d(TAG, "LAMBDA: Sending Data");
        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());

        Gson gson = new Gson();
        Log.d(TAG, "request to json: " + gson.toJson(request));

        final LambdaInterface myInterface = factory.build(LambdaInterface.class);

        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<CommonFindRequest, Void, PostDetailsResponse>() {

            @Override
            protected PostDetailsResponse doInBackground(CommonFindRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.PostDetails(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(PostDetailsResponse result) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                swipeRefreshLayout.setRefreshing(false);

                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    return;
                } else {
                    Log.d(TAG, "LAMBDA: Response from request have data" + result.getMessage());
                    postComments.clear();
                    postComments.addAll(result.getPostComments());
                    commentsAdapter.notifyDataSetChanged();

                    updateCommentAndCountComment(postID, result);

                }
            }
        }.execute(request);
    }

    /**
     * update comment count and comment text
     *
     * @param postID
     * @param result
     */
    private void updateCommentAndCountComment(String postID, PostDetailsResponse result) {
        for (int i = 0; i < defaultFragment.itemFeedList.size(); i++) {
            if (defaultFragment.itemFeedList.get(i).getId().equals(postID)) {
                defaultFragment.itemFeedList.get(i).getCountof().setComments(result.getPosts().getCountof().getComments());
                List<Comment> comments = new ArrayList<>();

                if (result.getPostComments().size() > 2) {
                    for (int j = 0; j < 2; j++) {
                        Comment comment1 = new Comment();
                        comment1.setStylist(defaultFragment.itemFeedList.get(i).getStylist());
                        comment1.setNickname(defaultFragment.itemFeedList.get(i).getStylist().getNickname());
                        comment1.setComment(result.getPostComments().get(j).getComment());
                        comment1.setPostid(postID);
                        comment1.setUserid(defaultFragment.itemFeedList.get(i).getStylist().getUserid());
                        comments.add(comment1);
                    }
                } else {
                    for (int j = 0; j < result.getPostComments().size(); j++) {
                        Comment comment1 = new Comment();
                        comment1.setStylist(defaultFragment.itemFeedList.get(i).getStylist());
                        comment1.setNickname(defaultFragment.itemFeedList.get(i).getStylist().getNickname());
                        comment1.setComment(result.getPostComments().get(j).getComment());
                        comment1.setPostid(postID);
                        comment1.setUserid(defaultFragment.itemFeedList.get(i).getStylist().getUserid());
                        comments.add(comment1);
                    }
                }
                //update to list feed item
                defaultFragment.itemFeedList.get(i).setComments(comments);
                // update to list

            }
        }
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {

// showing refresh animation before making http call
        swipeRefreshLayout.setRefreshing(true);
        CommonFindRequest commonFindRequest = new CommonFindRequest();
        commonFindRequest.setUniqueid(postID);
        commonFindRequest.setUserid(Session.getUsers().getUser().getId());
        requestPostDetails(commonFindRequest, false);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                defaultFragment.popback();
                break;
            case R.id.btn_send:
                if (edt_comment.getText().length() > 0) {
                    UserPosttNewCommentRequest request = new UserPosttNewCommentRequest();
                    request.setUserid(Session.getUsers().getUser().getId());
                    String sEncoded = "";
                    sEncoded = edt_comment.getText().toString();

                    if (sEncoded.length() > 3000) {
                        Toast.makeText(getContext(), getString(R.string.max_length_alert), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    request.setComment(sEncoded);
                    request.setPostid(postID);
                    requestPostNewComment(request, true);


                }
                break;
        }
    }

    private Dialog dialog;

    private void requestPostNewComment(UserPosttNewCommentRequest request, boolean isShowDialog) {

        if (isShowDialog)
        dialog = DialogUtil.showDialog(getContext());

        Gson gson = new Gson();
        Log.d(TAG, "request to json: " + gson.toJson(request));

        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        final LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());
        final LambdaInterface myInterface = factory.build(LambdaInterface.class);
        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<UserPosttNewCommentRequest, Void, CommonResponse>() {

            @Override
            protected CommonResponse doInBackground(UserPosttNewCommentRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.PostComment(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(CommonResponse result) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    return;
                } else {
                    Log.d(TAG, "LAMBDA: Response from request have data");
                    Log.d(TAG, "response: " + result.getValid());

                    if (result.getValid().equals("Y")) {

                        PostComment postComment = new PostComment();
                        postComment.setPostid(postID);
                        postComment.setUserid(Session.getUsers().getUser().getId());
                        postComment.setComment(edt_comment.getText().toString());
                        postComment.setPicurl(Session.getUsers().getUser().getFacebook().getPicurl());
                        postComment.setNickname(Session.getUsers().getUser().getNickname());

                        postComments.add(0, postComment);
                        commentsAdapter.notifyDataSetChanged();
                        edt_comment.setText("");

                        CommonFindRequest commonFindRequest = new CommonFindRequest();
                        commonFindRequest.setUniqueid(postID);
                        commonFindRequest.setUserid(Session.getUsers().getUser().getId());
                        requestPostDetails(commonFindRequest, false);

                    }
                }
            }
        }.execute(request);
    }


    class CommentsAdapter extends ArrayAdapter<PostComment> {
        private List<PostComment> postComments;
        private int sizeAvater = 100;

        public CommentsAdapter(Context context, int resource, List<PostComment> postCommentList) {
            super(context, resource, postCommentList);
            postComments = postCommentList;
            sizeAvater = (int) (ScreenUtils.getWidthPixel(context) * 0.13);
        }

        @Override
        public int getCount() {
            return postComments.size();
        }

        @Override
        public PostComment getItem(int position) {
            return postComments.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View rowView = convertView;
            PostComment item = getItem(position);
            if (rowView == null) {
                rowView = LayoutInflater.from(getContext()).inflate(R.layout.layout_item_commnet, parent, false);

                ViewHolder viewHolder = new ViewHolder();
                viewHolder.img_avatar = (CircleImageView) rowView.findViewById(R.id.img_avatar);
                viewHolder.post_comment_name = (TextViewPlus) rowView.findViewById(R.id.post_comment_name);
                viewHolder.comment_content = (EmojiconTextViewPlus) rowView.findViewById(R.id.comment_content);

                rowView.setTag(viewHolder);
            }

            ViewHolder holder = (ViewHolder) rowView.getTag();

            holder.post_comment_name.setText("@" + item.getNickname());

            holder.comment_content.setText(item.getComment());
            Log.d("TAG", "comment string: " + item.getComment());
            holder.img_avatar.setLayoutParams(new LinearLayout.LayoutParams(sizeAvater, sizeAvater));
            if (!item.getPicurl().equals(""))
                Glide.with(getContext())
                        .load(item.getPicurl())
                        .into(holder.img_avatar);

            return rowView;
        }


        class ViewHolder {
            public CircleImageView img_avatar;
            public TextViewPlus post_comment_name;
            public EmojiconTextViewPlus comment_content;
        }
    }

    private GetlaydDefaultFragment defaultFragment;

    public void setDefaultFragment(GetlaydDefaultFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }

}
