package com.islayapp.newfragment.getlayd;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.LambdaInterface;
import com.amazonaws.models.FindStylistRequest;
import com.amazonaws.models.FindStylistResponse;
import com.amazonaws.models.LambdaJsonBinder;
import com.amazonaws.models.googlemap.AddressResponse;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.core.APIService;
import com.islayapp.R;
import com.islayapp.adapter.AutocompletedSearchAdapter;
import com.islayapp.adapter.StyListAdapter;
import com.islayapp.customview.AutoCompleteTextViewPlus;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.models.awsmodels.AddressResultByLocation;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.util.Constants;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.Session;
import com.islayapp.util.location.SingleShotLocationProvider;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DINO on 07/04/2016.
 */
public class StyListDirectoryFragment extends BaseFragment implements View.OnClickListener {
    public static StyListDirectoryFragment newInstance() {
        StyListDirectoryFragment fragmentFirst = new StyListDirectoryFragment();
        return fragmentFirst;
    }

    private static final String TAG = StyListDirectoryFragment.class.getName();
    private StyListAdapter styListAdapter;
    private TextViewPlus tv_search;
    private AutoCompleteTextViewPlus edt_search;
    private AutocompletedSearchAdapter adapterAddressResult;
    private ListView list;
    private View footerListview;
    private FrameLayout search_result_frame;
    private FindStylistRequest request;
    private boolean isLoading = false;
    private boolean isMoreAvaiable = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_stylist_directory, container, false);
        initLayout();
        return rootView;
    }

    @Override
    protected void initTopbar() {
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText(getResources().getString(R.string.get_layd));
        rootView.findViewById(R.id.backOnQuoteFragment).setOnClickListener(this);
    }

    private void initLayout() {
        search_result_frame = (FrameLayout) rootView.findViewById(R.id.search_result_frame);
        tv_search = (TextViewPlus) rootView.findViewById(R.id.tv_search);
        tv_search.setOnClickListener(this);
        list = (ListView) rootView.findViewById(R.id.liststy);

        footerListview = LayoutInflater.from(getContext()).inflate(R.layout.bottom_listview, null);
        footerListview.setVisibility(View.GONE);
        list.addFooterView(footerListview);


        styListAdapter = new StyListAdapter(getContext(), R.layout.item_list_entry_stylist_director, defaultFragment.mStylists);
        list.setAdapter(styListAdapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                defaultFragment.addFragment(GetlaydDefaultFragment.GetLayDEnum.Profile, defaultFragment.mStylists.get(position));
            }
        });

        // listener event
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                //what is the bottom iten that is visible
                int lastInScreen = firstVisibleItem + visibleItemCount;
                //is the bottom item visible & not loading more already ? Load more !
                if ((lastInScreen == totalItemCount) && isMoreAvaiable && !isLoading) {
                    isLoading = true;
                    request.setPagenumber(request.getPagenumber() + 1);
                    searchRequest(request, false);
                }

            }
        });


        edt_search = (AutoCompleteTextViewPlus) rootView.findViewById(R.id.edt_search);
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edt_search.isPerformingCompletion()) {
                    // An item has been selected from the list. Ignore.
                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
                    return;
                }

                if (s.length() >= 2) {
                    fetchAutocompletePlaces(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edt_search.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    requestSearch(edt_search.getText().toString());
                    return true;
                }
                return false;
            }
        });


        edt_search.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(arg1.getWindowToken(), 0);
                String item = adapterAddressResult.getItem(arg2);
                requestSearch(item);
            }

        });

        getLocation(getContext());

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (edt_search.isPerformingCompletion()) {
            // An item has been selected from the list. Ignore.
            InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
        }
    }


    public void getLocation(Context context) {
        // when you need location
        // if inside activity context = this;

        SingleShotLocationProvider.requestSingleUpdate(context,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override
                    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        Log.d("Location", "my location is " + location.latitude + "," + location.longitude);
                        getAddressByLocation(location.longitude + "," + location.latitude);
                    }
                });
    }


    private void getAddressByLocation(String location) {

        APIService service = NetworkConnectionUtil.createGmapApiService(getContext());

        Call<AddressResultByLocation> response = service.searchAddressByLocation(location);
        response.enqueue(new Callback<AddressResultByLocation>() {
            @Override
            public void onResponse(Call<AddressResultByLocation> call, Response<AddressResultByLocation> response) {
                Log.d(TAG, "content onResponse : " + response.toString());
                try {

                    AddressResultByLocation item = response.body();
                    if (item.getResults().size() > 0) {

                        defaultFragment.mStylists.clear();
                        request = new FindStylistRequest();
                        request.setAddress(item.getResults().get(0).getFormattedAddress());
                        request.setPagesize(Constants.PAGE_SIZE);
                        request.setDistancetype(Constants.DISTANCE_TYPE);
                        request.setPagenumber(1);
                        request.setDistance(2000);
                        request.setUserid(Session.getUsers().getUser().getId());

                        searchRequest(request, false);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddressResultByLocation> call, Throwable t) {
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                defaultFragment.popback();
                break;
            case R.id.tv_search:
                requestSearch(edt_search.getText().toString());
                break;
        }
    }

    /**
     * autocompleted
     *
     * @param input
     */
    private void fetchAutocompletePlaces(String input) {
        if (input == null || input.equals(""))
            return;

        Log.d(TAG, "content input : " + input.toString());


        APIService service = NetworkConnectionUtil.createGmapApiService(getActivity());

        Call<AddressResponse> response = service.searchAutocompletedAddress(Constants.GOOGLE_MAPS_KEY, input);
        response.enqueue(new Callback<AddressResponse>() {
            @Override
            public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                try {
                    AddressResponse body = response.body();
                    ArrayList<String> newArr = new ArrayList<String>();
                    for (int i = 0; i < body.getPredictions().size(); i++) {
                        newArr.add(body.getPredictions().get(i).getDescription());
                    }

                    Log.d(TAG, "Length: " + body.getPredictions().size());
                    adapterAddressResult = new AutocompletedSearchAdapter(getContext(), R.layout.layout_autocompleted_search_stylist, newArr);
                    edt_search.setAdapter(adapterAddressResult);
                    edt_search.showDropDown();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddressResponse> call, Throwable t) {
            }
        });
    }

    /**
     * request search
     *
     * @param query
     */
    private void requestSearch(String query) {
        if (query.length() == 0)
            return;
        defaultFragment.mStylists.clear();
        request = new FindStylistRequest();
        request.setAddress(query);
        request.setPagesize(Constants.PAGE_SIZE);
        request.setDistancetype(Constants.DISTANCE_TYPE);
        request.setPagenumber(1);
        request.setDistance(2000);
        request.setUserid(Session.getUsers().getUser().getId());

        searchRequest(request, true);

    }


    private Dialog dialog;

    private void searchRequest(FindStylistRequest request, boolean isShowDialog) {

        if (isShowDialog)
            dialog = DialogUtil.showDialog(getContext());
        Log.d(TAG, "LAMBDA: Sending Data");
        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());

        // 3. Create an interface (see MyInterface)
        final LambdaInterface myInterface = factory.build(LambdaInterface.class, new LambdaJsonBinder());

        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<FindStylistRequest, Void, FindStylistResponse>() {

            @Override
            protected FindStylistResponse doInBackground(FindStylistRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.FindStylist(params[0]);
                } catch (Exception lfe) {
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(FindStylistResponse result) {
                if (dialog != null)
                    dialog.dismiss();
                isLoading = false;
                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    search_result_frame.setVisibility(View.VISIBLE);
                    list.setVisibility(View.GONE);
                    footerListview.setVisibility(View.GONE);

                    return;
                } else {
                    if (result.getStylists().size() > 0) {

                        // update adapter
                        search_result_frame.setVisibility(View.GONE);
                        list.setVisibility(View.VISIBLE);
                        list.setSelection(0);
                        defaultFragment.mStylists.addAll(result.getStylists());
                        styListAdapter.notifyDataSetChanged();

                        // check require allow load more or not
                        if (result.getMoreavailable().equals("Y")) {
                            isMoreAvaiable = true;
                            footerListview.setVisibility(View.VISIBLE);
                        } else {
                            isMoreAvaiable = false;
                            footerListview.setVisibility(View.GONE);
                        }
                    } else {
                        search_result_frame.setVisibility(View.VISIBLE);
                        list.setVisibility(View.GONE);
                        footerListview.setVisibility(View.GONE);
                        isMoreAvaiable = false;
                    }
                }
            }
        }.execute(request);
    }

    private GetlaydDefaultFragment defaultFragment;

    public void setDefaultFragment(GetlaydDefaultFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }
}