package com.islayapp.newfragment.getlayd.quote;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.CursorLoader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.TextViewPlus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FirstChoosePhotoFragment extends BaseFragment implements  View.OnClickListener {
    public static FirstChoosePhotoFragment newInstance() {
        FirstChoosePhotoFragment fragmentFirst = new FirstChoosePhotoFragment();
        Bundle args = new Bundle();
//        args.putInt("product_id", productID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = FirstChoosePhotoFragment.class.getName();
    private static final int REQUEST_CAMERA = 900;
    private static final int SELECT_FILE = 901;
    private ImageView img_user_selected;
    private RelativeLayout root_quote_choose_photo, root_quote_show_photo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_get_quote_choose_photo, container, false);
        init();
        initBottombarCircleSelect(1);
        return rootView;
    }

    private void init() {
        ImageButton back = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        back.setOnClickListener(this);

        ImageButton button_close = (ImageButton) rootView.findViewById(R.id.button_close);
        button_close.setOnClickListener(this);

        ImageButton nextOnChoosePhoto = (ImageButton) rootView.findViewById(R.id.nextOnChoosePhoto);
        nextOnChoosePhoto.setOnClickListener(this);

        TextViewPlus choose_photo = (TextViewPlus) rootView.findViewById(R.id.choose_photo);
        TextViewPlus take_photo = (TextViewPlus) rootView.findViewById(R.id.take_photo);
        choose_photo.setOnClickListener(this);
        take_photo.setOnClickListener(this);

        img_user_selected = (ImageView) rootView.findViewById(R.id.img_user_selected);
        root_quote_choose_photo = (RelativeLayout) rootView.findViewById(R.id.root_quote_choose_photo);
        root_quote_show_photo = (RelativeLayout) rootView.findViewById(R.id.root_quote_show_photo);
        root_quote_choose_photo.setVisibility(View.VISIBLE);
        root_quote_show_photo.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToScreenGetlaydDefault();
                break;

            case R.id.button_close:
                root_quote_choose_photo.setVisibility(View.VISIBLE);
                root_quote_show_photo.setVisibility(View.GONE);
                break;

            case R.id.choose_photo:

                Intent intent = new Intent( Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult( Intent.createChooser(intent, "Select File"), SELECT_FILE);

                break;
            case R.id.take_photo:
                Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent1, REQUEST_CAMERA);

                break;

            case R.id.nextOnChoosePhoto:
//                ((NewHomeActivity) getActivity()).updateFragmentGetlayd(Constants.FRAGMENT_GETLAYD_BUNDLES_AND_CLOSURES,0);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Fragment onActivityResult");

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Log.d(TAG, "Fragment onActivityResult ok");

            if (requestCode == REQUEST_CAMERA) {
                Log.d(TAG, "Fragment onActivityResult request camera ok");

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                root_quote_show_photo.setVisibility(View.VISIBLE);
                root_quote_choose_photo.setVisibility(View.GONE);
                img_user_selected.setImageBitmap(thumbnail);

            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(getActivity(), selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 400;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);

                img_user_selected.setImageBitmap(bm);


                root_quote_show_photo.setVisibility(View.VISIBLE);
                root_quote_choose_photo.setVisibility(View.GONE);

            }
        }
    }
}