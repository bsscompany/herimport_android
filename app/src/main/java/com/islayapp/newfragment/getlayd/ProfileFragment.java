package com.islayapp.newfragment.getlayd;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.LambdaInterface;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.amazonaws.models.CommonResponse;
import com.amazonaws.models.FindUserPostRequest;
import com.amazonaws.models.FindUserPostResponse;
import com.amazonaws.models.LambdaJsonBinder;
import com.amazonaws.models.Post;
import com.amazonaws.models.Stylist;
import com.amazonaws.models.UserFollowRequest;
import com.bumptech.glide.Glide;
import com.islayapp.R;
import com.islayapp.adapter.UserGalleryAdapter;
import com.islayapp.customview.Views.CircleImageView;
import com.islayapp.customview.Views.EmojiconTextViewPlus;
import com.islayapp.customview.Views.TextViewFocusPlus;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.newfragment.getlayd.Views.ImageGridView;
import com.islayapp.newfragment.getlayd.Views.ScrollViewExt;
import com.islayapp.newfragment.getlayd.Views.ScrollViewListener;
import com.islayapp.util.Constants;
import com.islayapp.util.ScreenUtils;
import com.islayapp.util.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DINO on 07/04/2016.
 */
public class ProfileFragment extends BaseFragment implements View.OnClickListener, ScrollViewListener {
    public static ProfileFragment newInstance(Stylist value) {
        ProfileFragment fragmentFirst = new ProfileFragment();

        Bundle args = new Bundle();
        args.putSerializable("stylist", value);
        fragmentFirst.setArguments(args);

        return fragmentFirst;
    }

    private static final String TAG = ProfileFragment.class.getName();
    //    private FrameLayout frame_preload;
    private FindUserPostRequest findStylistPostRequest;
    private Stylist stylist = new Stylist();
    private List<Post> mPostList = new ArrayList<>();
    private UserGalleryAdapter adapter;
    private View loading;
    private boolean isLoading = false;
    private boolean isMoreAvaiable = false;
    private TextViewFocusPlus btn_follower;
    //    private GridViewWithHeaderAndFooter gridView;
    private ImageGridView gridView;
    private FrameLayout frame_preload;
    private int width = 500;
    private ScrollViewExt scrollView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        stylist = (Stylist) getArguments().getSerializable("stylist");
        Log.d(TAG, "user id: " + stylist.getName());

        width = ScreenUtils.getWidthPixel(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_profile_new, container, false);

        frame_preload = (FrameLayout) rootView.findViewById(R.id.frame_preload);

        gridView = (ImageGridView) rootView.findViewById(R.id.gridView);


        //update header view
        initStylistInfo(stylist);


        loading = rootView.findViewById(R.id.progressBar);
//        gridView.addHeaderView(headerView);

        // initLayout();
        adapter = new UserGalleryAdapter(getContext(), R.layout.item_image_grid_profile, mPostList);

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                defaultFragment.PostDetailsFragment(ProfileFragment.this, stylist, mPostList.get(position), btn_follower.isSelected());
                Log.d(TAG, "click to item: " + mPostList.get(position).getId());
            }
        });

        scrollView = (ScrollViewExt) rootView.findViewById(R.id.scrollViewLayout);
        scrollView.setScrollViewListener(this);

        return rootView;
    }

    @Override
    protected void initTopbar() {
        // settext title
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText(stylist.getName());
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        rootView.findViewById(R.id.backOnQuoteFragment).setOnClickListener(this);

    }

    @Override
    public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
        // if diff is zero, then the bottom has been reached
        if (diff == 0) {
            // do stuff
            if (isMoreAvaiable && !isLoading) {
                isLoading = true;
                findStylistPostRequest.setPagenumber(findStylistPostRequest.getPagenumber() + 1);
                loading.setVisibility(View.VISIBLE);
                getStylistPost(findStylistPostRequest, false);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // parram request
        findStylistPostRequest = new FindUserPostRequest();
        findStylistPostRequest.setPagesize(Constants.PAGE_SIZE);
        findStylistPostRequest.setPagenumber(1);
        findStylistPostRequest.setUserid(Session.getUsers().getUser().getId());
        findStylistPostRequest.setUniqueid(stylist.getId());
        // start find stylist post
        getStylistPost(findStylistPostRequest, true);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, " onPause");
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, " onResume");
    }

    private void initStylistInfo(Stylist stylist) {

        btn_follower = (TextViewFocusPlus) rootView.findViewById(R.id.btn_follower);
        btn_follower.setOnClickListener(this);

        if (stylist.getId().equals(Session.getUsers().getUser().getId()))
            btn_follower.setVisibility(View.GONE);
        else
            btn_follower.setVisibility(View.VISIBLE);

        CircleImageView profile_img = (CircleImageView) rootView.findViewById(R.id.profile_img);
        profile_img.setLayoutParams(new LinearLayout.LayoutParams((int) (width * 0.28), (int) (width * 0.28)));

        Glide.with(getContext())
                .load(stylist.getPicurl())
                .fitCenter()
                .into(profile_img);

        ((TextView) rootView.findViewById(R.id.name_stylist)).setText(stylist.getName());
        EmojiconTextViewPlus emojicon_text_view = (EmojiconTextViewPlus) rootView.findViewById(R.id.emojicon_text_view);
        if (stylist != null && stylist.getDescription() != null && stylist.getDescription().length() > 0) {
            emojicon_text_view.setVisibility(View.VISIBLE);
            emojicon_text_view.setText(stylist.getDescription());
        } else
            emojicon_text_view.setVisibility(View.GONE);

        ((TextView) rootView.findViewById(R.id.stylist_follower)).setText(stylist.getFollowers());
        ((TextView) rootView.findViewById(R.id.stylist_post)).setText(stylist.getPosts());

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                defaultFragment.popback();
                break;
            case R.id.btn_follower:
                UserFollowRequest requestFollow = new UserFollowRequest();
                requestFollow.setUserid(Session.getUsers().getUser().getId());
                requestFollow.setFollowuserid(stylist.getId());
                if (btn_follower.isSelected())
                    requestFollow.setFollow(false);
                else
                    requestFollow.setFollow(true);
                requestUserFollow(requestFollow);

                Animation animScale = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale_btn_follow);
                v.startAnimation(animScale);

                break;
        }
    }

    /**
     *
     */

    /**
     * @param request
     * @param ishowFramePreload
     */
    private void getStylistPost(final FindUserPostRequest request, boolean ishowFramePreload) {
        if (ishowFramePreload) {
            frame_preload.setVisibility(View.VISIBLE);
            frame_preload.findViewById(R.id.progressBarCircularIndetermininate).setVisibility(View.VISIBLE);
            loading.setVisibility(View.GONE);
            frame_preload.findViewById(R.id.tv_no_result).setVisibility(View.GONE);
        } else
            frame_preload.setVisibility(View.GONE);

        Log.d(TAG, "LAMBDA: Sending Data");
        final LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());
        final LambdaInterface myInterface = factory.build(LambdaInterface.class, new LambdaJsonBinder());
        new AsyncTask<FindUserPostRequest, Void, FindUserPostResponse>() {

            @Override
            protected FindUserPostResponse doInBackground(FindUserPostRequest... params) {
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.FindStylistPosts(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(FindUserPostResponse result) {
                isLoading = false;
                loading.setVisibility(View.GONE);

                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    isMoreAvaiable = false;

                    if (isAdded()) {
                        frame_preload.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        frame_preload.findViewById(R.id.progressBarCircularIndetermininate).setVisibility(View.GONE);
                        frame_preload.findViewById(R.id.tv_no_result).setVisibility(View.VISIBLE);
                        ((TextView) frame_preload.findViewById(R.id.tv_no_result)).setText(getString(R.string.time_out));
                    }
                    return;
                } else if (isAdded()) {
                    Log.d(TAG, "LAMBDA: Response from request size:" + result.getPosts().size());
                    if (result.getUserfollowing() != null) {
                        btn_follower.setSelected(true);
                    }

                    if (result.getPosts().size() > 0) {
                        frame_preload.setVisibility(View.GONE);
                        mPostList.addAll(result.getPosts());

                        int A = scrollView.getVerticalScrollbarPosition();
                        adapter.notifyDataSetInvalidated();
                        scrollView.setVerticalScrollbarPosition(A);

                        // check require allow load more or not
                        if (result.getMoreavailable().equals("Y")) {
                            isMoreAvaiable = true;
                        } else {
                            isMoreAvaiable = false;
                        }
                    } else {
                        if (mPostList.size() == 0) {
                            frame_preload.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                            frame_preload.findViewById(R.id.progressBarCircularIndetermininate).setVisibility(View.GONE);
                            frame_preload.findViewById(R.id.tv_no_result).setVisibility(View.VISIBLE);
                        }
                        isMoreAvaiable = false;
                    }
                }
            }
        }.execute(request);
    }

//    private Dialog dialog;

    private void requestUserFollow(UserFollowRequest request) {


        Log.d(TAG, "LAMBDA: Sending Data");
        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());
        final LambdaInterface myInterface = factory.build(LambdaInterface.class);
        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<UserFollowRequest, Void, CommonResponse>() {

            @Override
            protected CommonResponse doInBackground(UserFollowRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.UserFollow(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(CommonResponse result) {
                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    return;
                } else if (isAdded()) {
                    Log.d(TAG, "LAMBDA: Response from request have data");
                    Log.d(TAG, "response: " + result.getValid());

                    if (result.getValid().equals("Y")) {
                        if (btn_follower.isSelected()) {
                            try {
                                int temp = Integer.valueOf(stylist.getFollowers());
                                if (temp > 0) {
                                    temp--;
                                    ((TextView) rootView.findViewById(R.id.stylist_follower)).setText(temp+"");
                                }
                            } catch (Exception ex) {

                            }

                            btn_follower.setSelected(false);
                        } else {
                            try {
                                int temp = Integer.valueOf(stylist.getFollowers());
                                if (temp > 0) {
                                    temp++;
                                    ((TextView) rootView.findViewById(R.id.stylist_follower)).setText(temp+"");
                                }
                            } catch (Exception ex) {

                            }
                            btn_follower.setSelected(true);

                        }
                    }
                }
            }
        }.execute(request);
    }

    private GetlaydDefaultFragment defaultFragment;

    public void setDefaultFragment(GetlaydDefaultFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }


}
