package com.islayapp.newfragment.getlayd.quote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.CircleColorView;
import com.islayapp.customview.RobotoCalendarView;
import com.islayapp.util.ScreenUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FiveCalenderFragment extends BaseFragment implements View.OnClickListener, RobotoCalendarView.RobotoCalendarListener {
    public static FiveCalenderFragment newInstance() {
        FiveCalenderFragment fragmentFirst = new FiveCalenderFragment();
        Bundle args = new Bundle();
//        args.putInt("product_id", productID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = FiveCalenderFragment.class.getName();
    private RobotoCalendarView robotoCalendarView;
    private int currentMonthIndex;
    private Calendar currentCalendar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_calender, container, false);
        init();
        initBottombarCircleSelect(5);
        return rootView;
    }

    private void init() {
        ImageButton back = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        back.setOnClickListener(this);

        ImageButton nextOnChoosePhoto = (ImageButton) rootView.findViewById(R.id.nextOnChoosePhoto);
        nextOnChoosePhoto.setOnClickListener(this);


        // Gets the calendar from the view
        robotoCalendarView = (RobotoCalendarView) rootView.findViewById(R.id.robotoCalendarPicker);

        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        // Initialize the RobotoCalendarPicker with the current index and date
        currentMonthIndex = 0;
        currentCalendar = Calendar.getInstance(Locale.getDefault());

        // Mark current day
        robotoCalendarView.markDayAsCurrentDay(currentCalendar.getTime());


        int widscreen = (int)(ScreenUtils.getWidthPixel(getActivity())*0.15);

        FrameLayout frameCircleHint = (FrameLayout)rootView.findViewById(R.id.frameCircleHint);
        frameCircleHint.setLayoutParams(new LinearLayout.LayoutParams(widscreen,widscreen));

        CircleColorView circleHint = (CircleColorView)rootView.findViewById(R.id.circle1111);
        circleHint.setLayoutParams(new FrameLayout.LayoutParams(widscreen,widscreen));
        circleHint.setColor("#e7e7e7");

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToScreenGetlaydDefault();
                break;
            case R.id.nextOnChoosePhoto:

                Log.d(TAG," click to next fragment");

//                ((NewHomeActivity) getActivity()).updateFragmentGetlayd(Constants.FRAGMENT_GETLAYD_SCHEDULE, 0);
                break;

        }
    }

    @Override
    public void onDateSelected(Date date) {
        // Mark calendar day
        robotoCalendarView.markDayAsSelectedDay(date);
//        robotoCalendarView.markSelectDayWithStyle(RobotoCalendarView.RED_COLOR, date);
    }

    @Override
    public void onRightButtonClick() {
        currentMonthIndex++;
        updateCalendar();
    }

    @Override
    public void onLeftButtonClick() {
        currentMonthIndex--;
        updateCalendar();
    }

    private void updateCalendar() {
        currentCalendar = Calendar.getInstance(Locale.getDefault());
        currentCalendar.add(Calendar.MONTH, currentMonthIndex);
        robotoCalendarView.initializeCalendar(currentCalendar);
        if(currentMonthIndex==0)
            robotoCalendarView.markDayAsCurrentDay(currentCalendar.getTime());

    }
}