package com.islayapp.newfragment.getlayd.quote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.newfragment.getlayd.GetlaydDefaultFragment;

public class EightPaymentFragment extends BaseFragment implements  View.OnClickListener {
    public static EightPaymentFragment newInstance() {
        EightPaymentFragment fragmentFirst = new EightPaymentFragment();
        Bundle args = new Bundle();
//        args.putInt("product_id", productID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = EightPaymentFragment.class.getName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_payment, container, false);
        init();
        return rootView;
    }

    private void init() {
        ImageButton back = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        back.setOnClickListener(this);
        Button btn_authorize_payment = (Button) rootView.findViewById(R.id.btn_authorize_payment);
        btn_authorize_payment.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToScreenGetlaydDefault();
                break;
            case R.id.btn_authorize_payment:
//                ((NewHomeActivity) getActivity()).updateFragmentGetlayd(Constants.FRAGMENT_GETLAYD_WAIT_AND_SUCCESS, 0);
                break;
        }
    }

    private GetlaydDefaultFragment defaultFragment;
    public void setDefaultFragment(GetlaydDefaultFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }
}