package com.islayapp.newfragment.getlayd.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;

/**
 * Created by DINO on 11/04/2016.
 */
public class ViewComment extends LinearLayout {
    Context context;
    int count;


    AdapterView.OnItemClickListener OnItemClickListener;

    public ViewComment(Context context) {
        super(context);
        init(context);
    }


    public ViewComment(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ViewComment(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        this.context = context;
        count = 0;
        setOrientation(VERTICAL);
        removeAllViews();
    }

    public void AddComment(String name, String commnet) {
        count++;
        ViewAComment comment = new ViewAComment(this.context, name, commnet, count - 1);
        addView(comment);
        requestLayout();
        invalidate();

    }
    public void setOnClickItemListener(AdapterView.OnItemClickListener OnItemClickListener) {
        this.OnItemClickListener = OnItemClickListener;
    }

    public void OnClickItemListener(View viewAComment, int postion) {
        if (this.OnItemClickListener != null) {
            OnItemClickListener.onItemClick(null, viewAComment, postion, postion);
        }
    }
}
