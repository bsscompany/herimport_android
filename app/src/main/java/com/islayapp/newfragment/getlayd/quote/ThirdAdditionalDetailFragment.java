package com.islayapp.newfragment.getlayd.quote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.islayapp.NewHomeActivity;
import com.islayapp.R;
import com.islayapp.customview.WheelView;

import java.util.Arrays;

public class ThirdAdditionalDetailFragment extends BaseFragment implements  View.OnClickListener {
    public static ThirdAdditionalDetailFragment newInstance() {
        ThirdAdditionalDetailFragment fragmentFirst = new ThirdAdditionalDetailFragment();
        Bundle args = new Bundle();
//        args.putInt("product_id", productID);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = ThirdAdditionalDetailFragment.class.getName();
    private static final String[] PLANETS = new String[]{"0 - 2 \"", "2 - 4 \"", "4 - 6 \"", "6 - 8 \"", "8 - 10 \"", "10 - 12 \"", "12 - 14 \"", "More than"};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_additional_detail, container, false);
        init();
        initBottombarCircleSelect(3);
        return rootView;
    }

    private void init() {
        ImageButton back = (ImageButton) rootView.findViewById(R.id.backOnQuoteFragment);
        back.setOnClickListener(this);

        ImageButton nextOnChoosePhoto = (ImageButton) rootView.findViewById(R.id.nextOnChoosePhoto);
        nextOnChoosePhoto.setOnClickListener(this);

        WheelView wva = (WheelView) rootView.findViewById(R.id.main_wv);

        wva.setOffset(1);
        wva.setItems(Arrays.asList(PLANETS));

        wva.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                Log.d(TAG, "selectedIndex: " + selectedIndex + ", item: " + item);
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                ((NewHomeActivity) getActivity()).backToScreenGetlaydDefault();
                break;

            case R.id.nextOnChoosePhoto:
//                ((NewHomeActivity) getActivity()).updateFragmentGetlayd(Constants.FRAGMENT_GETLAYD_ATTACH_VOICE, 0);

                break;
        }
    }
}