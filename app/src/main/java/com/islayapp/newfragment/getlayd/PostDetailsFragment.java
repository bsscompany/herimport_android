package com.islayapp.newfragment.getlayd;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.CognitoSyncClientManager;
import com.amazonaws.LambdaInterface;
import com.amazonaws.models.Comment;
import com.amazonaws.models.CommonFindRequest;
import com.amazonaws.models.CommonResponse;
import com.amazonaws.models.ItemFeed;
import com.amazonaws.models.Post;
import com.amazonaws.models.PostComment;
import com.amazonaws.models.PostDetailsResponse;
import com.amazonaws.models.Stylist;
import com.amazonaws.models.UserFollowRequest;
import com.amazonaws.models.UserLikePostRequest;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.islayapp.R;
import com.islayapp.customview.CircularImageView;
import com.islayapp.customview.RelativeTimeTextView;
import com.islayapp.customview.TextViewPlus;
import com.islayapp.customview.Views.EmojiconTextViewPlus;
import com.islayapp.customview.Views.TextViewFocusPlus;
import com.islayapp.newfragment.BaseFragment;
import com.islayapp.newfragment.getlayd.Views.ViewComment;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.ScreenUtils;
import com.islayapp.util.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DINO on 11/04/2016.
 */
public class PostDetailsFragment extends BaseFragment implements View.OnClickListener {
    public static PostDetailsFragment newInstance(Stylist stylist, Post post, boolean isFollow) {
        PostDetailsFragment fragmentFirst = new PostDetailsFragment();
        Bundle args = new Bundle();
        args.putBoolean("isFollow", isFollow);
        args.putSerializable("stylist", stylist);
        args.putSerializable("post", post);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    public static PostDetailsFragment newInstance(ItemFeed item) {
        Post post = new Post();
        post.setComment(item.getComment());
        post.setUserid(item.getUserid());
        post.setCountof(item.getCountof());
        post.setTimestamp(item.getTimestamp());
        post.setId(item.getId());
        post.setThumbUrl();
        post.setOriginalUrl();

        PostDetailsFragment fragmentFirst = new PostDetailsFragment();
        Bundle args = new Bundle();
        args.putBoolean("isFollow", false);
        args.putSerializable("stylist", item.getStylist());
        args.putSerializable("post", post);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private static final String TAG = PostDetailsFragment.class.getName();

//    private Stylist stylist = new Stylist();
//    private Post post = new Post();

    private boolean isFollow = false;
    private TextViewFocusPlus btn_follower;
    private ImageView like_unlike;
    private List<PostComment> postComments = new ArrayList<>();
    private int width = 500;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        defaultFragment.post = (Post) getArguments().getSerializable("post");
        defaultFragment.stylist = (Stylist) getArguments().getSerializable("stylist");
        isFollow = getArguments().getBoolean("isFollow");
        Log.d(TAG, "user id: " + defaultFragment.stylist.getName());
        width = ScreenUtils.getWidthPixel(getContext());

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_get_layd_image, container, false);

        btn_follower = (TextViewFocusPlus) rootView.findViewById(R.id.btn_follower);
        btn_follower.setOnClickListener(this);

        if (defaultFragment.stylist.getId().equals(Session.getUsers().getUser().getId()))
            btn_follower.setVisibility(View.GONE);
        else
            btn_follower.setVisibility(View.VISIBLE);
        btn_follower.setSelected(isFollow);


//        ImageView add_comment = (ImageView) rootView.findViewById(R.id.add_comment);
        ImageView imgcomment = (ImageView) rootView.findViewById(R.id.imgcomment);
        TextViewPlus allcomment = (TextViewPlus) rootView.findViewById(R.id.allcomment);
        imgcomment.setOnClickListener(this);
//        add_comment.setOnClickListener(this);
        allcomment.setOnClickListener(this);

        updateStylistInfo();

        like_unlike = (ImageView) rootView.findViewById(R.id.like_unlike);
        like_unlike.setOnClickListener(this);

        TextViewFocusPlus btn_follower = (TextViewFocusPlus) rootView.findViewById(R.id.btn_follower);
        btn_follower.setOnClickListener(this);

        if (defaultFragment.stylist.getId().equals(Session.getUsers().getUser().getId()))
            btn_follower.setVisibility(View.GONE);
        else
            btn_follower.setVisibility(View.VISIBLE);

        return rootView;
    }


    @Override
    protected void initTopbar() {
        rootView.findViewById(R.id.shop_card).setVisibility(View.INVISIBLE);
        ((TextView) rootView.findViewById(R.id.title_topbar)).setText(getResources().getString(R.string.stylist_feed));
        rootView.findViewById(R.id.backOnQuoteFragment).setOnClickListener(this);

    }

    private void updateStylistInfo() {
        CircularImageView thumb = (CircularImageView) rootView.findViewById(R.id.thumb);
        thumb.setLayoutParams(new RelativeLayout.LayoutParams((int) (width * 0.2), (int) (width * 0.2)));

        Glide.with(getContext())
                .load(defaultFragment.stylist.getPicurl())
                .into(thumb);
        TextViewPlus name_stylist = (TextViewPlus) rootView.findViewById(R.id.name_stylist);
        name_stylist.setText(defaultFragment.stylist.getName());

        TextViewPlus number_likes = (TextViewPlus) rootView.findViewById(R.id.number_likes);
        number_likes.setText(getString(R.string.number_like, defaultFragment.post.getCountof().getLikes()));

        EmojiconTextViewPlus commEmojiconTextViewPlus = (EmojiconTextViewPlus) rootView.findViewById(R.id.comment);

        commEmojiconTextViewPlus.setText(defaultFragment.post.getComment().toString());

        ImageView image = (ImageView) rootView.findViewById(R.id.image);

        Glide.with(getContext())
                .load(defaultFragment.post.getOriginalUrl())
                .placeholder(R.drawable.placeholder)
                .into(image);

        RelativeTimeTextView time_ago = (RelativeTimeTextView) rootView.findViewById(R.id.time_ago);
        time_ago.setReferenceTime(defaultFragment.post.getTimestamp().getAdded());

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, " onStart");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, " onPause");
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.d(TAG, " onHiddenChanged : " + hidden);

        CommonFindRequest commonFindRequest = new CommonFindRequest();
        commonFindRequest.setUniqueid(defaultFragment.post.getId());
        commonFindRequest.setUserid(Session.getUsers().getUser().getId());
        requestPostDetails(commonFindRequest, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, " onResume");

        CommonFindRequest commonFindRequest = new CommonFindRequest();
        commonFindRequest.setUniqueid(defaultFragment.post.getId());
        commonFindRequest.setUserid(Session.getUsers().getUser().getId());
        requestPostDetails(commonFindRequest, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backOnQuoteFragment:
                defaultFragment.popback();
                break;
            case R.id.allcomment:
//            case R.id.add_comment:
            case R.id.imgcomment:

                defaultFragment.CommentsFragment(PostDetailsFragment.this, postComments, defaultFragment.post.getId());

                break;
            case R.id.like_unlike:
                Animation animScale = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale);
                v.startAnimation(animScale);

                like_unlike = (ImageView) rootView.findViewById(R.id.like_unlike);

                UserLikePostRequest likeRequest = new UserLikePostRequest();
                likeRequest.setUserid(Session.getUsers().getUser().getId());
                likeRequest.setPostid(defaultFragment.post.getId());
                if (like_unlike.isSelected())
                    likeRequest.setLike(false);
                else
                    likeRequest.setLike(true);

                requestUserLike(likeRequest, true);
                break;
            case R.id.btn_follower:

                UserFollowRequest request = new UserFollowRequest();
                request.setUserid(Session.getUsers().getUser().getId());
                request.setFollowuserid(defaultFragment.stylist.getId());
                if (btn_follower.isSelected())
                    request.setFollow(false);
                else
                    request.setFollow(true);

                requestUserFollow(request);
                break;
        }
    }

    private Dialog dialog;
    private void requestPostDetails(CommonFindRequest request, boolean isShowDialog) {
        if (isShowDialog)
            dialog = DialogUtil.showDialog(getContext());
        Log.d(TAG, "LAMBDA: Sending Data");
        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());

        Gson gson = new Gson();
        Log.d(TAG, "request to json: " + gson.toJson(request));

        final LambdaInterface myInterface = factory.build(LambdaInterface.class);

        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<CommonFindRequest, Void, PostDetailsResponse>() {

            @Override
            protected PostDetailsResponse doInBackground(CommonFindRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.PostDetails(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(PostDetailsResponse result) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    return;
                } else {
                    Log.d(TAG, "LAMBDA: Response from request have data" + result.getMessage());

                    try {
                        updateComment(result);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }.execute(request);
    }


    private void requestUserFollow(UserFollowRequest request) {

        Log.d(TAG, "LAMBDA: Sending Data");
        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());
        final LambdaInterface myInterface = factory.build(LambdaInterface.class);
        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<UserFollowRequest, Void, CommonResponse>() {

            @Override
            protected CommonResponse doInBackground(UserFollowRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.UserFollow(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(CommonResponse result) {
                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    return;
                } else {
                    Log.d(TAG, "LAMBDA: Response from request have data");
                    Log.d(TAG, "response: " + result.getValid());

                    if (result.getValid().equals("Y")) {
                        if (btn_follower.isSelected())
                            btn_follower.setSelected(false);
                        else
                            btn_follower.setSelected(true);
                    }
                }
            }
        }.execute(request);
    }


    private void requestUserLike(UserLikePostRequest request, boolean isShowDialog) {

        Log.d(TAG, "LAMBDA: Sending Data");
        // 1. Setup a provider to allow posting to Amazon Lambda
        // 2. Setup a LambdaInvoker Factory w/ provider data
        final LambdaInvokerFactory factory = new LambdaInvokerFactory(
                getContext(),
                CognitoSyncClientManager.REGION,
                CognitoSyncClientManager.getCredentialsProvider());
        final LambdaInterface myInterface = factory.build(LambdaInterface.class);
        // 3. Send the data to the "digitsLogin" function on Amazon Lambda.
        // Note: Make sure it is done in background, not in main thread.
        new AsyncTask<UserLikePostRequest, Void, CommonResponse>() {

            @Override
            protected CommonResponse doInBackground(UserLikePostRequest... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    Log.d(TAG, "LAMBDA: Attempting to send data");
                    return myInterface.UserLikesPost(params[0]);
                } catch (Exception lfe) {
                    lfe.printStackTrace();
                    Log.e("amazon", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(CommonResponse result) {

                if (result == null) {
                    Log.d(TAG, "LAMBDA: Response from request is null");
                    return;
                } else {
                    Log.d(TAG, "LAMBDA: Response from request have data");
                    Log.d(TAG, "response: " + result.getValid());

                    if (result.getValid().equals("Y")) {
                        TextViewPlus number_likes = (TextViewPlus) rootView.findViewById(R.id.number_likes);
                        if (like_unlike.isSelected()) {
                            like_unlike.setSelected(false);
                            if (defaultFragment.post.getCountof().getLikes() > 0) {
                                int count = defaultFragment.post.getCountof().getLikes() - 1;
                                defaultFragment.post.getCountof().setLikes(count);
                                number_likes.setText(getString(R.string.number_like, count));
                                updateLikeRootListNewFeed(defaultFragment.post.getId(), count, false);
                            }
                        } else {
                            like_unlike.setSelected(true);
                            int count = defaultFragment.post.getCountof().getLikes() + 1;
                            defaultFragment.post.getCountof().setLikes(count);
                            number_likes.setText(getString(R.string.number_like, count));
                            updateLikeRootListNewFeed(defaultFragment.post.getId(), count, true);
                        }
                    }
                }
            }
        }.execute(request);
    }


    /**
     * update count like
     *
     * @param postID
     * @param countLike
     */
    private void updateLikeRootListNewFeed(String postID, int countLike, boolean isLike) {

        for (int i = 0; i < defaultFragment.itemFeedList.size(); i++) {
            if (defaultFragment.itemFeedList.get(i).getId().equals(postID)) {
                defaultFragment.itemFeedList.get(i).getCountof().setLikes(countLike);
                defaultFragment.itemFeedList.get(i).setCommentliked(isLike);
            }
        }
    }

    /**
     * update comment count and comment text
     *
     * @param postID
     * @param result
     */
    private void updateCommentAndCountComment(String postID, PostDetailsResponse result) {
        for (int i = 0; i < defaultFragment.itemFeedList.size(); i++) {
            if (defaultFragment.itemFeedList.get(i).getId().equals(postID)) {
                defaultFragment.itemFeedList.get(i).getCountof().setComments(result.getPosts().getCountof().getComments());
                List<Comment> comments = new ArrayList<>();

                if (result.getPostComments().size() > 2) {
                    for (int j = 0; j < 2; j++) {
                        Comment comment1 = new Comment();
                        comment1.setStylist(defaultFragment.stylist);
                        comment1.setNickname(defaultFragment.stylist.getNickname());
                        comment1.setComment(result.getPostComments().get(j).getComment());
                        comment1.setPostid(postID);
                        comment1.setUserid(defaultFragment.stylist.getUserid());
                        comments.add(comment1);
                    }
                } else {
                    for (int j = 0; j < result.getPostComments().size(); j++) {
                        Comment comment1 = new Comment();
                        comment1.setStylist(defaultFragment.stylist);
                        comment1.setNickname(defaultFragment.stylist.getNickname());
                        comment1.setComment(result.getPostComments().get(j).getComment());
                        comment1.setPostid(postID);
                        comment1.setUserid(defaultFragment.stylist.getUserid());
                        comments.add(comment1);
                    }
                }
                defaultFragment.itemFeedList.get(i).setComments(comments);
            }
        }
    }

    private void updateComment(PostDetailsResponse result) {

        updateCommentAndCountComment(defaultFragment.post.getId(), result);

        postComments = result.getPostComments();
        TextViewPlus allcomment = (TextViewPlus) rootView.findViewById(R.id.allcomment);
        ViewComment view = (ViewComment) rootView.findViewById(R.id.viewcomment);
        view.removeAllViews();
        if (result.getPostComments().size() > 0) {
            allcomment.setText(getString(R.string.view_all_number_comment, result.getPostComments().size()));
            if (result.getPostComments().size() >= 2) {
                allcomment.setVisibility(View.VISIBLE);
                for (int i = 0; i < 2; i++) {
                    view.AddComment(result.getPostComments().get(i).getNickname(), result.getPostComments().get(i).getComment());
                }
            } else {
                for (int i = 0; i < result.getPostComments().size(); i++) {
                    view.AddComment(result.getPostComments().get(i).getNickname(), result.getPostComments().get(i).getComment());
                }
                allcomment.setVisibility(View.GONE);
            }
        } else {
            allcomment.setText(getString(R.string.have_no_comment));
        }

        if (result.getUserpostlike() != null) {
            like_unlike.setSelected(true);
        } else
            like_unlike.setSelected(false);

    }

    private GetlaydDefaultFragment defaultFragment;

    public void setDefaultFragment(GetlaydDefaultFragment iDefaultFragment) {
        defaultFragment = iDefaultFragment;
    }
}
