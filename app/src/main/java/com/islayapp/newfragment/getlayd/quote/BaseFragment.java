package com.islayapp.newfragment.getlayd.quote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.islayapp.R;
import com.islayapp.customview.CircleColorView;

/**
 * Created by maxo on 3/28/16.
 */
public class BaseFragment extends Fragment {
    protected View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected void initBottombarCircleSelect(int numSelect) {
        switch (numSelect) {
            case 1:
                CircleColorView circle11 = (CircleColorView) rootView.findViewById(R.id.circle1);
                circle11.setColor("#000000");
                break;
            case 2:
                CircleColorView circle21 = (CircleColorView) rootView.findViewById(R.id.circle1);
                CircleColorView circle22 = (CircleColorView) rootView.findViewById(R.id.circle2);
                circle21.setColor("#000000");
                circle22.setColor("#000000");

                View line21Right = (View) rootView.findViewById(R.id.line1Right);
                View line22Left = (View) rootView.findViewById(R.id.line2Left);

                line21Right.setBackgroundResource(R.color.black);
                line22Left.setBackgroundResource(R.color.black);
                break;
            case 3:
                CircleColorView circle31 = (CircleColorView) rootView.findViewById(R.id.circle1);
                CircleColorView circle32 = (CircleColorView) rootView.findViewById(R.id.circle2);
                CircleColorView circle33 = (CircleColorView) rootView.findViewById(R.id.circle3);
                circle31.setColor("#000000");
                circle32.setColor("#000000");
                circle33.setColor("#000000");

                View line31Right = (View) rootView.findViewById(R.id.line1Right);
                View line32Left = (View) rootView.findViewById(R.id.line2Left);
                View line32Right = (View) rootView.findViewById(R.id.line2Right);
                View line33Left = (View) rootView.findViewById(R.id.line3Left);

                line31Right.setBackgroundResource(R.color.black);
                line32Left.setBackgroundResource(R.color.black);
                line32Right.setBackgroundResource(R.color.black);
                line33Left.setBackgroundResource(R.color.black);

                break;
            case 4:
                CircleColorView circle41 = (CircleColorView) rootView.findViewById(R.id.circle1);
                CircleColorView circle42 = (CircleColorView) rootView.findViewById(R.id.circle2);
                CircleColorView circle43 = (CircleColorView) rootView.findViewById(R.id.circle3);
                CircleColorView circle44 = (CircleColorView) rootView.findViewById(R.id.circle4);
                circle41.setColor("#000000");
                circle42.setColor("#000000");
                circle43.setColor("#000000");
                circle44.setColor("#000000");
                View line41Right = (View) rootView.findViewById(R.id.line1Right);
                View line42Left = (View) rootView.findViewById(R.id.line2Left);
                View line42Right = (View) rootView.findViewById(R.id.line2Right);
                View line43Left = (View) rootView.findViewById(R.id.line3Left);
                View line43Right = (View) rootView.findViewById(R.id.line3Right);
                View line44Left = (View) rootView.findViewById(R.id.line4Left);
                line41Right.setBackgroundResource(R.color.black);
                line42Left.setBackgroundResource(R.color.black);
                line42Right.setBackgroundResource(R.color.black);
                line43Left.setBackgroundResource(R.color.black);
                line43Right.setBackgroundResource(R.color.black);
                line44Left.setBackgroundResource(R.color.black);

                break;
            case 5:

                CircleColorView circle51 = (CircleColorView) rootView.findViewById(R.id.circle1);
                CircleColorView circle52 = (CircleColorView) rootView.findViewById(R.id.circle2);
                CircleColorView circle53 = (CircleColorView) rootView.findViewById(R.id.circle3);
                CircleColorView circle54 = (CircleColorView) rootView.findViewById(R.id.circle4);
                CircleColorView circle55 = (CircleColorView) rootView.findViewById(R.id.circle5);
                circle51.setColor("#000000");
                circle52.setColor("#000000");
                circle53.setColor("#000000");
                circle54.setColor("#000000");
                circle55.setColor("#000000");
                View line51Right = (View) rootView.findViewById(R.id.line1Right);
                View line52Left = (View) rootView.findViewById(R.id.line2Left);
                View line52Right = (View) rootView.findViewById(R.id.line2Right);
                View line53Left = (View) rootView.findViewById(R.id.line3Left);
                View line53Right = (View) rootView.findViewById(R.id.line3Right);
                View line54Left = (View) rootView.findViewById(R.id.line4Left);
                View line54Right = (View) rootView.findViewById(R.id.line4Right);
                View line55Left = (View) rootView.findViewById(R.id.line5Left);
                line51Right.setBackgroundResource(R.color.black);
                line52Left.setBackgroundResource(R.color.black);
                line52Right.setBackgroundResource(R.color.black);
                line53Left.setBackgroundResource(R.color.black);
                line53Right.setBackgroundResource(R.color.black);
                line54Left.setBackgroundResource(R.color.black);
                line54Right.setBackgroundResource(R.color.black);
                line55Left.setBackgroundResource(R.color.black);

                break;
            case 6:

                CircleColorView circle61 = (CircleColorView) rootView.findViewById(R.id.circle1);
                CircleColorView circle62 = (CircleColorView) rootView.findViewById(R.id.circle2);
                CircleColorView circle63 = (CircleColorView) rootView.findViewById(R.id.circle3);
                CircleColorView circle64 = (CircleColorView) rootView.findViewById(R.id.circle4);
                CircleColorView circle65 = (CircleColorView) rootView.findViewById(R.id.circle5);
                CircleColorView circle66 = (CircleColorView) rootView.findViewById(R.id.circle6);
                circle61.setColor("#000000");
                circle62.setColor("#000000");
                circle63.setColor("#000000");
                circle64.setColor("#000000");
                circle65.setColor("#000000");
                circle66.setColor("#000000");
                View line61Right = (View) rootView.findViewById(R.id.line1Right);
                View line62Left = (View) rootView.findViewById(R.id.line2Left);
                View line62Right = (View) rootView.findViewById(R.id.line2Right);
                View line63Left = (View) rootView.findViewById(R.id.line3Left);
                View line63Right = (View) rootView.findViewById(R.id.line3Right);
                View line64Left = (View) rootView.findViewById(R.id.line4Left);
                View line64Right = (View) rootView.findViewById(R.id.line4Right);
                View line65Left = (View) rootView.findViewById(R.id.line5Left);
                View line65Right = (View) rootView.findViewById(R.id.line5Right);
                View line66Left = (View) rootView.findViewById(R.id.line6Left);
                line61Right.setBackgroundResource(R.color.black);
                line62Left.setBackgroundResource(R.color.black);
                line62Right.setBackgroundResource(R.color.black);
                line63Left.setBackgroundResource(R.color.black);
                line63Right.setBackgroundResource(R.color.black);
                line64Left.setBackgroundResource(R.color.black);
                line64Right.setBackgroundResource(R.color.black);
                line65Left.setBackgroundResource(R.color.black);
                line65Right.setBackgroundResource(R.color.black);
                line66Left.setBackgroundResource(R.color.black);
                break;
        }
    }

    protected  void resetBottombarCircleSelect(){
        CircleColorView circle61 = (CircleColorView) rootView.findViewById(R.id.circle1);
        CircleColorView circle62 = (CircleColorView) rootView.findViewById(R.id.circle2);
        CircleColorView circle63 = (CircleColorView) rootView.findViewById(R.id.circle3);
        CircleColorView circle64 = (CircleColorView) rootView.findViewById(R.id.circle4);
        CircleColorView circle65 = (CircleColorView) rootView.findViewById(R.id.circle5);
        CircleColorView circle66 = (CircleColorView) rootView.findViewById(R.id.circle6);
        circle61.setColor("#777777");
        circle62.setColor("#777777");
        circle63.setColor("#777777");
        circle64.setColor("#777777");
        circle65.setColor("#777777");
        circle66.setColor("#777777");
        View line61Right = (View) rootView.findViewById(R.id.line1Right);
        View line62Left = (View) rootView.findViewById(R.id.line2Left);
        View line62Right = (View) rootView.findViewById(R.id.line2Right);
        View line63Left = (View) rootView.findViewById(R.id.line3Left);
        View line63Right = (View) rootView.findViewById(R.id.line3Right);
        View line64Left = (View) rootView.findViewById(R.id.line4Left);
        View line64Right = (View) rootView.findViewById(R.id.line4Right);
        View line65Left = (View) rootView.findViewById(R.id.line5Left);
        View line65Right = (View) rootView.findViewById(R.id.line5Right);
        View line66Left = (View) rootView.findViewById(R.id.line6Left);
        line61Right.setBackgroundResource(R.color.gray);
        line62Left.setBackgroundResource(R.color.gray);
        line62Right.setBackgroundResource(R.color.gray);
        line63Left.setBackgroundResource(R.color.gray);
        line63Right.setBackgroundResource(R.color.gray);
        line64Left.setBackgroundResource(R.color.gray);
        line64Right.setBackgroundResource(R.color.gray);
        line65Left.setBackgroundResource(R.color.gray);
        line65Right.setBackgroundResource(R.color.gray);
        line66Left.setBackgroundResource(R.color.gray);
    }

}
