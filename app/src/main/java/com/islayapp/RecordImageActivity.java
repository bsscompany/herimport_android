package com.islayapp;

import java.io.IOException;

import com.islayapp.models.shop.CameraPreview;
import com.islayapp.models.shop.GlobalVariable;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class RecordImageActivity extends Activity implements OnClickListener {
	
	Camera 			mCamera;
	CameraPreview 	mPreview;
	MediaRecorder 	mediaRecorder;
	LinearLayout 	cameraPreview;
	
	Button			captureButton;
	Button			flashButton;
	Button			switchCameraButton;
	
	boolean			flashFlag = false;
	boolean 		cameraFront = false;
	boolean			recordedFlag = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_record_profileimage);
		
		captureButton = (Button)findViewById(R.id.RecordButton);
		captureButton.setOnClickListener(this);
						
		cameraPreview = (LinearLayout) findViewById(R.id.camera_preview);
		mPreview = new CameraPreview(RecordImageActivity.this, mCamera);
		cameraPreview.addView(mPreview);
		
		Button btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		Typeface font = Typeface.createFromAsset(getAssets(), "FuturaLT.ttf");  
		TextView txtTitle = (TextView) findViewById(R.id.textView1);
		txtTitle.setTypeface(font);
	}
	
	public void onClickBack(View v) {		
		Intent returnIntent = new Intent();
		setResult(RESULT_CANCELED, returnIntent);
		finish();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if ( v == captureButton ) {
			try {
				

				Resources resources = getResources();
			    Configuration config = resources.getConfiguration();
			    DisplayMetrics dm = resources.getDisplayMetrics();
				int width = (int)(config.screenWidthDp * dm.density);
				int height = width * dm.heightPixels / dm.widthPixels;
				
				Bitmap bmp = mPreview.getBitmap();
				
				if (height == bmp.getWidth())
					height = bmp.getWidth() - (int) (140 * getResources().getDisplayMetrics().density);
				else
					height = bmp.getWidth();
				bmp = Bitmap.createScaledBitmap(bmp, height / 2, bmp.getHeight() /2, true);
		        Matrix matrix = new Matrix();
		        if (cameraFront) {
		        	matrix.preRotate(270);
				} else {
					matrix.preRotate(90);
				}	
		        
		        Bitmap adjustedBitmap = Bitmap.createBitmap( bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
		        //adjustedBitmap.setConfig(Bitmap.Config.ARGB_8888);
		        /*adjustedBitmap.setHasAlpha(true); 
		       
		        Log.e("Wang", "" + bmp.getWidth() + " : " + bmp.getHeight());
		        Bitmap maskBmp = BitmapFactory.decodeResource(getResources(), R.raw.cameraimagemask1);
		        maskBmp = Bitmap.createScaledBitmap(maskBmp, bmp.getHeight(), bmp.getWidth(), true);
		        
		        Canvas canvas = new Canvas(adjustedBitmap);
		        Paint paint = new Paint();
		        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
		        canvas.drawBitmap(maskBmp, 0, 0, paint);*/
		        // We do not need the mask bitmap anymore.
		        //maskBmp.recycle();
		        
				GlobalVariable.userPhoto = adjustedBitmap;
				
				GlobalVariable.writeUserPhoto();
				finish();
			} catch (IOException e) {
				
			}
		}
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		
	}
		
	@SuppressLint("NewApi")
	private int findFrontFacingCamera() {
		int cameraId = -1;
		// Search for the front facing camera
		int numberOfCameras = Camera.getNumberOfCameras();
		for (int i = 0; i < numberOfCameras; i++) {
			CameraInfo info = new CameraInfo();
			Camera.getCameraInfo(i, info);
			if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
				cameraId = i;
				cameraFront = true;
				break;
			}
		}
		return cameraId;
	}

	private int findBackFacingCamera() {
		int cameraId = -1;
		// Search for the back facing camera
		// get the number of cameras
		int numberOfCameras = Camera.getNumberOfCameras();
		// for every camera check
		for (int i = 0; i < numberOfCameras; i++) {
			CameraInfo info = new CameraInfo();
			Camera.getCameraInfo(i, info);
			if (info.facing == CameraInfo.CAMERA_FACING_BACK) {
				cameraId = i;
				cameraFront = false;
				break;
			}
		}
		return cameraId;
	}

	public void onResume() {
		super.onResume();
		if (!hasCamera(RecordImageActivity.this)) {
			Toast toast = Toast.makeText(RecordImageActivity.this, "Sorry, your phone does not have a camera!", Toast.LENGTH_LONG);
			toast.show();
			finish();
		}
		if (mCamera == null) {
			// if the front facing camera does not exist
			if (findFrontFacingCamera() < 0) {
				Toast.makeText(this, "No front facing camera found.", Toast.LENGTH_LONG).show();
				switchCameraButton.setVisibility(View.GONE);
			}
			mCamera = Camera.open(findFrontFacingCamera());	
			mPreview.refreshCamera(mCamera);
		}
	}
	
	public void chooseCamera() {
		// if the camera preview is the front
		if (cameraFront) {
			int cameraId = findBackFacingCamera();
			if (cameraId >= 0) {
				mCamera = Camera.open(cameraId);
				mPreview.refreshCamera(mCamera);
			}
		} else {
			int cameraId = findFrontFacingCamera();
			if (cameraId >= 0) {
				mCamera = Camera.open(cameraId);
				mPreview.refreshCamera(mCamera);
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		// when on Pause, release camera in order to be used from other
		// applications
		releaseCamera();
	}

	private boolean hasCamera(Context context) {
		// check if the device has camera
		if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			return true;
		} else {
			return false;
		}
	}
	
	private void releaseCamera() {
		// stop and release camera
		if (mCamera != null) {
			mCamera.release();
			mCamera = null;
		}
	}
}
