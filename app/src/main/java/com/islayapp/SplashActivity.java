package com.islayapp;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;
import android.widget.VideoView;

import com.amazonaws.CognitoSyncClientManager;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.core.APIService;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.islayapp.models.awsmodels.FaceBook;
import com.islayapp.models.awsmodels.Meta;
import com.islayapp.models.awsmodels.Users;
import com.islayapp.models.awsmodels.user.ResponseUsers;
import com.islayapp.util.DialogUtil;
import com.islayapp.util.JSONUtil;
import com.islayapp.util.NetworkConnectionUtil;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Session;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.islayapp.util.LoginResultType.ERROR_FB_Email_missing;

public class SplashActivity extends Activity {
    private static final String TAG = SplashActivity.class.getName();

    private CallbackManager callbackManager;
    private Dialog progress;
    private VideoView vv;
//    private Dataset datasetUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_splash);

        CognitoSyncClientManager.init(this);


        // Add code to print out the key hash
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.islayapp",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }

        vv = (VideoView) this.findViewById(R.id.bgView);
        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });


        // reser cart ID
        PreferenceHelpers.setPreference(this, "quoteId", "");

        Button btnFacebook = (Button) findViewById(R.id.btnFacebook);

        //If access token is already here, set fb session
        final AccessToken fbAccessToken = AccessToken.getCurrentAccessToken();
        if (fbAccessToken != null) {
            btnFacebook.setVisibility(View.GONE);
            progress = DialogUtil.showDialog(SplashActivity.this);

            // Facebook Email address
            GraphRequest request = GraphRequest.newMeRequest(
                    fbAccessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {

                            Log.v("GraphRequest Response ", response.toString());
                            new ParserData(SplashActivity.this, fbAccessToken).execute(object);
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, friends, first_name, last_name, gender, link, verified, picture.type(large), email");
            request.setParameters(parameters);
            request.executeAsync();

        }

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        Log.d("dd", "facebook login onSuccess");

                        progress = DialogUtil.showDialog(SplashActivity.this);


                        // Facebook Email address
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("GraphRequest Response ", response.toString());
                                        new ParserData(SplashActivity.this, loginResult.getAccessToken()).execute(object);
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, friends, first_name, last_name, gender, link, verified, picture.type(large), email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.e("dd", "facebook login canceled");
                        if (progress != null)
                            progress.dismiss();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(getBaseContext(), "Facebook login error!", Toast.LENGTH_SHORT).show();
                        if (progress != null)
                            progress.dismiss();
                        // App code
                        Log.e("dd", "facebook login failed error" + exception.toString());
                    }
                });
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(SplashActivity.this, Arrays.asList("public_profile"));

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (vv != null) {
            Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bg_video);
            vv.setVideoURI(uri);
            vv.start();
        }
    }

    private SimpleTarget target = new SimpleTarget<GlideBitmapDrawable>() {
        @Override
        public void onResourceReady(GlideBitmapDrawable bitmap, GlideAnimation glideAnimation) {
            // do something with the bitmap

            // for demonstration purposes, let's just set it to an ImageView
            View root = findViewById(R.id.container);
//            Drawable d = new BitmapDrawable(getResources(), bitmap);
            root.setBackground(bitmap);
        }
    };

    private void setFacebookSession(AccessToken accessToken) {
//        Log.i(TAG, "facebook token: " + accessToken.getToken());
        CognitoSyncClientManager.getCredentialsProvider().clear();
        CognitoSyncClientManager.addLogins("graph.facebook.com", accessToken.getToken());
        CognitoSyncClientManager.getCredentialsProvider().refresh();

//        Log.i(TAG, "getIdentityId(): " + CognitoSyncClientManager.getCredentialsProvider().getIdentityId());
//        Log.i(TAG, "getIdentityPoolId(): " + CognitoSyncClientManager.getCredentialsProvider().getIdentityPoolId());

    }


    private class ParserData extends AsyncTask<JSONObject, Void, Users> {
        private Context context;
        private AccessToken fbAccessToken;

        public ParserData(Context mContext, AccessToken fbAccessToken) {
            this.fbAccessToken = fbAccessToken;
            context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Users doInBackground(JSONObject... jsonObjects) {

            setFacebookSession(fbAccessToken);

            JSONObject object = jsonObjects[0];
            Users users = new Users();
            users.setFirstname(JSONUtil.stringTryGetValue(object, "first_name"));
            users.setLastname(JSONUtil.stringTryGetValue(object, "last_name"));

            String email = JSONUtil.stringTryGetValue(object, "email");
            if (email.equals(""))
                users.setLoginResultType(ERROR_FB_Email_missing);
            users.setEmail(email);
            users.setTimestamp(String.valueOf(System.currentTimeMillis()));

            String gender = JSONUtil.stringTryGetValue(object, "gender");
            if (gender.equals("male"))
                gender = "M";
            else if (gender.equals("female"))
                gender = "F";
            else
                gender = "U";

            Meta meta = new Meta();
            meta.setGender(gender);
            meta.setCognitoidentityid(CognitoSyncClientManager.getCredentialsProvider().getIdentityId());
            Log.d(TAG, meta.getCognitoidentityid());

            FaceBook faceBook = new FaceBook();
            faceBook.setId(JSONUtil.stringTryGetValue(object, "id"));

            if (JSONUtil.jsonObjectTryGetValue(object, "friends") != null &&
                    JSONUtil.jsonObjectTryGetValue(JSONUtil.jsonObjectTryGetValue(object, "friends"), "summary") != null)
                faceBook.setFriendcount(JSONUtil.intTryGetValue(JSONUtil.jsonObjectTryGetValue(JSONUtil.jsonObjectTryGetValue(object, "friends"), "summary"), "total_count"));

            if (JSONUtil.jsonObjectTryGetValue(object, "picture") != null &&
                    JSONUtil.jsonObjectTryGetValue(JSONUtil.jsonObjectTryGetValue(object, "picture"), "data") != null &&
                    JSONUtil.stringTryGetValue(JSONUtil.jsonObjectTryGetValue(JSONUtil.jsonObjectTryGetValue(object, "picture"), "data"), "url") != null)
                faceBook.setPicurl(JSONUtil.stringTryGetValue(JSONUtil.jsonObjectTryGetValue(JSONUtil.jsonObjectTryGetValue(object, "picture"), "data"), "url"));

            faceBook.setAuthtoken(fbAccessToken.getToken());
            String verified = JSONUtil.stringTryGetValue(object, "verified");

            if (verified.equals("1"))
                verified = "Y";
            else
                verified = "N";

            faceBook.setVerified(verified);
            users.setMeta(meta);
            users.setFacebook(faceBook);


            return users;
        }

        @Override
        protected void onPostExecute(Users result) {

            switch (result.getLoginResultType()) {
//                case ERROR_FB_Email_missing:
//                    Toast.makeText(getBaseContext(), "Facebook email missing!", Toast.LENGTH_SHORT).show();
//                    if (progress != null)
//                        progress.dismiss();
//                    return;
                case ERROR_FB_Declined:
                    Toast.makeText(getBaseContext(), "Facebook Declined!", Toast.LENGTH_SHORT).show();
                    if (progress != null)
                        progress.dismiss();
                    return;
                case ERROR_FB_Error:
                    Toast.makeText(getBaseContext(), "Facebook Error!", Toast.LENGTH_SHORT).show();
                    if (progress != null)
                        progress.dismiss();
                    return;
                case ERROR_FB_Graph_Data_Missing:
                    Toast.makeText(getBaseContext(), "Facebook data missing!", Toast.LENGTH_SHORT).show();
                    if (progress != null)
                        progress.dismiss();
                    return;
            }

            requestRegister(result.toJSON());
        }
    }

    /**
     * @param user
     */

    private void requestRegister(JSONObject user) {


        Log.d(TAG, "get list hostest product" + user.toString());
        APIService service = NetworkConnectionUtil.createApiService(this);

        String string_user = user.toString();
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), string_user);

        Call<ResponseUsers> response = service.requestUserRegister(body);

        response.enqueue(new Callback<ResponseUsers>() {
            @Override
            public void onResponse(Call<ResponseUsers> call, Response<ResponseUsers> response) {
                if (progress != null)
                    progress.dismiss();
                try {
                    ResponseUsers responseUsers = response.body();
                    if (responseUsers.getValid().equals("Y")) {
//
                        // setuser to session
                        Session.setUsers(responseUsers);
                        Session.setCurrentPoint(responseUsers.getAvailablepoints());

                        if (responseUsers.getUser().getIsbanned().equals("Y")) {
                            Toast.makeText(getBaseContext(), "Banned!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
//                            if (responseUsers.getUser().getEmail() != null && !responseUsers.getUser().getEmail().equals("")) {
//                                Toast.makeText(getBaseContext(), "Email missing!", Toast.LENGTH_SHORT).show();
//                                return;
//                            }

//                            Intent intent = new Intent(SplashActivity.this, ListRecordsActivity.class);

                            boolean isFirstTime = PreferenceHelpers.firstTimeRunning(getBaseContext());
                            if (isFirstTime && responseUsers.getNewUser() != null && responseUsers.getNewUser().equals("Y")) {
                                Intent intent = new Intent(SplashActivity.this, VerifyPhoneActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(SplashActivity.this, NewHomeActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    } else {
                        Toast.makeText(getBaseContext(), "Login failed!", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(), "Login failed!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseUsers> call, Throwable t) {
                Log.d(TAG, "content error " + t.toString());
                if (progress != null)
                    progress.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }


}
