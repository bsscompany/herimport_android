package com.islayapp;

/**
 * Created by maxo on 6/2/16.
 */

import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.AWSCreateEndpointTask;
import com.amazonaws.CognitoSyncClientManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.islayapp.util.Constants;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Session;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        PreferenceHelpers.setPreference(this, Constants.SENT_TOKEN_TO_SERVER, false);

        FirebaseInstanceId.getInstance().getId();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        PreferenceHelpers.setPreference(this, Constants.GCM_TOKEN, refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        if (!TextUtils.isEmpty(token) && Session.getUsers()!=null&& Session.getUsers().getUser()!=null && Session.getUsers().getUser().getId() != null) {
            new AWSCreateEndpointTask(this).execute(
                    CognitoSyncClientManager.ARN_PUSH,
                    token,
                    Session.getUsers().getUser().getId());
        }
    }
}