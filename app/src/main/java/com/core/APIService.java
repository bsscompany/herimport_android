package com.core;

import com.amazonaws.models.CallPhoneResponse;
import com.amazonaws.models.CheckNicknameResponse;
import com.amazonaws.models.EmailStatusResponse;
import com.amazonaws.models.NewFeedResponse;
import com.amazonaws.models.OrderIdResponse;
import com.amazonaws.models.RedeemPointsResponse;
import com.amazonaws.models.SendEmailResponse;
import com.amazonaws.models.StoresLocatorResponse;
import com.amazonaws.models.UpdateNicknameResponse;
import com.amazonaws.models.googlemap.AddressResponse;
import com.amazonaws.models.googlemap.ResponseGetLocationByAddress;
import com.islayapp.models.awsmodels.AddressResultByLocation;
import com.islayapp.models.awsmodels.SendPhoneActionSmsResponse;
import com.islayapp.models.awsmodels.user.JsonResource;
import com.islayapp.models.awsmodels.user.ResponseUsers;
import com.islayapp.models.shop.AddToCart;
import com.islayapp.models.shop.CountryModel;
import com.islayapp.models.shop.GetCartModel;
import com.islayapp.models.shop.HostestProduct;
import com.islayapp.models.shop.RemoveItemCart;
import com.islayapp.models.shop.ResponseAddress;
import com.islayapp.models.shop.SaveObjectResult;
import com.islayapp.models.shop.SaveShippingResult;
import com.islayapp.models.shop.ShippingMethod;
import com.islayapp.models.shop.StateModel;
import com.islayapp.models.shop.SubmitOrder;
import com.islayapp.models.shop.SuperProductDetails;
import com.islayapp.models.shop.UpdateItemCart;
import com.islayapp.models.shop.order.OrderReview;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by maxo on 4/3/16.
 */
public interface APIService {

    @GET("/mobileapp/index/getHostestProducts")
    Call<HostestProduct> getHostlesProduct();

    @GET("/mobileapp/index/getAccessoriesProducts")
    Call<HostestProduct> getAccessoriesProduct();

    @GET("/mobileapp/index/getProductInfoById")
    Call<SuperProductDetails> getProductDetails(@Query("idProduct") String id);

    @FormUrlEncoded
    @POST("mobileapp/cart/addToCart")
    Call<AddToCart> hostestAddToCart(@FieldMap Map<String, String> fields);

    @POST("/mobileapp/cart/getCartDetail")
    Call<GetCartModel> getCartContent(@Query("quoteId") String quoteId);

    @POST("/mobileapp/cart/removeItem")
    Call<RemoveItemCart> removeItemInCart(@Query("quoteId") String quoteId, @Query("itemId") String itemId);

    @POST("/mobileapp/cart/updateCart/")
    Call<UpdateItemCart> updateItemInCart(@Query("quoteId") String quoteId, @Query("itemId") String itemId, @Query("qty") String qty);

    @GET("/mobileapp/checkout/getListCountryData")
    Call<CountryModel> getListCountryData();

    @GET("/mobileapp/checkout/getListStateData")
    Call<StateModel> getListState(@Query("countryCode") String countryCode);

    @GET("maps/api/geocode/json")
    Call<ResponseAddress> requestGetAddressByZipcode(@Query("address") String zipcode,@Query("region") String countryCode);

    @FormUrlEncoded
    @POST("mobileapp/checkout/saveBillingAddress")
    Call<SaveObjectResult> saveBillingAddress(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST("mobileapp/checkout/saveShippingAddress")
    Call<SaveObjectResult> saveShippingddress(@FieldMap Map<String, String> fields);

    @POST("mobileapp/checkout/getShippingMethod")
    Call<ShippingMethod> getShippingMethod(@Query("quoteId") String quoteId);

    @POST("mobileapp/checkout/saveShippingMethod")
    Call<SaveShippingResult> saveShippingMethod(@Query("quoteId") String quoteId, @Query("code") String code);

    @FormUrlEncoded
    @POST("mobileapp/checkout/savePaymentMethod")
    Call<SaveObjectResult> savePaymentMethod(@FieldMap Map<String, String> fields);

    @POST("mobileapp/checkout/getOrderReview")
    Call<OrderReview> getOrderReview(@Query("quoteId") String quoteId);

    @FormUrlEncoded
    @POST("mobileapp/checkout/submitOrder")
    Call<SubmitOrder> submitOrder(@FieldMap Map<String, String> fields);

    @POST("mobileapp/checkout/getOrderId")
    Call<OrderIdResponse> getOrderID(@Query("quoteId") String quoteId);

    @POST("mobileapp/checkout/applyCoupon")
    Call<SaveObjectResult> applyCoupon(@Query("quoteId") String quoteId, @Query("couponCode") String couponCode);

    @POST("mobileapp/checkout/applyCoupon")
    Call<SaveObjectResult> removeCoupon(@Query("quoteId") String quoteId, @Query("couponCode") String couponCode);

    @Headers("Content-Type: application/json")
    @POST("users/authenticate")
    Call<ResponseUsers> requestUserRegister(@Body RequestBody body);

    @Headers("Content-Type: application/json")
    @POST("users/redeempoints")
    Call<RedeemPointsResponse> requestRedeemPoint(@Header("Authorization") String string, @Body RequestBody body);

    @GET("maps/api/place/autocomplete/json")
    Call<AddressResponse> searchAutocompletedAddress(@Query("key") String key, @Query("input") String input);

    @GET("maps/api/geocode/json")
    Call<AddressResultByLocation> searchAddressByLocation(@Query("latlng") String latlng);

    @Headers("Content-Type: application/json")
    @POST("users/stylistfeed")
    Call<NewFeedResponse> requestNewFeed(@Header("Authorization") String string, @Body RequestBody body);

    @GET
    Call<JsonResource> getResource(@Url String url);

    //======================================================  profile =====================================================
    @GET("tools/nicknameavailable/{id}")
    Call<CheckNicknameResponse> requestCheckNickname(@Header("Authorization") String string, @Path("id") String nickname);

    // update nickname
    @GET("tools/update/updatenickname/{id}")
    Call<UpdateNicknameResponse> requestUpdateNickname(@Header("Authorization") String string, @Path("id") String nickname);

    // send phone number
    @GET("tools/twilio/sms/verify/{phone}")
    Call<SendPhoneActionSmsResponse> requestSendPhoneVerify(@Header("Authorization") String string, @Path("phone") String phone);

    @GET("tools/update/phoneverified")
    Call<SendPhoneActionSmsResponse> requestSendhoneVerified(@Header("Authorization") String string);

    @GET("users/signupcomplete")
    Call<SendPhoneActionSmsResponse> requestSignupcomplete(@Header("Authorization") String string);


    @GET("tools/twilio/call/request/{phone}")
    Call<CallPhoneResponse> requestCallToPhone(@Header("Authorization") String string, @Path("phone") String phone);

    // send email
    @GET("tools/email/verify/{email}")
    Call<SendEmailResponse> requestSendEmail(@Header("Authorization") String string, @Path("email") String email);

    // verify email status
    @GET("tools/email/verifystatus/{requestID}")
    Call<EmailStatusResponse> requestVerifyEmailStatus(@Header("Authorization") String string, @Path("requestID") String requestID);

    // ================================================= general ==================================================

    @GET("stores/list")
    Call<StoresLocatorResponse> requestStoresLocator();

    @Headers("Content-Type: application/json")
    @POST("/stylists/apply")
    Call<ResponseBody> requestApplyToStylist(@Header("Authorization") String string, @Body RequestBody body);

    @GET("/maps/api/geocode/json")
    Call<ResponseGetLocationByAddress> requestGetLocationByAddress(@Query("address") String address);

}