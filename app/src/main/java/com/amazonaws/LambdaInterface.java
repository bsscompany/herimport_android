package com.amazonaws;

import com.amazonaws.models.CommonFindRequest;
import com.amazonaws.models.CommonRequest;
import com.amazonaws.models.CommonResponse;
import com.amazonaws.models.FindStylistRequest;
import com.amazonaws.models.FindStylistResponse;
import com.amazonaws.models.FindUserPostRequest;
import com.amazonaws.models.FindUserPostResponse;
import com.amazonaws.models.PostDetailsResponse;
import com.amazonaws.models.UserFollowRequest;
import com.amazonaws.models.UserLikePostRequest;
import com.amazonaws.models.UserPosttNewCommentRequest;
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction;
import com.islayapp.models.awsmodels.PointsGetResponse;

/**
 * Created by maxo on 5/4/16.
 */
public interface LambdaInterface {

    /**
     * Invoke lambda function "echo". The function name is the method name
     */
    @LambdaFunction
    FindStylistResponse FindStylist(FindStylistRequest findStylistRequest);

    @LambdaFunction
    FindUserPostResponse FindStylistPosts(FindUserPostRequest findStylistPostRequest);

      @LambdaFunction
    PostDetailsResponse PostDetails(CommonFindRequest commonFindRequest);

    @LambdaFunction
    CommonResponse UserFollow(UserFollowRequest commonFindRequest);

    @LambdaFunction
    CommonResponse UserLikesPost(UserLikePostRequest commonFindRequest);

    @LambdaFunction
    CommonResponse PostComment(UserPosttNewCommentRequest posttNewCommentRequest);

    @LambdaFunction
    PointsGetResponse PointsGet(CommonRequest commonRequest);

}
