package com.amazonaws;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.islayapp.R;
import com.islayapp.util.PreferenceHelpers;
import com.islayapp.util.Constants;

/**
 * Created by maxo on 7/20/16.
 */
public class AWSCreateEndpointTask extends AsyncTask<String, Void, CreatePlatformEndpointResult> {
    private static final String TAG = "AWSCreateEndpointTask";

    Context context;

    public AWSCreateEndpointTask(Context context) {
        super();
        this.context = context;
    }

    @Override

    protected CreatePlatformEndpointResult doInBackground(String[] params) {

        if (params.length < 3) {
            return null;
        }

        String arn = params[0];
        String gcmToken = params[1];
        String userData = params[2];

        try {

            CreatePlatformEndpointRequest request = new CreatePlatformEndpointRequest();
            request.setCustomUserData(userData);
            request.setToken(gcmToken);
            request.setPlatformApplicationArn(arn);
            return CognitoSyncClientManager.getSNSClient().createPlatformEndpoint(request);

        } catch (Exception ex) {
            PreferenceHelpers.setPreference(context, Constants.REGISTRATION_ENDPOINT_COMPLETED, false);
            ex.printStackTrace();
            return null;
        }
    }

    @Override

    protected void onPostExecute(CreatePlatformEndpointResult result) {
        PreferenceHelpers.setPreference(context, Constants.REGISTRATION_ENDPOINT_COMPLETED, true);

        if (result != null) {
            String endpointArn = result.getEndpointArn();
            PreferenceHelpers.setPreference(context, context.getString(R.string.endpoint_arn), endpointArn);
            Log.i(TAG, "endpointArn: " + endpointArn);

        }
    }
}