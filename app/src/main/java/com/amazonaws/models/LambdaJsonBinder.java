package com.amazonaws.models;

/**
 * Created by maxo on 5/9/16.
 */

import com.amazonaws.mobileconnectors.lambdainvoker.LambdaDataBinder;
import com.amazonaws.util.StringUtils;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

/**
 * A Json data binder backed by Gson.
 */
public class LambdaJsonBinder implements LambdaDataBinder {

    private final Gson gson;

    /**
     * Constructs a Lambda Json binder.
     */
    public LambdaJsonBinder() {
        this.gson = new Gson();
    }

    @Override
    public <T> T deserialize(byte[] content, Class<T> clazz) {
        if (content == null) {
            return null;
        }

        try {
            System.out.println(new String(content, "UTF-8"));
//            System.out.println(new String(content, "US-ASCII"));
//            System.out.println(new String(content, "ISO-8859-1"));
//            System.out.println( new String(content, "UTF-16LE"));
//            System.out.println("\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E\uD83C\uDF4E");
//
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//
//        String str = null; // for UTF-8 encoding
//        str = new String(content, Charset.forName("utf-8"));
//        Log.d("tag", "String json result : " + str);

//        Log.d("tag", "String read = "+str);
//        String ss= "Ma Xó";
//        byte[] b = ss.getBytes();
//        String str1 = null; // for UTF-8 encoding
//        str1 =  new String(b, Charset.forName("utf-8"));
//        Log.d("tag", "String ss = "+str1);

        Reader reader1 = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(content), StringUtils.UTF8));
        return gson.fromJson(reader1, clazz);
    }

    @Override
    public byte[] serialize(Object object) {
        return gson.toJson(object).getBytes(StringUtils.UTF8);
    }
}
