package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/4/16.
 */
public class FindUserPostResponse {

    @SerializedName("data")
    @Expose
    private List<Post> posts = new ArrayList<Post>();
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("moreavailable")
    @Expose
    private String moreavailable;
    @SerializedName("pagenumber")
    @Expose
    private String pagenumber;
    @SerializedName("results")
    @Expose
    private String results;
    @SerializedName("valid")
    @Expose
    private String valid;
    @SerializedName("userfollowing")
    @Expose
    private UserFollowing userfollowing;

    /**
     *
     * @return
     * The data
     */
    public List<Post> getPosts() {
        return posts;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setPosts(List<Post> data) {
        this.posts = data;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The moreavailable
     */
    public String getMoreavailable() {
        return moreavailable;
    }

    /**
     * @param moreavailable The moreavailable
     */
    public void setMoreavailable(String moreavailable) {
        this.moreavailable = moreavailable;
    }

    /**
     * @return The pagenumber
     */
    public String getPagenumber() {
        return pagenumber;
    }

    /**
     * @param pagenumber The pagenumber
     */
    public void setPagenumber(String pagenumber) {
        this.pagenumber = pagenumber;
    }

    /**
     * @return The results
     */
    public String getResults() {
        return results;
    }

    /**
     * @param results The results
     */
    public void setResults(String results) {
        this.results = results;
    }

    /**
     * @return The valid
     */
    public String getValid() {
        return valid;
    }

    /**
     * @param valid The valid
     */
    public void setValid(String valid) {
        this.valid = valid;
    }

    public UserFollowing getUserfollowing() {
        return userfollowing;
    }

    public void setUserfollowing(UserFollowing userfollowing) {
        this.userfollowing = userfollowing;
    }
}
