package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by maxo on 5/6/16.
 */
public class PostComment implements Serializable {

    @SerializedName("id")
    @Expose
    private String id = "";
    @SerializedName("userid")
    @Expose
    private String userid = "";
    @SerializedName("postid")
    @Expose
    private String postid = "";
    @SerializedName("comment")
    @Expose
    private String comment = "";
    @SerializedName("picurl")
    @Expose
    private String picurl = "";
    @SerializedName("nickname")
    @Expose
    private String nickname = "";
    @SerializedName("flaggedts")
    @Expose
    private int flaggedts;
    @SerializedName("countof")
    @Expose
    private PostCommentCountOf countof = new PostCommentCountOf();
    @SerializedName("timestamp")
    @Expose
    private PostCommentTs timestamp = new PostCommentTs();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getFlaggedts() {
        return flaggedts;
    }

    public void setFlaggedts(int flaggedts) {
        this.flaggedts = flaggedts;
    }

    public PostCommentCountOf getCountof() {
        return countof;
    }

    public void setCountof(PostCommentCountOf countof) {
        this.countof = countof;
    }

    public PostCommentTs getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(PostCommentTs timestamp) {
        this.timestamp = timestamp;
    }


    public class PostCommentTs {
        @SerializedName("added")
        @Expose
        private long added;

        public long getAdded() {
            return added;
        }

        public void setAdded(long added) {
            this.added = added;
        }


    }

    public class PostCommentCountOf {
        @SerializedName("flagged")
        @Expose
        private int flagged;

        public PostCommentCountOf() {
        }

        public PostCommentCountOf(int flagged) {
            this.flagged = flagged;
        }

        public int getFlagged() {
            return flagged;
        }

        public void setFlagged(int flagged) {
            this.flagged = flagged;
        }
    }
}
