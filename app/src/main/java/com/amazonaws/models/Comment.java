package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by maxo on 5/24/16.
 */
public class Comment implements Serializable {

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("postid")
    @Expose
    private String postid;
    @SerializedName("comment")
    @Expose
    private String comment="";
    @SerializedName("nickname")
    @Expose
    private String nickname="";

    public Comment() {
    }

    private Stylist stylist = new Stylist();

    public Stylist getStylist() {
        return stylist;
    }

    public void setStylist(Stylist stylist) {
        this.stylist = stylist;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @return The userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid The userid
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return The postid
     */
    public String getPostid() {
        return postid;
    }

    /**
     * @param postid The postid
     */
    public void setPostid(String postid) {
        this.postid = postid;
    }

    /**
     * @return The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

}