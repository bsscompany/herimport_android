package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by maxo on 5/17/16.
 */
public class RedeemPointsResponse implements Serializable {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("rewardid")
    @Expose
    private String rewardid;
    @SerializedName("remainingpoints")
    @Expose
    private Integer remainingpoints;
    @SerializedName("message")
    @Expose
    private String message="";
    @SerializedName("item")
    @Expose
    private Item item;

    /**
     * @return The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * @return The rewardid
     */
    public String getRewardid() {
        return rewardid;
    }

    /**
     * @param rewardid The rewardid
     */
    public void setRewardid(String rewardid) {
        this.rewardid = rewardid;
    }

    /**
     * @return The remainingpoints
     */
    public Integer getRemainingpoints() {
        return remainingpoints;
    }

    /**
     * @param remainingpoints The remainingpoints
     */
    public void setRemainingpoints(Integer remainingpoints) {
        this.remainingpoints = remainingpoints;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The item
     */
    public Item getItem() {
        return item;
    }

    /**
     * @param item The item
     */
    public void setItem(Item item) {
        this.item = item;
    }

    public class Item implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("points")
        @Expose
        private Integer points;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("video")
        @Expose
        private String video;

        /**
         * @return The id
         */
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The description
         */
        public String getDescription() {
            return description;
        }

        /**
         * @param description The description
         */
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The points
         */
        public Integer getPoints() {
            return points;
        }

        /**
         * @param points The points
         */
        public void setPoints(Integer points) {
            this.points = points;
        }

        /**
         * @return The image
         */
        public String getImage() {
            return image;
        }

        /**
         * @param image The image
         */
        public void setImage(String image) {
            this.image = image;
        }

        /**
         * @return The video
         */
        public String getVideo() {
            return video;
        }

        /**
         * @param video The video
         */
        public void setVideo(String video) {
            this.video = video;
        }

    }
}