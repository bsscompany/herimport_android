package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/4/16.
 */
public class FindStylistResponse {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("moreAvailable")
    @Expose
    private String moreAvailable;
    @SerializedName("moreavailable")
    @Expose
    private String moreavailable;
    @SerializedName("pagenumber")
    @Expose
    private String pagenumber;
    @SerializedName("results")
    @Expose
    private String results;
    @SerializedName("valid")
    @Expose
    private String valid;
    @SerializedName("stylists")
    @Expose
    private List<Stylist> stylists = new ArrayList<Stylist>();

    @SerializedName("suggestions")
    @Expose
    private String[] suggestions;

    public FindStylistResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMoreAvailable() {
        return moreAvailable;
    }

    public void setMoreAvailable(String moreAvailable) {
        this.moreAvailable = moreAvailable;
    }

    public String getMoreavailable() {
        return moreavailable;
    }

    public void setMoreavailable(String moreavailable) {
        this.moreavailable = moreavailable;
    }

    public String getPagenumber() {
        return pagenumber;
    }

    public void setPagenumber(String pagenumber) {
        this.pagenumber = pagenumber;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public List<Stylist> getStylists() {
        return stylists;
    }

    public void setStylists(List<Stylist> stylists) {
        this.stylists = stylists;
    }

    public String[] getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(String[] suggestions) {
        this.suggestions = suggestions;
    }
}
