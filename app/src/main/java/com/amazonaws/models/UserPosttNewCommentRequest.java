package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/8/16.
 */
public class UserPosttNewCommentRequest {

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("postid")
    @Expose
    private String postid;
    @SerializedName("comment")
    @Expose
    private String comment;

    public UserPosttNewCommentRequest() {
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
