package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/7/16.
 */
public class UserLikePostRequest {

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("postid")
    @Expose
    private String postid;

    @SerializedName("like")
    @Expose
    private boolean like;

    public UserLikePostRequest() {
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }
}
