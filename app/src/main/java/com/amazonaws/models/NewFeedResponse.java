package com.amazonaws.models;

/**
 * Created by maxo on 5/13/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NewFeedResponse {

    @SerializedName("data")
    @Expose
    private List<ItemFeed> itemFeedList = new ArrayList<ItemFeed>();
    @SerializedName("uniqueid")
    @Expose
    private String uniqueid;
    @SerializedName("pagesize")
    @Expose
    private Integer pagesize;
    @SerializedName("pagenumber")
    @Expose
    private Integer pagenumber;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("results")
    @Expose
    private Integer results;
    @SerializedName("moreavailable")
    @Expose
    private String moreavailable;
    @SerializedName("stylists")
    @Expose
    private List<Stylist> stylists = new ArrayList<Stylist>();

    public List<Stylist> getStylists() {
        return stylists;
    }

    public void setStylists(List<Stylist> stylists) {
        this.stylists = stylists;
    }

    /**
     * @return The data
     */
    public List<ItemFeed> getItemFeedList() {
        return itemFeedList;
    }

    /**
     * @param data The data
     */
    public void setItemFeedList(List<ItemFeed> data) {
        this.itemFeedList = data;
    }

    /**
     * @return The uniqueid
     */
    public String getUniqueid() {
        return uniqueid;
    }

    /**
     * @param uniqueid The uniqueid
     */
    public void setUniqueid(String uniqueid) {
        this.uniqueid = uniqueid;
    }

    /**
     * @return The pagesize
     */
    public Integer getPagesize() {
        return pagesize;
    }

    /**
     * @param pagesize The pagesize
     */
    public void setPagesize(Integer pagesize) {
        this.pagesize = pagesize;
    }

    /**
     * @return The pagenumber
     */
    public Integer getPagenumber() {
        return pagenumber;
    }

    /**
     * @param pagenumber The pagenumber
     */
    public void setPagenumber(Integer pagenumber) {
        this.pagenumber = pagenumber;
    }

    /**
     * @return The userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid The userid
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return The results
     */
    public Integer getResults() {
        return results;
    }

    /**
     * @param results The results
     */
    public void setResults(Integer results) {
        this.results = results;
    }

    /**
     * @return The moreavailable
     */
    public String getMoreavailable() {
        return moreavailable;
    }

    /**
     * @param moreavailable The moreavailable
     */
    public void setMoreavailable(String moreavailable) {
        this.moreavailable = moreavailable;
    }


}