package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.islayapp.util.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/4/16.
 */
public class Post implements Serializable {
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("countof")
    @Expose
    private Countof countof;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("timestamp")
    @Expose
    private Timestamp timestamp;
    @SerializedName("userid")
    @Expose
    private String userid;

    private List<Comment> comments = new ArrayList<Comment>();

    private String originalUrl;
    private String thumbUrl;

    public String getOriginalUrl() {
        originalUrl = Constants.POST_IMAGES + getUserid() + "/" + getId() + ".jpg";
        return originalUrl;
    }

    public void setOriginalUrl() {
        this.originalUrl = Constants.POST_IMAGES + getUserid() + "/" + getId() + ".jpg";
    }

    public String getThumbUrl() {
        thumbUrl = Constants.POST_IMAGES + getUserid() + "/thumb" + getId() + ".jpg";
        return thumbUrl;
    }

    public void setThumbUrl() {
        this.thumbUrl = Constants.POST_IMAGES + getUserid() + "/thumb" + getId() + ".jpg";
    }

    /**
     * @return The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return The countof
     */
    public Countof getCountof() {
        return countof;
    }

    /**
     * @param countof The countof
     */
    public void setCountof(Countof countof) {
        this.countof = countof;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid The userid
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}


