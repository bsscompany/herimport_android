package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.islayapp.models.awsmodels.Stores;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 7/6/16.
 */
public class StoresLocatorResponse {
    @SerializedName("stores")
    @Expose
    private List<Stores> stores = new ArrayList<Stores>();

    /**
     *
     * @return
     * The stores
     */
    public List<Stores> getStores() {
        return stores;
    }

    /**
     *
     * @param stores
     * The stores
     */
    public void setStores(List<Stores> stores) {
        this.stores = stores;
    }

}