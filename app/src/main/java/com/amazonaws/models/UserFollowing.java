package com.amazonaws.models;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/4/16.
 */
public class UserFollowing {
    @SerializedName("userid")
    @Expose
    private String userid = "";

    @SerializedName("followuserid")
    @Expose
    private  String followuserid = "";

    @SerializedName("timestamp")
    @Expose
    private  UserFollowTs timestamp = new UserFollowTs();

    boolean follow = false;

    class UserFollowTs {
        long added = 0;

        public UserFollowTs() {
        }

        public long getAdded() {
            return added;
        }

        public void setAdded(long added) {
            this.added = added;
        }
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFollowuserid() {
        return followuserid;
    }

    public void setFollowuserid(String followuserid) {
        Log.d("hao gay" , "Hao gầy: " + followuserid);
        this.followuserid = followuserid;
    }

    public UserFollowTs getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(UserFollowTs timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isFollow() {
        return follow;
    }

    public void setFollow(boolean follow) {
        this.follow = follow;
    }
}
