package com.amazonaws.models;

/**
 * Created by maxo on 5/4/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonRequest {
    @SerializedName("pagesize")
    @Expose
    private int pagesize = 20;
    @SerializedName("pagenumber")
    @Expose
    private int pagenumber = 1;
    @SerializedName("userid")
    @Expose
    private String userid = "";

    public CommonRequest() {
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPagenumber() {
        return pagenumber;
    }

    public void setPagenumber(int pagenumber) {
        this.pagenumber = pagenumber;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}