package com.amazonaws.models;

/**
 * Created by maxo on 5/4/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FindStylistRequest {
    @SerializedName("address")
    @Expose
    private String address = "";
    @SerializedName("distance")
    @Expose
    private double distance = 30;
    @SerializedName("distancetype")
    @Expose
    private String distancetype = "miles";
    @SerializedName("name")
    @Expose
    private String name = "";
    @SerializedName("pagesize")
    @Expose
    private int pagesize = 30;
    @SerializedName("pagenumber")
    @Expose
    private int pagenumber = 1;
    @SerializedName("userid")
    @Expose
    private String userid = "";

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getDistancetype() {
        return distancetype;
    }

    public void setDistancetype(String distancetype) {
        this.distancetype = distancetype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPagenumber() {
        return pagenumber;
    }

    public void setPagenumber(int pagenumber) {
        this.pagenumber = pagenumber;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}