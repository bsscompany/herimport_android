package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by maxo on 5/16/16.
 */
public class Countof implements Serializable {

    @SerializedName("comments")
    @Expose
    private int comments;
    @SerializedName("likes")
    @Expose
    private int likes;
    @SerializedName("shares")
    @Expose
    private int shares;
    @SerializedName("words")
    @Expose
    private int words;


    public Countof(int comments, int likes, int shares, int words) {
        this.comments = comments;
        this.likes = likes;
        this.shares = shares;
        this.words = words;
    }

    /**
     * @return The comments
     */
    public int getComments() {
        return comments;
    }

    /**
     * @param comments The comments
     */
    public void setComments(int comments) {
        this.comments = comments;
    }

    /**
     * @return The likes
     */
    public int getLikes() {
        return likes;
    }

    /**
     * @param likes The likes
     */
    public void setLikes(int likes) {
        this.likes = likes;
    }

    /**
     * @return The shares
     */
    public int getShares() {
        return shares;
    }

    /**
     * @param shares The shares
     */
    public void setShares(int shares) {
        this.shares = shares;
    }

    /**
     * @return The words
     */
    public int getWords() {
        return words;
    }

    /**
     * @param words The words
     */
    public void setWords(int words) {
        this.words = words;
    }

}
