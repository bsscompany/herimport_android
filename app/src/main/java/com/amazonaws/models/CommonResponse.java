package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/7/16.
 */
public class CommonResponse {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("valid")
    @Expose
    private String valid;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }
}
