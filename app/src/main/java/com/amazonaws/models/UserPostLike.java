package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/6/16.
 */
public class UserPostLike {
    @SerializedName("userid")
    @Expose
    private String userid = "";
    @SerializedName("postid")
    @Expose
    private String postid = "";
    @SerializedName("timestamp")
    @Expose
    private UserPostLikeTs timestamp = new UserPostLikeTs();

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public UserPostLikeTs getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(UserPostLikeTs timestamp) {
        this.timestamp = timestamp;
    }

    class UserPostLikeTs {

        @SerializedName("added")
        @Expose
        long added = 0;

        public long getAdded() {
            return added;
        }

        public void setAdded(long added) {
            this.added = added;
        }
    }


}
