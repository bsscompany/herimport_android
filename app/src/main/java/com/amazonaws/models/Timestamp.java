package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by maxo on 5/16/16.
 */
public class Timestamp implements Serializable{

    @SerializedName("added")
    @Expose
    private long added;

    @SerializedName("lastcomment")
    @Expose
    private long lastcomment;

    public long getLastcomment() {
        return lastcomment;
    }

    public void setLastcomment(long lastcomment) {
        this.lastcomment = lastcomment;
    }

    /**
     * @return The added
     */
    public long getAdded() {
        return added;
    }

    /**
     * @param added The added
     */
    public void setAdded(long added) {
        this.added = added;
    }

}
