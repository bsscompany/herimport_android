package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.islayapp.util.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/24/16.
 */
public class ItemFeed implements Serializable{

    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("timestamp")
    @Expose
    private Timestamp timestamp;
    @SerializedName("countof")
    @Expose
    private Countof countof;
    @SerializedName("comments")
    @Expose
    private List<Comment> comments = new ArrayList<Comment>();
    @SerializedName("commentliked")
    @Expose
    private boolean commentliked = false;

    private Stylist stylist = new Stylist();
    private String imageThumb;
    private String imageOrigine;

    public String getThumbnailImageUrl() {
        imageThumb = Constants.POST_IMAGES + getUserid() + "/thumb" + getId() + ".jpg";
        return imageThumb;
    }

    public String getOriginalImageUrl() {
        imageOrigine = Constants.POST_IMAGES + getUserid() + "/" + getId() + ".jpg";
        return imageOrigine;
    }

    public boolean getCommentliked() {
        return commentliked;
    }

    public void setCommentliked(Boolean commentliked) {
        this.commentliked = commentliked;
    }

    public Stylist getStylist() {
        return stylist;
    }

    public void setStylist(Stylist stylist) {
        this.stylist = stylist;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(String imageThumb) {
        this.imageThumb = imageThumb;
    }

    public String getImageOrigine() {
        return imageOrigine;
    }

    public void setImageOrigine(String imageOrigine) {
        this.imageOrigine = imageOrigine;
    }

    /**
     * @return The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid The userid
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return The timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The countof
     */
    public Countof getCountof() {
        return countof;
    }

    /**
     * @param countof The countof
     */
    public void setCountof(Countof countof) {
        this.countof = countof;
    }

}
