package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/30/16.
 */
public class CallPhoneResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("sid")
    @Expose
    private String sid;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("code")
    @Expose
    private String code;

    /**
     *
     * @return
     * The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     *
     * @param success
     * The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     *
     * @return
     * The sid
     */
    public String getSid() {
        return sid;
    }

    /**
     *
     * @param sid
     * The sid
     */
    public void setSid(String sid) {
        this.sid = sid;
    }

    /**
     *
     * @return
     * The uri
     */
    public String getUri() {
        return uri;
    }

    /**
     *
     * @param uri
     * The uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }

}