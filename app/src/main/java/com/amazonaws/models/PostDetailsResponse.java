package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxo on 5/6/16.
 */
public class PostDetailsResponse implements Serializable {
    @SerializedName("userpostlike")
    @Expose
    private UserPostLike userpostlike;
    @SerializedName("data")
    @Expose
    private List<PostComment> postComments = new ArrayList<PostComment>();
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("moreavailable")
    @Expose
    private String moreavailable;
    @SerializedName("pagenumber")
    @Expose
    private int pagenumber;
    @SerializedName("post")
    @Expose
    private Post post;
    @SerializedName("results")
    @Expose
    private int results;
    @SerializedName("valid")
    @Expose
    private String valid;


    public int getPagenumber() {
        return pagenumber;
    }

    public void setPagenumber(int pagenumber) {
        this.pagenumber = pagenumber;
    }

    public int getResults() {
        return results;
    }

    public void setResults(int results) {
        this.results = results;
    }

    public String getMoreavailable() {
        return moreavailable;
    }

    public void setMoreavailable(String moreavailable) {
        this.moreavailable = moreavailable;
    }

    public Post getPosts() {
        return post;
    }

    public void setPosts(Post posts) {
        this.post = posts;
    }

    public UserPostLike getUserpostlike() {
        return userpostlike;
    }

    public void setUserpostlike(UserPostLike userpostlike) {
        this.userpostlike = userpostlike;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PostComment> getPostComments() {
        return postComments;
    }

    public void setPostComments(List<PostComment> data) {
        this.postComments = data;
    }

}
