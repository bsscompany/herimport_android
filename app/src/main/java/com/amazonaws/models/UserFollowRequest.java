package com.amazonaws.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by maxo on 5/7/16.
 */
public class UserFollowRequest {

    @SerializedName("userid")
    @Expose
    String userid = "";
    @SerializedName("followuserid")
    @Expose
    String followuserid = "";
    @SerializedName("follow")
    @Expose
    boolean follow = false;


    public boolean isFollow() {
        return follow;
    }

    public void setFollow(boolean follow) {
        this.follow = follow;
    }

    public String getFollowuserid() {
        return followuserid;
    }

    public void setFollowuserid(String followuserid) {
        this.followuserid = followuserid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
