package com.amazonaws.models;

/**
 * Created by maxo on 5/21/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestReward {

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("itemid")
    @Expose
    private String itemid;
    @SerializedName("shiptoaddress")
    @Expose
    private Shiptoaddress shiptoaddress;

    /**
     *
     * @return
     * The userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     *
     * @param userid
     * The userid
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     *
     * @return
     * The itemid
     */
    public String getItemid() {
        return itemid;
    }

    /**
     *
     * @param itemid
     * The itemid
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    /**
     *
     * @return
     * The shiptoaddress
     */
    public Shiptoaddress getShiptoaddress() {
        return shiptoaddress;
    }

    /**
     *
     * @param shiptoaddress
     * The shiptoaddress
     */
    public void setShiptoaddress(Shiptoaddress shiptoaddress) {
        this.shiptoaddress = shiptoaddress;
    }

}
