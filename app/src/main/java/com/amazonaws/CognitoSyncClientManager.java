/**
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * <p/>
 * http://aws.amazon.com/apache2.0
 * <p/>
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.amazonaws;

import android.content.Context;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.auth.CognitoCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sns.AmazonSNSClient;

import java.util.HashMap;
import java.util.Map;

public class CognitoSyncClientManager {

    private static final String TAG = "CognitoSyncClientManager";

    /**
     * Enter here the Identity Pool associated with your app and the AWS
     * region where it belongs. Get this information from the AWS console.
     */

    public static final String IDENTITY_POOL_ID = "us-east-1:3ccf57ce-1038-4819-ac01-886c86d1b1c3";
    public static final String ARN = "arn:aws:iam::575820516119:role/Cognito_SEWINAuth_Role";
    public static final String ARN_PUSH = "arn:aws:sns:us-east-1:575820516119:app/GCM/com.islayapp";

    public static final Regions REGION = Regions.US_EAST_1;
    public static final String BUCKET_NAME = "sewin-media/uploads";
    public static final String STREAM_NAME = "sewinAppStream";

    private static CognitoSyncManager syncClient;

    protected static CognitoCachingCredentialsProvider credentialsProvider = null;


    private static AmazonS3Client s3Client = null;
    private static AmazonSNSClient snsClient = null;


    /**
     * Set this flag to true for using developer authenticated identities
     * Make sure you configured it in DeveloperAuthenticationProvider.java.
     */


    /**
     * Initializes the Cognito Identity and Sync clients. This must be called before getInstance().
     *
     * @param context a context of the app
     */
    public static void init(Context context) {

        if (syncClient != null) return;

        credentialsProvider = new CognitoCachingCredentialsProvider(context, IDENTITY_POOL_ID, REGION);

        syncClient = new CognitoSyncManager(context, REGION, credentialsProvider);
    }


    /**
     * Sets the login so that you can use authorized identity. This requires a
     * network request, so you should call it in a background thread.
     *
     * @param providerName the name of the external identity provider
     * @param token        openId token
     */
    public static void addLogins(String providerName, String token) {
        if (syncClient == null) {
            throw new IllegalStateException("CognitoSyncClientManager not initialized yet");
        }

        Map<String, String> logins = credentialsProvider.getLogins();
        if (logins == null) {
            logins = new HashMap<String, String>();
        }
        logins.put(providerName, token);
        credentialsProvider.setLogins(logins);
    }

    /**
     * Gets the singleton instance of the CognitoClient. init() must be called
     * prior to this.
     *
     * @return an instance of CognitoClient
     */
    public static CognitoSyncManager getInstance() {
        if (syncClient == null) {
            throw new IllegalStateException("CognitoSyncClientManager not initialized yet");
        }
        return syncClient;
    }

    /**
     * returns the instance of the amazon S3 Client for this app
     */
    public static AmazonS3Client getS3Client() {
        if (s3Client == null) {
            s3Client = new AmazonS3Client(credentialsProvider);
        }
        return s3Client;
    }

    /**
     * returns the instance of the amazon SNS Client for this app
     */
    public static AmazonSNSClient getSNSClient() {
        if (snsClient == null) {
            snsClient = new AmazonSNSClient(credentialsProvider);
            snsClient.setRegion(Region.getRegion(Regions.US_EAST_1));
        }
        return snsClient;
    }


    /**
     * Returns a credentials provider object
     *
     * @return
     */
    public static CognitoCredentialsProvider getCredentialsProvider() {
        return credentialsProvider;
    }

}
